<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Soal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->get_datasess = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('M_Front');
        $this->load->model('M_Admin');
        $this->load->model('M_Soal');
        $this->load->model('M_Kandidat');
        $this->get_datasetupapp = $this->M_Front->fetchsetupapp();
    }

    public function interview($kandidat_id){
    
        is_admin();

        $soalList = $this->M_Soal->querySoal('interview');

        $soal = [];
        foreach($soalList as $val){

           $val['pilihan'] = (array) json_decode($val['pilihan']);
           $soal[$val['category_title']][] = $val; 
        }

        $kandidat = $this->M_Kandidat->getById($kandidat_id);
      
        $data = [
            'action' => site_url('soal/add_action'),
            'tipe_soal'=> 'nilai_test_hr',
            'kandidat_id'=> $kandidat_id,
            'title' => 'Soal Interview',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'soalList' => $soal,
            'kode_kandidat'=> $kandidat->kode_kandidat,
            'nama_lengkap'=> $kandidat->nama_lengkap
        ];


        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/soal/interview', $data);
        $this->load->view('admin/soal/footer', $data);
      

    }

    public function pengetahuan($kandidat_id){
    
        is_admin();

        $soalList = $this->M_Soal->querySoal('pengetahuan');

        $soal = [];
        foreach($soalList as $val){

           $val['pilihan'] = (array) json_decode($val['pilihan']);
           $soal[$val['category_title']][] = $val; 
        }

        $kandidat = $this->M_Kandidat->getById($kandidat_id);
      

        $data = [
            'action' => site_url('soal/add_action'),
            'tipe_soal'=> 'nilai_test_pengetahuan',
            'title' => 'Soal Pengetahuan',
            'kandidat_id'=> $kandidat_id,
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'soalList' => $soal,
            'kode_kandidat'=> $kandidat->kode_kandidat,
            'nama_lengkap'=> $kandidat->nama_lengkap
        ];


        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/soal/pengetahuan', $data);
        $this->load->view('admin/soal/footer', $data);
      

    }

    public function skill($kandidat_id){
    
        is_admin();

        $soalList = $this->M_Soal->querySoal('skill');

        $soal = [];
        foreach($soalList as $val){

           $val['pilihan'] = (array) json_decode($val['pilihan']);
           $soal[$val['category_title']][] = $val; 
        }

        $kandidat = $this->M_Kandidat->getById($kandidat_id);
      

        $data = [
            'title' => 'Soal-Soal Skill (Nilai Score 1 - 5)',
            'action' => site_url('soal/add_action'),
            'tipe_soal'=> 'nilai_test_skill',
            'kandidat_id'=> $kandidat_id,
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'soalList' => $soal,
            'kode_kandidat'=> $kandidat->kode_kandidat,
            'nama_lengkap'=> $kandidat->nama_lengkap
        ];


        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/soal/skill', $data);
        $this->load->view('admin/soal/footer', $data);
      

    }

    public function add_action()
    {

        $tipe_soal =  $this->input->post('tipe_soal');
        $nilai = $this->input->post($tipe_soal);

        // echo $tipe_soal;
        // echo "<br/>";
        // echo $nilai;
        // die();
      
        $kandidat_id = $this->input->post('kandidat_id');
            
        $this->M_Kandidat->saveNilaiInterview($kandidat_id, $tipe_soal, $nilai);
        $this->session->set_flashdata('success', 'Data was successfully added');
        redirect(site_url('admin/kandidat'));

    
    }



}