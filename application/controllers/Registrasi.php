<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Registrasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->get_datasess = $this->db->get_where('login_rekapgaji', ['nik' =>
        $this->session->userdata('nik')])->row_array();
        $this->load->model('M_Auth');
        $this->load->model('M_Front');
        $this->load->model('M_Kandidat');
        $this->get_datasetupapp = $this->M_Front->fetchsetupapp();
    }

    public function index()
    {
        // $data = [
        //     'title' => 'Login Absensi',
        //     'dataapp' => $this->get_datasetupapp
        // ];
        $data['area']=$this->M_Kandidat->getArea();

        $data['message'] = 'new';
        $this->load->view('register/v_register', $data);
    }

    public function success()
    {
        $data = [
            'title' => 'Login Absensi',
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('register/success');
    }

    public function failed()
    {
        $data = [
            'title' => 'Login Absensi',
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('register/failed');
    }

    public function cekfirst_no_ktp($str)
    {
        if (substr($str, 0, 1) != 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function cekfirst_no_tlp($str)
    {
        if (substr($str, 0, 1) != 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function proses_registrasi()
    {
      
        $kandidat = $this->M_Kandidat;
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required', ['required' => 'Mohon masukan nama lengkap anda!']);
        $this->form_validation->set_rules(
            'ktp',
            'KTP',
            'trim|required|numeric|min_length[16]|max_length[16]|is_unique[kandidat.ktp]|callback_cekfirst_no_ktp',
            [
                'is_unique' => 'Nomor KTP sudah terdaftar!',
                'numeric' => 'KTP hanya diisi number!',
                'min_length' => 'Minimal Nomor KTP 16 digit!',
                'max_length' => 'Maksimal Nomor KTP 16 digit!',
                'cekfirst_no_ktp' => 'Nomor KTP tidak boleh diawali angka 0 !',
                'required' => 'Mohon masukan No KTP anda'
            ]
        );
        $this->form_validation->set_rules(
            'no_telp',
            'Nomor Telephone',
            'trim|required|max_length[13]|numeric|is_unique[kandidat.telp]|callback_cekfirst_no_tlp',
            [
                'is_unique' => 'Nomor handphone anda sudah terdaftar!',
                'numeric' => 'No handphone hanya diisi number!',
                'max_length' => 'No handphone Makasimal 13 Digit!',
                'cekfirst_no_tlp' => 'No handphone Harus diawali angka 0 !',
                'required' => 'Mohon masukan No Handphone anda'
            ]
        );
        $this->form_validation->set_rules('email', 'email', 'required|trim|valid_email', ['required' => 'Mohon masukan alamat email anda!']);
        $this->form_validation->set_rules('pekerjaan_sebelumnya', 'Pekerjaan Sebelumnya', 'required|alpha_numeric', ['required' => 'Mohon masukan pekerjaan anda sebelumnya!', 'alpha_numeric' => 'Ketikan hanya karakter saja !']);
        $this->form_validation->set_rules('perusahaan_sebelumnya', 'Perusahaan Sebelumnya', 'required|alpha_numeric', ['required' => 'Mohon masukan nama perusahaan anda sebelumnya!', 'alpha_numeric' => 'Ketikan hanya karakter saja !']);
        $this->form_validation->set_rules('lama_bekerja', 'Lama Kerja', 'required', ['required' => 'Silahkan pilih terlebih dahulu']);
        $this->form_validation->set_rules('pendidikan_terakhir', 'Pendidikan Terakhir', 'required', ['required' => 'Silahkan pilih terlebih dahulu']);
        $this->form_validation->set_rules('jobs', 'Jobs', 'required', ['required' => 'Silahkan pilih terlebih dahulu']);
        $this->form_validation->set_rules('sim', 'SIM', 'required', ['required' => 'Silahkan pilih terlebih dahulu']);
        $this->form_validation->set_rules('district', 'district', 'required', ['required' => 'Silahkan pilih terlebih dahulu']);
        $this->form_validation->set_rules('surat_lamaran', 'Surat Lamaran', 'uploaded[surat_lamaran]');
        $this->form_validation->set_rules('surat_lamaran', '', 'callback_cek_file');

        if ($this->form_validation->run() == false) {
            $data['message'] = 'error_validation';
            $data['area']=$this->M_Kandidat->getArea();
            $this->load->view('register/v_register', $data);
        } else {
            $surat_lamaran = $_FILES['surat_lamaran'];


            $config['upload_path'] = './storage/surat_lamaran';
            $config['allowed_types'] = 'jpg|png|pdf';
            $config['max_size'] = 2048;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('surat_lamaran')) {
                $data['message'] = 'error_upload';
                $this->load->view('register/v_register', $data);
            } else {
                $cv = $this->upload->data('file_name');
                $kandidat->simpanKandidat();
                $data['message'] = 'success';
                $this->load->view('register/v_register', $data);
            }
        }
    }

    public function cek_file($str)
    {
        $allowed_mime_type_arr = array('application/pdf', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
        $mime = get_mime_by_extension($_FILES['surat_lamaran']['name']);
        if (isset($_FILES['surat_lamaran']['name']) && $_FILES['surat_lamaran']['name'] != "") {
            if (in_array($mime, $allowed_mime_type_arr)) {
                return true;
            } else {
                $this->form_validation->set_message('cek_file', 'Silahkan pilih hanya file pdf/gif/jpg/png.');
                return false;
            }
        } else {
            $this->form_validation->set_message('cek_file', 'Silakan pilih file untuk diupload.');
            return false;
        }
    }

    public function login()
    {
        if ($this->session->userdata('logged_in')) {
            if ($this->session->userdata('kandidat_status') == 'lulus') {
                redirect('kelengkapan-data', 'refresh');
            } else {
                redirect('pengetahuan-test', 'refresh');
            }
        }
        $this->load->view('register/v_login');
    }

    public function login_payroll()
    {
        $data = [
                'title' => 'Login Sistem MBK',
                'dataapp' => $this->get_datasetupapp
            ];
        if ($this->session->userdata('logged_in')) {
               redirect('payroll');
        }else{
            $this->load->view('register/v_loginPayroll',$data);
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $sess_kandidat = array(
            'logged_in' => FALSE,
        );
        $this->session->set_userdata($sess_kandidat);
        redirect(site_url('login-kandidat'));
    }

    public function logoutPayroll()
    {
        $this->session->sess_destroy();
        $sess_kandidat = array(
            'logged_in' => FALSE,
        );
        $this->session->set_userdata($sess_kandidat);
        redirect(site_url('login-payroll'));
    }

    public function loginProses()
    {
        $proses = $this->M_Kandidat->loginProses();
        if ($proses) {
            $sess_kandidat = array(
                'logged_in' => TRUE,
                'kandidat_id'  => $proses->id,
                'kandidat_kode'  => $proses->kode_kandidat,
                'kandidat_nama'  => $proses->nama_lengkap,
                'kandidat_status'  => $proses->status_kandidat,
            );
            $this->session->set_userdata($sess_kandidat);
            $data['message'] = 'success';
            $data['status'] = $proses->status_kandidat;
            echo json_encode($data);
        } else {
            $data['message'] = 'failed';
            echo json_encode($data);
        }
    }

    public function loginProsesPayroll()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $user = $this->db->get_where('login_rekapgaji', ['nik' => $username])->row_array();
        if ($user) {
                //check password
                if (password_verify($password, $user['password'])) {
                    $membuat_session = [
                        'id' => $user['id'],
                        'nik' => $user['nik'],
                        'nama' => $user['nama_lengkap'],
                        'district' => $user['district'],
                        'role_id' => 5,
                        'logged_in' => true
                    ];
                    $this->db->where('login_rekapgaji.id', $user['id']);
                    $this->session->set_userdata($membuat_session); //Memasukan / menyimpan data ke session
                    
                    $data['message'] = 'success';
                    echo json_encode($data);
                } else {
                    $data['message'] = 'failed';
                    echo json_encode($data);
                }
        } else {
            $data['message'] = 'failed';
            echo json_encode($data);
        }
    }


    public function pengetahuan()
    {
        $this->load->view('register/v_pengetahuan');
    }

    public function update_nilai_pengetahuan()
    {
        $id = $this->session->userdata('kandidat_id');
        $this->M_Kandidat->saveNilaiInterview($id, 'nilai_test_pengetahuan',  $_POST['nilai']);
    }

    public function kelengkapan()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('login-kandidat', 'refresh');
        }

        $data['data'] = $this->M_Kandidat->getById($this->session->userdata('kandidat_id'));
        $data['detail'] = $this->M_Kandidat->getDetailById($this->session->userdata('kandidat_id'));

        $data['message'] = 'new';
        $this->load->view('register/v_kelengkapan', $data);
    }

    public function getDataKandidat()
    {
        $data['data'] = $this->M_Kandidat->getById($this->session->userdata('kandidat_id'));
        $data['detail'] = $this->M_Kandidat->getDetailById($this->session->userdata('kandidat_id'));
        echo json_encode($data);
    }

    public function getDataAgama()
    {
        $data = $this->M_Kandidat->getAgama();
        echo json_encode($data);
    }

    public function update_kandidat()
    {
        $id = $this->session->userdata('kandidat_id');
        $this->load->library('form_validation');
        $this->M_Kandidat->updateKandidat($id);
        $data['message'] = 'success';
        $this->session->set_flashdata('message', 'success');
        redirect('kelengkapan-data', 'refresh');
        // $this->load->view('register/v_kelengkapan', $data);
        // }
    }

    public function load_soal_pengetahuan()
    {
        $data = $this->M_Kandidat->get_soal('2');
        echo json_encode($data);
    }
    
    public function dashboard_pegawai(){
        is_pegawai();
        $data = [
            'title' => 'Dashboard Sistem MBK',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('payroll/dashboard', $data);
        $this->load->view('layout/footer', $data);
    }
    
    public function create_slip()
    {
        is_pegawai();
        $this->cetakslipPdf($this->input->get('id'),$this->input->get('dokid'));
    }

    public function cetakslipPdf($id,$dokid)
    {
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A5-L']);
        if($id == '000'){
            $where = "where dokumen_id = $dokid ";
            $enter = "<br><br><br><br><br>"; 
        }else{
            $where = "where id_slipgaji = $id";
            $enter = "";
        } 

        $getData = $this->db->query("SELECT * from tmp_rekapabsen $where")->result();
        $show = "";
        foreach ($getData as $row) {
            $totalPot = $row->pot_absen + $row->bpjs_tk + $row->bpjs_kes + $row->pensiun + $row->pph21 + $row->pot_backup + $row->pot_sph + $row->pot_lain;
            $totalPendapatan = $row->gaji_pokok + $row->rapel + $row->insentif + $row->lemburan + $row->premi_hadir + $row->tj_pulsa + $row->tj_transport + $row->tj_um;
        // var_dump($row->gaji_pokok);die();

            $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td width='100%' align='center'><img style='width: 480px;height: auto;' src='" . base_url('storage/profile/header.png') . "'></td>
                    </tr>
                </table>";
            $show .= "<table width='100%' border='0' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td style='border-left:solid 1px black;' width='30%'>NO</td>
                        <td width='20%'>: </td>
                        <td width='30%' style='border-left:solid 1px black;'></td>
                        <td width='20%' style='border-right:solid 1px black;' colspan='3'></td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>NIK</td>
                        <td>: " . $row->nik . "</td>
                        <td style='border-left:solid 1px black;'>JABATAN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $row->jabatan . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>NAMA</td>
                        <td>: " . $row->nama . "</td>
                        <td style='border-left:solid 1px black;'>ALOKASI</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $row->customer . "-" . $row->area . "</td>
                    </tr>
                    
                </table>";
            $show .= "<table width='100%' border='0' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td style='border-left:solid 1px black;border-top:solid 1px black;' width='30%'><b>PENDAPATAN</b></td>
                        <td style='border-top:solid 1px black;' width='20%'> </td>
                        <td style='border-left:solid 1px black;border-top:solid 1px black;' width='30%'><b>POTONGAN</b></td>
                        <td style='border-right:solid 1px black;border-top:solid 1px black;' colspan='3' width='20%'> </td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>HK</td>
                        <td>: ".$row->hk." hari</td>
                        <td style='border-left:solid 1px black;'>BPJS TK</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->bpjs_tk) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>GAJI POKOK</td>
                        <td>: Rp." . number_format($row->gaji_pokok) . "</td>
                        <td style='border-left:solid 1px black;'>BPJS KES</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->bpjs_kes) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>RAPELAN</td>
                        <td>: Rp." . number_format($row->rapel) . "</td>
                        <td style='border-left:solid 1px black;'>PENSIUN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pensiun) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>INSENTIF</td>
                        <td>: Rp." . number_format($row->insentif) . "</td>
                        <td style='border-left:solid 1px black;'>POT. BACKUP</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_backup) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>LEMBURAN</td>
                        <td>: Rp." . number_format($row->lemburan) . "</td>
                        <td style='border-left:solid 1px black;'>POT. SERAGAM & PERL</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_seragam) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>PREMI HADIR</td>
                        <td>: Rp." . number_format($row->premi_hadir) . "</td>
                        <td style='border-left:solid 1px black;'>POT. ABSEN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_absen) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>TJ. PULSA</td>
                        <td>: Rp." . number_format($row->tj_pulsa) . "</td>
                        <td style='border-left:solid 1px black;'>POT. SPH</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_sph) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>TJ. TRANSPORT</td>
                        <td>: Rp." . number_format($row->tj_transport) . "</td>
                        <td style='border-left:solid 1px black;'>PPH 21</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pph21) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>TJ. UANG MAKAN</td>
                        <td>: Rp." . number_format($row->tj_um) . "</td>
                        <td style='border-left:solid 1px black;'>POT. LAIN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_lain) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'><br></td>
                        <td><br></td>
                        <td style='border-left:solid 1px black;'> </td>
                        <td style='border-right:solid 1px black;' colspan='3'> </td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'><b>TOTAL PENDAPATAN</b></td>
                        <td>: Rp." . number_format($totalPendapatan) . "</td>
                        <td style='border-left:solid 1px black;'><b>TOTAL POT</b></td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($totalPot) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;border-top:solid 1px black;border-bottom:solid 1px black;'></td>
                        <td style='border-top:solid 1px black;border-bottom:solid 1px black;'></td>
                        <td style='border-top:solid 1px black;border-bottom:solid 1px black;'><b>GAJI DITERIMA</b></td>
                        <td style='border-right:solid 1px black;border-top:solid 1px black;border-bottom:solid 1px black;' colspan='3'>: Rp." . number_format($row->netto_sistem) ."</td>
                    </tr>
                    
                </table>".$enter;
                
            

                
        }
        // $mpdf->AddPage();
        $mpdf->WriteHTML($show);
        $mpdf->Output();
        
        // return $show;
        
    }
}
