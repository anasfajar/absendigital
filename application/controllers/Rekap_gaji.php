<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_gaji extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->get_datasess = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('M_Front');
        $this->load->model('M_Admin');
        $this->load->model('M_Kandidat');
        $this->load->model('M_Op');
        $this->load->model('M_Dokumen');
        $this->load->model('M_Kandidat');
        $this->load->library('upload');
        $this->load->helper('tanggal_indonesia');
        $this->get_datasetupapp = $this->M_Front->fetchsetupapp();
        $this->load->library('form_validation');
    }

    public function index()
    {
        is_admin();
        $data = [
            'title' => 'Dashboard Absensi',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/dataslipgaji', $data);
        $this->load->view('layout/footer', $data);
    }

    public function list_gaji()
    {
        is_admin();
        $data = [
            'title' => 'Dashboard Absensi',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'id' => $this->input->get('id'),
        ];

        $idd = $this->input->get('id');
        $data['slipgaji'] = $this->db->get_where('tmp_rekapabsen', ['dokumen_id' => $idd])->row_array();
        $data['dokumen'] = $this->db->get_where('dokumen_upload_gaji', ['dokumen_id' => $idd])->row_array();
        $this->session->set_userdata('idDok',$idd);

        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/datarekapabsen', $data);
        $this->load->view('layout/footer', $data);
    }

    public function import_slip()
    {
        is_admin();
        $data = [
            'title' => 'Rekap Gaji',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/tambahfile', $data);
        $this->load->view('layout/footer', $data);
    }


       


    public function excel()
    {

        $this->form_validation->set_rules('district', 'district', 'required', ['required' => 'Mohon masukan nama District!']);
        $this->form_validation->set_rules('nama_upload', 'nama_upload', 'required', ['required' => 'Mohon masukan Nama Upload!']);
        $this->form_validation->set_rules('file', 'Template Slip Gaji', 'uploaded[file]');
        $this->form_validation->set_rules('file', '', 'callback_cek_file');

        if ($this->form_validation->run() == false) {
            $this->import_slip();
        }else{

            error_reporting(0);
            $q = $this->db->query("SELECT MAX(RIGHT(dokumen_id,4)) AS kd_max FROM dokumen_upload_gaji");
            $kd = "";
            if($q->num_rows()>0){
                foreach($q->result() as $k){
                    $tmp = ((int)$k->kd_max)+1;
                    $kd = sprintf("%04s", $tmp);
                }
            }else{
                $kd = "0001";
            }
            date_default_timezone_set('Asia/Jakarta');
            $no_otomatis =  date('dmy').$kd;


            if (isset($_FILES["file"]["name"])) {
                // upload
                $file_tmp = $_FILES['file']['tmp_name'];
                $file_name = $_FILES['file']['name'];
                $file_size = $_FILES['file']['size'];
                $file_type = $_FILES['file']['type'];
                //move_uploaded_file($file_tmp,"uploads/".$file_name); // simpan filenya di folder uploads

                $object = PHPExcel_IOFactory::load($file_tmp);

                foreach ($object->getWorksheetIterator() as $worksheet) {

                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();

                    for ($row = 2; $row <= $highestRow; $row++) {

                        $nik = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                        $nama = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                        $jabatan = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $area = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $customer = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                        $hk = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                        $status_k = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                        $gaji_pokok = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                        $rapel = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                        $pot_absen = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                        $insentif = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                        $lemburan = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                        $premi_hadir = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                        $tj_pulsa = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                        $tj_transport = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                        $tj_um = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                        $hasil = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                        $bpjs_tk = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                        $bpjs_kes = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                        $pensiun = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                        $pkp = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                        $pph21 = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                        $bruto = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                        $pot_backup = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                        $pot_seragam = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                        $pot_sph = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                        $pot_lain = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                        $netto = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                        $netto_sistem = ($gaji_pokok+$rapel+$insentif+$lemburan+$premi_hadir+$tj_pulsa+$tj_transport+$tj_um)-($pot_absen+$bpjs_tk+$bpjs_kes+$pensiun+$pph21+$pot_backup+$pot_sph+$pot_lain);

                        if(!empty($nik) || !empty($nama)){
                            $data[] = array(
                                'nik'           => $nik,
                                'nama'          => $nama,
                                'jabatan'       => $jabatan,
                                'area'          => $area,
                                'customer'      => $customer,
                                'hk'            => $hk,
                                'status_k'      => $status_k,
                                'gaji_pokok'    => $gaji_pokok,
                                'rapel'         => $rapel,
                                'pot_absen'     => $pot_absen,
                                'insentif'      => $insentif,
                                'lemburan'      => $lemburan,
                                'premi_hadir'   => $premi_hadir,
                                'tj_pulsa'      => $tj_pulsa,
                                'tj_transport'  => $tj_transport,
                                'tj_um'         => $tj_um,
                                'hasil'         => $hasil,
                                'bpjs_tk'       => $bpjs_tk,
                                'bpjs_kes'      => $bpjs_kes,
                                'pensiun'       => $pensiun,
                                'pkp'           => $pkp,
                                'pph21'         => $pph21,
                                'bruto'         => $bruto,
                                'pot_backup'    => $pot_backup,
                                'pot_seragam'   => $pot_seragam,
                                'pot_sph'       => $pot_sph,
                                'pot_lain'      => $pot_lain,
                                'netto'         => $netto,
                                'netto_sistem'  => $netto_sistem,
                                'dokumen_id'    => $no_otomatis
                            );
                        }
                    }
                }

                $this->db->insert_batch('tmp_rekapabsen', $data);

                $dataslip[] = array(
                    'nama_dokumen'   => $file_name,
                    'nama_upload'    => $this->input->post('nama_upload'),
                    'district'       => $this->input->post('district'),
                    'tahun'          => $this->input->post('tahun_slip'),
                    'bulan'          => $this->input->post('bulan_slip'),
                    'status'         => '0',
                    'dokumen_id'         => $no_otomatis
                );
                            
                $this->db->insert_batch('dokumen_upload_gaji', $dataslip);

                $message = array(
                    'message' => '<div class="alert alert-success">Import file excel berhasil disimpan di database</div>',
                );

                $this->session->set_flashdata($message);
                redirect('rekap-gaji');
            } else {
                $message = array(
                    'message' => '<div class="alert alert-danger">Import file gagal, coba lagi</div>',
                );

                $this->session->set_flashdata($message);
                redirect('rekap-gaji');
            }

        }
    }

    public function cek_file($str)
    {
        $allowed_mime_type_arr = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel', 'application/msexcel', 'application/xls', 'application/excel');
        $mime = get_mime_by_extension($_FILES['file']['name']);
        if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
            if (in_array($mime, $allowed_mime_type_arr)) {
                return true;
            } else {
                $this->form_validation->set_message('cek_file', 'Silahkan pilih hanya file xls/xlsx.');
                return false;
            }
        } else {
            $this->form_validation->set_message('cek_file', 'Silakan pilih file untuk diupload.');
            return false;
        }
    }
 

    public function exportslipgaji(){
        is_admin();
        
        $idd = $this->input->get('id');
        $data['rekap'] = $this->db->get_where('tmp_rekapabsen', ['dokumen_id' => $idd])->result();
        $data['dokumen'] = $this->db->get_where('dokumen_upload_gaji', ['dokumen_id' => $idd])->row_array();

        //$data['rekap'] = $this->db->get('tmp_rekapabsen')->result();
        $this->load->view('layout/rekapabsen/exportgajixls', $data);
      }

    public function exportslipgajiperiode(){
        is_admin();
        
        $bulan = $this->input->post('bulan_slip');
        $tahun = $this->input->post('tahun_slip');
        
        //$data['rekap'] = $this->db->get_where('tmp_rekapabsen', ['dokumen_id' => $idd])->result();
        //$data['dokumen'] = $this->db->get_where('dokumen_upload_gaji', ['dokumen_id' => $idd])->row_array();

        $data['rekap'] = $this->db->query("SELECT * FROM tmp_rekapabsen 
        JOIN dokumen_upload_gaji ON tmp_rekapabsen.dokumen_id = dokumen_upload_gaji.dokumen_id
        WHERE dokumen_upload_gaji.bulan = '$bulan' AND dokumen_upload_gaji.tahun ='$tahun' AND dokumen_upload_gaji.status != 2")->result();

        //$data['rekap'] = $this->db->get('tmp_rekapabsen')->result();
        $this->load->view('layout/rekapabsen/exportgajixlsperiode', $data);
    }

    function get_datatbl()
    {
        $dataabsen = $this->input->get('type');
        $bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
        $nowday = $hari[(int)date("w")] . ', ' . date("j ") . $bulan[(int)date('m')] . date(" Y");
        $enddate = new DateTime(date('Y-m-d'));
        $data = [];
        $no = 1;
        if ($dataabsen == 'dataslip') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $query = $this->db->get("tmp_rekapabsen");
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    //'<img class="img-thumbnail" src="'. ($r->image == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('storage/profile/' . //$r->image)) . '" class="card-img" style="width: 100%;">',
                    $r->jabatan,
                    $r->nett,
                    '<div class="btn-group btn-small " style="text-align: right;">
                   <a href="" class="btn btn-primary"><span class="fas fa-fw fa-address-card"></span></a>
                   <a href="" target="_blank" class="btn btn-warning"><span class="fas fa-fw fa-file"></span></a>
                  </div>'

                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        }
    }

    public function create_slip()
    {
        is_admin();

        $this->cetakslipPdf($this->input->get('id'),$this->input->get('dokid'));
    }

    public function cetakslipPdf($id,$dokid)
    {
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A5-L']);
        if($id == '000'){
            $where = "where dokumen_id = $dokid ";
            $enter = "<br><br><br><br><br>"; 
        }else{
            $where = "where id_slipgaji = $id";
            $enter = "";
        } 

        $getData = $this->db->query("SELECT * from tmp_rekapabsen $where")->result();
        $show = "";
        foreach ($getData as $row) {
            $totalPot = $row->pot_absen + $row->bpjs_tk + $row->bpjs_kes + $row->pensiun + $row->pph21 + $row->pot_backup + $row->pot_sph + $row->pot_lain;
            $totalPendapatan = $row->gaji_pokok + $row->rapel + $row->insentif + $row->lemburan + $row->premi_hadir + $row->tj_pulsa + $row->tj_transport + $row->tj_um;
            $hk = empty($row->hk) ? '-' : $row->hk." Hari";
        // var_dump($row->gaji_pokok);die();

            $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td width='100%' align='center'><img style='width: 480px;height: auto;' src='" . base_url('storage/profile/header.png') . "'></td>
                    </tr>
                </table>";
            $show .= "<table width='100%' border='0' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td style='border-left:solid 1px black;' width='30%'>NO</td>
                        <td width='20%'>: </td>
                        <td width='30%' style='border-left:solid 1px black;'></td>
                        <td width='20%' style='border-right:solid 1px black;' colspan='3'></td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>NIK</td>
                        <td>: " . $row->nik . "</td>
                        <td style='border-left:solid 1px black;'>JABATAN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $row->jabatan . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>NAMA</td>
                        <td>: " . $row->nama . "</td>
                        <td style='border-left:solid 1px black;'>ALOKASI</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $row->customer . "-" . $row->area . "</td>
                    </tr>
                    
                </table>";
            $show .= "<table width='100%' border='0' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td style='border-left:solid 1px black;border-top:solid 1px black;' width='30%'><b>PENDAPATAN</b></td>
                        <td style='border-top:solid 1px black;' width='20%'> </td>
                        <td style='border-left:solid 1px black;border-top:solid 1px black;' width='30%'><b>POTONGAN</b></td>
                        <td style='border-right:solid 1px black;border-top:solid 1px black;' colspan='3' width='20%'> </td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>HK</td>
                        <td>: ".$hk."</td>
                        <td style='border-left:solid 1px black;'>BPJS TK</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->bpjs_tk) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>GAJI POKOK</td>
                        <td>: Rp." . number_format($row->gaji_pokok) . "</td>
                        <td style='border-left:solid 1px black;'>BPJS KES</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->bpjs_kes) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>RAPELAN</td>
                        <td>: Rp." . number_format($row->rapel) . "</td>
                        <td style='border-left:solid 1px black;'>PENSIUN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pensiun) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>INSENTIF</td>
                        <td>: Rp." . number_format($row->insentif) . "</td>
                        <td style='border-left:solid 1px black;'>POT. BACKUP</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_backup) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>LEMBURAN</td>
                        <td>: Rp." . number_format($row->lemburan) . "</td>
                        <td style='border-left:solid 1px black;'>POT. SERAGAM & PERL</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_seragam) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>PREMI HADIR</td>
                        <td>: Rp." . number_format($row->premi_hadir) . "</td>
                        <td style='border-left:solid 1px black;'>POT. ABSEN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_absen) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>TJ. PULSA</td>
                        <td>: Rp." . number_format($row->tj_pulsa) . "</td>
                        <td style='border-left:solid 1px black;'>POT. SPH</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_sph) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>TJ. TRANSPORT</td>
                        <td>: Rp." . number_format($row->tj_transport) . "</td>
                        <td style='border-left:solid 1px black;'>PPH 21</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pph21) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>TJ. UANG MAKAN</td>
                        <td>: Rp." . number_format($row->tj_um) . "</td>
                        <td style='border-left:solid 1px black;'>POT. LAIN</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($row->pot_lain) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'><br></td>
                        <td><br></td>
                        <td style='border-left:solid 1px black;'> </td>
                        <td style='border-right:solid 1px black;' colspan='3'> </td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'><b>TOTAL PENDAPATAN</b></td>
                        <td>: Rp." . number_format($totalPendapatan) . "</td>
                        <td style='border-left:solid 1px black;'><b>TOTAL POT</b></td>
                        <td style='border-right:solid 1px black;' colspan='3'>: Rp." . number_format($totalPot) . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;border-top:solid 1px black;border-bottom:solid 1px black;'></td>
                        <td style='border-top:solid 1px black;border-bottom:solid 1px black;'></td>
                        <td style='border-top:solid 1px black;border-bottom:solid 1px black;'><b>GAJI DITERIMA</b></td>
                        <td style='border-right:solid 1px black;border-top:solid 1px black;border-bottom:solid 1px black;' colspan='3'>: Rp." . number_format($row->netto_sistem) ."</td>
                    </tr>
                    
                </table>".$enter;
                
            

                
        }
        // $mpdf->AddPage();
        $mpdf->WriteHTML($show);
        $mpdf->Output();
        
        // return $show;
        
    }

    public function viewdetailslip()
    {
        is_admin();
        $data = [
            'title' => 'Data Detail Gaji Karyawan',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['viewpgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        //$data['viewpgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/viewdetailslipgaji', $data);
        $this->load->view('layout/footer', $data);
    }

    public function editprofile()
    {
        is_admin();
        $data = [
            'title' => 'Data Kategori Profile',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/editkategoriprofile', $data);
        $this->load->view('layout/footer', $data);
    }

    public function editpendapatan()
    {
        is_admin();
        $data = [
            'title' => 'Data Kategori Profile',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/editkategoripendapatan', $data);
        $this->load->view('layout/footer', $data);
    }

    public function editpotongan()
    {
        is_admin();
        $data = [
            'title' => 'Data Kategori Potongan',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/editkategoripotongan', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updateprofile()
    {
        is_admin();    
        
        $this->form_validation->set_rules('nik', 'NIK', 'required', ['required' => 'Mohon masukan NIK karyawan!']);
        $this->form_validation->set_rules('nama', 'Nama', 'required', ['required' => 'Mohon masukan Nama Lengkap!']);
        $this->form_validation->set_rules('area', 'Area', 'required', ['required' => 'Mohon masukan Nama Area!']);
        $this->form_validation->set_rules('customer', 'Customer', 'required', ['required' => 'Mohon masukan Nama Customer!']);
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required', ['required' => 'Mohon masukan Jabatan!']);
        $this->form_validation->set_rules('hk', 'HK', 'required', ['required' => 'Mohon masukan HK!']);

        if ($this->form_validation->run() == false) {
        is_admin();
        $data = [
            'title' => 'Data Kategori Profile',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/editkategoriprofile', $data);
        $this->load->view('layout/footer', $data);

        }else{

            $id = $this->input->post('id');
            $data = [
                'nik' => $this->input->post('nik'),
                'nama' => $this->input->post('nama'),
                'jabatan' => $this->input->post('jabatan'),
                'area' => $this->input->post('area'),
                'customer' => $this->input->post('customer'),
                'hk' => $this->input->post('hk')   
            ];
            $this->db->where('id_slipgaji', $id );
            $this->db->update('tmp_rekapabsen', $data);
            $this->session->set_flashdata('success', 'Perubahan Data Berhasil');
            redirect('editkategoriprofile?id='.$id.'');

        }       
        
    }

    public function updatependapatan()
    {
        is_admin();    
        
        $this->form_validation->set_rules('gaji_pokok', 'gaji_pokok', 'required', ['required' => 'Mohon masukan Gaji Pokok karyawan!']);
        $this->form_validation->set_rules('rapel', 'rapel', 'required', ['required' => 'Mohon masukan rapel!']);
        $this->form_validation->set_rules('insentif', 'insentif', 'required', ['required' => 'Mohon masukan Nama Insentif!']);
        $this->form_validation->set_rules('lemburan', 'lemburan', 'required', ['required' => 'Mohon masukan Nama Lemburan!']);
        $this->form_validation->set_rules('premi_hadir', 'premi_hadir', 'required', ['required' => 'Mohon masukan Premi Hadir!']);
        $this->form_validation->set_rules('tj_pulsa', 'tj_pulsa', 'required', ['required' => 'Mohon masukan Tunjangan Pulsa!']);
        $this->form_validation->set_rules('tj_transport', 'tj_transport', 'required', ['required' => 'Mohon masukan Tunjangan Transport!']);
        $this->form_validation->set_rules('tj_um', 'tj_um', 'required', ['required' => 'Mohon masukan Tunjangan Uang Makan!']);
        

        if ($this->form_validation->run() == false) {
        is_admin();
        $data = [
            'title' => 'Data Kategori Profile',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/editkategoriprofile', $data);
        $this->load->view('layout/footer', $data);

        }else{

            $id = $this->input->post('id');
            $gaji_pokok = $this->input->post('gaji_pokok');
            $rapel = $this->input->post('rapel');
            $insentif = $this->input->post('insentif');
            $lemburan = $this->input->post('lemburan');
            $premi_hadir = $this->input->post('premi_hadir');
            $tj_pulsa = $this->input->post('tj_pulsa');
            $tj_transport = $this->input->post('tj_transport');
            $tj_um = $this->input->post('tj_um');
            
            $getPotongan = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
            $potongan = $getPotongan['pot_absen']+$getPotongan['bpjs_tk']+$getPotongan['bpjs_kes']+$getPotongan['pensiun']+$getPotongan['pph21']+$getPotongan['pot_backup']+$getPotongan['pot_sph']+$getPotongan['pot_lain']+$getPotongan['pot_seragam'];

            $data = [
                'gaji_pokok' => $gaji_pokok,
                'rapel' => $rapel,
                'insentif' => $insentif,
                'lemburan' => $lemburan,
                'premi_hadir' => $premi_hadir,
                'tj_pulsa' => $tj_pulsa,
                'tj_transport' => $tj_transport,
                'tj_um' => $tj_um,
                'netto_sistem' => ($gaji_pokok+$rapel+$insentif+$lemburan+$premi_hadir+$tj_pulsa+$tj_transport+$tj_um) - $potongan
            ];
            $this->db->where('id_slipgaji', $id );
            $this->db->update('tmp_rekapabsen', $data);
            $this->session->set_flashdata('success', 'Perubahan Data Berhasil');
            redirect('editkategoripendapatan?id='.$id.'');

        }       
        
    }

    public function updatepotongan()
    {
        is_admin();    
        
        $this->form_validation->set_rules('pot_absen', 'pot_absen', 'required', ['required' => 'Mohon masukan nominal Potongan Absen!']);
        $this->form_validation->set_rules('bpjs_tk', 'bpjs_tk', 'required', ['required' => 'Mohon masukan nominal BPJS Ketenagakerjaan!']);
        $this->form_validation->set_rules('bpjs_kes', 'bpjs_kes', 'required', ['required' => 'Mohon masukan nominal BPJS Kesehatan!']);
        $this->form_validation->set_rules('pensiun', 'pensiun', 'required', ['required' => 'Mohon masukan nominal Pensiun!']);
        $this->form_validation->set_rules('pph21', 'pph21', 'required', ['required' => 'Mohon masukan nominal PPH21!']);
        $this->form_validation->set_rules('pot_backup', 'pot_backup', 'required', ['required' => 'Mohon masukan nominal Potongan Backup!']);
        $this->form_validation->set_rules('pot_sph', 'pot_sph', 'required', ['required' => 'Mohon masukan Tunjangan nominal Potongan SPH!']);
        $this->form_validation->set_rules('pot_lain', 'pot_lain', 'required', ['required' => 'Mohon masukan Tunjangan nominal Potongan Lain-lain!']);

        if ($this->form_validation->run() == false) {
        is_admin();
        $data = [
            'title' => 'Data Kategori Profile',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/rekapabsen/editkategoriprofile', $data);
        $this->load->view('layout/footer', $data);

        }else{

            $id = $this->input->post('id');
            $pot_absen = $this->input->post('pot_absen');
            $bpjs_tk = $this->input->post('bpjs_tk');
            $bpjs_kes = $this->input->post('bpjs_kes');
            $pensiun = $this->input->post('pensiun');
            $pph21 = $this->input->post('pph21');
            $pot_backup = $this->input->post('pot_backup');
            $pot_seragam = $this->input->post('pot_seragam');
            $pot_sph = $this->input->post('pot_sph');
            $pot_lain = $this->input->post('pot_lain');

            $getPendapatan = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $id)->row_array();
            $pendapatan = $getPendapatan['gaji_pokok']+$getPendapatan['rapel']+$getPendapatan['insentif']+$getPendapatan['lemburan']+$getPendapatan['premi_hadir']+$getPendapatan['tj_pulsa']+$getPendapatan['tj_transport']+$getPendapatan['tj_um'];
            $data = [
                'pot_absen' => $pot_absen,
                'bpjs_tk' => $bpjs_tk,
                'bpjs_kes' => $bpjs_kes,
                'pensiun' => $pensiun,
                'pph21' => $pph21,
                'pot_backup' => $pot_backup,
                'pot_seragam' => $pot_seragam,
                'pot_sph' => $pot_sph,
                'pot_lain' => $pot_lain,
                'netto_sistem' => $pendapatan - ($pot_absen+$bpjs_tk+$bpjs_kes+$pensiun+$pph21+$pot_backup+$pot_sph+$pot_lain+$pot_seragam)    
            ];
            $this->db->where('id_slipgaji', $id );
            $this->db->update('tmp_rekapabsen', $data);
            $this->session->set_flashdata('success', 'Perubahan Data Berhasil');
            redirect('editkategoripotongan?id='.$id.'');

        }       
        
    }

    public function approve(){
        $dokumen_id = $this->input->get('id');
        $update = array(
            'status' => 1
        );

        $select = $this->db->query("SELECT a.*,b.district,b.nama_dokumen,b.nama_upload FROM tmp_rekapabsen a,dokumen_upload_gaji b where a.dokumen_id = b.dokumen_id and b.dokumen_id = ".$dokumen_id." AND a.nik NOT IN (SELECT nik FROM login_rekapgaji) GROUP BY a.nik");

        foreach($select->result() as $row){
            $insert[] = array(
                'nik' => $row->nik,
                'password' => password_hash($row->nik, PASSWORD_DEFAULT),
                'nama_lengkap' => $row->nama,
                'jabatan' => $row->jabatan,
                'image' => 'default.png',
                'role_id' => 5,
                'district' => $row->district
            );
            $namafile = $row->nama_dokumen.'/'.$row->nama_upload;
        }
        // echo "<pre>";
        // var_dump($insert);die();

        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well 

        $this->db->update('dokumen_upload_gaji', $update, "dokumen_id = ".$dokumen_id); 

        for($i=0;$i<count($insert);$i++){
            $this->db->insert('login_rekapgaji', $insert[$i]); # Inserting data
        }

        $this->db->trans_complete(); # Completing transaction

        /*Optional*/

        if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            // return FALSE;
            $this->session->set_flashdata('error', 'Proses Approve dokumen '.$namafile.' GAGAL');
            redirect("rekap-gaji");

        } 
        else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
            // return TRUE;
            $this->session->set_flashdata('success', 'Proses Approve dokumen '.$namafile.' Berhasil');
            redirect("rekap-gaji");
        }


    }

    public function hapusDok(){
        $dokumen_id = $this->input->get('id');
        $update = array(
            'status' => 2
        );
        $updateStatus = $this->db->update('dokumen_upload_gaji', $update, "dokumen_id = ".$dokumen_id); 
        if ($updateStatus === FALSE) {
            $this->session->set_flashdata('error', 'Proses Hapus dokumen GAGAL');
            redirect("rekap-gaji");

        } else {
            $this->session->set_flashdata('success', 'Proses Hapus dokumen Berhasil');
            redirect("rekap-gaji");
        }
    }

    public function templateExcel(){
        $data = file_get_contents(site_url().'/storage/templateMasterPayroll.xlsx');
        $name = 'templateMasterPayroll.xlsx';

        force_download($name, $data);
    }

}
