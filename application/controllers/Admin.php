<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->get_datasess = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('M_Front');
        $this->load->model('M_Admin');
        $this->load->model('M_Kandidat');
        $this->load->model('M_Op');
        $this->load->model('M_Dokumen');
        $this->load->model('M_Kandidat');
        $this->load->library('upload');
        $this->load->helper('tanggal_indonesia');
        $this->get_datasetupapp = $this->M_Front->fetchsetupapp();
    }


    public function settingapp()
    {
        is_admin();
        $data = [
            'title' => 'Setting Aplikasi',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/settingapp', $data);
        $this->load->view('layout/footer', $data);
    }

    public function dashboard()
    {
        is_admin();
        $data = [
            'title' => 'Dashboard Absensi',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $data['jmlpegawai'] = $this->M_Admin->hitungjumlahdata('jmlpgw');
        $data['pegawaitelat'] = $this->M_Admin->hitungjumlahdata('pgwtrl');
        $data['pegawaimasuk'] = $this->M_Admin->hitungjumlahdata('pgwmsk');
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('layout/footer', $data);
    }

    public function accident()
    {
        is_admin();
        $data = [
            'title' => 'Data Kecelakaan',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'fetchdbkecelakaan' => $this->M_Admin->fetchlistkecelakaan()
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/datakecelakaan', $data);
        $this->load->view('layout/footer', $data);
    }

    public function addaccident()
    {
        is_admin();
        $data = [
            'title' => 'Data Kecelakaan',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/dataaccident/tambahkecelakaan', $data);
        $this->load->view('layout/footer', $data);
    }

    public function detailaccident()
    {
        is_admin();
        $data = [
            'title' => 'Detail Kecelakaan',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'content' => $this->M_Admin->detailKecelakaan(),
            'id' => $_REQUEST['id']
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/dataaccident/detailkecelakaan', $data);
        $this->load->view('layout/footer', $data);
    }

    public function cetakaccident()
    {
        is_admin();

        $mpdf = new \Mpdf\Mpdf();
        $show = $this->M_Admin->detailKecelakaan();

        $mpdf->WriteHTML($show);
        $mpdf->Output();
    }


    public function simpankecelakaan()
    {
        $data = $_POST;
        $config['upload_path'] = '../public/storage/kecelakaan';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        for ($im = 1; $im < 4; $im++) {
            if (!empty($_FILES['filefoto' . $im]['name'])) {
                if ($this->upload->do_upload('filefoto' . $im)) {
                    $gbr = $this->upload->data();
                    $gambar = $gbr['file_name'];
                    $data['foto' . $im] = $gambar;
                }
            }
        }
        $data['tanggal'] = mysql_date($_POST['tanggal']);

        $proses = $this->M_Admin->simpanKecelakaan($data);
        redirect('accident', 'refresh');
    }

    public function datapegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'fetchdbpegawai' => $this->M_Admin->fetchlistpegawai()
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/datapegawai', $data);
        $this->load->view('layout/footer', $data);
    }

    public function datapegawaifico()
    {
        // var_dump($_POST);die();
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'fetchdbpegawai' => $this->M_Admin->fetchlistpegawai(),
            'op' => $this->input->post('inputOp'),
            'cs' => $this->input->post('inputCustomer')
        ];

        $this->session->set_userdata('op',$this->input->post('inputOp'));
        $this->session->set_userdata('cs',$this->input->post('inputCustomer'));
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/datapegawaifico', $data);
        $this->load->view('layout/footer', $data);
    }

    public function searchdatapegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai Pencarian',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'op' => $this->M_Op->getAll()
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/searchpegawai', $data);
    }

    public function tambahpegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,

        ];
        $data['nik'] = $this->M_Admin->get_no_invoice();
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/tambahpegawai', $data);
        $this->load->view('layout/footer', $data);
    }

    public function viewpegawaimbk()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['viewpgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/viewpegawaimbk', $data);
        $this->load->view('layout/footer', $data);
    }

    public function editpegawaimbk()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editpegawaimbk', $data);
        $this->load->view('layout/footer', $data);
    }

    public function editketeranganbank()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editketeranganbank', $data);
        $this->load->view('layout/footer', $data);
    }

    public function editphotopegawaimbk()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editfotopegawaimbk', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updatepgw()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        //proses update data master_karyawan
        $this->M_Admin->update_pegawai();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
    }

    public function updateketeranganbank()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        //proses update data master_karyawan
        $this->M_Admin->update_keteranganbank();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function savepgw()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        //$id = $this->input->get('id');
        //$data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        //proses update data master_karyawan

        $this->form_validation->set_rules(
            'nik',
            'Nomor Induk Karyawan',
            'trim|required|numeric|is_unique[master_karyawan.nik]',
            ['is_unique' => 'NIK anda sudah terdaftar!', 'numeric' => 'NIK hanya diisi number!', 'required' => 'Mohon masukan NIK anda']
        );
        $this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim', ['required' => 'Mohon masukan nama lengkap anda!']);
        $this->form_validation->set_rules(
            'no_ktp',
            'KTP',
            'trim|required|numeric|min_length[16]|max_length[16]|is_unique[master_karyawan.no_ktp]',
            ['is_unique' => 'Nomor KTP sudah terdaftar!', 'numeric' => 'KTP hanya diisi number!', 'min_length' => 'Minimal Nomor KTP 16 digit!', 'max_length' => 'Maksimal Nomor KTP 16 digit!', 'required' => 'Mohon masukan No KTP anda']
        );
        $this->form_validation->set_rules('nomor_hp', 'Handphone', 'required|trim', ['required' => 'Mohon masukan Nomor Handphone anda!']);
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required', ['required' => 'Mohon masukan Tempat Lahir anda!']);
        $this->form_validation->set_rules('tglblnthn_lahir', 'Tanggal Lahir', 'required', ['required' => 'Mohon masukan Tanggal Lahir anda!']);
        $this->form_validation->set_rules('alamat', 'Alamat', 'required', ['required' => 'Mohon masukan alamat anda!']);
        $this->form_validation->set_rules('nomor_kartu_keluarga', 'Nomor Kartu Keluarga', 'required', ['required' => 'Mohon masukan No Kartu Keluarga anda!']);
        $this->form_validation->set_rules('npwp', 'NPWP', 'trim|required|numeric', ['required' => 'Mohon masukan NPWP anda!']);
        $this->form_validation->set_rules('email_address', 'Email', 'required|trim|valid_email', ['required' => 'Mohon masukan alamat email anda!']);
        $this->form_validation->set_rules('jenis_kelamin', 'Gender', 'required', ['required' => 'Mohon pilih Gender anda!']);

        $this->form_validation->set_rules('nama_di_bank', 'Nama diBank', 'required', ['required' => 'Mohon masukan Nama Anda di Bank anda!']);
        $this->form_validation->set_rules('bank', 'Nama Bank', 'required', ['required' => 'Mohon masukan Nama Bank anda!']);
        $this->form_validation->set_rules('no_account', 'No Rekening', 'required', ['required' => 'Mohon masukan No Rekening anda!']);

        $this->form_validation->set_rules('keterangan_aktivasi_karyawan', 'Aktifasi Karyawan', 'required', ['required' => 'Mohon masukan Aktifasi Karyawan!']);
        $this->form_validation->set_rules('nomor_po_nama_driver_yg_diganti', 'Nama Driver', 'required', ['required' => 'Mohon masukan Nama Driver yang diganti!']);
        $this->form_validation->set_rules('tanggal_po', 'Tanggal PO', 'required', ['required' => 'Mohon masukan Tanggal PO!']);
        $this->form_validation->set_rules('tgl_inaktif_kerja', 'Tanggal Inaktif Kerja', 'required', ['required' => 'Mohon masukan Tanggal Inaktif Kerja']);
        $this->form_validation->set_rules('lead_time', 'Lead Time', 'required', ['required' => 'Mohon masukan Lead Time!']);

        $this->form_validation->set_rules('no_sim', 'SIM', 'required|numeric|trim', ['required' => 'Mohon masukan Nomos SIM!']);
        $this->form_validation->set_rules('masa_berlaku_sim', 'Masa Berlaku SIm', 'required', ['required' => 'Mohon masukan Masa Berlaku SIM!']);
        $this->form_validation->set_rules('start_kontrak', 'Awal Kontrak', 'required', ['required' => 'Mohon masukan Awal Kontrak!']);
        $this->form_validation->set_rules('finish_kontrak', 'Akhir Kontrak', 'required', ['required' => 'Mohon masukan Akhir Kontrak']);
        $this->form_validation->set_rules('status_kontrak', 'Status Kontrak', 'required', ['required' => 'Mohon masukan Status Kontrak!']);
        $this->form_validation->set_rules('masa_kontrak_berakhir', 'Masa Kontrak Berakhir', 'required', ['required' => 'Mohon masukan Masa Kontrak Berakhir!']);
        $this->form_validation->set_rules('lama_kerja', 'Lama Kerja', 'required', ['required' => 'Mohon masukan Lama Kerja!']);
        $this->form_validation->set_rules('seragam', 'Seragam', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('id_card', 'ID Card', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('tanggal_pengiriman_seragam', 'Pengiriman Seragam', 'required', ['required' => 'Mohon masukan Tanggal Pengiriman Seragam!']);
        $this->form_validation->set_rules('tanggal_pengiriman_id_card', 'Tanggal Pengiriman ID Card', 'required', ['required' => 'Mohon masukan Tanggal Pengiriman ID Card!']);
        $this->form_validation->set_rules('training_safety_driving', 'Training Safety Driving', 'required', ['required' => 'Mohon pilih Keterangan!']);
        $this->form_validation->set_rules('pelaksanaan_training_terakhir', 'Pelaksanaan Training Terakhir', 'required', ['required' => 'Mohon masukan Training Terakhir!']);
        $this->form_validation->set_rules('masa_training', 'Masa Training', 'required', ['required' => 'Mohon masukan Masa Training!']);
        $this->form_validation->set_rules('nilai_training_safety_driving_pree_test', 'Nilai Training Safety Driving', 'required', ['required' => 'Mohon masukan Nilai Training Safety Driving!']);
        $this->form_validation->set_rules('nilai_training_safety_driverng_post_test', 'Nilai Training Safety Driving', 'required', ['required' => 'Mohon masukan Nilai Training Safety Driving!']);
        $this->form_validation->set_rules('status_vaksin_pertama', 'Status Vaksin Pertama', 'required', ['required' => 'Mohon pilih Keterangan!']);
        $this->form_validation->set_rules('mengapa_belum_vaksi_pertama', 'Keterangan', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('tanggal_vaksin_pertama', 'Tanggal Vaksin Pertama', 'required', ['required' => 'Mohon masukan Tanggal Vaksin 1!']);
        $this->form_validation->set_rules('lokasi_vaksin_pertama', 'Lokasi Vaksin', 'required', ['required' => 'Lokasi Vaksin!']);
        $this->form_validation->set_rules('status_vaksin_kedua', 'Status Vaksin Kedua', 'required', ['required' => 'Mohon pilih Keterangan!']);
        $this->form_validation->set_rules('mengapa_belum_vaksi_kedua', 'Ketarangan', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('tanggal_vaksin_kedua', 'Tanggal Vaksin Kedua', 'required', ['required' => 'Mohon masukan Tanggal Vaksin 2!']);
        $this->form_validation->set_rules('lokasi_vaksin_kedua', 'Lokasi Vaksin', 'required', ['required' => 'Lokasi Vaksin!']);
        $this->form_validation->set_rules('status_vaksin_booster', 'Status Vaksin Booster', 'required', ['required' => 'Mohon masukan Status Vaksin Booster!']);
        $this->form_validation->set_rules('mengapa_belum_vaksin_booster', 'Ketarangan', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('tanggal_vaksin_booster', 'Tanggal Vaksin Booster', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('lokasi_vaksin_booster', 'Lokasi Vaksin Booster', 'required', ['required' => 'Mohon masukan Vaksin Booster!']);
        $this->form_validation->set_rules('tgl_training_online_part_1', 'Tanggal Training Online', 'required', ['required' => 'Mohon masukan Tanggal Training Online!']);
        $this->form_validation->set_rules('nilai_training_safety_driverng_post_test', 'Nilai Training', 'required', ['required' => 'Mohon masukan Nilai Training!']);
        $this->form_validation->set_rules('tgl_training_online_part_2', 'Tanggal Training Online', 'required', ['required' => 'Mohon masukan Tanggal Training Online!']);
        $this->form_validation->set_rules('point_part_2', 'Nilai Training', 'required', ['required' => 'Mohon masukan Nilai Training!']);
        $this->form_validation->set_rules('tgl_training_attitude_n_knowledge', 'tgl_training_attitude_n_knowledge', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('point_attitude_n_knowledge', 'Point Attitude', 'required', ['required' => 'Mohon masukan Keterangan!']);
        $this->form_validation->set_rules('nomor_bpjs_kesehatan', 'BPJS Kesehatan', 'required', ['required' => 'Mohon masukan BPJS Kesehatan!']);
        $this->form_validation->set_rules('keterangan_proses_bpjs_kesehatan', 'Keterangan Proses BPJS Kesehatan', 'required', ['required' => 'Mohon masukan Keterangan Proses BPJS Kesehatan!']);
        $this->form_validation->set_rules('keterangan_status_bpjs_kesehatan', 'Keterangan Status BPJS Kesehatan', 'required', ['required' => 'Mohon masukan Keterangan Status BPJS Kesehatan!']);
        $this->form_validation->set_rules('nomor_jamsostek', 'Nomor Jamsostek', 'required', ['required' => 'Mohon masukan Nomor Jamsostek!']);
        $this->form_validation->set_rules('keterangan_jamsostek', 'Keterangan Jamsostek', 'required', ['required' => 'Mohon masukan Keterangan Jamsostek!']);
        $this->form_validation->set_rules('reward', 'Reward', 'required', ['required' => 'Mohon masukan Reward!']);
        $this->form_validation->set_rules('tglblnthn_pemberian_reward', 'Tanggal Pemeberian Reward', 'required', ['required' => 'Mohon masukan Tanggal Pemeberian Reward!']);
        $this->form_validation->set_rules('accident_report', 'Accident Report', 'required', ['required' => 'Mohon masukan Accident Report!']);
        $this->form_validation->set_rules('tglblnthn_accident', 'Tanggal Accident', 'required', ['required' => 'Mohon masukan Tanggal Accident!']);
        $this->form_validation->set_rules('kasus', 'kasus', 'required', ['required' => 'Mohon masukan Kasus!']);
        $this->form_validation->set_rules('tglblnthn_kasus', 'Tanggal Bulan Kasus', 'required', ['required' => 'Mohon masukan Tanggal Bulan Kasus!']);
        $this->form_validation->set_rules('sp_i', 'SP 1', 'required', ['required' => 'Mohon masukan SP 1!']);
        $this->form_validation->set_rules('sp_ii', 'SP 2', 'required', ['required' => 'Mohon masukan SP 2!']);
        $this->form_validation->set_rules('sp_iii', 'SP 3', 'required', ['required' => 'Mohon masukan SP 3!']);
        $this->form_validation->set_rules('keterangan_out_dan_mutasi', 'Keterangan Put & Mutasi', 'required', ['required' => 'Keterangan Put & Mutasi!']);
        $this->form_validation->set_rules('masa_training_2', 'Masa Training', 'required', ['required' => 'Mohon masukan Masa Training!']);
        $this->form_validation->set_rules('keterangan_training_2', 'Keterangan Training', 'required', ['required' => 'Mohon masukan Keterangan Training!']);
        $this->form_validation->set_rules('jadwal_training_2', 'Jadwal Training', 'required', ['required' => 'Mohon masukan Jadwal Training!']);
        $this->form_validation->set_rules('driver_leader', 'Driver Leader', 'required', ['required' => 'Mohon masukan Awal Kontrak!']);


        if ($this->form_validation->run() == false) {
            $data['message'] = 'error_validation';
            $data['nik'] = $this->M_Admin->get_no_invoice();
            $this->load->view('layout/header', $data);
            $this->load->view('layout/navbar', $data);
            $this->load->view('layout/sidebar', $data);
            $this->load->view('layout/datapegawai/tambahpegawai', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $this->M_Admin->save_pegawai();
            $this->session->set_flashdata('message', '<div class="alert alert-warning" 
            role="alert">Data Pegawai has been saved!</div>');
            redirect('admin/datapegawai');
        }
    }

    public function updatepgwfoto()
    {
       //is_admin();
       //$data = [
       //     'title' => 'Data Pegawai',
       //     'user' => $this->get_datasess,
       //     'dataapp' => $this->get_datasetupapp
       //];

        $id = $this->input->post('id');        

        //$datasql = $this->db->get_where('master_karyawan', ['id' =>
        //$id])->row_array();

        //$data['master_karyawan'] =  $datasql;

        $upload_image = $_FILES['pasfoto']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size']      = '2048';
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = '../public/storage/profile/';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('pasfoto')) {
                    $gbr = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = base_url('storage/profile/') . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 300;
                    $config['height'] = 300;
                    $config['new_image'] = '../public/storage/profile/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $old_image = $this->get_datasess['image'];
                    if ($old_image != 'default.png') {
                        unlink(FCPATH . 'storage/profile/' . $old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    return "default.png";
                }
            }
            //$this->db->set($sendsave);
            $this->db->where('id', htmlspecialchars($this->input->post('id', true))); // mengambil data dari session
            $this->db->update('master_karyawan');
            redirect('admin/datapegawai');
    }

    public function absensi()
    {
        is_moderator();
        $data = [
            'title' => 'Kehadiran Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/absenpegawai', $data);
        $this->load->view('layout/footer', $data);
    }

    public function kandidat()
    {
        is_admin();

        $data = [
            'title' => 'Data Kandidat Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/kandidat/list', $data);
        $this->load->view('admin/kandidat/footer', $data);
    }

    public function kandidat_data()
    {

        $kandidatList = $this->M_Kandidat->getKandidatDataTables();

        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : 0;

        if (count((array) $kandidatList) > 0) {
            foreach ($kandidatList as $field) {


                if ($field->status_kandidat === 'new') {
                    $label_status = 'label label-primary';
                } else {
                    $label_status = 'label label-warning';
                }

                $actions = ' <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-secondary btn-flat btn-xs dropdown-toggle" type="button">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul role="menu" class="dropdown-menu">
                 ';

                $telepon = substr($field->telp, 0);
                $text = "Assalamualaikum%0AKami%20dari%20PT%20MBK%20ingin%20menindak%20lanjuti%20proses%20recruitment%20saudara%20*" . $field->nama_lengkap . "*,%0Aapakah%20bisa%20di%20adakan%20pertemuan%20di%20minggu%20ini%20di%20kantor%20kami%20?";
                //$actions .= '<li><a href="' . base_url('/kandidat/view/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-eye"></span>  Detail</a></li>';
                $actions .= '<li><button  id="detailkandidat" data-kandidat-id="' . $field->id . '" class="btn btn-default btn-sm"><span class="fa fa-eye"></span>  Detail</button></li>';
                $actions .= '<li><a href="' . base_url('/dokumen/kandidat/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-file"></span> Dokumen</a></li>';
                $actions .= '<li><a href="' . base_url('/soal/interview/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-user-check"></span> Interview</a></li>';
                $actions .= '<li><a href="' . base_url('/soal/pengetahuan/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-edit"></span> Pengetahuan</a></li>';
                $actions .= '<li><a href="' . base_url('/soal/skill/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-edit"></span> Skill</a></li>';
                $actions .= '<li><a href="https://api.whatsapp.com/send?phone=62' . $telepon . '&text=' . $text . '" target="_blank" class="btn btn-default btn-sm"><span class="fab fa-whatsapp"></span> Whatsapp</a></li>';
                $actions .= '</ul></div>

                <button id="whatsapp" data-wa-id="' . $field->id . '" class="btn btn-default btn-sm"><i class="fa fa-comments"></i></button>';

                $tanggal_lahir = ($field->tanggal_lahir !== null) ? date('d-m-Y', strtotime($field->tanggal_lahir))  : '';


                $no++;
                $row = array();
                $row['no'] = $no;
                $row['kode_kandidat'] = $field->kode_kandidat;
                // $row['tanggal_registrasi'] = date_time_indo($field->created_at);
                $row['nama_lengkap'] = $field->nama_lengkap;
                // $row['tempat_lahir'] = $field->tempat_lahir;
                // $row['tanggal_lahir'] = $tanggal_lahir;
                // $row['alamat_domisili'] = $field->alamat_domisili;
                // $row['ktp'] = $field->ktp;
                $row['telp'] = $field->telp;
                // $row['status_dokumen'] = $field->status_dokumen;
                // $row['nilai_test_hr'] = $field->nilai_test_hr;
                // $row['nilai_test_pengetahuan'] = $field->nilai_test_pengetahuan;
                // $row['nilai_test_skill'] = $field->nilai_test_skill;
                $row['status'] =  '<span class="' . $label_status . '">' . ucwords($field->status_kandidat) . '</span>';
                $row['actions'] = $actions;
                $data[] = $row;
            }
        }

        $output = array(
            "draw" => isset($_POST['draw']) ? $_POST['draw'] : 1,
            "recordsTotal" => count((array) $kandidatList),
            "recordsFiltered" => $this->M_Kandidat->countKandidatDataList(),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function datakandidat()
    {
        $typesend = $this->input->get('type');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash()
        ];
        if ($typesend == 'addpgw') {
        } elseif ($typesend == 'viewkandidat') {
            $data['datakandidat'] = $this->db->query("SELECT a.*,b.nama_jobs from kandidat a, m_jobs b Where a.jobs = b.id AND a.id = " . $this->input->post('pgw_id'))->row_array();
            // $this->db->get_where('kandidat', ['id' => $this->input->post('pgw_id')])->row_array();
            $data['kandidatdetail'] =  $this->db->get_where('kandidat_detail', ['kandidat_id' => $this->input->post('pgw_id')])->row_array();
            $id = $this->input->post('pgw_id');
            $data['dokumen'] = $this->M_Kandidat->Dokumenkandidat($id);

            $html = $this->load->view('admin/kandidat/detailkandidat', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        }elseif ($typesend == 'viewslipgaji') {
            $data['datakandidat'] = $this->db->query("SELECT * from tmp_rekapabsen Where id_slipgaji = " . $this->input->post('pgw_id'))->row_array();
            $html = $this->load->view('layout/rekapabsen/detailslipgajikaryawan', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        }elseif ($typesend == 'wakandidat'){
            $data['datakandidat'] = $this->db->query("SELECT a.*,b.nama_jobs from kandidat a, m_jobs b Where a.jobs = b.id AND a.id = " . $this->input->post('pgw_id'))->row_array();
            // $this->db->get_where('kandidat', ['id' => $this->input->post('pgw_id')])->row_array();
            $data['kandidatdetail'] =  $this->db->get_where('kandidat_detail', ['kandidat_id' => $this->input->post('pgw_id')])->row_array();
            $id = $this->input->post('pgw_id');
            $data['dokumen'] = $this->M_Kandidat->Dokumenkandidat($id);

            $html = $this->load->view('admin/kandidat/konfirmasikandidat', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        }
        echo json_encode($reponse);
    }

    public function listkandidatlulus()
    {
        is_admin();

        $data = [
            'title' => 'Data Kandidat Lulus',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/kandidat/list_lulus', $data);
        $this->load->view('admin/kandidat/footer', $data);
    }

    public function editkandidatlulus($id = '')
    {
        is_admin();

        $data = [
            'title' => 'Data Kandidat Lulus',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'data' => $this->M_Kandidat->getById($id),
            'detail' => $this->M_Kandidat->getDetailById($id)
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/kandidat/edit_kandidat', $data);
        $this->load->view('admin/kandidat/footer', $data);
    }

    public function update_kandidat()
    {
        $id =  $this->input->post('id');
        $this->load->library('form_validation');
        $this->M_Kandidat->updateKandidat($id);
        $data['message'] = 'success';
        $this->session->set_flashdata('message', 'success');
        redirect('kandidat-lulus', 'refresh');
    }

    public function kandidat_lulus_data()
    {
        $kandidatList = $this->M_Kandidat->getKandidatLulusDataTables();

        $data = array();
        $no = isset($_POST['start']) ? $_POST['start'] : 0;

        if (count((array) $kandidatList) > 0) {
            foreach ($kandidatList as $field) {


                if ($field->status_kandidat === 'new') {
                    $label_status = 'label label-primary';
                } else {
                    $label_status = 'label label-warning';
                }

                $actions = ' <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-secondary btn-flat btn-xs dropdown-toggle" type="button">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul role="menu" class="dropdown-menu">
                 ';

                $actions .= '<li><a href="' . base_url('/edit-kandidat/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-edit"></span> Edit Data</a></li>';
                $actions .= '<li><a href="' . base_url('/export-kandidat/') . $field->id . '"  class="btn btn-default btn-sm"><span class="fa fa-file-excel"></span> &nbsp;Export Excel</a></li>';

                $actions .= '</ul></div>';

                $tanggal_lahir = ($field->tanggal_lahir !== null) ? date('d-m-Y', strtotime($field->tanggal_lahir))  : '';


                $no++;
                $row = array();
                $row['no'] = $no;
                $row['kode_kandidat'] = $field->kode_kandidat;
                $row['nama_lengkap'] = $field->nama_lengkap;
                $row['telp'] = $field->telp;
                $row['status'] =  '<span class="' . $label_status . '">' . ucwords($field->status_kandidat) . '</span>';
                $row['actions'] = $actions;
                $data[] = $row;
            }
        }

        $output = array(
            "draw" => isset($_POST['draw']) ? $_POST['draw'] : 1,
            "recordsTotal" => count((array) $kandidatList),
            "recordsFiltered" => $this->M_Kandidat->countKandidatLulusDataList(),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function exportkandidatlulus($id)
    {
        $data = $this->M_Kandidat->exportKandidat($id);

        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Data Kandidat.xls");
        echo $data;
    }

    public function pdfpegawai()
    {
        $this->load->library('dompdf_gen');
        $id = $this->input->get('id');
        $data['viewpgw'] = $this->M_Admin->getviewpegawaimbk($id);

        $this->load->view('admin/kandidat/laporan_pdf', $data);

        $paper_size = 'A4';
        $orientation = 'potrait';
        $html = $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('profil_kandidat.pdf', array('Attachment' => 0));
    }

    public function suratketkerja()
    {
        $this->load->library('dompdf_gen');
        $id = $this->input->get('id');
        $data['viewkandidat'] = $this->M_Kandidat->getviewkandidat($id);

        $this->load->view('admin/kandidat/suratkerja_pdf', $data);

        $paper_size = 'A4';
        $orientation = 'potrait';
        $html = $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('Surat_Keterangan_Kerja.pdf', array('Attachment' => 0));
    }

    public function datapegawaiAbsen()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'dpAbsen' => $this->M_Admin->fetchlistpegawaibydriverleaderid()
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/absen/absenpegawai', $data);
    }

    public function simpanAbsen()
    {
        $idPegawai = $this->input->post('idAbsen');
        $namaPegawai = substr($this->input->post('namaAbsen'), 7);
        $nik = $this->input->post('nik');
        $status = $this->input->post('opsi');
        $status_pegawai = ($this->input->post('opsi') == 'hadir') ? '1' : '0';
        $keterangan = $this->input->post('keterangan');

        $bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
        $tgl_absen = $hari[(int)date("w")] . ', ' . date("j ") . $bulan[(int)date('m')] . date(" Y");

        date_default_timezone_set('Asia/Jakarta');
        $data = array(
            'nama_pegawai' => $namaPegawai,
            'kode_pegawai' => $nik,
            'tgl_absen' => $tgl_absen,
            'jam_masuk' => date("h:i:s"),
            'status_absen' => $status,
            'status_pegawai' => $status_pegawai,
            'keterangan_absen' => $keterangan,
            'created_at' => date("Y-m-d h:i:s")
        );
        $query = $this->db->query("SELECT status_absen FROM db_absensi WHERE kode_pegawai = $nik AND DATE_FORMAT(created_at,'%Y%m%d') = " . date('Ymd'));
        if (count($query->result()) == 0) {
            $insert = $this->db->insert('db_absensi', $data);
            if ($insert) {
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil Absensi Pegawai</div>');
                redirect('dpAbsen', 'refresh');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Pegawai Sudah Absen Sebelumnya</div>');
            redirect('dpAbsen', 'refresh');
        }
    }

    public function mutasi()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'dpAbsen' => $this->M_Admin->fetchlistmutasidata()
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/mutasi/home', $data);
    }

    public function editaktifasikaryawan()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editaktifasikaryawan', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updateaktifasikaryawan()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updateaktifasikaryawan();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function editklasifikasiskill()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editklasifikasiskill', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updateklasifikasiskill()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updateklasifikasiskill();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function editkontrakkerja()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editkontrakkerja', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updatekontrakkerja()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updatekontrakkerja();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function edittrainingsafetydriving()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/edittrainingsafetydriving', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updatetrainingsafetydriving()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updatetrainingsafetydriving();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function editketeranganvaksinpegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editketeranganvaksinpegawai', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updateketeranganvaksinpegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updateketeranganvaksinpegawai();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function edittrainingpegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/edittrainingpegawai', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updatetrainingpegawai()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updatetrainingpegawai();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function editketeranganbpjs()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editketeranganbpjs', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updateketeranganbpjs()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updateketeranganbpjs();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function editaccident()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/editaccident', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updateaccident()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updateaccident();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function edittraining()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->get('id');
        $data['pgw'] = $this->M_Admin->getviewpegawaimbk($id);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('layout/datapegawai/edittraining', $data);
        $this->load->view('layout/footer', $data);
    }

    public function updatetraining()
    {
        is_admin();
        $data = [
            'title' => 'Data Pegawai',
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp
        ];
        $id = $this->input->post('id');
        //proses update data master_karyawan
        $this->M_Admin->updatetraining();
        $this->session->set_flashdata('message', '<div class="alert alert-warning" 
		role="alert">Data Pegawai has been edited!</div>');
        redirect('viewpegawaimbk?id='.$id.'');
        
    }

    public function update_appointment()
    {
             
        $this->db->set('ket_appointment', $this->input->post('ket_appointment'));
        $this->db->set('appointment', $this->input->post('appointment'));
	    $this->db->where('id', $this->input->post('id'));
	    $this->db->update('kandidat');
        
        redirect('admin/kandidat');
        
    }

    public function update_listdokument()
    {
             
        $this->db->set('ket_list_dokumen', $this->input->post('ket_list_dokumen'));
        $this->db->set('list_dokumen', $this->input->post('list_dokumen'));
	    $this->db->where('id', $this->input->post('id'));
	    $this->db->update('kandidat');
        
        redirect('admin/kandidat');
        
    }

    public function update_login_test()
    {
             
        $this->db->set('ket_login_test', $this->input->post('ket_login_test'));
        $this->db->set('login_test', $this->input->post('login_test'));
	    $this->db->where('id', $this->input->post('id'));
	    $this->db->update('kandidat');
        
        redirect('admin/kandidat');
        
    }

    public function update_lengkapi_data()
    {
             
        $this->db->set('ket_lengkapi_data', $this->input->post('ket_lengkapi_data'));
        $this->db->set('lengkapi_data', $this->input->post('lengkapi_data'));
	    $this->db->where('id', $this->input->post('id'));
	    $this->db->update('kandidat');
        
        redirect('admin/kandidat');
        
    }

    
    
}
