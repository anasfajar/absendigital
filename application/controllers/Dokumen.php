<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dokumen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->get_datasess = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->load->model('M_Front');
        $this->load->model('M_Admin');
        $this->load->model('M_Dokumen');
        $this->load->model('M_Kandidat');
        $this->get_datasetupapp = $this->M_Front->fetchsetupapp();
    }

    public function kandidat($kandidatId){
    
        is_admin();

        $dokumenList = $this->M_Dokumen->getAll();
        $kandidat = $this->M_Kandidat->getById($kandidatId);

        $dokumenKandidat = $this->M_Dokumen->getDokumenKandidat($kandidatId);
      
        $selectedDokumen = [];
        foreach($dokumenKandidat as $val){           
            $selectedDokumen[$val['dokumen_id']] = ['check'=>$val['value'], 'keterangan'=> $val['keterangan']];
        }

    
        $data = [
            'title' => 'Kelengkapan Dokumen',
            'action' => site_url('dokumen/add'),
            'user' => $this->get_datasess,
            'dataapp' => $this->get_datasetupapp,
            'dokumenList' => $dokumenList,
            'kandidat_id' => $kandidatId,
            'kode_kandidat'=> $kandidat->kode_kandidat,
            'nama_lengkap'=> $kandidat->nama_lengkap,
            'documentList'=> $dokumenList,
            'selectedDokumen'=> $selectedDokumen
        ];


        $this->load->view('layout/header', $data);
        $this->load->view('layout/navbar', $data);
        $this->load->view('layout/sidebar', $data);
        $this->load->view('admin/document/list', $data);
        $this->load->view('admin/document/footer', $data);
      

    }

   
    public function add(){

        $documentList = $this->M_Dokumen->getAll();
        $documentSelected = $this->input->post('dokumen');
        $keteranganSelected = $this->input->post('keterangan');
        $kandidatId = $this->input->post('kandidat_id');

        $kosong = "0";

        for ($i = 0; $i < count($documentList); $i++) {
            if (empty($documentSelected[$i]) && empty($keteranganSelected[$i])){
                $kosong++;
            }                
        }

        if($kosong > 0){
        $this->session->set_flashdata('error', 'Jika Dokumen tidak diceklis maka Keterangan WAJIB diisi!');
        redirect(site_url('dokumen/kandidat/').$kandidatId);   
        }else{
        $this->M_Dokumen->saveDokumenKandidat($kandidatId, $documentList, $documentSelected, $keteranganSelected);
        $this->session->set_flashdata('success', 'Data was successfully added');
        redirect(site_url('dokumen/kandidat/').$kandidatId); 
        }

   
        //$this->M_Dokumen->saveDokumenKandidat($kandidatId, $documentList, $documentSelected, $keteranganSelected);
        //$this->session->set_flashdata('success', 'Data was successfully added');
        //redirect(site_url('dokumen/kandidat/').$kandidatId);
       
        

    
    }

   


}