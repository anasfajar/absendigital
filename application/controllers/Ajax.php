<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
        $this->get_today_date2 = $hari[(int)date("w")] . ', ' . date("j ") . $bulan[(int)date('m')] . date(" Y");
        $this->load->model('M_Auth');
        $this->load->model('M_Front');
        $this->load->model('M_Settings');
        $this->load->model('M_User');
        $this->load->model('M_Admin');
        $this->load->model('M_IzinCutiSakit');
        $this->get_datasess = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->appsetting = $this->db->get_where('db_setting', ['status_setting' => 2])->row_array();
        $timezone_all = $this->appsetting;
        date_default_timezone_set($timezone_all['timezone']);
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            exit('Opss you cannot access this [Hacking Attemp].');
        }
    }

    public function clear_rememberme()
    {
        $rmbtype = $this->input->get('rmbtype');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
        ];
        $this->M_User->clearremember($rmbtype);
        echo json_encode($reponse);
    }

    //Fitur Ajax Tombol Absensi

    public function absenajax()
    {
        $clocknow = date("H:i:s");
        $today = $this->input->post('tanggal');
        $appsettings = $this->appsetting;
        $is_pulang = $this->db->get_where('db_absensi', ['tgl_absen' => $today, 'kode_pegawai' => $this->get_datasess['kode_pegawai']])->row_array();
        $absentoday = $this->M_Front->fetchdbabsen($this->get_datasess['kode_pegawai']);
        $absentoday ? $absen = true : $absen = false;

        if (strtotime($clocknow) <= strtotime($appsettings['absen_mulai'])) {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'msgabsen' => '<div class="alert alert-danger text-center" role="alert">Belum Waktunya Absen Datang</div>'
            ];
        } elseif (strtotime($clocknow) >= strtotime($appsettings['absen_mulai_to']) && strtotime($clocknow) <= strtotime($appsettings['absen_pulang']) && $absen == true) {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'msgabsen' => '<div class="alert alert-danger text-center" role="alert">Belum Waktunya Absen Pulang</div>'
            ];
        } elseif (strtotime($clocknow) >= strtotime($appsettings['absen_mulai_to']) && strtotime($clocknow) >= strtotime($appsettings['absen_pulang']) && !empty($is_pulang['jam_pulang'])) {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => false,
                'msgabsen' => '<div class="alert alert-danger text-center" role="alert">Anda Sudah Absen Pulang</div>'
            ];
        } else {
            $this->M_Front->do_absen($today, $clocknow);
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        }
        echo json_encode($reponse);
    }

    //Fitur Ajax Setelan User

    public function usersetting()
    {
        $data = [
            'user' => $this->get_datasess
        ];
        $usrsetting = $this->input->get('type');
        if ($usrsetting == 'chgpwd') {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => False,
                'messages' => []
            ];
            $validation = [
                [
                    'field' => 'pass_lama',
                    'label' => 'Password Lama',
                    'rules' => 'trim|required|xss_clean|min_length[8]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.', 'min_length' => 'Password terlalu pendek, Minimal 8 Karakter!']
                ],
                [
                    'field' => 'pass_baru',
                    'label' => 'Password Baru',
                    'rules' => 'required|xss_clean|min_length[8]|matches[pass_baru_confirm]',
                    'errors' => [
                        'required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.', 'matches' => 'Password tidak sama!',
                        'min_length' => 'Password terlalu pendek!'
                    ]
                ],
                [
                    'field' => 'pass_baru_confirm',
                    'label' => 'Konfirmasi Password Baru',
                    'rules' => 'trim|required|xss_clean|min_length[8]|matches[pass_baru]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ]
            ];
            $this->form_validation->set_rules($validation);
            $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
            if ($this->form_validation->run() == FALSE) {
                foreach ($_POST as $key => $value) {
                    $reponse['messages'][$key] = form_error($key);
                }
            } else {
                $current_password = $this->input->post('pass_lama');
                $new_password = $this->input->post('pass_baru');
                if (!password_verify($current_password, $data['user']['password'])) {
                    $reponse = [
                        'csrfName' => $this->security->get_csrf_token_name(),
                        'csrfHash' => $this->security->get_csrf_hash(),
                        'success' => False,
                        'infopass' => '<div class="alert alert-danger text-center" role="alert">Password lama salah</div>'
                    ];
                } else {
                    if ($current_password == $new_password) {
                        $reponse = [
                            'csrfName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => False,
                            'infopass' => '<div class="alert alert-danger text-center" role="alert">Password baru tidak boleh sama dengan password lama</div>'
                        ];
                    } else {
                        // password sudah benar
                        $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                        $this->db->set('password', $password_hash); //set password yang sudah di hash ke database
                        $this->db->where('username', $this->session->userdata('username')); // mengambil data dari session
                        $this->db->update('user');
                        $reponse = [
                            'csrfName' => $this->security->get_csrf_token_name(),
                            'csrfHash' => $this->security->get_csrf_hash(),
                            'success' => true
                        ];
                    }
                }
            }
        } elseif ($usrsetting == 'basic') {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => False,
                'messages' => []
            ];
            $validation = [
                [
                    'field' => 'district_pegawai',
                    'label' => 'District Pegawai',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'area_pegawai',
                    'label' => 'area Pegawai',
                    'rules' => 'trim|xss_clean',
                    'errors' => ['xss_clean' => 'Please check your form on %s.']
                ]
            ];
            $this->form_validation->set_rules($validation);
            $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
            if ($this->form_validation->run() == FALSE) {
                foreach ($_POST as $key => $value) {
                    $reponse['messages'][$key] = form_error($key);
                }
            } else {
                $this->M_User->user_setting($usrsetting);
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true
                ];
            }
        }
        echo json_encode($reponse);
    }

    //Fitur Ajax Logout
    public function logoutajax()
    {
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
        ];
        $this->M_Auth->do_logout();
        echo json_encode($reponse);
    }

    //Fitur CRUD Absensi
    public function dataabs()
    {
        $typesend = $this->input->get('type');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash()
        ];
        if ($typesend == 'delabs') {
            $this->M_Front->crudabs($typesend);
        } elseif ($typesend == 'viewabs') {
            $data = [
                'dataabsensi' => $this->db->get_where('db_absensi', ['id_absen' => $this->input->post('absen_id')])->row_array(),
                'dataapp' => $this->appsetting
            ];
            $html = $this->load->view('layout/dataabsensi/viewabsensi', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        } elseif ($typesend == 'delallabs') {
            $this->M_Front->crudabs($typesend);
        }
        echo json_encode($reponse);
    }

    //Fitur CRUD Data Pegawai

    public function datapgw()
    {
        $typesend = $this->input->get('type');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash()
        ];
        if ($typesend == 'addpgw') {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => False,
                'messages' => []
            ];
            $validation = [
                [
                    'field' => 'nama_pegawai',
                    'label' => 'Nama Pegawai',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'username_pegawai',
                    'label' => 'Username',
                    'rules' => 'trim|required|xss_clean|is_unique[user.username]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.', 'is_unique' => 'Username ini telah terdaftar didatabase!']
                ],
                [
                    'field' => 'password_pegawai',
                    'label' => 'Password',
                    'rules' => 'trim|required|xss_clean|min_length[8]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.', 'max_length' => 'Password terlalu pendek, Minimal 8 Karakter!']
                ],
                [
                    'field' => 'jabatan_pegawai',
                    'label' => 'Jabatan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'area_pegawai',
                    'label' => 'area',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'district_pegawai',
                    'label' => 'District Pegawai',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'tempat_lahir_pegawai',
                    'label' => 'Tempat Lahir',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'role_pegawai',
                    'label' => 'Role Pegawai',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'tgl_lahir_pegawai',
                    'label' => 'Tanggal Lahir',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'jenis_kelamin_pegawai',
                    'label' => 'Jenis Kelamin',
                    'rules' => 'required|xss_clean|in_list[Laki - Laki,Perempuan]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'shift_pegawai',
                    'label' => 'Bagian Shift',
                    'rules' => 'required|xss_clean|in_list[1,2,3]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'verifikasi_pegawai',
                    'label' => 'Verifikasi Pegawai',
                    'rules' => 'required|xss_clean|in_list[0,1]',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
            ];
            $this->form_validation->set_rules($validation);
            if ($this->form_validation->run() == FALSE) {
                $reponse['messages'] = '<div class="alert alert-danger" role="alert">' . validation_errors() . '</div>';
            } else {
                $this->M_Admin->crudpgw($typesend);
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true
                ];
            }
        } elseif ($typesend == 'delpgw') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'message' => [],
                'success' => false
            ];
            if ($this->get_datasess['role_id'] != 1 || $check_admin->num_rows() < 1) {
                $reponse['message'] = 'Hanya admin yang boleh menghapus user!';
            } else {
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'message' => 'Anda telah menghapus user!',
                    'success' => true
                ];
                $this->M_Admin->crudpgw($typesend);
            }
        } elseif ($typesend == 'actpgw') {
            if ($this->db->get_where('user', ['id_pegawai' => $this->input->post('pgw_id'), 'is_active' => 1])->row_array()) {
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => false
                ];
            } else {
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true
                ];
                $this->M_Admin->crudpgw($typesend);
            }
        } elseif ($typesend == 'viewpgw') {
            $data['datapegawai'] =  $this->db->get_where('master_karyawan', ['id' => $this->input->post('pgw_id')])->row_array();
            $html = $this->load->view('layout/datapegawai/viewpegawai', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        } elseif ($typesend == 'edtpgw') {
            $data['dataapp'] = $this->appsetting;
            $data['datapegawai'] =  $this->db->get_where('master_karyawan', ['id' => $this->input->post('pgw_id')])->row_array();
            $html = $this->load->view('layout/datapegawai/editpegawai', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash()
            ];
        }
        echo json_encode($reponse);
    }

    public function getKaryawan()
    {
        $data = $this->db->get('ms_karyawan')->result_array();
        echo json_encode($data);
    }

    public function editpgwbc()
    {
        $typesend = $this->input->get('type');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
            'success' => False,
            'messages' => []
        ];

        $validation = [
            [
                'field' => 'nama_kary',
                'label' => 'Nama Karyawan',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'nik_kary',
                'label' => 'NIK Karyawan',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'jenis_pengajuan',
                'label' => 'Jenis Pengajuan',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'ket',
                'label' => 'Keterangan',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'no_hp',
                'label' => 'No Handphone',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'tgl_awal',
                'label' => 'Tanggal Awal',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'tgl_akhir',
                'label' => 'Tanggal Akhir',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'jum_hari',
                'label' => 'Jumlah Hari',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
        ];
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $reponse['messages'] = '<div class="alert alert-danger" role="alert">' . validation_errors() . '</div>';
        } else {
            $this->M_IzinCutiSakit->crudizincutisakit($typesend);
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        }
        echo json_encode($reponse);
    }

    // Fitur CRUD Data Pengajuan Izin
    public function dataizincutisakit()
    {
        $typesend = $this->input->get('type');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash()
        ];
        if ($typesend == 'addpengajuan') {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => False,
                'messages' => []
            ];
            $validation = [
                [
                    'field' => 'nama_kary',
                    'label' => 'Nama Karyawan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'nik_kary',
                    'label' => 'NIK Karyawan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'jenis_pengajuan',
                    'label' => 'Jenis Pengajuan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'no_hp',
                    'label' => 'No Handphone',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'alamat',
                    'label' => 'Alamat',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'tgl_awal',
                    'label' => 'Tanggal Awal',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'tgl_akhir',
                    'label' => 'Tanggal Akhir',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'jum_hari',
                    'label' => 'Jumlah Hari',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
            ];

            if ($this->input->post('jenis_pengajuan') == 'sakit') {
                $validation[] =                 [
                    'field' => 'skd',
                    'label' => 'Surat Keterangan',
                    'rules' => 'required',
                    'errors' => ['required' => 'You must provide a %s.']
                ];
            }
            $this->form_validation->set_rules($validation);
            if ($this->form_validation->run() == FALSE) {
                $reponse['messages'] = '<div class="alert alert-danger" role="alert">' . validation_errors() . '</div>';
            } else {
                $this->M_IzinCutiSakit->crudizincutisakit($typesend);
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true
                ];
            }
        } elseif ($typesend == 'approvepengajuan') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'message' => [],
                'success' => false
            ];
            if ($this->get_datasess['role_id'] != 1 || $check_admin->num_rows() < 1) {
                $reponse['message'] = 'Hanya admin yang boleh menyetujui pengajuan!';
            } else {
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'message' => 'Berhasil menyetujui pengajuan!',
                    'success' => true
                ];
                $this->M_IzinCutiSakit->crudizincutisakit($typesend);
            }
        } elseif ($typesend == 'rejectpengajuan') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'message' => [],
                'success' => false
            ];
            if ($this->get_datasess['role_id'] != 1 || $check_admin->num_rows() < 1) {
                $reponse['message'] = 'Hanya admin yang boleh menolak pengajuan!';
            } else {
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'message' => 'Berhasil menolak pengajuan!',
                    'success' => true
                ];
                $this->M_IzinCutiSakit->crudizincutisakit($typesend);
            }
        } elseif ($typesend == 'cancelpengajuan') {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'message' => 'Berhasil Membatalkan pengajuan!',
                'success' => true
            ];
            $this->M_IzinCutiSakit->crudizincutisakit($typesend);
        } elseif ($typesend == 'editpengajuan') {
            $data['data'] =  $this->db->get_where('tx_ijincutisakit', ['id' => $this->input->post('pengajuan_id')])->row_array();
            $html = $this->load->view('pengajuan_izin/edit_pengajuan_izin', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash()
            ];
        } elseif ($typesend == 'viewpengajuan') {
            $data['data'] =  $this->db->get_where('tx_ijincutisakit', ['id' => $this->input->post('pengajuan_id')])->row_array();
            $html = $this->load->view('pengajuan_izin/view_pengajuan_izin', $data);
            $reponse = [
                'html' => $html,
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash()
            ];
        } elseif ($typesend == 'ubahpengajuan') {
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => False,
                'messages' => []
            ];
            $validation = [
                [
                    'field' => 'nama_kary',
                    'label' => 'Nama Karyawan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'nik_kary',
                    'label' => 'NIK Karyawan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'jenis_pengajuan',
                    'label' => 'Jenis Pengajuan',
                    'rules' => 'trim|required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'ket',
                    'label' => 'Keterangan',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'no_hp',
                    'label' => 'No Handphone',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'alamat',
                    'label' => 'Alamat',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'tgl_awal',
                    'label' => 'Tanggal Awal',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'tgl_akhir',
                    'label' => 'Tanggal Akhir',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
                [
                    'field' => 'jum_hari',
                    'label' => 'Jumlah Hari',
                    'rules' => 'required|xss_clean',
                    'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
                ],
            ];

            if ($this->input->post('jenis_pengajuan') == 'sakit') {

                if (!$this->input->post('dok_surat_lama')) {
                    $validation[] =                 [
                        'field' => 'skd',
                        'label' => 'Surat Keterangan',
                        'rules' => 'required',
                        'errors' => ['required' => 'You must provide a %s.']
                    ];
                }
            }
            $this->form_validation->set_rules($validation);
            if ($this->form_validation->run() == FALSE) {
                $reponse['messages'] = '<div class="alert alert-danger" role="alert">' . validation_errors() . '</div>';
            } else {
                $this->M_IzinCutiSakit->crudizincutisakit($typesend);
                $reponse = [
                    'csrfName' => $this->security->get_csrf_token_name(),
                    'csrfHash' => $this->security->get_csrf_hash(),
                    'success' => true
                ];
            }
        }
        echo json_encode($reponse);
    }

    function get_jumlah_cuti()
    {
        $nik = $this->input->get('nik');

        $jumlah_cuti = $this->db->query("SELECT cuti FROM ms_karyawan WHERE nik = '$nik'")->result();
        $sudah_cuti = $this->db->query("SELECT SUM(jum_hari) as jumlah_cuti FROM tx_ijincutisakit WHERE YEAR(created_at) = YEAR(curdate()) AND nik_kary = '$nik'")->result();
        $data = [
            'jumlah_cuti' => $jumlah_cuti,
            'sudah_cuti' => $sudah_cuti
        ];

        echo json_encode($data);
    }

    //Fitur Ajax Tabel Absensi

    function get_datatbl()
    { //data absen by JSON object
        $dataabsen = $this->input->get('type');
        $datapegawai = $this->get_datasess;
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
        $nowday = $hari[(int)date("w")] . ', ' . date("j ") . $bulan[(int)date('m')] . date(" Y");
        $enddate = new DateTime(date('Y-m-d'));
        $data = [];
        $no = 1;
        if ($dataabsen == 'datapgw') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $query = $this->db->get("master_karyawan");
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    //'<img class="img-thumbnail" src="'. ($r->image == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('storage/profile/' . //$r->image)) . '" class="card-img" style="width: 100%;">',
                    $r->vendor,
                    $r->jabatan,
                    '<div class="btn-group btn-small " style="text-align: right;">
                   <a href="' . base_url('viewpegawaimbk') . '?id=' . $r->id . '" class="btn btn-primary"><span class="fas fa-fw fa-address-card"></span></a>
                   <a href="' . base_url('lap_profilpegawai') . '?id=' . $r->id . '" target="_blank" class="btn btn-warning"><span class="fas fa-fw fa-file"></span></a>
                  </div>'

                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        } elseif ($dataabsen == 'dataslip') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $id_dok = $this->session->userdata('idDok');
            $query = $this->db->get_where('tmp_rekapabsen', ['dokumen_id' => $id_dok]);
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    //'<img class="img-thumbnail" src="'. ($r->image == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('storage/profile/' . //$r->image)) . '" class="card-img" style="width: 100%;">',
                    $r->area,
                    "Rp " . number_format($r->netto_sistem,0,',','.'),
                    //$r->netto,
                    '<div class="btn-group btn-small " style="text-align: right;">
                   <button id="detailslipgaji" data-slipgaji-id="' . $r->id_slipgaji . '" class="btn btn-primary" title="Lihat Data"><span class="fas fa-fw fa-address-card"></span></button>
                   <a href="' . base_url('viewdetailslipkaryawan') . '?id=' . $r->id_slipgaji . '" class="btn btn-warning" title="Edit Data"><span class="fas fa-fw fa-edit"></span></a>
                   <a href="'.base_url('createslip'). '?id='.$r->id_slipgaji.'" target="_blank" class="btn btn-warning" title="Cetak Slip"><span class="fas fa-fw fa-file"></span></a>
                  </div>'

                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        }elseif ($dataabsen == 'rekapdataslip') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $query = $this->db->query("SELECT * FROM dokumen_upload_gaji WHERE status != 2 order by id DESC");
            foreach ($query->result() as $r) {
                if($r->status == 1){
                    $btnApp = " ";
                    $btnHapus=" ";
                }else{
                    $btnApp = '<a href="'. base_url('approve_dokumen') .'?id=' . $r->dokumen_id . '" class="btn btn-success"><span class="fas fa-fw fa-check"></span> Approve</a>';
                    $btnHapus = '<a href="'. base_url('hapus_dokumen') .'?id=' . $r->dokumen_id . '" class="btn btn-danger"><span class="fas fa-fw fa-trash"></span> Hapus</a>';
                }
                $data[] = [
                    $no++,
                    $r->nama_dokumen,
                    $r->nama_upload,
                    $r->district,
                    //'<img class="img-thumbnail" src="'. ($r->image == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('storage/profile/' . //$r->image)) . '" class="card-img" style="width: 100%;">',
                    $r->tahun,
                    $r->bulan,
                    ($r->status == 0) ? '<span class="badge badge-pill badge-danger">Belum Approve</span>' : (($r->status == 1) ? '<span class="badge badge-pill badge-success">Approved</span>' : 'b'),
                    '<div class="btn-group btn-small " style="text-align: right;">
                    '.$btnApp.'&nbsp
                   <a href="'. base_url('list_gaji') .'?id=' . $r->dokumen_id . '" class="btn btn-warning"><span class="fas fa-fw fa-file"></span> View List</a>&nbsp
                   <a href="'.base_url('exportslip').'?id=' . $r->dokumen_id . '" class="btn btn-primary"><span class="fas fa-fw fa-file-export"></span> Export</a>&nbsp
                   '.$btnHapus.'
                  </div>'
                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        }elseif ($dataabsen == 'dataizincutisakit') {
            $nik = $this->input->get('nik');
            $role = $this->input->get('role');
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            if ($role == 3) {
                $query = $this->db->get_where('tx_ijincutisakit', ['nik_kary =' => $nik]);
            } else {
                $query = $this->db->get_where('tx_ijincutisakit', ['f_approve !=' => 3]);
            }

            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->nik_kary,
                    $r->nama_kary,
                    $r->jenis_pengajuan,
                    $r->jum_hari,
                    date('d M Y', strtotime($r->tgl_awal)),
                    date('d M Y', strtotime($r->tgl_akhir)),
                    ($r->f_approve == 0) ? '<span class="badge badge-warning ml-1">Not Approved</span>' : (($r->f_approve == 1) ? '<span class="badge badge-success">Approved</span>' : (($r->f_approve == 2) ? '<span class="badge badge-danger">Rejected</span>' : '<span class="badge badge-danger">Canceled</span>')),
                    ($this->session->userdata('role_id') == 1 && $r->f_approve == 0)
                        ? '<div class="btn-group btn-small" style="text-align: right;">
                        <button class="btn btn-success btn-sm approve-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Setujui Pengajuan"><span class="fas fa-fw fa-check-circle"></span></button>
                        <button class="btn btn-danger btn-sm reject-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Tolak Pengajuan"><span class="fas fa-fw fa-times-circle"></span></button>
                        <button class="btn btn-primary btn-sm view-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Preview Pengajuan"><span class="fas fa-fw fa-file-alt"></span></button>
                    </div>'
                        : (($this->session->userdata('role_id') == 2 || $this->session->userdata('role_id') == 3 && $r->f_approve == 0)
                            ? '<div class="btn-group btn-small" style="text-align: right;">
                        <button class="btn btn-danger btn-sm cancel-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Batalkan Pengajuan"><span class="fas fa-fw fa-times"></span></button>
                        <button class="btn btn-info btn-sm edit-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Edit Pengajuan"><span class="fas fa-fw fa-edit"></span></button>
                        <button class="btn btn-primary btn-sm view-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Preview Pengajuan"><span class="fas fa-fw fa-file-alt"></span></button>
                    </div>'
                            : '<div class="btn-group btn-small" style="text-align: right;">
                        <button class="btn btn-primary btn-sm view-pengajuan-izin" data-pengajuan-izin-id="' . $r->id . '" title="Preview Pengajuan"><span class="fas fa-fw fa-file-alt"></span></button>
                    </div>'),

                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        } elseif ($dataabsen == 'all') {
            if ($this->session->userdata('role_id') == 1) {
                $query = $this->db->get("db_absensi");
                foreach ($query->result() as $r) {
                    $data[] = [
                        $no++,
                        $r->tgl_absen,
                        $r->nama_pegawai,
                        $r->jam_masuk,
                        $r->jam_pulang,
                        (empty($r->status_pegawai)) ? '<span class="badge badge-primary">Belum Absen</span>' : (($r->status_pegawai == 1) ? '<span class="badge badge-success">Sudah Absen</span>' : '<span class="badge badge-danger">Absen Terlambat</span>'),
                        '<div class="btn-group btn-small " style="text-align: right;">
                    <button class="btn btn-primary detail-absen" data-absen-id="' . $r->id_absen . '" title="Lihat Absensi"><span class="fas fa-fw fa-address-card"></span></button>
                    <button class="btn btn-danger delete-absen" title="Hapus Absensi" data-absen-id="' . $r->id_absen . '"><span class="fas fa-trash"></span></button>
                    <button class="btn btn-warning print-absen" title="Cetak Absensi" data-absen-id="' . $r->id_absen . '" data-toggle="modal" data-target="#printabsensimodal"><span class="fas fa-print"></span></button>
                    </div>'
                    ];
                }
            } elseif ($this->session->userdata('role_id') == 2) {
                $query = $this->db->get("db_absensi");
                foreach ($query->result() as $r) {
                    $data[] = [
                        $no++,
                        $r->tgl_absen,
                        $r->nama_pegawai,
                        $r->jam_masuk,
                        $r->jam_pulang,
                        (empty($r->status_pegawai)) ? '<span class="badge badge-primary">Belum Absen</span>' : (($r->status_pegawai == 1) ? '<span class="badge badge-success">Sudah Absen</span>' : '<span class="badge badge-danger">Absen Terlambat</span>'),
                        '<div class="btn-group btn-small " style="text-align: right;">
                    <button class="btn btn-primary detail-absen" data-absen-id="' . $r->id_absen . '" title="Lihat Absensi"><span class="fas fa-fw fa-address-card"></span></button>
                    <button class="btn btn-warning print-absen" title="Cetak Absensi" data-absen-id="' . $r->id_absen . '" data-toggle="modal" data-target="#printabsensimodal"><span class="fas fa-print"></span></button>
                    </div>'
                    ];
                }
            }
        } elseif ($dataabsen == 'allself') {
            $query = $this->db->get_where("db_absensi", ['kode_pegawai' => $datapegawai['kode_pegawai']]);
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->tgl_absen,
                    $r->nama_pegawai,
                    $r->jam_masuk,
                    $r->jam_pulang,
                    (empty($r->status_pegawai)) ? '<span class="badge badge-primary">Belum Absen</span>' : (($r->status_pegawai == 1) ? '<span class="badge badge-success">Sudah Absen</span>' : '<span class="badge badge-danger">Absen Terlambat</span>'),
                    '<div class="btn-group btn-small " style="text-align: right;">
                    <button class="btn btn-primary detail-absen" data-absen-id="' . $r->id_absen . '" title="Lihat Absensi"><span class="fas fa-fw fa-address-card"></span></button>
                    </div>'
                ];
            }
        } elseif ($dataabsen == 'getallmsk') {
            $query = $this->db->get_where("db_absensi", ['tgl_absen' => $nowday, 'status_pegawai' => 1]);
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->jam_masuk,
                    $r->nama_pegawai,
                    (empty($r->status_pegawai)) ? '<span class="badge badge-primary">Belum Absen</span>' : (($r->status_pegawai == 1) ? '<span class="badge badge-success">Sudah Absen</span>' : '<span class="badge badge-danger">Absen Terlambat</span>')
                ];
            }
        } elseif ($dataabsen == 'getalltrl') {
            $query = $this->db->get_where("db_absensi", ['tgl_absen' => $nowday, 'status_pegawai' => 2]);
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->jam_masuk,
                    $r->nama_pegawai,
                    (empty($r->status_pegawai)) ? '<span class="badge badge-primary">Belum Absen</span>' : (($r->status_pegawai == 1) ? '<span class="badge badge-success">Sudah Absen</span>' : '<span class="badge badge-danger">Absen Terlambat</span>')
                ];
            }
        } elseif ($dataabsen == 'getsimhabis') {
            $this->db->select("nik,nama,datediff(masa_berlaku_sim,now()) as selisih", FALSE);
            $this->db->from('master_karyawan');
            $this->db->where("DATEDIFF(masa_berlaku_sim,now()) <= 30");
            $this->db->order_by("3", "ASC");
            $query = $this->db->get();
            //$query = $this->db->get_where("master_karyawan", ['masa_berlaku_sim' => "2022-08-17"]);
            foreach ($query->result() as $r) {
                $pesan = (($r->selisih < 0) ? 'Lewat ' . $r->selisih * -1 : 'Sisa ' . $r->selisih);
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    '<span class="badge badge-danger">' . $pesan . ' Hari </span>'
                ];
            }
        } elseif ($dataabsen == 'getkontrakhabis') {
            $this->db->select("nik,nama,datediff(finish_kontrak,now()) as selisih", FALSE);
            $this->db->from('master_karyawan');
            $this->db->where("DATEDIFF(finish_kontrak,now()) <= 30");
            $this->db->order_by("3", "ASC");
            $query = $this->db->get();
            //$query = $this->db->get_where("master_karyawan", ['finish_kontrak' => "2022-08-17"]);
            foreach ($query->result() as $r) {
                $pesan = (($r->selisih < 0) ? 'Lewat ' . $r->selisih * -1 : 'Sisa ' . $r->selisih);
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    '<span class="badge badge-danger">' . $pesan . ' Hari </span>'
                ];
            }
        } elseif ($dataabsen == 'datapgwfico') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $op = $this->input->userdata('op');
            $cs = $this->input->userdata('cs');
            $query = $this->db->get_where("master_karyawan", array('operation_poin' => $op, 'customer' => $cs));
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    $r->vendor,
                    $r->jabatan,
                    '<a href="' . base_url('viewpegawaimbk') . '?id=' . $r->id . '" class="btn btn-primary"><span class="fas fa-fw fa-address-card"></span></a>'
                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        } elseif ($dataabsen == 'datapgwbydriverleader') {
            $dlid = $this->session->userdata('id');
            $query = $this->db->get_where("master_karyawan", array('driver_leader_id' => $dlid));
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    $r->nik,
                    $r->nama,
                    $r->vendor,
                    $r->jabatan,
                    '<a href="' . base_url('viewpegawaimbk') . '?id=' . $r->id . '" class="btn btn-primary"><span class="fas fa-fw fa-address-card"></span></a>'
                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        } elseif ($dataabsen == 'dataKecelakaan') {
            $check_admin = $this->db->get_where('user', ['role_id' => 1]);
            $query = $this->db->get("kecelakaan");
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    date_indo($r->tanggal),
                    $r->driver,
                    $r->no_polisi,
                    '<a href="' . base_url('detailkecelakaan') . '?id=' . $r->id . '" class="btn btn-primary" title="Detail Kecelakaan"><span class="fas fa-fw fa-tv"></span></a>    <a href="' . base_url('cetakkecelakaan') . '?id=' . $r->id . '" class="btn btn-danger" title="Cetak Data Kecelakaan" target="_blank"><span class="fas fa-fw fa-file-pdf"></span></a>',
                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        } elseif ($dataabsen == 'getslipgaji') {
            // $query = $this->db->where("tmp_rekapabsen", ['nik' => $this->session->userdata('nik')])->order_by('id_slipgaji','ASC');
            // var_dump('test');die();
            $this->db->select("a.bulan,a.tahun,b.nik,b.nama,b.netto_sistem,b.id_slipgaji");
            $this->db->from('dokumen_upload_gaji a');
            $this->db->join('tmp_rekapabsen b', 'a.dokumen_id = b.dokumen_id','left');
            $this->db->where("b.nik",$this->session->userdata('nik'));
            $this->db->where("a.status",1);
            $this->db->order_by("b.id_slipgaji", "ASC");
            $query = $this->db->get();
            foreach ($query->result() as $r) {
                $data[] = [
                    $no++,
                    // $r->nik,
                    // $r->nama,
                    $r->bulan.'-'.$r->tahun,
                    "Rp " . number_format($r->netto_sistem,0,',','.'),
                   '<a href="'.base_url('cetak_slip'). '?id='.$r->id_slipgaji.'" target="_blank" class="btn btn-success" title="Cetak Slip"><span class="fas fa-fw fa-file"></span>Unduh Slip</a>'
                ];
            }
            $result = array(
                "draw" => $draw,
                "recordsTotal" => $query->num_rows(),
                "recordsFiltered" => $query->num_rows(),
                "data" => $data
            );
        }
        $result = array(
            "draw" => $draw,
            "recordsTotal" => $query->num_rows(),
            "recordsFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
    }

    // Fitur AJAX Settings Aplikasi

    public function initsettingapp()
    {
        $typeinit = $this->input->get('type');
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
        ];
        $this->M_Settings->init_setting($typeinit);
        echo json_encode($reponse);
    }

    public function savingsettingapp()
    {
        $reponse = [
            'csrfName' => $this->security->get_csrf_token_name(),
            'csrfHash' => $this->security->get_csrf_hash(),
            'success' => False,
            'messages' => []
        ];
        $validation = [
            [
                'field' => 'nama_instansi',
                'label' => 'Nama Instansi',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'pesan_jumbotron',
                'label' => 'Pesan Jumbotron',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'nama_app_absen',
                'label' => 'Nama Aplikasi Absen',
                'rules' => 'trim|required|xss_clean|max_length[20]',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.', 'max_length' => 'Nama aplikasi terlalu panjang, Max Karakter 20!']
            ],
            [
                'field' => 'timezone_absen',
                'label' => 'Zona Waktu Absen',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'absen_mulai',
                'label' => 'Absen Mulai',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'absen_sampai',
                'label' => 'Batas Absen Masuk',
                'rules' => 'required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ],
            [
                'field' => 'absen_pulang_sampai',
                'label' => 'Absen Pulang',
                'rules' => 'trim|required|xss_clean',
                'errors' => ['required' => 'You must provide a %s.', 'xss_clean' => 'Please check your form on %s.']
            ]
        ];
        $this->form_validation->set_rules($validation);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if ($this->form_validation->run() == FALSE) {
            foreach ($_POST as $key => $value) {
                $reponse['messages'][$key] = form_error($key);
            }
        } else {
            $this->M_Settings->update_setting();
            $reponse = [
                'csrfName' => $this->security->get_csrf_token_name(),
                'csrfHash' => $this->security->get_csrf_hash(),
                'success' => true
            ];
        }
        echo json_encode($reponse);
    }

    function get_customer()
    {
        $op = $this->input->get('op');

        $customer = $this->db->query("SELECT distinct customer FROM master_karyawan WHERE operation_poin = '$op'")->result();
        if ($customer) {
            echo "[";
            $i = 0;
            foreach ($customer as $data) {
                if ($i > 0) {
                    echo ",\r\n";
                }
                echo "{\"id\"  : \"$i\", \"text\":\"$data->customer\"}";
                $i++;
            }

            echo "]";
        } else {
            echo "Data Kosong!";
        }
    }
}
