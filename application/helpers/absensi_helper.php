<?php

function is_logged_in()
{
    $ci = get_instance();
    $ci->load->model('M_Auth');
    $username = $ci->session->userdata('username');
    if (!$ci->session->userdata('logged_in')) {
        if (get_cookie('absensi_rememberme')) {
            $ci->M_Auth->check_rememberme();
        } else {
            redirect('login');
        }
    } elseif (empty($ci->db->get_where('user', ['username' => $username])->row_array())) {
        $ci->session->sess_destroy();
        redirect('login');
    }
}

function rememberme_check()
{
    $ci = get_instance();
    $ci->load->model('M_Auth');
    if (get_cookie('absensi_rememberme')) {
        $ci->M_Auth->check_rememberme();
    }
}


function is_admin()
{
    $ci = get_instance();
    $role_id = $ci->session->userdata('role_id');

    if ($role_id != 1 && $role_id != 4) {
        redirect('block');
    }
}

function is_moderator()
{
    $ci = get_instance();
    $role_id = $ci->session->userdata('role_id');

    if ($role_id != 1 && $role_id != 2) {
        redirect('block');
    }
}

function is_pegawai()
{
    $ci = get_instance();
    $ci->load->model('M_Auth');
    $username = $ci->session->userdata('nik');
    if (!$ci->session->userdata('logged_in')) {
        if (get_cookie('absensi_rememberme')) {
            $ci->M_Auth->check_rememberme();
        } else {
            redirect('login');
        }
    } elseif (empty($ci->db->get_where('login_rekapgaji', ['nik' => $username])->row_array())) {
        $ci->session->sess_destroy();
        redirect('login');
    }
}

if (!function_exists('time_indo')) {
    function time_indo($fulldate)
    {
        $time = substr($fulldate, 11, 5);
        return $time . ' WIB';
    }
}


if (!function_exists('date_indo')) {
    function date_indo($fulldate)
    {
        $date = substr($fulldate, 8, 2);
        $month = get_month(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        $time = substr($fulldate, 11, 8);
        return $date . ' ' . $month . ' ' . $year . ' ' . $time;
    }
}

if (!function_exists('date_indonesia')) {
    function date_indonesia($fulldate)
    {
        $date = substr($fulldate, 8, 2);
        $month = get_month(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        return $date . ' ' . $month . ' ' . $year;
    }
}

if (!function_exists('date_simple')) {
    function date_simple($fulldate)
    {
        $date = substr($fulldate, 8, 2);
        $month = substr($fulldate, 5, 2);
        $year = substr($fulldate, 0, 4);
        return $date . '-' . $month . '-' . $year;
    }
}

if (!function_exists('date_normal')) {
    function date_normal($fulldate)
    {
        $date = substr($fulldate, 8, 2);
        $month = get_month3(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        $time = substr($fulldate, 11, 8);
        return $date . '/' . $month . '/' . $year . ' ' . $time;
    }
}

if (!function_exists('date_time')) {
    function date_time($fulldate)
    {
        $date = substr($fulldate, 0, 2);
        $month = get_month2(substr($fulldate, 3, 3));
        $year = substr($fulldate, 7, 4);
        $time = substr($fulldate, 12, 5);
        return $year . '-' . $month . '-' . $date . ' ' . $time;
    }
}

if (!function_exists('mysql_date')) {
    function mysql_date($fulldate)
    {
        $date = substr($fulldate, 0, 2);
        $month = substr($fulldate, 3, 2);
        $year = substr($fulldate, 6, 4);
        $time = substr($fulldate, 11, 8);
        return $year . '-' . $month . '-' . $date . ' ' . $time;
    }
}

if (!function_exists('month_romawi')) {
    function month_romawi($month)
    {
        switch ($month) {
            case 1:
                return "I";
            case 2:
                return "II";
            case 3:
                return "III";
            case 4:
                return "IV";
            case 5:
                return "V";
            case 6:
                return "VI";
            case 7:
                return "VII";
            case 8:
                return "VIII";
            case 9:
                return "IX";
            case 10:
                return "X";
            case 11:
                return "XI";
            case 12:
                return "XII";
        }
    }
}

if (!function_exists('get_month')) {
    function get_month($month)
    {
        switch ($month) {
            case 1:
                return "Januari";
            case 2:
                return "Februari";
            case 3:
                return "Maret";
            case 4:
                return "April";
            case 5:
                return "Mei";
            case 6:
                return "Juni";
            case 7:
                return "Juli";
            case 8:
                return "Agustus";
            case 9:
                return "September";
            case 10:
                return "Oktober";
            case 11:
                return "November";
            case 12:
                return "Desember";
        }
    }
}

if (!function_exists('get_month2')) {
    function get_month2($month)
    {
        switch ($month) {
            case "Jan":
                return "01";
            case "Feb":
                return "02";
            case "Mar":
                return "03";
            case "Apr":
                return "04";
            case "May":
                return "05";
            case "Jun":
                return "06";
            case "Jul":
                return "07";
            case "Aug":
                return "08";
            case "Sep":
                return "09";
            case "Oct":
                return "10";
            case "Nov":
                return "11";
            case "Dec":
                return "12";
        }
    }
}

if (!function_exists('get_month3')) {
    function get_month3($month)
    {
        switch ($month) {
            case "01":
                return "Jan";
            case "02":
                return "Feb";
            case "03":
                return "Mar";
            case "04":
                return "Apr";
            case "05":
                return "May";
            case "06":
                return "Jun";
            case "07":
                return "Jul";
            case "08":
                return "Aug";
            case "09":
                return "Sep";
            case "10":
                return "Oct";
            case "11":
                return "Nov";
            case "12":
                return "Dec";
        }
    }
}


if (!function_exists('terbilang')) {
    function terbilang($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = terbilang($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = terbilang($nilai / 10) . " puluh" . terbilang($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . terbilang($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = terbilang($nilai / 100) . " ratus" . terbilang($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . terbilang($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = terbilang($nilai / 1000) . " ribu" . terbilang($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = terbilang($nilai / 1000000) . " juta" . terbilang($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = terbilang($nilai / 1000000000) . " milyar" . terbilang(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = terbilang($nilai / 1000000000000) . " trilyun" . terbilang(fmod($nilai, 1000000000000));
        }
        return $temp;
    }
}
