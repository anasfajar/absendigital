<div class="container-fluid">
    <div class="my-4 d-sm-flex align-items-center justify-content-between">
        <h1>Daftar Pembayaran : </h1>
        <!-- <div class="btn btn-primary" id="sync-data-dashboard"><span class="fas fa-sync-alt mr-1"></span>Refresh Data</div> -->
    </div>
    <h5>Nama Lengkap : <?php echo $user['nama_lengkap']?></h5>
    <h5>NIK : <?php echo $user['nik']?></h5>
    <div class="card mb-4">
        <div class="card-header">
            <div class="float-right">
                <div class="btn btn-primary" id="refresh-tabel-pegawai"><span class="fas fa-sync-alt mr-1"></span>Refresh Data</div>
                <!-- <a href="<?= base_url('importslip'); ?>" class="btn btn-primary"><span class="fas fa-file-import mr-1"></span>Import Excel</a> -->
                <!--<button class="btn btn-success" data-toggle="modal" data-target="#addpegawaimodal" id="pgwadduser"><span class="fas fa-user-plus mr-1"></span>Tambah Pegawai</button>-->
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="getslipgajibyid" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <!-- <th>NIK</th>
                            <th>Nama Lengkap</th> -->
                            <th>Bulan - Tahun</th>
                            <th>Netto</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <!-- <th>NIK</th>
                            <th>Nama Lengkap</th> -->
                            <th>Bulan - Tahun</th>
                            <th>Netto</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>