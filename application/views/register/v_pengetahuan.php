<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <link rel="icon" type="image/png" href="<?= (empty($dataapp['logo_instansi'])) ? base_url('assets/img/clock-image.png') : (($dataapp['logo_instansi'] == 'default-logo.png') ? base_url('assets/img/clock-image.png') : base_url('storage/setting/' . $dataapp['logo_instansi'])); ?>">
    <meta name="author" content="" />
    <title>Test Pengetahuan</title>
    <link href="<?= base_url('assets'); ?>/css/styles.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
</head>

<body class="bg-auth">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <div class="card shadow-lg border-0 rounded-lg p-2">
                <div class="card-header">
                    <h3 class="text-center font-weight-bold">Test Pengetahuan
                    </h3>
                </div>
                <div class=" card-body">
                    <div class="form-group row">

                        <div class="col-lg-2 mb-3">
                            <span>Kode Registrasi</span>
                        </div>
                        <div class="col-lg-10 mb-3">
                            <span>: <?= $this->session->userdata('kandidat_kode'); ?></span>
                        </div>

                        <div class="col-lg-2 mb-1">
                            <span>Nama</span>
                        </div>
                        <div class="col-lg-10 mb-1">
                            <span>: <?= $this->session->userdata('kandidat_nama'); ?></span>
                        </div>

                        <div class="col-lg-12 mb-1">
                            <hr>
                        </div>

                        <div class="pertanyaan"> </div>

                        <div class="col-lg-12 mb-1">
                            <hr>
                        </div>
                        <div class="col-lg-12 mb-1" align="center">
                            <button class="btn btn-primary" id="selesai">Selesai</button>
                        </div>
                    </div>
                    <hr>
                    <div class="container">
                        <div class="d-flex align-items-center justify-content-center small">
                            <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                                <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>

    <script>
        $(document).ready(function() {
            var tanya = "<div class='row'>";
            $.get("<?= site_url('soal-pengetahuan') ?>", function(res) {
                result = JSON.parse(res);
                no = 1;
                for (let i = 0; i < result.length; i++) {

                    tanya += "<div class='col-lg-12 mt-2'>" +
                        "<span>" + no++ + ". " + result[i].pertanyaan + "</span>" +
                        "</div>";

                    pilih = JSON.parse(result[i].pilihan);

                    for (const x in pilih) {
                        tanya += " <div class='col-lg-12 ml-5'>" +
                            "<input type='radio' name='soal" + result[i].id + "' class='form-check-input' value='" + pilih[x] + "'> " + x + "</div>";
                    }
                }
                $(".pertanyaan").html(tanya + "</div>");

            });
        });

        $("#selesai").on('click', function() {

            $.get("<?= site_url('soal-pengetahuan') ?>", function(res) {
                result = JSON.parse(res);
                var valNilai = 0;
                for (let iv = 0; iv < result.length; iv++) {
                    valN = $('input[name="soal' + result[iv].id + '"]:checked').val();
                    if (valN) {
                        valNilai = (parseInt(valNilai) + parseInt(valN));
                    }
                }

                $.post("<?= site_url('update-score-pengetahuan'); ?>", {
                    nilai: valNilai
                }, function(res) {
                    Swal.fire({
                        title: 'Selamat',
                        icon: 'success',
                        text: 'Test pengetahuan telah selesai...'
                    }).then(okay => {
                        if (okay) {
                            location.href = "<?= site_url('logout-kandidat'); ?>";
                        }
                    });

                });
            });

        });

        $("#masuk").on('click', function() {
            telp = $("#telp").val();
            pass = $("#password").val();
            $.ajax({
                type: 'post',
                url: "<?= site_url('proses-login') ?>",
                data: {
                    telp: telp,
                    pass: pass
                },
                dataType: 'json',
                success: function(res) {
                    if (res == 'success') {
                        location.href = "<?= site_url('pengetahuan-test'); ?>";
                    } else {
                        Swal.fire({
                            icon: 'error',
                            text: 'Nomor Handphone atau Password Salah'
                        })
                    }
                }
            });
        });
    </script>
</body>

</html>