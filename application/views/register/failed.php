<!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <link rel="icon" type="image/png" href="<?= (empty($dataapp['logo_instansi'])) ? base_url('assets/img/clock-image.png') : (($dataapp['logo_instansi'] == 'default-logo.png') ? base_url('assets/img/clock-image.png') : base_url('storage/setting/' . $dataapp['logo_instansi'])); ?>">
            <meta name="author" content="" />
            <title>Registrasi Kandidat</title>
            <link href="<?= base_url('assets'); ?>/css/styles.css" rel="stylesheet" />
            <link href="<?= base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet" />
            <link href="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
            <link href="<?= base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
        </head>

        <body class="bg-auth">
            <div id="layoutAuthentication">
                <div id="layoutAuthentication_content">
                    <div class="overlay-auth">
                    </div>
                    <main>
                        <div class="container auth-card">
                            <div class="row justify-content-center">
                                <div class="col-lg-10">
                                    <div class="card shadow-lg border-0 rounded-lg p-2">
                                        <div class="card-header">
                                            <h3 class="text-center font-weight-light">Belum Berhasil Registrasi</h3>
                                        </div>
                                        <div class="card-body">
                                                <div class="form-group row">
                                                    <h4 text-align="cente">Mohon maaf Surat Lamaran yang diupload melebihi 2MB atau File yang anda masukan buka berekstensi PDF atau JPG, Silahkan untuk registrasi ulang.</h4>   
                                                    
                                                </div>     
                                                <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                    <a href="<?= base_url('registrasi') ?>" class="btn btn-primary"><span class="fas fa-fw fa-sign-in-alt mr-2"></span>Registrasi Ulang</a>
                                                </div>                                   
                                            <hr>
                                            <div class="container">
                                                <div class="d-flex align-items-center justify-content-center small">
                                                    <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                                                        <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
                    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                    <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                </div>
                <div class="text-muted">
                    Page rendered in <strong>{elapsed_time}</strong> detik.
                </div>
            </div>
        </div>
    </footer>
    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>
    <script>
        $(document).ready(function(){
            // alert('register');
        })
    </script>
    </body>

    </html>