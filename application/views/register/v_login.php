<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <link rel="icon" type="image/png" href="<?= (empty($dataapp['logo_instansi'])) ? base_url('assets/img/clock-image.png') : (($dataapp['logo_instansi'] == 'default-logo.png') ? base_url('assets/img/clock-image.png') : base_url('storage/setting/' . $dataapp['logo_instansi'])); ?>">
    <meta name="author" content="" />
    <title>Login Kandidat</title>
    <link href="<?= base_url('assets'); ?>/css/styles.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
</head>

<body class="bg-auth">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <div class="overlay-auth">
            </div>
            <main>
                <div class="container auth-card">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="card shadow-lg border-0 rounded-lg p-2">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-bold">Login Kandidat
                                    </h3>
                                </div>
                                <div class=" card-body">
                                    <div class="form-group row">

                                        <div class="col-lg-12 mb-3">
                                            <label for="telp">Nomor Handphone</label>
                                            <input class="form-control py-4" name="telp" id="telp" type="text" placeholder="contoh : 08123456xxx" value="" />
                                        </div>

                                        <div class="col-lg-12 mb-3">
                                            <label for="pass">Password</label>
                                            <input class="form-control py-4" name="password" id="password" type="password" placeholder="Ketikan Password" value="" />
                                        </div>


                                        <div class="col-lg-12 mb-3" align="center">
                                            <button type="submit" class="btn btn-primary" id="masuk"><span class="fas fa-fw fa-sign-in-alt mr-2"></span>Masuk</button>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="container">
                                        <div class="d-flex align-items-center justify-content-center small">
                                            <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                                                <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                        <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                    </div>
                    <div class="text-muted">
                        Page rendered in <strong>{elapsed_time}</strong> detik.
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>
    <script>
        $("#masuk").on('click', function() {
            telp = $("#telp").val();
            pass = $("#password").val();

            $.ajax({
                type: 'post',
                url: "<?= site_url('proses-login') ?>",
                data: {
                    telp: telp,
                    pass: pass
                },
                dataType: 'json',
                success: function(res) {
                    if (res.message == 'success') {
                        if (res.status == 'lulus') {
                            window.location.replace("<?= site_url('kelengkapan-data'); ?>");
                        } else {
                            window.location.replace("<?= site_url('pengetahuan-test'); ?>");
                        }

                    } else {
                        Swal.fire({
                            icon: 'error',
                            text: 'Nomor Handphone atau Password Salah'
                        })
                    }
                }
            });
        });
    </script>
</body>

</html>