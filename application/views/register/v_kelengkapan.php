<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <link rel="icon" type="image/png" href="<?= (empty($dataapp['logo_instansi'])) ? base_url('assets/img/clock-image.png') : (($dataapp['logo_instansi'] == 'default-logo.png') ? base_url('assets/img/clock-image.png') : base_url('storage/setting/' . $dataapp['logo_instansi'])); ?>">
    <meta name="author" content="" />
    <title>Kelengkapan Data Kandidat</title>
    <link href="<?= base_url('assets'); ?>/css/styles.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" />
</head>

<body class="bg-auth">

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card shadow-lg border-0 rounded-lg p-2">
                <div class="card-header">
                    <h3 class="text-center font-weight-bold">Kelengkapan Data Kandidat </h3>
                </div>

                <form action="<?= site_url('update-kandidat') ?>" method="POST" enctype="multipart/form-data">
                    <div class=" card-body">
                        <div class="form-group row">
                            <div class="col-lg-2 mb-3">
                                <span>Kode Registrasi</span>
                            </div>
                            <div class="col-lg-10 mb-3">
                                <span>: <?= $this->session->userdata('kandidat_kode'); ?></span>
                            </div>

                            <div class="col-lg-6 mb-3">
                                <label for="nama_lengkap">Nama Lengkap</label>
                                <input class="form-control py-4" name="nama_lengkap" id="nama_lengkap" type="text" placeholder="contoh : Stephani Azhari" value="<?= $data->nama_lengkap; ?>" required />
                            </div>

                            <div class="col-lg-6 mb-3">
                                <label for="ktp">Nomor KTP</label>
                                <input class="form-control py-4" name="ktp" id="ktp" type="text" placeholder="contoh : 341189XXX" value="<?= $data->ktp; ?>" required />
                            </div>

                            <div class="col-lg-6 mb-3">
                                <label for="no_telp">Nomor Telepon / HP</label>
                                <input class="form-control py-4" name="no_telp" id="no_telp" type="text" placeholder="contoh : 08139xxx" value="<?= $data->telp; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label for="email">Alamat Email</label>
                                <input class="form-control py-4" name="email" id="email" type="email" placeholder="contoh : muliabintangkejora@gmail.com" value="<?= $data->email; ?>" required />
                            </div>

                            <div class="col-lg-6 mb-3">
                                <label for="pekerjaan_sebelumnya">Pekerjaan Sebelumnya</label>
                                <input class="form-control py-4" name="pekerjaan_sebelumnya" id="pekerjaan_sebelumnya" type="text" placeholder="contoh : Driver" value="<?= $data->pekerjaan_sebelumnya; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label for="pekerjaan_sebelumnya">Perusahaan Sebelumnya</label>
                                <input class="form-control py-4" name="perusahaan_sebelumnya" id="perusahaan_sebelumnya" type="text" placeholder="contoh : PT. xxxx" value="<?= $data->perusahaan_sebelumnya; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label for="lama_bekerja">Lama Bekerja</label>
                                <select name="lama_bekerja" id="lama_bekerja" class="form-control" value="<?= $data->lama_bekerja; ?>">
                                    <option value="">Pilih</option>
                                    <option value="1">Kurang dari 1 Tahun</option>
                                    <option value="2">1 sampai 3 Tahun</option>
                                    <option value="3">Lebih dari 5 Tahun</option>
                                    <option value="4">LEbih dari 10 Tahun</option>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control" value="<?= $data->pendidikan_terakhir; ?>">
                                    <option value="">Pilih</option>
                                    <option value="1">SMP</option>
                                    <option value="2">SMA / SMK</option>
                                    <option value="3">Diploma</option>
                                    <option value="4">S1</option>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label for="jobs">Melamar Sebagai :</label>
                                <select name="jobs" id="jobs" class="form-control" value="<?= $data->jobs; ?>">
                                    <option value="">Pilih</option>
                                    <option value="1">Driver</option>
                                    <option value="2">Admin</option>
                                    <option value="3">IT</option>
                                    <option value="4">Checker</option>
                                    <option value="5">Dispatcher</option>
                                    <option value="6">Monitoring GPS</option>
                                    <option value="7">Operationg Cashier</option>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label for="sim">Surat Izin Mengemudi</label>
                                <select name="sim" id="sim" class="form-control" value="<?= $data->sim; ?>">
                                    <option value="">Pilih</option>
                                    <option value="1">SIM A</option>
                                    <option value="2">SIM B1</option>
                                    <option value="3">SIM B2</option>
                                    <option value="4">SIM C</option>
                                    <option value="5">SIM A Umum</option>
                                    <option value="6">SIM B1 Umum</option>
                                    <option value="7">SIM B2 Umum</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Tempat Lahir</label>
                                <input class="form-control py-4" name="tempat_lahir" id="tempat_lahir" type="text" placeholder="contoh : Karawang" value="<?= (isset($detail->tempat_lahir)) ? $detail->tempat_lahir : '';; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Tanggal Lahir</label>
                                <input class="form-control py-4" name="tanggal_lahir" id="tanggal_lahir" type="text" placeholder="contoh : 30-12-1990" value="<?= (isset($detail->tanggal_lahir)) ? $detail->tanggal_lahir : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Alamat Sesuai KTP</label>
                                <textarea name="alamat_ktp" id="alamat_ktp" class="form-control" alamat_ktp="10" rows="alamat_ktp" required></textarea>

                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Alamat Domisili</label>
                                <textarea name="alamat_domisili" id="alamat_domisili" class="form-control" cols="10" rows="3" required></textarea>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>NPWP</label>
                                <input class="form-control py-4" name="npwp" id="npwp" type="text" placeholder="contoh : 99.999.999.9-999.999" value="<?= (isset($detail->npwp)) ? $detail->npwp : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor SIM</label>
                                <input class="form-control py-4" name="nomor_sim" id="nomor_sim" type="text" placeholder="contoh : 9999-9999-999999" value="<?= (isset($detail->nomor_sim)) ? $detail->nomor_sim : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Masa Berlaku SIM</label>
                                <input class="form-control py-4" name="masa_berlaku_sim" id="masa_berlaku_sim" type="text" placeholder="contoh : 30-12-2024" value="<?= (isset($detail->masa_berlaku_sim)) ? $detail->masa_berlaku_sim : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor KK</label>
                                <input class="form-control py-4" name="nomor_kk" id="nomor_kk" type="text" placeholder="contoh : 999999xxxx" value="<?= (isset($detail->nomor_kk)) ? $detail->nomor_kk : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Status Perkawinan</label>
                                <select name="status_kawin" id="status_kawin" class="form-control">
                                    <option value="L">Lajang</option>
                                    <option value="K0">Kawin</option>
                                    <option value="K1">Kawin Anak 1</option>
                                    <option value="K2">Kawin Anak 2</option>
                                    <option value="K3">Kawin Anak 3</option>
                                    <option value="C0">Duda / Janda </option>
                                    <option value="C1">Duda / Janda Anak 1</option>
                                    <option value="C2">Duda / Janda Anak 2</option>
                                    <option value="C3">Duda / Janda Anak 3</option>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Jumlah Anak</label>
                                <input class="form-control py-4" name="jumlah_anak" id="jumlah_anak" type="text" placeholder="contoh : 2" value="<?= (isset($detail->jumlah_anak)) ? $detail->jumlah_anak : ''; ?>" required />
                            </div>

                            <div id="anak1" class=""> </div>
                            <div id="lahirAnak1" class=""> </div>
                            <div id="anak2" class=""> </div>
                            <div id="lahirAnak2" class=""> </div>
                            <div id="anak3" class=""> </div>
                            <div id="lahirAnak3" class=""> </div>
                            <div id="pasangannya" class=""> </div>
                            <div id="lahirPasangan" class=""> </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nama Ibu Kandung</label>
                                <input class="form-control py-4" name="nama_ibu" id="nama_ibu" type="text" placeholder="contoh : Kartini" value="<?= (isset($detail->nama_ibu)) ? $detail->nama_ibu : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Agama</label>
                                <select name="agama" id="agama" class="form-control" required>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Bank</label>
                                <input class="form-control py-4" name="bank" id="bank" type="text" placeholder="contoh : Bank BRI" value="<?= (isset($detail->bank)) ? $detail->bank : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nama Akun Bank</label>
                                <input class="form-control py-4" name="nama_akun_bank" id="nama_akun_bank" type="text" placeholder="contoh : Stephani Azhari" value="<?= (isset($detail->nama_akun_bank)) ? $detail->nama_akun_bank : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor Rekening</label>
                                <input class="form-control py-4" name="no_rekening" id="no_rekening" type="text" placeholder="contoh : 9999xxx" value="<?= (isset($detail->no_rekening)) ? $detail->no_rekening : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Status Vaksin</label>
                                <select name="status_vaksin" id="status_vaksin" class="form-control" required>
                                    <option value="">Pilih Status Vaksin</option>
                                    <option value="1">Vaksin ke-1</option>
                                    <option value="2">Vaksin ke-2</option>
                                    <option value="3">Vaksin ke-3</option>
                                </select>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor BPJS</label><input class="form-control py-4" name="no_bpjs" id="no_bpjs" type="text" placeholder="contoh : 99999xxx" value="<?= (isset($detail->no_bpjs)) ? $detail->no_bpjs : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor Jamsostek</label>
                                <input class="form-control py-4" name="no_jamsostek" id="no_jamsostek" type="text" placeholder="contoh : 99999xxx" value="<?= (isset($detail->no_jamsostek)) ? $detail->no_jamsostek : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor SKBN</label>
                                <input class="form-control py-4" name="no_skbn" id="no_skbn" type="text" maxlength="16" placeholder="contoh : 99999xxx" value="<?= (isset($detail->no_skbn)) ? $detail->no_skbn : ''; ?>" required />
                                <small class='text-danger pl-3 notSkbn'></small>
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Nomor PBI</label>
                                <input class="form-control py-4" name="no_pbi" id="no_pbi" type="text" placeholder="contoh : 99999xxx" value="<?= (isset($detail->no_pbi)) ? $detail->no_pbi : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Ukuran Seragam Baju</label>
                                <input class="form-control py-4" name="ukuran_baju" id="ukuran_baju" type="text" placeholder="contoh : S/M/L/XL/XXL" value="<?= (isset($detail->ukuran_baju)) ? $detail->ukuran_baju : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3">
                                <label>Ukuran Seragam Celana</label>
                                <input class="form-control py-4" name="ukuran_celana" id="ukuran_celana" type="text" placeholder="contoh : 40" value="<?= (isset($detail->ukuran_celana)) ? $detail->ukuran_celana : ''; ?>" required />
                            </div>
                            <div class="col-lg-6 mb-3 ukuran_sepatu">
                                <label>Ukuran Seragam Sepatu</label>
                                <input class="form-control py-4" name="ukuran_sepatu" id="ukuran_sepatu" type="text" placeholder="contoh : 38" value="<?= (isset($detail->ukuran_sepatu)) ? $detail->ukuran_sepatu : ''; ?>" required>
                            </div>

                            <div class="col-lg-6 mb-3">
                                <label>Foto</label>
                                <input type="file" name="filefoto" class="dropify" data-max-file-size="3M" data-height="200" data-default-file="<?= (isset($detail->foto)) ? base_url('storage/kandidat/') . $detail->foto : ''; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group d-flex justify-content-center  "><button type="submit" class="btn btn-primary"><span class="fas fa-fw fa-save mr-2"></span>Simpan</button></div>
                </form>
                <div class="d-flex align-items-center justify-content-center small">
                    <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                        <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script>
        $(document).ready(function() {
            msg = "<?= $this->session->flashdata('message'); ?>";
            if (msg == 'success') {
                Swal.fire({
                    icon: 'success',
                    title: 'Data Berhasil diperbaharui'
                });
            }

            $('.dropify').dropify({
                messages: {
                    default: 'Drag atau drop untuk memilih gambar',
                    replace: 'Ganti',
                    remove: 'Hapus',
                    error: 'error'
                }
            });

            $("#no_skbn").on('keyup', function() {
                if (this.value.length != 16) {
                    $(".notSkbn").html('Nomor SKBN Harus 16 Digit');
                } else {
                    $(".notSkbn").html('');
                }
            });

            $("#status_kawin").on('change', function() {
                cek_status(this.value);
            });

            function cek_status(stat) {
                var ank1 = '';
                var ank2 = '';
                var ank3 = '';
                var lhrank1 = '';
                var lhrank2 = '';
                var lhrank3 = '';
                var psg = '';
                var lhrpsg = '';
                $("#anak1").removeClass('col-lg-6 mb-3');
                $("#anak2").removeClass('col-lg-6 mb-3');
                $("#anak3").removeClass('col-lg-6 mb-3');
                $("#lahirAnak1").removeClass('col-lg-6 mb-3');
                $("#lahirAnak2").removeClass('col-lg-6 mb-3');
                $("#lahirAnak3").removeClass('col-lg-6 mb-3');
                if (stat == 'K1' || stat == 'C1') {
                    ank1 = "<label>Nama Anak 1</label>" +
                        "<input class='form-control py-4' name='anak_1' type='text' placeholder='' value='<?= (isset($detail->anak1)) ? $detail->anak1 : ''; ?>' required> ";
                    lhrank1 = "<label>Tanggal Lahir Nama Anak 1</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_anak1' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_anak1)) ? date_simple($detail->tgl_lahir_anak1) : ''; ?>' required> ";
                    $("#anak1").addClass('col-lg-6 mb-3');
                    $("#lahirAnak1").addClass('col-lg-6 mb-3');
                } else if (stat == 'K2' || stat == 'C2') {
                    ank1 = "<label>Nama Anak 1</label>" +
                        "<input class='form-control py-4' name='anak_1' type='text' placeholder='' value='<?= (isset($detail->anak1)) ? $detail->anak1 : ''; ?>' required> ";
                    ank2 = "<label>Nama Anak 2</label>" +
                        "<input class='form-control py-4' name='anak_2' type='text' placeholder='' value='<?= (isset($detail->anak2)) ? $detail->anak2 : ''; ?>' required> ";
                    lhrank1 = "<label>Tanggal Lahir Nama Anak 1</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_anak1' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_anak1)) ? date_simple($detail->tgl_lahir_anak1) : ''; ?>' required> ";
                    lhrank2 = "<label>Tanggal Lahir Nama Anak 2</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_anak2' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_anak2)) ? date_simple($detail->tgl_lahir_anak2) : ''; ?>' required> ";
                    $("#anak1").addClass('col-lg-6 mb-3');
                    $("#anak2").addClass('col-lg-6 mb-3');
                    $("#lahirAnak1").addClass('col-lg-6 mb-3');
                    $("#lahirAnak2").addClass('col-lg-6 mb-3');
                } else if (stat == 'K3' || stat == 'C3') {
                    ank1 = "<label>Nama Anak 1</label>" +
                        "<input class='form-control py-4' name='anak_1' type='text' placeholder='' value='<?= (isset($detail->anak1)) ? $detail->anak1 : ''; ?>' required> ";
                    ank2 = "<label>Nama Anak 2</label>" +
                        "<input class='form-control py-4' name='anak_2' type='text' placeholder='' value='<?= (isset($detail->anak2)) ? $detail->anak2 : ''; ?>' required> ";
                    ank3 = "<label>Nama Anak 3</label>" +
                        "<input class='form-control py-4' name='anak_3' type='text' placeholder='' value='<?= (isset($detail->anak3)) ? $detail->anak3 : ''; ?>' required> ";
                    lhrank1 = "<label>Tanggal Lahir Nama Anak 1</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_anak1' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_anak1)) ? date_simple($detail->tgl_lahir_anak1) : ''; ?>' required> ";
                    lhrank2 = "<label>Tanggal Lahir Nama Anak 2</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_anak2' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_anak2)) ? date_simple($detail->tgl_lahir_anak2) : ''; ?>' required> ";
                    lhrank3 = "<label>Tanggal Lahir Nama Anak 3</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_anak3' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_anak3)) ? date_simple($detail->tgl_lahir_anak3) : ''; ?>' required> ";
                    $("#anak1").addClass('col-lg-6 mb-3');
                    $("#anak2").addClass('col-lg-6 mb-3');
                    $("#anak3").addClass('col-lg-6 mb-3');
                    $("#lahirAnak1").addClass('col-lg-6 mb-3');
                    $("#lahirAnak2").addClass('col-lg-6 mb-3');
                    $("#lahirAnak3").addClass('col-lg-6 mb-3');

                }
                if (stat == 'K0' || stat == 'K1' || stat == 'K2' || stat == 'K3') {
                    psg = "<label>Nama Suami / Istri</label>" +
                        "<input class='form-control py-4' name='pasangan' id='pasangan' type='text' placeholder='contoh : Juliet' value='<?= (isset($detail->pasangan)) ? $detail->pasangan : ''; ?>' required> ";
                    lhrpsg = "<label>Tanggal Lahir Suami / Istri</label>" +
                        "<input class='form-control py-4' name='tgl_lahir_pasangan' id='tgl_lahir_pasangan' type='text' placeholder='contoh : 31-12-1990' value='<?= (isset($detail->tgl_lahir_pasangan)) ? date_simple($detail->tgl_lahir_pasangan) : ''; ?>' required> ";
                    $("#pasangannya").addClass('col-lg-6 mb-3');
                    $("#lahirPasangan").addClass('col-lg-6 mb-3');
                }
                $("#anak1").html(ank1);
                $("#anak2").html(ank2);
                $("#anak3").html(ank3);
                $("#lahirAnak1").html(lhrank1);
                $("#lahirAnak2").html(lhrank2);
                $("#lahirAnak3").html(lhrank3);
                $("#pasangannya").html(psg);
                $("#lahirPasangan").html(lhrpsg);
            }

            $.get("<?= site_url('Registrasi/getDataKandidat'); ?>", function(res) {
                result = JSON.parse(res);
                $('#nama_lengkap').val(result.data.nama_lengkap);
                $('#ktp').val(result.data.ktp);
                $('#no_telp').val(result.data.telp);
                $('#email').val(result.data.email);
                $('#pekerjaan_sebelumnya').val(result.data.pekerjaan_sebelumnya);
                $('#perusahaan_sebelumnya').val(result.data.perusahaan_sebelumnya);
                $('#lama_bekerja').val(result.data.lama_bekerja);
                $('#pendidikan_terakhir').val(result.data.pendidikan_terakhir);
                $('#jobs').val(result.data.jobs);
                $('#sim').val(result.data.sim);
                if (result.detail) {
                    $('#tempat_lahir').val(result.detail.tempat_lahir);
                    $('#tanggal_lahir').val(result.detail.tanggal_lahir);
                    $('#alamat_ktp').val(result.detail.alamat_ktp);
                    $('#alamat_domisili').val(result.detail.alamat_domisili);
                    $('#npwp').val(result.detail.npwp);
                    $('#nomor_sim').val(result.detail.nomor_sim);
                    $('#masa_berlaku_sim').val(result.detail.masa_berlaku_sim);
                    $('#nomor_kk').val(result.detail.nomor_kk);
                    $('#status_kawin').val(result.detail.status_kawin);
                    $('#jumlah_anak').val(result.detail.jumlah_anak);
                    $('#agama').val(result.detail.agama);
                    $('#jenis_kelamin').val(result.detail.jenis_kelamin);
                    $('#bank').val(result.detail.bank);
                    $('#nama_akun_bank').val(result.detail.nama_akun_bank);
                    $('#no_rekening').val(result.detail.no_rekening);
                    $('#status_vaksin').val(result.detail.status_vaksin);
                    $('#no_bpjs').val(result.detail.no_bpjs);
                    $('#no_jamsostek').val(result.detail.no_jamsostek);
                    cek_status(result.detail.status_kawin);
                }

            });

            $.get("<?= site_url('Registrasi/getDataAgama') ?>", function(res) {
                result = JSON.parse(res);
                console.log(result);
                htAg = "<option value=''>Pilih Agama</option>";
                for (let ii = 0; ii < result.length; ii++) {
                    htAg += "<option value='" + result[ii].id + "'>" + result[ii].nama_agama + "</option>";
                }
                $("#agama").html(htAg);
                $("#agama").val("<?= (isset($detail->agama)) ? $detail->agama : ''; ?>");
            });
        });

        $('#formKecelakaan').submit(function() {
            event.preventDefault();
            var _this = $(this);
            _this.unbind('submit').submit();
        });
    </script>
</body>

</html>