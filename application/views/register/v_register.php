<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <link rel="icon" type="image/png" href="<?= (empty($dataapp['logo_instansi'])) ? base_url('assets/img/clock-image.png') : (($dataapp['logo_instansi'] == 'default-logo.png') ? base_url('assets/img/clock-image.png') : base_url('storage/setting/' . $dataapp['logo_instansi'])); ?>">
    <meta name="author" content="" />
    <title>Registrasi Kandidat</title>
    <link href="<?= base_url('assets'); ?>/css/styles.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
    <link href="<?= base_url('assets'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" />
    <style type="text/css">
        .notif {
            color: red;
            font-size: small;
        }
    </style>
</head>

<body class="bg-auth">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <div class="overlay-auth">
            </div>
            <main>
                <div class="container auth-card">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="card shadow-lg border-0 rounded-lg p-2">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-light">Registrasi Kandidat
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <?= $this->session->flashdata('authmsg'); ?>
                                    <!--<form action=" //base_url('Registrasi/proses_registrasi') "?>" method="post">-->
                                    <?= form_open_multipart('Registrasi/proses_registrasi'); ?>
                                    <div class="form-group row">
                                        <!-- Nama Lengkap -->
                                        <div class="col-lg-6 mb-3">
                                            <label for="nama_lengkap">Nama Lengkap</label>
                                            <input class="form-control py-4" name="nama_lengkap" id="nama_lengkap" type="text" placeholder="contoh : Stephani Azhari" value="<?= set_value('nama_lengkap') ?>" />
                                            <?= form_error('nama_lengkap', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <!-- Nama Lengkap -->
                                        <!-- No KTP -->
                                        <div class="col-lg-6 mb-3">
                                            <label for="ktp">Nomor KTP</label>
                                            <input class="form-control py-4" name="ktp" id="ktp" type="text" placeholder="contoh : 341189XXX" value="<?= set_value('ktp') ?>" />
                                            <span class="notif Nktp"></span>
                                            <?= form_error('ktp', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <!-- No KTP -->
                                        <div class="col-lg-6 mb-3">
                                            <label for="no_telp">Nomor Telepon / HP</label>
                                            <input class="form-control py-4" name="no_telp" id="no_telp" type="text" placeholder="contoh : 08139xxx" value="<?= set_value('no_telp') ?>" />
                                            <span class="notif Ntelp"></span>
                                            <?= form_error('no_telp', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="email">Alamat Email</label>
                                            <input class="form-control py-4" name="email" id="email" type="email" placeholder="contoh : muliabintangkejora@gmail.com" value="<?= set_value('email') ?>" />
                                            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <hr>
                                        <div class="col-lg-6 mb-3">
                                            <label for="pekerjaan_sebelumnya">Pekerjaan Sebelumnya</label>
                                            <input class="form-control py-4 notSimbol" name="pekerjaan_sebelumnya" id="pekerjaan_sebelumnya" type="text" placeholder="contoh : Driver" value="<?= set_value('pekerjaan_sebelumnya') ?>" />
                                            <?= form_error('pekerjaan_sebelumnya', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="pekerjaan_sebelumnya">Perusahaan Sebelumnya</label>
                                            <input class="form-control py-4 notSimbol" name="perusahaan_sebelumnya" id="perusahaan_sebelumnya" type="text" placeholder="contoh : PT. xxxx" value="<?= set_value('perusahaan_sebelumnya') ?>" />
                                            <?= form_error('perusahaan_sebelumnya', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="lama_bekerja">Lama Bekerja</label>
                                            <select name="lama_bekerja" id="lama_bekerja" class="form-control" value="<?= set_value('lama_bekerja') ?>">
                                                <option value="">Pilih</option>
                                                <option value="1">Kurang dari 1 Tahun</option>
                                                <option value="2">1 sampai 3 Tahun</option>
                                                <option value="3">Lebih dari 5 Tahun</option>
                                                <option value="4">LEbih dari 10 Tahun</option>
                                            </select>
                                            <?= form_error('lama_bekerja', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                            <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control" value="<?= set_value('pendidikan_terakhir') ?>">
                                                <option value="">Pilih</option>
                                                <option value="1">SMP</option>
                                                <option value="2">SMA / SMK</option>
                                                <option value="3">Diploma</option>
                                                <option value="4">S1</option>
                                            </select>
                                            <?= form_error('pendidikan_terakhir', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="jobs">Melamar Sebagai :</label>
                                            <select name="jobs" id="jobs" class="form-control" value="<?= set_value('jobs') ?>">
                                                <option value="">Pilih</option>
                                                <option value="1">Driver</option>
                                                <option value="2">Admin</option>
                                                <option value="3">IT</option>
                                                <option value="4">Checker</option>
                                                <option value="5">Dispatcher</option>
                                                <option value="6">Monitoring GPS</option>
                                                <option value="7">Operationg Cashier</option>
                                            </select>
                                            <?= form_error('jobs', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="jobs">Surat Izin Mengemudi</label>
                                            <select name="sim" id="sim" class="form-control" value="<?= set_value('jobs') ?>">
                                                <option value="">Pilih</option>
                                                <option value="1">SIM A</option>
                                                <option value="2">SIM B1</option>
                                                <option value="3">SIM B2</option>
                                                <option value="4">SIM C</option>
                                                <option value="5">SIM A Umum</option>
                                                <option value="6">SIM B1 Umum</option>
                                                <option value="7">SIM B2 Umum</option>
                                            </select>
                                            <?= form_error('jobs', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="pekerjaan_sebelumnya">Upload Surat Lamaran</label>
                                            <input class="form-control" name="surat_lamaran" id="surat_lamaran" type="file">
                                            <?= form_error('surat_lamaran', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                        <div class="col-lg-6 mb-3">
                                            <label for="district">Area</label>
                                            <select name="district" id="district" class="form-control" value="<?= set_value('district') ?>">
                                                <option value="">Pilih</option>
                                                <?php
                                                foreach ($area as $value) {
                                                    echo "<option value='$value->kode_op'>$value->kode_op</option>";
                                                }
                                                ?>
                                            </select>
                                            <?= form_error('district', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </div>
                                    </div>


                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0"><button type="submit" class="btn btn-primary"><span class="fas fa-fw fa-sign-in-alt mr-2"></span>Registrasi</button></div>
                                    </form>

                                    <hr>
                                    <div class="container">
                                        <div class="d-flex align-items-center justify-content-center small">
                                            <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                                                <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                        <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                    </div>
                    <div class="text-muted">
                        Page rendered in <strong>{elapsed_time}</strong> detik.
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#lama_bekerja").val("<?= set_value('lama_bekerja'); ?>");
            $("#pendidikan_terakhir").val("<?= set_value('pendidikan_terakhir'); ?>");
            $("#jobs").val("<?= set_value('jobs'); ?>");
            $("#sim").val("<?= set_value('sim'); ?>");

            $("#ktp").on('keyup', function() {

                if (this.value.length != 16) {
                    $(".Nktp").html('Nomor KTP harus 16 Digit');
                } else {
                    $(".Nktp").html('');
                }
                if (this.value.charAt(0) == 0) {
                    $(".Nktp").html('Nomor KTP Tidak Boleh Diawali Angka 0');
                }
            });

            $("#no_telp").on('keyup', function() {
                if (this.value.length != 13) {
                    $(".Nno_telp").html('Nomor Telepon harus 13 Digit');
                } else {
                    $(".Nno_telp").html('');
                }
                if (this.value.charAt(0) != 0) {
                    $(".Nno_telp").html('Nomor Telepon harus Diawali Angka 0');
                }
            });

            $('.notSimbol').bind('keyup blur', function() {
                var valDat = $(this);
                valDat.val(valDat.val().replace(/[^a-z0-9]/gi, ''));
            });


            mess = "<?= $message; ?>";
            if (mess == 'error_upload') {
                Swal.fire({
                    icon: 'error',
                    text: 'Mohon maaf Surat Lamaran yang diupload melebihi 2MB atau File yang anda masukan bukan berekstensi PDF atau JPG, Silahkan untuk registrasi ulang'
                })
            } else if (mess == 'success') {
                Swal.fire({
                    icon: 'success',
                    title: 'Selamat! Anda Berhasil Registrasi',
                    text: 'Terimakasih sudah melakukan registrasi, Tim recruitment akan segera menghubungi anda!'
                });
                $(".form-control").val('');
            }

        })
    </script>
</body>

</html>