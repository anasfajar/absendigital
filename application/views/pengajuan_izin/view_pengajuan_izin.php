<div class="form-group row">
    <label for="nama_karyawan" class="col-sm-4 col-form-label">Nama Karyawan</label>
    <div class="col-8">
        <label class="col-form-label">: <?= $data['nama_kary'] ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="nik_karyawan" class="col-sm-4 col-form-label">NIK Karyawan</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= $data['nik_kary'] ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="jenis_pengajuan" class="col-sm-4 col-form-label">Jenis Pengajuan</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= $data['jenis_pengajuan'] ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="jenis_pengajuan" class="col-sm-4 col-form-label">Status</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= ($data['f_approve'] == 0) ? '<span class="badge badge-warning"> Not Approved </span>' : (($data['f_approve'] == 1) ? '<span class="badge badge-success"> Approved </span>' : '<span class="badge badge-danger"> Rejected </span>' )?></label>
    </div>
</div>
<div class="form-group row">
    <label for="ket" class="col-sm-4 col-form-label">Keterangan</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= $data['ket'] ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="no_hp" class="col-sm-4 col-form-label">No HP</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= $data['no_hp'] ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= $data['alamat'] ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_awal" class="col-sm-4 col-form-label">Tanggal Awal</label>
    <div class="col-sm-8">
    <label class="col-form-label">: <?= date('d M Y', strtotime($data['tgl_awal'])) ?></label>
    </div>
</div>
<div class="form-group row">
    <label for="jumlah_hari" class="col-sm-4 col-form-label">Jumlah Hari</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= $data['jum_hari']. " Hari"?></label>
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_akhir" class="col-sm-4 col-form-label">Tanggal Akhir</label>
    <div class="col-sm-8">
        <label class="col-form-label">: <?= date('d M Y', strtotime($data['tgl_akhir'])) ?></label>
    </div>
</div>
<?php if($data['jenis_pengajuan'] == 'izin' || $data['jenis_pengajuan'] == 'cuti'){ $tampil='d-none';}else{$tampil='';}?>
<div class="form-group row <?= $tampil; ?>">
    <div class="col-sm-4">Dokumen Surat</div>
    <div class="col-sm-8">
        <?php if($data['dok_surat']) : ?>
            <img src="<?= base_url('storage/skd/' . $data['dok_surat']) ?? '' ?>" class="img-thumbnail ml-auto" style="width:100%">
        <?php endif; ?>
    </div>
</div>
