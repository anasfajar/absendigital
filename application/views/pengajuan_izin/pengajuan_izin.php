<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-file-alt mr-2"></span>Data Pengajuan Izin</h1>
    <div class="card mb-4">
        <div class="card-header">
            <div class="float-right d-inline">
                <div class="btn btn-primary" id="refresh-tabel-pengajuan-izin"><span class="fas fa-sync-alt mr-1"></span>Refresh Tabel</div>
                <!-- <a href="<?php base_url('form_pengajuan_izin') ?>" class="btn btn-success"><span class="fas fa-plus mr-1"></span>Ajukan Izin</a> -->
                <button class="btn btn-success" data-toggle="modal" data-target="#addizincutisakitmodal" id="addizin"><span class="fas fa-plus mr-1"></span>Ajukan Izin</button>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered dashboard" id="dataizincutisakit" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK Pegawai</th>
                            <th>Nama Pegawai</th>
                            <th>Jenis Pengajuan</th>
                            <th>Jumlah Hari</th>
                            <th>Tanggal Awal</th>
                            <th>Tanggal Akhir</th>
                            <th>Approval</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>    
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add Izin Cuti Sakit -->
<div class="modal fade" id="addizincutisakitmodal" tabindex="-1" role="dialog" aria-labelledby="addizincutisakitmodal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="addizincutisakitmodallabel"><span class="fas fa-edit mr-1"></span>Ajukan Pengajuan Izin</h5>
            </div>
            <div class="modal-body">
                <!-- <form action="" method="post" enctype="multipart/form-data"> -->
                <?= form_open_multipart('#', ['id' => 'addizincutisakit']) ?>
                    <div class="form-group row">
                        <label for="nama_karyawan" class="col-sm-4 col-form-label">Nama Karyawan</label>
                        <div class="col-sm-8">
                            <?php if($user['role_id'] == 3 ){
                                    $nik_karyawan = $user['kode_pegawai'];
                                ?>
                                <input type="text" class="form-control" id="add_nama_karyawan" name="nama_kary" value="<?= $user['nama_lengkap'] ?>">
                            <?php }else{
                                    $nik_karyawan = '';
                                ?>
                                <select class="form-control" id="add_nama_karyawan" name="nama_kary">
                                    <option value="">Select Employee</option>
                                </select>
                            <?php }?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nik_karyawan" class="col-sm-4 col-form-label">NIK Karyawan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="add_nik_karyawan" name="nik_kary" value="<?= $nik_karyawan ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_pengajuan" class="col-sm-4 col-form-label">Jenis Pengajuan</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="jenis_pengajuan" name="jenis_pengajuan">
                                <option hidden>Select Permission Type</option>
                                <option value="izin">Izin</option>
                                <option value="cuti">Cuti</option>
                                <option value="sakit">Sakit</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ket" class="col-sm-4 col-form-label">Keterangan</label>
                        <div class="col-sm-8">
                            <textarea cols="10" rows="4" class="form-control" id="keterangan" name="ket"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_hp" class="col-sm-4 col-form-label">No HP</label>
                        <div class="col-sm-8">
                            <input type="number" cols="10" rows="4" class="form-control" id="no_hp" name="no_hp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
                        <div class="col-sm-8">
                            <textarea cols="10" rows="4" class="form-control" id="alamat" name="alamat"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_awal" class="col-sm-4 col-form-label">Tanggal Awal</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="tanggal_awal" name="tgl_awal">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah_hari" class="col-sm-4 col-form-label">Jumlah Hari</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" id="jumlah_hari" name="jum_hari">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tanggal_akhir" class="col-sm-4 col-form-label">Tanggal Akhir</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="tanggal_akhir" name="tgl_akhir" readonly>
                        </div>
                    </div>
                    <div class="form-group row input-skd">
                        <div class="col-sm-4">Dokumen Surat</div>
                        <div class="col-sm-8">
                            <div class="preview-image"></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="dokumen_surat" name="dok_surat">
                                <input type="hidden" class="custom-file-input" id="skd" name="skd">
                                <label class="custom-file-label" for="dokumen_surat">Choose file. Max 2 MB</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="sisa_cuti">
                    <div class="my-2" id="info-data"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
                    <button type="submit" class="btn btn-primary" id="addizincutisakit-btn"><span class="fas fa-plus mr-1"></span>Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>

<!-- Modal Edit Pengajuan Izin -->
<div class="modal fade" id="editizincutisakitmodal" tabindex="-1" role="dialog" aria-labelledby="editizincutisakitmodal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="editizincutisakitlabel"><span class="fas fa-user-edit mr-1"></span>Edit Pengajuan Izin</h5>
            </div>
            <div class="modal-body">
                <div id="editdataizincutisakit"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal View Pengajuan Izin -->
<div class="modal fade" id="viewizincutisakitmodal" tabindex="-1" role="dialog" aria-labelledby="viewizincutisakitmodal" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="viewizincutisakitmodallabel"><span class="fas fa-file-alt mr-1"></span>Preview Pengajuan Izin</h5>
            </div>
            <div class="modal-body">
                <div id="viewizincutisakit"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Tutup</button>
            </div>
        </div>
    </div>
</div>