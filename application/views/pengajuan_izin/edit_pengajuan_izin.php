<?= form_open_multipart('#', ['id' => 'editizincutisakit']) ?>
<input type="hidden" name="id" value="<?= $data['id'] ?>">
<input type="hidden" name="dok_surat_lama" value="<?= $data['dok_surat'] ?>">
    <div class="form-group row">
        <label for="nama_karyawan" class="col-sm-4 col-form-label">Nama Karyawan</label>
        <div class="col-sm-8">
            <select class="form-control" id="edit_nama_karyawan" name="nama_kary">
               <option value="<?= $data['nama_kary'] ?>" selected><?= $data['nama_kary'] ?></option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="nik_karyawan" class="col-sm-4 col-form-label">NIK Karyawan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="edit_nik_karyawan" name="nik_kary" value="<?= $data['nik_kary'] ?>" readonly>
        </div>
    </div>
    <div class="form-group row">
        <label for="jenis_pengajuan" class="col-sm-4 col-form-label">Jenis Pengajuan</label>
        <div class="col-sm-8">
            <select class="form-control" id="jenis_pengajuan" name="jenis_pengajuan">
                <option hidden>Select Permission Type</option>
                <option value="izin" <?= $data['jenis_pengajuan'] == 'izin' ? 'selected' : '' ?>>Izin</option>
                <option value="cuti" <?= $data['jenis_pengajuan'] == 'cuti' ? 'selected' : '' ?>>Cuti</option>
                <option value="sakit" <?= $data['jenis_pengajuan'] == 'sakit' ? 'selected' : '' ?>>Sakit</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="ket" class="col-sm-4 col-form-label">Keterangan</label>
        <div class="col-sm-8">
            <textarea cols="10" rows="4" class="form-control" id="keterangan" name="ket"><?= $data['ket'] ?></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="no_hp" class="col-sm-4 col-form-label">No HP</label>
        <div class="col-sm-8">
            <input type="number" class="form-control" id="no_hp" name="no_hp" value="<?= $data['no_hp'] ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
        <div class="col-sm-8">
            <textarea cols="10" rows="4" class="form-control" id="alamat" name="alamat"><?= $data['alamat'] ?></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="tanggal_awal" class="col-sm-4 col-form-label">Tanggal Awal</label>
        <div class="col-sm-8">
            <input type="date" class="form-control" id="tanggal_awal" name="tgl_awal" value="<?= $data['tgl_awal'] ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="jumlah_hari" class="col-sm-4 col-form-label">Jumlah Hari</label>
        <div class="col-sm-8">
            <input type="number" class="form-control" id="jumlah_hari" name="jum_hari" value="<?= $data['jum_hari'] ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="tanggal_akhir" class="col-sm-4 col-form-label">Tanggal Akhir</label>
        <div class="col-sm-8">
            <input type="date" class="form-control" id="tanggal_akhir" name="tgl_akhir" value="<?= $data['tgl_akhir'] ?>" readonly>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4">Dokumen Surat</div>
        <div class="col-sm-8">
            <?php if($data['dok_surat']) : ?>
                <div class="preview-image">
                    <img src="<?= base_url('storage/skd/' . $data['dok_surat']) ?? '' ?>" class="img-thumbnail ml-auto" style="width:250px">
                </div>
            <?php else : ?>
                <div class="preview-image"></div>
            <?php endif; ?>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="dokumen_surat" name="dok_surat">
                <label class="custom-file-label" for="dokumen_surat">Choose file. Max 2 MB</label>
            </div>
        </div>
    </div>
<div class="my-2" id="info-data"></div>
<div class="my-2" id="info-edit"></div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
    <button type="submit" class="btn btn-primary" id="editizincutisakit-btn"><span class="fas fa-pen mr-1"></span>Edit</button>
</div>
</form>
<script>
    selectKaryawan();
</script>