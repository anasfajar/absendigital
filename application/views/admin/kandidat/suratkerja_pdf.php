
<?php
function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
?>

<html><head>
<title>SPKWT</title>
</head><body>
<br>
<br>
<div align="center">
<font size="4" align="center"><b><u>SURAT PERJANJIAN KERJA WAKTU TERTENTU</u></b></font><br>
</div>
<br>
<br>
<br>
<br>
<table>
    <tr>
      <td><b>Pada Hari ini tanggal  <?php echo tgl_indo(date('Y-m-d'));?></b></td>
    </tr>
    <tr>
      <td>di Karawang telah mengadakan perjanjian antara  :</td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
      <td width="20px">1.</td>
      <td width="200px">Nama</td>
      <td>: Fatria Riza</td>
    </tr>
    <tr>
      <td></td>
      <td>Jabatan</td>
      <td>: Direktur</td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
      <td>Dalam perjanjian ini selanjutnya disebut Pihak Perusahaan/Pihak Pertama</td>
    </tr>
</table>
<br>
<br>
<table>
    <tr>
      <td width="20px">2.</td>
      <td width="200px">Nama</td>
      <td>: <?php echo $viewkandidat['nama_lengkap']; ?></td>
    </tr>
    <tr>
      <td></td>
      <td>Jabatan</td>
      <td>: <?php echo $viewkandidat['nama_jobs']; ?></td>
    </tr>
    <tr>
      <td></td>
      <td>Tempat & Tanggal Lahir</td>
      <td>: <?php echo $viewkandidat['tempat_lahir']; ?>, <?php echo tanggal_indonesia_medium($viewkandidat['tanggal_lahir']); ?></td>
    </tr>
    <tr>
      <td></td>
      <td>Jenis Kelamin </td>
      <td>: <?php 
      if($viewkandidat['jenis_kelamin']== "1"){
        echo "Laki-laki";
      }else{
        echo "Perempuan";
      }
      ?></td>
    </tr>
    <tr>
      <td></td>
      <td>Alamat</td>
      <td>: <?php echo $viewkandidat['alamat_ktp']; ?></td>
    </tr>
    <tr>
      <td></td>
      <td>Penempatan</td>
      <td>: PT. ADI SARANA ARMADA LOGISTICS Tbk</td>
    </tr>
    <tr>
      <td></td>
      <td>Area</td>
      <td>: AOP DENPASAR</td>
    </tr>
</table>
<br>
<br>
<table border="0">
    <tr height="60px">
      <td align="justify">Dalam perjanjian ini bertindak dan bertanggung jawab untuk dan atas nama sendiri, selanjutnya disebut  Pihak Buruh/Pihak kedua.</td>
    </tr>
    <tr height="60px">
      <td align="justify"></td>
    </tr>
    <tr height="20px">
      <td align="justify">Kedua belah pihak telah sepakat dan setuju untuk mengadakan perjanjian kerja waktu tertentu dan masing-masing pihak mengikatkan diri pada ketentuan-ketentuan sebagai berikut:</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 1<b></td>
    </tr>
    <tr>
      <td align="center">KETENTUAN UMUM</td>
    </tr>
</table>
<table border="0">
    <tr height="60px">
      <td align="justify">Dengan ditanda tanganinya  surat perjanjian ini oleh kedua belah pihak, maka pihak perusahaan setuju mempekerjakan Buruh tersebut diatas, dan pihak Buruh menerima dengan baik untuk bekerja pada perusahaan yang bekerjasama dengan PT. Mulia Bintang kejora , sebagai Buruh yang penempatannya di   PT. Assa Logistics  selama  <b>PKWT I (3)</b> bulan terhitung <b>mulai</b>:</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>01 August 2022</b>  Sampai Dengan  <b>31 October 2022</b></td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 2<b></td>
    </tr>
    <tr>
      <td align="center">PENEMPATAN KERJA</td>
    </tr>
</table>
<table border="0">
    <tr height="60px">
      <td align="justify">Pihak Buruh dalam hubungan kerja ini akan ditempatkan sebagai DRIVER Logistics yang sesuai dengan kepentingan perusahaan ditempat kerja.</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 3<b></td>
    </tr>
    <tr>
      <td align="center">UPAH</td>
    </tr>
</table>
<?php $upah ="2.802.926";?>
<table>
    <tr>
      <td width="20px">1.</td>
      <td width="200px">Upah Pokok</td>
      <td>: Rp. <?php echo $upah; ?>/ Bulan</td>
    </tr>
    <tr>
      <td>2.</td>
      <td>Premi Hadir</td>
      <td>: Rp. / Hari</td>
    </tr>
    <tr>
      <td>3.</td>
      <td>Tunjangan Operasional</td>
      <td>: Rp. / Bulan</td>
    </tr>
    <tr>
      <td>4.</td>
      <td>Insentif Lainnya</td>
      <td>: Rp. / Bulan</td>
    </tr>
    <tr>
      <td>5.</td>
      <td>THR</td>
      <td>: Dihitung sesuai dengan ketentuan yang berlaku di perusahaan tempat kerja dan berpedoman pada UU yang berlaku.</td>
    </tr>
    <tr>
      <td width="20px">6.</td>
      <td width="200px">Potongan</td>
      <td>: Akan disesuaikan dengan aturan perusahaan ditempat kerja.</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 4<b></td>
    </tr>
    <tr>
      <td align="center">JAM KERJA</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td>Jam kerja sesuai dengan kebijakan perusahaan.</td>
    </tr>
    <tr>
      <td>2.</td>
      <td>Tidak berkeberatan bekerja diluar jam kerja bilamana dianggap perlu oleh perusahaan dimana pihak Buruh bekerja, tidak dipaksakan dan atau berpedoman pada UU yang berlaku.</td>
    </tr>
    <tr>
      <td>3.</td>
      <td>Hari dan jam kerja ditetapkan oleh perusahaan dimana pihak Buruh bekerja.</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 5<b></td>
    </tr>
    <tr>
      <td align="center">JAMINAN SOSIAL TENAGA KERJA (BPJS KETENAGAKERJAAN)</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td>Selama perjanjian ini berlaku, pihak Buruh diikut sertakan dalam program Jamsostek  yang mencakup jaminan hari tua (JHT), Jaminan kecelakaan kerja (JKK),  Jaminan Kematian (JKM) dan Jaminan Pensiun (JP).</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 6<b></td>
    </tr>
    <tr>
      <td align="center">PERAWATAN DAN PENGOBATAN</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td>Selama perjanjian ini berlaku, Buruh bersedia diperiksa kesehatannya setiap waktu dipandang perlu oleh pihak Perusahaan.</td>
    </tr>
    <tr>
      <td width="20px">2.</td>
      <td>Pengobatan diberikan kepada Buruh dalam bentuk fasilitas Jaminan Pemeliharaan Kesehatan yang di kelola oleh perusahaan.</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 7<b></td>
    </tr>
    <tr>
      <td align="center">TERPUTUSNYA HUBUNGAN KERJA</td>
    </tr>
</table>
<table>
    <tr>
      <td>Pihak kedua bersedia menerima pemutusan hubungan kerja secara sepihak oleh Pihak Pertama tanpa pesangon dari   PT. Mulia Bintang kejora mengenai pemutusan hubungan kerja apabila:</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td>Apabila project antara PT. Adi Sarana Armada Logistics dengan customer terjadi penurunan atau pengurangan order atau habis dan selesai kontraknya.</td>
    </tr>
    <tr>
      <td width="20px">2.</td>
      <td><u><b>Dikembalikan oleh perusahaan PT. ASSA LOGISTICS  dimana pihak Buruh bekerja karena dianggap tidak mampu bekerja dengan baik.</b></u></td>
    </tr>
    <tr>
      <td width="20px">3.</td>
      <td>Terjadi suatu kondisi yang tidak diduga sebelumnya (Force Majeure).</td>
    </tr>
    <tr>
      <td width="20px">4.</td>
      <td>Melakukan tindakan Kriminal, seperti mencuri, melakukan kekerasan, terlibat kasus narkoba dan minum minuman keras di area kerja.</td>
    </tr>
    <tr>
      <td width="20px">5.</td>
      <td>Wajib <b>tidak menuntut</b> dijadikan Buruh tetap / PKWT diperusahaan <b>PT. Mulia Bintang Kejora<b> setelah selesai habis PKWT.</td>
    </tr>
    <tr>
      <td width="20px">6.</td>
      <td>Dengan terputusnya hubungan kerja yang diakibatkan oleh yang tersebut dalam pasal 7 point 1-5, maka perjanjian kerja ini batal demi hukum dan pekerja tidak berhak menuntut sisa kontrak</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 8<b></td>
    </tr>
    <tr>
      <td align="center">TATA TERTIB PEKERJAAN</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td>Selama terikat oleh perjanjian kerja, masing-masing pihak tidak bisa memutuskan hubungan kerja tanpa pemberitahuan terlebih dahulu dan tanpa persetujuan kedua belah pihak, kecuali terjadi keadaan-keadaan seperti tertulis dalam pasal 7 perjanjian ini.</td>
    </tr>
    <tr>
      <td width="20px">2.</td>
      <td>Pihak Buruh telah mengerti bahwa sewaktu-waktu dapat diberhentikan dari pekerjaannya menurut perjanjian ini sebelum jangka waktu PKWT berakhir, apabila dengan sengaja maupun tidak sengaja merusak,mencuri,melakukan tindakan yang melanggar hukum,kesopanan ( susila ) dan ketertiban umum yang dapat dikatagorikan kesalahan berat menurut UU yang berlaku.</td>
    </tr>
    <tr>
      <td width="20px">3.</td>
      <td>Buruh akan mematuhi segala peraturan dan tata tertib pekerjaan yang berlaku dan bersedia menerima tindakan disipliner apabila pihak Buruh melakukan pelanggaran yang dapat pula mengakibatkan pemutusan hubungan kerja.</td>
    </tr>
    <tr>
      <td width="20px">4.</td>
      <td>Dalam hal pemutusan hubungan kerja akibat tindakan pelanggaran yang dilakukan oleh Buruh atau terjadi keadaan sesuai dengan pasal 7 maka pihak perusahaan tidak akan membayar ganti rugi apapun dan pihak Buruh tidak mempunyai tuntutan apapun baik administratif maupun non administrative.</td>
    </tr>
    <tr>
      <td width="20px">5.</td>
      <td>Pihak Pertama dapat mengakhiri hubungan kerja seketika tanpa pemberitahuan sebelumnya kepada Pihak Kedua, apabila Pihak Kedua melakukan pelanggaran atau mangkir 3 (tiga) hari kerja berturut-turut dalam satu bulan dan atau telah dipanggil 2 (dua) kali secara tertulis, maka dikwalifikasikan mengundurkan diri dan Pihak Kedua hanya memperoleh upah sampai hari terakhir bekerja.</td>
    </tr>
    <tr>
      <td width="20px">6.</td>
      <td>Selama kesepakatan PKWT kerja ini berlaku,Pihak Kedua tidak diperkenankan bekerja pada pihak lain.</td>
    </tr>
    <tr>
      <td width="20px">7.</td>
      <td>Tindakan sanksi disiplin akan diambil terhadap pelanggaran tata tertib yang dilakukan Pihak Kedua oleh Pihak Pertama dalam bentuk teguran lisan maupun tertulis (dengan melalui surat peringatan), skorsing ataupun Pemutusan Hubungan Kerja sesuai berat kesalahannya.</td>
    </tr>
    <tr>
      <td width="20px">08.</td>
      <td>Berjanji tidak akan mempengaruhi / menghasut kawan-kawan sekerja dalam bentuk apapun yang bertentangan dengan UU yang berlaku.</td>
    </tr>
    <tr>
      <td width="20px">09.</td>
      <td>Berjanji tidak akan membawa soal politik dalam pekerjaan.</td>
    </tr>
    <tr>
      <td width="20px">10.</td>
      <td>Bersikap setia taat kepada pimpinan/ atasan serta  bekerja dengan kesungguhan hati, penuh tanggung Jawab, rajin dan jujur.</td>
    </tr>
    <tr>
      <td width="20px">11.</td>
      <td>Pihak Buruh bersedia menjaga dan memelihara serta bertanggung jawab atas barang-barang /alat milik perusahaan tempat kerja dari kemungkinan kehilangan, pencurian, kerusakan/ pengrusakan dari dan sebagainya.</td>
    </tr>
    <tr>
      <td width="20px">12.</td>
      <td>Pihak Buruh wajib scan tangan / mengisi kartu absensi setiap saat akan masuk kerja dan saat akan pulang kerja.</td>
    </tr>
    <tr>
      <td width="20px">13.</td>
      <td>Pihak Buruh bersedia menjaga barang inventaris perusahaan dengan  baik dan mengembalikan secara utuh apabila masa PKWT kerja telah berakhir.</td>
    </tr>
    <tr>
      <td width="20px">14.</td>
      <td>Pihak Buruh bersedia mengganti barang inventaris perusahaan sesuai aslinya apabila hilang atau rusak.</td>
    </tr>
    <tr>
      <td width="20px">15.</td>
      <td>Bila pihak kedua mengundurkan diri sebelum masa berakhirnya PKWT kerja, maka pihak kedua akan dikenakan denda administrasi sebesar satu bulan nilai gaji pokok ( Sesuai UMK setempat ).</td>
    </tr>
    <tr>
      <td width="20px">16.</td>
      <td>Besaran denda administrasinya adalah sisa bulan dalam PKWT dikali nilai Gaji Pokok ( sesuai UMK setempat ).</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 9<b></td>
    </tr>
    <tr>
      <td align="center">PENYELESAIAN PERSELISIHAN</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td>Apabila timbul perselisihan atau sengketa, maka kedua belah pihak sepakat akan menyelesaikan melalui musyawarah mufakat.</td>
    </tr>
    <tr>
      <td width="20px">2.</td>
      <td>Apabila melalui musyawarah dan mufakat tidak tercapai kesepakatan maka akan diselesaikan sesuai dengan peraturan dan hukum yang berlaku.</td>
    </tr>
</table>
<br>
<br>
<table align="center" width="100%">
    <tr>
      <td align="center"><b>PASAL 10<b></td>
    </tr>
    <tr>
      <td align="center">PENUTUP</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">Perjanjian ini di buat secara sadar dan tanpa ada paksaan, penipuan, kekhilafan dari pihak manapun dan setelah di baca serta dipahami maksud dan isinya kemudian ditanda tangani oleh masing-masing pihak dan dibubuhi bea materai secukupnya yang mempunyai kekuatan hukum dan berlaku sejak tanggal ditanda tanganinya surat perjanjian ini.</td>
    </tr>
</table>
<br>
<br>
<br>
<br>
<table border="0" width="100%">
    <tr>
    <td>
        <font><center>Pihak Pertaman</center></font><br>
        <font align="center">&nbsp;</font><br>
        <br>
        <br>
        <font size="1"><center>&nbsp;</center></font><br>
        <br>
        <br>
        <font><center>(<u>Fatria Riza</u>)</center></font><br>
        <font><center>Direktur</center></font>
      </td>
      <td>
        <font><center>Pihak Pertama</center></font><br>
        <font align="center">&nbsp;</font><br>
        <br>
        <br>
        <center><font size="1">Materi 10.000</font></center><br>
        <br>
        <br>
        <font><center>(<u><?php echo $viewkandidat['nama_lengkap']; ?></u>)</center></font><br>
        <font align="center">&nbsp;</font><br>
      </td>      
    </tr>
</table>
<br>
<br>
<br>
<table>
    <tr>
      <td width="20px">Tembusan.</td>
    </tr>
</table>
<table>
    <tr>
      <td width="20px">1.</td>
      <td width="200px">Pihak Buruh / Pihak kedua</td>
    </tr>
    <tr>
      <td width="20px">2.</td>
      <td width="200px">PT. Assa Logistics</td>
    </tr>
    <tr>
      <td width="20px">1.</td>
      <td width="200px">Arsip</td>
    </tr>
</table>

</body></html>
