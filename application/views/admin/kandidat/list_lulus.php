<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-check mr-2"></span>Data Kandidat Lulus</h1>
    <div class="card">
        <div class="card-header">

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered dashboard" id="kandidatLulusListTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Kandidat</th>
                            <th>Nama Lengkap</th>
                            <th>Telp</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        msg = "<?= $this->session->flashdata('message'); ?>";
        if (msg == 'success') {
            Swal.fire({
                icon: 'success',
                title: 'Data Berhasil diperbaharui'
            });
        }
        $('#kandidatLulusListTable').DataTable({
            "stateSave": true,
            "stateDuration": 60,
            dom: 'Bfrtip',
            buttons: [{
                    className: "btn btn-default",
                    text: 'Create',
                    action: function(e, dt, node, config) {
                        window.location = '<?php echo site_url('admin/kandidat/create'); ?>';
                    }
                },

                {
                    className: "btn btn-default",
                    extend: 'excelHtml5',
                    text: 'Export Excel',
                    title: 'Data Documents',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    },
                    filename: function() {
                        return 'Exl_Documents_' + d.getDate() + "_" + months[d.getMonth()] + "_" + d.getFullYear() + "_" + n;
                    },

                },

                {
                    className: "btn btn-default",
                    extend: 'pdfHtml5',
                    title: 'Data Documents',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                    text: 'Export  PDF',
                    filename: function() {
                        return 'Pdf_Documents_' + d.getDate() + "_" + months[d.getMonth()] + "_" + d.getFullYear() + "_" + n;
                    }
                }
            ],
            processing: true,
            serverSide: true,
            aaSorting: [],
            ajax: {
                url: '<?php echo base_url('admin/kandidat_lulus_data'); ?>',
                type: "POST",
                dataType: 'json'
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    orderable: false
                },
                {
                    data: 'kode_kandidat',
                    name: 'kode_kandidat'
                },
                {
                    data: 'nama_lengkap',
                    name: 'nama_lengkap'
                },
                {
                    data: 'telp',
                    name: 'telp'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'actions',
                    name: 'actions'
                }

            ],

            "oLanguage": {
                "sProcessing": "Memproses...",
                "sZeroRecords": "Tidak ada data untuk ditampilkan..."
            },
        });
    });
</script>