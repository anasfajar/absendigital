<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          BIODATA DRIVER BARU
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-10 col-sm-8 col-8">
          <dl class="row">
            <dt class="col-sm-5">Nama Kandidat</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nama_lengkap'] ?></dd>
            <dt class="col-sm-5">Hari/Tanggal Melamar</dt>
            <dd class="col-sm-7">: <?= $datakandidat['created_at'] ?></dd>
            <dt class="col-sm-5">No. Hp</dt>
            <dd class="col-sm-7">: <?= $datakandidat['telp'] ?></dd>
            <dt class="col-sm-5">Rencana Peruntukan</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nama_jobs'] ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          KELENGKAPAN DOKUMEN
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12 col-sm-8 col-8">
          <dl class="row">
            <dt class="col-sm-5">Surat Lamaran</dt>
            <dd class="col-sm-7">: Ada</dd>
            <?php foreach ($dokumen as $va) :  ?>
              <dt class="col-sm-5"><?= $va['nama_dokumen'] ?></dt>
              <?php if ($va['value'] == "1" && !empty($va['keterangan'])) { ?>
                <dd class="col-sm-7">: <b>Ada / <?= $va['keterangan'] ?></b></dd>
              <?php } elseif($va['value'] == "1"){ ?>
                <dd class="col-sm-7">: Ada</dd>
              <?php }else{ ?>
                <dd class="col-sm-7">: <b>Tidak Ada / <?= $va['keterangan'] ?></b></dd>
              <?php } ?>

            <?php endforeach; ?>

          </dl>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          HASIL TES
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-10 col-sm-8 col-8">
          <dl class="row">
            <dt class="col-sm-5">Hasil Interview</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nilai_test_hr'] ?></dd>
            <dt class="col-sm-5">Nilai Tes Pengetahuan</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nilai_test_pengetahuan'] ?></dd>
            <dt class="col-sm-5">Nilai Tes Skill</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nilai_test_skill'] ?></dd>
            <dt class="col-sm-5">Kemampuan Mengemudi</dt>
            <dd class="col-sm-7">: Wing Box</dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 mt-3" align="center">
    <button type="button" class="btn btn-success">Lulus</button>
    <button type="button" class="btn btn-warning">Belum Lulus</button>
  </div>
  <div class="col-md-12 mt-3" align="center">
  <?php 
  if($datakandidat['status_kandidat']=="1"){
    echo
    '<a href="'. base_url('lap_surat') .'?id='. $datakandidat['id'] .'" target="_blank" class="btn btn-primary"><span class="fas fa-fw fa-file"></span>Cetak Surat Penawaran Kerja</a>';
  }
   
  
  ?>
  </div>
</div>