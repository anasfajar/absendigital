<html><head>
<title>Profil Kandidat</title>
</head><body>
<div align="center">
<h3><b>DATA PROFIL PEGAWAI MBK</b></h3>
</div>
<br>
<br>
<table><tr><td><b>BIODATA PEGAWAI</b></td></tr></table>
<table>
    <tr>
      <td width="200px">NIK</td>
      <td>:</td>
      <td><?php echo $viewpgw['nik']; ?></td>
      <td rowspan="11" valign="top"><img src="assets/img/default-profile.png" style="width:20%;"></td>
    </tr>
    <tr>
      <td>Nama Lengkap</td>
      <td>:</td>
      <td><?php echo $viewpgw['nama']; ?></td>
    </tr>
    <tr>
      <td>No. KTP</td>
      <td>:</td>
      <td><?php echo $viewpgw['no_ktp']; ?></td>
    </tr>
    <tr>
      <td>Masa Berlaku</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_berlaku']; ?></td>
    </tr>
    <tr>
      <td>No Handphone</td>
      <td>:</td>
      <td><?php echo $viewpgw['nomor_hp']; ?></td>
    </tr>
    <tr>
      <td>Tempat Lahir</td>
      <td>:</td>
      <td><?php echo $viewpgw['tempat_lahir']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Lahir</td>
      <td>:</td>
      <td><?php echo $viewpgw['tglblnthn_lahir']; ?></td>
    </tr>
    <tr>
      <td>Alamat</td>
      <td>:</td>
      <td><?php echo $viewpgw['alamat']; ?></td>
    </tr>
    <tr>
      <td>Status</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_pajak']; ?></td>
    </tr>
    <tr>
      <td>Agama</td>
      <td>:</td>
      <td><?php echo $viewpgw['agama']; ?></td>
    </tr>
    <tr>
      <td>Jenis Kelamin</td>
      <td>:</td>
      <td><?php echo $viewpgw['jenis_kelamin']; ?></td>
    </tr>
    <tr>
      <td>Pendidikan Terakhir</td>
      <td>:</td>
      <td><?php echo $viewpgw['pendidikan_terakhir']; ?></td>
    </tr>
    <tr>
      <td>No Kartu Kerluarga</td>
      <td>:</td>
      <td><?php echo $viewpgw['nomor_kartu_keluarga']; ?></td>
    </tr>
    <tr>
      <td>NPWP</td>
      <td>:</td>
      <td><?php echo $viewpgw['npwp']; ?></td>
    </tr>
    <tr>
      <td>Email</td>
      <td>:</td>
      <td><?php echo $viewpgw['email_address']; ?></td>
    </tr>
    <tr>
      <td>Vendor</td>
      <td>:</td>
      <td><?php echo $viewpgw['vendor']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>KETERANGAN BANK</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Atas Nama diBank</td>
      <td>:</td>
      <td><?php echo $viewpgw['nama_di_bank']; ?></td>
    </tr>
    <tr>
      <td>Nama Bank</td>
      <td>:</td>
      <td><?php echo $viewpgw['bank']; ?></td>
    </tr>
    <tr>
      <td>No. Account</td>
      <td>:</td>
      <td><?php echo $viewpgw['no_account']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>AKTIFIASI KARYAWAN</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Keterangan Aktifasi Karyawan</td>
      <td>:</td>
      <td><?php echo $viewpgw['keterangan_aktivasi_karyawan']; ?></td>
    </tr>
    <tr>
      <td>Nomor PO / Nama Driver Yang Diganti</td>
      <td>:</td>
      <td><?php echo $viewpgw['nomor_po_nama_driver_yg_diganti']; ?></td>
    </tr>
    <tr>
      <td>Tanggal PO</td>
      <td>:</td>
      <td><?php echo $viewpgw['tanggal_po']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Aktif Kerja</td>
      <td>:</td>
      <td><?php echo $viewpgw['tgl_inaktif_kerja']; ?></td>
    </tr>
    <tr>
      <td>Jobs</td>
      <td>:</td>
      <td><?php echo $viewpgw['jobs']; ?></td>
    </tr>
    <tr>
      <td>Lead Time</td>
      <td>:</td>
      <td><?php echo $viewpgw['lead_time']; ?></td>
    </tr>
    <tr>
      <td>Operation Point</td>
      <td>:</td>
      <td><?php echo $viewpgw['operation_poin']; ?></td>
    </tr>
    <tr>
      <td>Customer</td>
      <td>:</td>
      <td><?php echo $viewpgw['customer']; ?></td>
    </tr>
    <tr>
      <td>District</td>
      <td>:</td>
      <td><?php echo $viewpgw['district']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>KLASIFIKASI SKILL</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Skill</td>
      <td>:</td>
      <td><?php echo $viewpgw['klasifikasi_skill']; ?></td>
    </tr>
    <tr>
      <td>Jenis SIM</td>
      <td>:</td>
      <td><?php echo $viewpgw['jenis_sim']; ?></td>
    </tr>
    <tr>
      <td>Nomor SIM</td>
      <td>:</td>
      <td><?php echo $viewpgw['no_sim']; ?></td>
    </tr>
    <tr>
      <td>Masa Berlaku</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_berlaku_sim']; ?></td>
    </tr>
</table>
<br>
<br>
<br>
<br>
<br>
<table><tr><td><b>KONTRAK KERJA</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Start</td>
      <td>:</td>
      <td><?php echo $viewpgw['start_kontrak']; ?></td>
    </tr>
    <tr>
      <td>Finish</td>
      <td>:</td>
      <td><?php echo $viewpgw['finish_kontrak']; ?></td>
    </tr>
    <tr>
      <td>Status Kontrak</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_kontrak']; ?></td>
    </tr>
    <tr>
      <td>Masa Berakhir</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_kontrak_berakhir']; ?></td>
    </tr>
    <tr>
      <td>Lama Kerja</td>
      <td>:</td>
      <td><?php echo $viewpgw['lama_kerja']; ?></td>
    </tr>
    <tr>
      <td>Seragam</td>
      <td>:</td>
      <td><?php echo $viewpgw['seragam']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Pengiriman Seragam</td>
      <td>:</td>
      <td><?php echo $viewpgw['tanggal_pengiriman_seragam']; ?></td>
    </tr>
    <tr>
      <td>Id Card</td>
      <td>:</td>
      <td><?php echo $viewpgw['id_card']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Pengiriman ID Card</td>
      <td>:</td>
      <td><?php echo $viewpgw['tanggal_pengiriman_id_card']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>TRAINING SAFETY DRIVING</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Keterangan Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['training_safety_driving']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Pelaksanaan Training Berakhir</td>
      <td>:</td>
      <td><?php echo $viewpgw['pelaksanaan_training_terakhir']; ?></td>
    </tr>
    <tr>
      <td>Masa Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_training']; ?></td>
    </tr>
    <tr>
      <td>Status Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_training']; ?></td>
    </tr>
    <tr>
      <td>Masa Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_training']; ?></td>
    </tr>
    <tr>
      <td>Nilai Training Safety Driving (PREE TEST)</td>
      <td>:</td>
      <td><?php echo $viewpgw['nilai_training_safety_driving_pree_test']; ?></td>
    </tr>
    <tr>
      <td>Nilai Training Safety Driving (POST TEST)</td>
      <td>:</td>
      <td><?php echo $viewpgw['nilai_training_safety_driverng_post_test']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>KETERANGAN VAKSIN PEGAWAI</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Status Vaksin Pertama</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_vaksin_pertama']; ?></td>
    </tr>
    <tr>
      <td>Mengapa Belum Vaksin Pertama</td>
      <td>:</td>
      <td><?php echo $viewpgw['mengapa_belum_vaksi_pertama']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Vaksin Pertama</td>
      <td>:</td>
      <td><?php echo $viewpgw['tanggal_vaksin_pertama']; ?></td>
    </tr>
    <tr>
      <td>Lokasi Vaksin Pertama</td>
      <td>:</td>
      <td><?php echo $viewpgw['lokasi_vaksin_pertama']; ?></td>
    </tr>
    <tr>
      <td>Status Vaksin Kedua</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_vaksin_kedua']; ?></td>
    </tr>
    <tr>
      <td>Mengapa Belum Vaksin Kedua</td>
      <td>:</td>
      <td><?php echo $viewpgw['mengapa_belum_vaksi_kedua']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Vaksin Kedua</td>
      <td>:</td>
      <td><?php echo $viewpgw['tanggal_vaksin_kedua']; ?></td>
    </tr>
    <tr>
      <td>Lokasi Vaksin Kedua</td>
      <td>:</td>
      <td><?php echo $viewpgw['lokasi_vaksin_kedua']; ?></td>
    </tr>
    <tr>
      <td>Status Vaksin Booster</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_vaksin_booster']; ?></td>
    </tr>
    <tr>
      <td>Mengapa Belum Vaksin Booster</td>
      <td>:</td>
      <td><?php echo $viewpgw['mengapa_belum_vaksin_booster']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Vaksin Booster</td>
      <td>:</td>
      <td><?php echo $viewpgw['tanggal_vaksin_booster']; ?></td>
    </tr>
    <tr>
      <td>Lokasi Vaksin Booster</td>
      <td>:</td>
      <td><?php echo $viewpgw['lokasi_vaksin_booster']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>TRAINING PEGAWAI</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Tanggal Training Online Part 1</td>
      <td>:</td>
      <td><?php echo $viewpgw['tgl_training_online_part_1']; ?></td>
    </tr>
    <tr>
      <td>Status Training Online Part 1</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_training_online_part_1']; ?></td>
    </tr>
    <tr>
      <td>Point Part 1</td>
      <td>:</td>
      <td><?php echo $viewpgw['point_part_1']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Training Online Part 2</td>
      <td>:</td>
      <td><?php echo $viewpgw['tgl_training_online_part_2']; ?></td>
    </tr>
    <tr>
      <td>Status Training Online Part 2</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_training_online_part_2']; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="300px">Point Part 2</td>
      <td>:</td>
      <td><?php echo $viewpgw['point_part_2']; ?></td>
    </tr>
    <tr>
      <td>Status Training Attitude & Knowledge</td>
      <td>:</td>
      <td><?php echo $viewpgw['tgl_training_attitude_n_knowledge']; ?></td>
    </tr>
    <tr>
      <td>Point Attitude & Knowledge</td>
      <td>:</td>
      <td><?php echo $viewpgw['status_training_attitude_n_knowledge']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>KETERANGAN BPJS, JAMSOSTEK DAN REWARD</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Nomor BPJS Kesehatan</td>
      <td>:</td>
      <td><?php echo $viewpgw['nomor_bpjs_kesehatan']; ?></td>
    </tr>
    <tr>
      <td>Keterangan Proses BPJS Kesehatan</td>
      <td>:</td>
      <td><?php echo $viewpgw['keterangan_proses_bpjs_kesehatan']; ?></td>
    </tr>
    <tr>
      <td>Keterangan Status BPJS Kesehatan</td>
      <td>:</td>
      <td><?php echo $viewpgw['keterangan_status_bpjs_kesehatan']; ?></td>
    </tr>
    <tr>
      <td>Nomor JAMSOSTEK</td>
      <td>:</td>
      <td><?php echo $viewpgw['nomor_jamsostek']; ?></td>
    </tr>
    <tr>
      <td>Keterangan JAMSOSTEK</td>
      <td>:</td>
      <td><?php echo $viewpgw['keterangan_jamsostek']; ?></td>
    </tr>
    <tr>
      <td>Reward</td>
      <td>:</td>
      <td><?php echo $viewpgw['reward']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Pemberian Reward</td>
      <td>:</td>
      <td><?php echo $viewpgw['tglblnthn_pemberian_reward']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>ACCIDENT</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Accident Report</td>
      <td>:</td>
      <td><?php echo $viewpgw['accident_report']; ?></td>
    </tr>
    <tr>
      <td width="300px">Tanggal Accident</td>
      <td>:</td>
      <td><?php echo $viewpgw['tglblnthn_accident']; ?></td>
    </tr>
    <tr>
      <td>Case</td>
      <td>:</td>
      <td><?php echo $viewpgw['kasus']; ?></td>
    </tr>
    <tr>
      <td>Tanggal Case</td>
      <td>:</td>
      <td><?php echo $viewpgw['tglblnthn_kasus']; ?></td>
    </tr>
    <tr>
      <td>SP 1</td>
      <td>:</td>
      <td><?php echo $viewpgw['sp_i']; ?></td>
    </tr>
    <tr>
      <td>SP 2</td>
      <td>:</td>
      <td><?php echo $viewpgw['sp_ii']; ?></td>
    </tr>
    <tr>
      <td>SP 3</td>
      <td>:</td>
      <td><?php echo $viewpgw['sp_iii']; ?></td>
    </tr>
    <tr>
      <td>Keterangan Out & Mutasi</td>
      <td>:</td>
      <td><?php echo $viewpgw['keterangan_out_dan_mutasi']; ?></td>
    </tr>
</table>
<br>
<br>
<table><tr><td><b>TRAINING</b></td></tr></table>
<table border="0">
    <tr>
      <td width="300px">Masa Berlaku SIM</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_berlaku_sim_2']; ?></td>
    </tr>
    <tr>
      <td>Masa Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['masa_training_2']; ?></td>
    </tr>
    <tr>
      <td>Ketarangan Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['keterangan_training_2']; ?></td>
    </tr>
    <tr>
      <td>Jadwal Training</td>
      <td>:</td>
      <td><?php echo $viewpgw['jadwal_training_2']; ?></td>
    </tr>
    <tr>
      <td>Driver Leader</td>
      <td>:</td>
      <td><?php echo $viewpgw['driver_leader']; ?></td>
    </tr>
    <tr>
      <td>Jabatan</td>
      <td>:</td>
      <td><?php echo $viewpgw['jabatan']; ?></td>
    </tr>
</table>
</body></html>
