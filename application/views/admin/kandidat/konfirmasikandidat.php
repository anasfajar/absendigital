<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          HUBUNGI APPOINTMENT
        </button>
      </h2>
    </div>
    <?php 

//ubah timezone menjadi jakarta
date_default_timezone_set("Asia/Jakarta");
//ambil jam dan menit
$jam = date('H:i');
//atur salam menggunakan IF
if ($jam > '05:30' && $jam < '10:00') {
    $salam = 'Pagi';
} elseif ($jam >= '10:00' && $jam < '15:00') {
    $salam = 'Siang';
} elseif ($jam < '18:00') {
    $salam = 'Sore';
} else {
    $salam = 'Malam';
}

?>
    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12 col-sm-12 col-12">
          <dl class="row">

          <dt class="col-sm-12">
          <?php 
          $telepon = substr($datakandidat['telp'], 0);
          $text = "Selamat%20" . $salam . ",%0AKami%20dari%20PT%20MBK%20ingin%20menindaklanjuti%20proses%20recruitment%20saudara%20*" . $datakandidat['nama_lengkap'] . "*,%0Aapakah%20bisa%20di%20adakan%20pertemuan%20di%20minggu%20ini%20di%20kantor%20kami%20?"; ?>
          <?php echo '<a href="https://api.whatsapp.com/send?phone=62' . $telepon . '&text=' . $text . '" target="_blank"class="btn btn-primary btn-sm">WA Appointment</a>'; ?>
          
          </br>
          </br>
          <form action="<?= site_url('admin/update_appointment') ?>" method="POST" enctype="multipart/form-data">        
          <div class="form-group">
            <input type="hidden" name="id" class="form-control" value="<?= $datakandidat['id'] ?>">
            <input type="text" name="ket_appointment" class="form-control" value="<?= $datakandidat['ket_appointment'] ?>" >
            <small id="emailHelp" class="form-text text-muted">Silahkan isi dengan keterangan balasan whatsapp</small>
          </div>
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" <?php if ($datakandidat['appointment'] == "1") echo "checked";?> value="1" name="appointment" >
            <label class="form-check-label" for="exampleCheck1">ceklis jika sudah mengirim pesan!</label>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
        </form>
        </dt>

          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        HUBUNGI LIST DOKUMEN
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12 col-sm-8 col-8">
          <dl class="row">
            
          <dt class="col-sm-12">
          <?php
          $teleponn = substr($datakandidat['telp'], 0);
          $textt = "Selamat%20" . $salam . ",%0AKami%20dari%20PT.MBK%20terkait%20lanjutan%20proses%20rekruitment dimohon%20saudara/i%20*" . $datakandidat['nama_lengkap'] . "*%20untuk%20membawa%20peryaratan%20sebagai%20berikut%20:%0A1.Surat%20Lamaran%0A2.Daftar%20Riwayat%20Hidup%0A3.FC%20KTP%20Kandidat%0A4.FC%20KTP%20Istri/Suami%0A5.FC%20SIM%0A6.FC%20KK%0A7.FC%20NPWP%0A8.SKCK%0A9.FC%20Buku%20Rekening%0A10.FC%20Ijasah%0A11.SKBN%0A12.Surat%20Keterangan%20Domisili%0A13.Sertifikat%20Vaksin%0A%0ATerimakasih."; ?>
          <?php echo '<a href="https://api.whatsapp.com/send?phone=62' . $teleponn . '&text=' . $textt . '" target="_blank"class="btn btn-primary btn-sm">WA List Dokumen</a>'; ?>
          
          </br>
          </br>
          <form action="<?= site_url('admin/update_listdokument') ?>" method="POST" enctype="multipart/form-data">          
          <div class="form-group">
            <input type="hidden" name="id" class="form-control" value="<?= $datakandidat['id'] ?>">
            <input type="text" name="ket_list_dokumen" class="form-control" value="<?= $datakandidat['ket_list_dokumen'] ?>">
            <small id="emailHelp" class="form-text text-muted">Silahkan isi dengan keterangan balasan whatsapp</small>
          </div>
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" value="1" <?php if ($datakandidat['list_dokumen'] == "1") echo "checked";?> name="list_dokumen" >
            <label class="form-check-label" for="exampleCheck1">ceklis jika sudah mengirim pesan!</label>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
        </form>
        </dt>

          </dl>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        HUBUNGI KODE LOGIN TEST
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-10 col-sm-8 col-8">
          <dl class="row">
            
          <dt class="col-sm-12">
          <?php
          $telepo = substr($datakandidat['telp'], 0);
          $tex = "Selamat%20" . $salam . "%2C%0AKami%20dari%20%2APT.MBK%2A%20terkait%20tahapan%20selanjutnya%20dimohon%20saudara%2Fi%20" . $datakandidat['nama_lengkap'] . "%20untuk%20melaksanakan%20%2ATES%20ONLINE%2A%20dengan%20klik%20%20link%20dibawah%20ini%20%3A%0Ahttp%3A%2F%2Flocalhost%2Fabsendigital%2Fpublic%2Flogin-kandidat%0ASelanjutnya%20dimohon%20untuk%20calon%20kandidat%20login%20dengan%20menggunakan%20akun%20dibawah%20ini%20%3A%0ANomor%20Handphone%20%3A%20%2A" . $datakandidat['telp'] . "%2A%0APassword%20%3A%20%2A" . $datakandidat['kode_kandidat'] . "%2A%0A%0ATerimakasih%0A%0A%2ACatatan%2A%20%3A%20mohon%20untuk%20tidak%20menyebarluaskan%20pesan%20ini.";
          //$tex = "Selamat%20,%0Akami%20dari%20PT.MBK%20terkait%20tahapan%20selanjutnya%20dimohon%20saudara/i%20*" . $datakandidat['nama_lengkap'] . "*%20untuk%20melaksanakan%20*TES ONLINE*%20dengan%20klik%20 tautan%20:%20http%3A%2F%2Flocalhost%2Fabsendigital%2Fpublic%2Flogin-kandidat%20,dengan%20menggunakan%20akun%0ANomor%20Handphone%20:%20*" . $datakandidat['telp'] . "*%0APassword%20:%20*" . $datakandidat['kode_kandidat'] . "*%0A%0ATerimakasih"; ?>
          <?php echo '<a href="https://api.whatsapp.com/send?phone=62' . $telepo . '&text=' . $tex . '" target="_blank"class="btn btn-primary btn-sm">WA Logi Test</a>'; ?>
          </br>
          </br>
          <form action="<?= site_url('admin/update_login_test') ?>" method="POST" enctype="multipart/form-data">          
          <div class="form-group">
            <input type="hidden" name="id" class="form-control" value="<?= $datakandidat['id'] ?>">
            <input type="text" name="ket_login_test" class="form-control" value="<?= $datakandidat['ket_login_test'] ?>" >
            <small id="emailHelp" class="form-text text-muted">Silahkan isi dengan keterangan balasan whatsapp</small>
          </div>
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" value="1" <?php if ($datakandidat['login_test'] == "1") echo "checked";?> name="login_test" >
            <label class="form-check-label" for="exampleCheck1">ceklis jika sudah mengirim pesan!</label>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
        </form>
        </dt>

          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingFour">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
        HUBUNGI MELENGKAPI DATA
        </button>
      </h2>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-10 col-sm-8 col-8">
          <dl class="row">
            
          <dt class="col-sm-12">
          <?php
          $tele = substr($datakandidat['telp'], 0);
          $texa = "Selamat%20" . $salam . "%2C%20%0AKami%20dari%20%2APT.MBK%2A%20terkait%20tahapan%20selanjutnya%20dimohon%20saudara%2Fi%20%2A" . $datakandidat['nama_lengkap'] . "%2A%20untuk%20Melengkapi%20Biodata%20dengan%20klik%20%20link%20dibawah%20ini%20%3A%20%0Ahttp%3A%2F%2Flocalhost%2Fabsendigital%2Fpublic%2Flogin-kandidat%20%0ASelanjutnya%20dimohon%20calon%20kandidat%20login%20dengan%20menggunakan%20akun%20dibawah%20ini%20%3A%0ANomor%20handphone%20%3A%20%2A" . $datakandidat['telp'] . "%2A%0APassword%20%3A%20%2A" . $datakandidat['kode_kandidat'] . "%2A%0A%0ATerimakasih%0A%0A%2ACatatan%2A%20%3A%20mohon%20untuk%20tidak%20menyebarluaskan%20pesan%20ini."; ?>
          <?php echo '<a href="https://api.whatsapp.com/send?phone=62' . $tele . '&text=' . $texa . '" target="_blank"class="btn btn-primary btn-sm">WA Melengkapi Data</a>'; ?>
          </br>
          </br>
          <form action="<?= site_url('admin/update_lengkapi_data') ?>" method="POST" enctype="multipart/form-data">          
          <div class="form-group">
            <input type="hidden" name="id" class="form-control" value="<?= $datakandidat['id'] ?>">
            <input type="text" name="ket_lengkapi_data" class="form-control" value="<?= $datakandidat['ket_lengkapi_data'] ?>" >
            <small id="emailHelp" class="form-text text-muted">Silahkan isi dengan keterangan balasan whatsapp</small>
          </div>
          <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" value="1" <?php if ($datakandidat['lengkapi_data'] == "1") echo "checked";?> name="lengkapi_data" >
            <label class="form-check-label" for="exampleCheck1">ceklis jika sudah mengirim pesan!</label>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
        </form>
        </dt>

          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 mt-3" align="center">
  </div>
</div>