</main>
<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
            </div>
            <div class="text-muted">
                Page rendered in <strong>{elapsed_time}</strong> detik.
            </div>
        </div>
    </div>
</footer>

<script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
<script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap-toggle-master/js/bootstrap4-toggle.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/jonthornton-jquery-timepicker/jquery.timepicker.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js" charset="UTF-8"></script>
<script src="<?= base_url('assets'); ?>/vendor/moment/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<script type="text/javascript">
    <?php if ($this->session->flashdata('success')) { ?>
        toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php } else if ($this->session->flashdata('error')) {  ?>
        toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php } else if ($this->session->flashdata('warning')) {  ?>
        toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php } else if ($this->session->flashdata('info')) {  ?>
        toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var d = new Date();
        var n = d.getTime();
        var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        $('#kandidatListTable').DataTable({
            "stateSave": true,
            "stateDuration": 60,
            dom: 'Bfrtip',
            buttons: [{
                    className: "btn btn-default",
                    text: 'Create',
                    action: function(e, dt, node, config) {
                        window.location = '<?php echo site_url('admin/kandidat/create'); ?>';
                    }
                },

                {
                    className: "btn btn-default",
                    extend: 'excelHtml5',
                    text: 'Export Excel',
                    title: 'Data Documents',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                    },
                    filename: function() {
                        return 'Exl_Documents_' + d.getDate() + "_" + months[d.getMonth()] + "_" + d.getFullYear() + "_" + n;
                    },

                },

                {
                    className: "btn btn-default",
                    extend: 'pdfHtml5',
                    title: 'Data Documents',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    },
                    text: 'Export  PDF',
                    filename: function() {
                        return 'Pdf_Documents_' + d.getDate() + "_" + months[d.getMonth()] + "_" + d.getFullYear() + "_" + n;
                    }
                }
            ],
            processing: true,
            serverSide: true,
            aaSorting: [],
            ajax: {
                url: '<?php echo base_url('admin/kandidat_data'); ?>',
                type: "POST",
                dataType: 'json'
            },
            columns: [{
                    data: 'no',
                    name: 'no',
                    orderable: false
                },
                {
                    data: 'kode_kandidat',
                    name: 'kode_kandidat'
                },
                {
                    data: 'nama_lengkap',
                    name: 'nama_lengkap'
                },
                {
                    data: 'telp',
                    name: 'telp'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'actions',
                    name: 'actions'
                }

            ],

            "oLanguage": {
                "sProcessing": "Memproses...",
                "sZeroRecords": "Tidak ada data untuk ditampilkan..."
            },
        });

        $(".logout").click(function(event) {
            $.ajax({
                type: "POST",
                url: "<?= base_url('ajax/logoutajax'); ?>",
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Logging Out",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Logout',
                        text: 'Anda Telah Keluar!',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    location.reload();
                }
            });
            event.preventDefault();
        });

    });

        
</script>

</body>

</html>