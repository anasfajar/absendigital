<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" />
<div class="container-fluid mt-2">
    <div class="card">
        <div class="card-header">
            <h3><span class="fas fa-user-check mr-2"></span>Data Kandidat</h3>
        </div>
        <form action="<?= site_url('admin/update_kandidat') ?>" method="POST" enctype="multipart/form-data">
            <div class="card-body">
                <div class="form-group row">
                    <input class="form-control py-4" name="id" type="text" placeholder="contoh : Stephani Azhari" value="<?= $data->id; ?>" hidden />
                    <div class="col-lg-6 mb-3">
                        <label for="nama_lengkap">Nama Lengkap</label>
                        <input class="form-control py-4" name="nama_lengkap" id="nama_lengkap" type="text" placeholder="contoh : Stephani Azhari" value="<?= $data->nama_lengkap; ?>" required />
                    </div>

                    <div class="col-lg-6 mb-3">
                        <label for="ktp">Nomor KTP</label>
                        <input class="form-control py-4" name="ktp" id="ktp" type="text" placeholder="contoh : 341189XXX" value="<?= $data->ktp; ?>" required />
                    </div>

                    <div class="col-lg-6 mb-3">
                        <label for="no_telp">Nomor Telepon / HP</label>
                        <input class="form-control py-4" name="no_telp" id="no_telp" type="text" placeholder="contoh : 08139xxx" value="<?= $data->telp; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label for="email">Alamat Email</label>
                        <input class="form-control py-4" name="email" id="email" type="email" placeholder="contoh : muliabintangkejora@gmail.com" value="<?= $data->email; ?>" required />
                    </div>

                    <div class="col-lg-6 mb-3">
                        <label for="pekerjaan_sebelumnya">Pekerjaan Sebelumnya</label>
                        <input class="form-control py-4" name="pekerjaan_sebelumnya" id="pekerjaan_sebelumnya" type="text" placeholder="contoh : Driver" value="<?= $data->pekerjaan_sebelumnya; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label for="pekerjaan_sebelumnya">Perusahaan Sebelumnya</label>
                        <input class="form-control py-4" name="perusahaan_sebelumnya" id="perusahaan_sebelumnya" type="text" placeholder="contoh : PT. xxxx" value="<?= $data->perusahaan_sebelumnya; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label for="lama_bekerja">Lama Bekerja</label>
                        <select name="lama_bekerja" id="lama_bekerja" class="form-control" value="<?= $data->lama_bekerja; ?>">
                            <option value="">Pilih</option>
                            <option value="1">Kurang dari 1 Tahun</option>
                            <option value="2">1 sampai 3 Tahun</option>
                            <option value="3">Lebih dari 5 Tahun</option>
                            <option value="4">LEbih dari 10 Tahun</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                        <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control" value="<?= $data->pendidikan_terakhir; ?>">
                            <option value="">Pilih</option>
                            <option value="1">SMP</option>
                            <option value="2">SMA / SMK</option>
                            <option value="3">Diploma</option>
                            <option value="4">S1</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label for="jobs">Melamar Sebagai :</label>
                        <select name="jobs" id="jobs" class="form-control" value="<?= $data->jobs; ?>">
                            <option value="">Pilih</option>
                            <option value="1">Driver</option>
                            <option value="2">Admin</option>
                            <option value="3">IT</option>
                            <option value="4">Checker</option>
                            <option value="5">Dispatcher</option>
                            <option value="6">Monitoring GPS</option>
                            <option value="7">Operationg Cashier</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label for="sim">Surat Izin Mengemudi</label>
                        <select name="sim" id="sim" class="form-control" value="<?= $data->sim; ?>">
                            <option value="">Pilih</option>
                            <option value="1">SIM A</option>
                            <option value="2">SIM B1</option>
                            <option value="3">SIM B2</option>
                            <option value="4">SIM C</option>
                            <option value="5">SIM A Umum</option>
                            <option value="6">SIM B1 Umum</option>
                            <option value="7">SIM B2 Umum</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Tempat Lahir</label>
                        <input class="form-control py-4" name="tempat_lahir" id="tempat_lahir" type="text" placeholder="contoh : Karawang" value="<?= $detail->tempat_lahir;; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Tanggal Lahir</label>
                        <input class="form-control py-4" name="tanggal_lahir" id="tanggal_lahir" type="text" placeholder="contoh : 30-12-1990" value="<?= $detail->tanggal_lahir; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Alamat Sesuai KTP</label>
                        <textarea name="alamat_ktp" id="alamat_ktp" class="form-control" alamat_ktp="10" rows="alamat_ktp"></textarea>

                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Alamat Domisili</label>
                        <textarea name="alamat_domisili" id="alamat_domisili" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>NPWP</label>
                        <input class="form-control py-4" name="npwp" id="npwp" type="text" placeholder="contoh : 99.999.999.9-999.999" value="<?= $detail->npwp; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor SIM</label>
                        <input class="form-control py-4" name="nomor_sim" id="nomor_sim" type="text" placeholder="contoh : 9999-9999-999999" value="<?= $detail->nomor_sim; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Masa Berlaku SIM</label>
                        <input class="form-control py-4" name="masa_berlaku_sim" id="masa_berlaku_sim" type="text" placeholder="contoh : 30-12-2024" value="<?= $detail->masa_berlaku_sim; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor KK</label>
                        <input class="form-control py-4" name="nomor_kk" id="nomor_kk" type="text" placeholder="contoh : 999999xxxx" value="<?= $detail->nomor_kk; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Status Perkawinan</label>
                        <select name="status_kawin" id="status_kawin" class="form-control">
                            <option value="L">Lajang</option>
                            <option value="K0">Kawin</option>
                            <option value="K1">Kawin Anak 1</option>
                            <option value="K2">Kawin Anak 2</option>
                            <option value="K3">Kawin Anak 3</option>
                            <option value="C0">Duda / Janda </option>
                            <option value="C1">Duda / Janda Anak 1</option>
                            <option value="C2">Duda / Janda Anak 2</option>
                            <option value="C3">Duda / Janda Anak 3</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Jumlah Anak</label>
                        <input class="form-control py-4" name="jumlah_anak" id="jumlah_anak" type="text" placeholder="contoh : 2" value="<?= $detail->jumlah_anak; ?>" required />
                    </div>

                    <div id="anak1" class=""> </div>
                    <div id="lahirAnak1" class=""> </div>
                    <div id="anak2" class=""> </div>
                    <div id="lahirAnak2" class=""> </div>
                    <div id="anak3" class=""> </div>
                    <div id="lahirAnak3" class=""> </div>
                    <div id="pasangannya" class=""> </div>
                    <div id="lahirPasangan" class=""> </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nama Ibu Kandung</label>
                        <input class="form-control py-4" name="nama_ibu" id="nama_ibu" type="text" placeholder="contoh : Kartini" value="<?= $detail->nama_ibu; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Agama</label>
                        <select name="agama" id="agama" class="form-control">
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Bank</label>
                        <input class="form-control py-4" name="bank" id="bank" type="text" placeholder="contoh : Bank BRI" value="<?= $detail->bank; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nama Akun Bank</label>
                        <input class="form-control py-4" name="nama_akun_bank" id="nama_akun_bank" type="text" placeholder="contoh : Stephani Azhari" value="<?= $detail->nama_akun_bank; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor Rekening</label>
                        <input class="form-control py-4" name="no_rekening" id="no_rekening" type="text" placeholder="contoh : 9999xxx" value="<?= $detail->no_rekening; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Status Vaksin</label>
                        <select name="status_vaksin" id="status_vaksin" class="form-control">
                            <option value="">Pilih Status Vaksin</option>
                            <option value="1">Vaksin ke-1</option>
                            <option value="2">Vaksin ke-2</option>
                            <option value="3">Vaksin ke-3</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor BPJS</label><input class="form-control py-4" name="no_bpjs" id="no_bpjs" type="text" placeholder="contoh : 99999xxx" value="<?= $detail->no_bpjs; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor Jamsostek</label>
                        <input class="form-control py-4" name="no_jamsostek" id="no_jamsostek" type="text" placeholder="contoh : 99999xxx" value="<?= $detail->no_jamsostek; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor SKBN</label>
                        <input class="form-control py-4" name="no_skbn" id="no_skbn" type="text" maxlength="16" placeholder="contoh : 99999xxx" value="<?= $detail->no_skbn; ?>" required />
                        <small class='text-danger pl-3 notSkbn'></small>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Nomor PBI</label>
                        <input class="form-control py-4" name="no_pbi" id="no_pbi" type="text" placeholder="contoh : 99999xxx" value="<?= $detail->no_pbi; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Ukuran Seragam Baju</label>
                        <input class="form-control py-4" name="ukuran_baju" id="ukuran_baju" type="text" placeholder="contoh : S/M/L/XL/XXL" value="<?= $detail->ukuran_baju; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label>Ukuran Seragam Celana</label>
                        <input class="form-control py-4" name="ukuran_celana" id="ukuran_celana" type="text" placeholder="contoh : 40" value="<?= $detail->ukuran_celana; ?>" required />
                    </div>
                    <div class="col-lg-6 mb-3 ukuran_sepatu">
                        <label>Ukuran Seragam Sepatu</label>
                        <input class="form-control py-4" name="ukuran_sepatu" id="ukuran_sepatu" type="text" placeholder="contoh : 38" value="<?= $detail->ukuran_sepatu; ?>" required>
                    </div>

                    <div class="col-lg-6 mb-3">
                        <label>Foto</label>
                        <input type="file" name="filefoto" class="dropify" data-max-file-size="3M" data-height="200" data-default-file="<?= base_url('storage/kandidat/') . $detail->foto; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group d-flex justify-content-center  "><button type="submit" class="btn btn-primary"><span class="fas fa-fw fa-save mr-2"></span>Simpan</button></div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script>
    $('.dropify').dropify({
        messages: {
            default: 'Drag atau drop untuk memilih gambar',
            replace: 'Ganti',
            remove: 'Hapus',
            error: 'error'
        }
    });
    $(document).ready(function() {

        $.get("<?= site_url('Registrasi/getDataAgama') ?>", function(res) {
            result = JSON.parse(res);
            console.log(result);
            htAg = "<option value=''>Pilih Agama</option>";
            for (let ii = 0; ii < result.length; ii++) {
                htAg += "<option value='" + result[ii].id + "'>" + result[ii].nama_agama + "</option>";
            }
            $("#agama").html(htAg);
            $("#agama").val("<?= $detail->agama; ?>");
        });
        $("#no_skbn").on('keyup', function() {
            if (this.value.length != 16) {
                $(".notSkbn").html('Nomor SKBN Harus 16 Digit');
            } else {
                $(".notSkbn").html('');
            }
        });

        $("#status_kawin").on('change', function() {
            cek_status(this.value);
        });

        resData = JSON.parse('<?= json_encode($data); ?>');
        resDetail = JSON.parse('<?= json_encode($detail); ?>');
        $("#lama_bekerja").val(resData.lama_bekerja);
        $("#pendidikan_terakhir").val(resData.pendidikan_terakhir);
        $("#jobs").val(resData.jobs);
        $("#sim").val(resData.sim);
        $("#alamat_ktp").val(resDetail.alamat_ktp);
        $("#alamat_domisili").val(resDetail.alamat_domisili);
        $("#status_kawin").val(resDetail.status_kawin);
        $("#status_vaksin").val(resDetail.status_vaksin);
        $("#jenis_kelamin").val(resDetail.jenis_kelamin);
        cek_status(resDetail.status_kawin);
        console.log(resData);

        function cek_status(stat) {
            var ank1 = '';
            var ank2 = '';
            var ank3 = '';
            var lhrank1 = '';
            var lhrank2 = '';
            var lhrank3 = '';
            var psg = '';
            var lhrpsg = '';
            $("#anak1").removeClass('col-lg-6 mb-3');
            $("#anak2").removeClass('col-lg-6 mb-3');
            $("#anak3").removeClass('col-lg-6 mb-3');
            $("#lahirAnak1").removeClass('col-lg-6 mb-3');
            $("#lahirAnak2").removeClass('col-lg-6 mb-3');
            $("#lahirAnak3").removeClass('col-lg-6 mb-3');
            if (stat == 'K1' || stat == 'C1') {
                ank1 = "<label>Nama Anak 1</label>" +
                    "<input class='form-control py-4' name='anak_1' type='text' placeholder='' value='<?= $detail->anak1; ?>' required> ";
                lhrank1 = "<label>Tanggal Lahir Nama Anak 1</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_anak1' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_anak1); ?>' required> ";
                $("#anak1").addClass('col-lg-6 mb-3');
                $("#lahirAnak1").addClass('col-lg-6 mb-3');
            } else if (stat == 'K2' || stat == 'C2') {
                ank1 = "<label>Nama Anak 1</label>" +
                    "<input class='form-control py-4' name='anak_1' type='text' placeholder='' value='<?= $detail->anak1; ?>' required> ";
                ank2 = "<label>Nama Anak 2</label>" +
                    "<input class='form-control py-4' name='anak_2' type='text' placeholder='' value='<?= $detail->anak2; ?>' required> ";
                lhrank1 = "<label>Tanggal Lahir Nama Anak 1</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_anak1' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_anak1); ?>' required> ";
                lhrank2 = "<label>Tanggal Lahir Nama Anak 2</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_anak2' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_anak2); ?>' required> ";
                $("#anak1").addClass('col-lg-6 mb-3');
                $("#anak2").addClass('col-lg-6 mb-3');
                $("#lahirAnak1").addClass('col-lg-6 mb-3');
                $("#lahirAnak2").addClass('col-lg-6 mb-3');
            } else if (stat == 'K3' || stat == 'C3') {
                ank1 = "<label>Nama Anak 1</label>" +
                    "<input class='form-control py-4' name='anak_1' type='text' placeholder='' value='<?= $detail->anak1; ?>' required> ";
                ank2 = "<label>Nama Anak 2</label>" +
                    "<input class='form-control py-4' name='anak_2' type='text' placeholder='' value='<?= $detail->anak2; ?>' required> ";
                ank3 = "<label>Nama Anak 3</label>" +
                    "<input class='form-control py-4' name='anak_3' type='text' placeholder='' value='<?= $detail->anak3; ?>' required> ";
                lhrank1 = "<label>Tanggal Lahir Nama Anak 1</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_anak1' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_anak1); ?>' required> ";
                lhrank2 = "<label>Tanggal Lahir Nama Anak 2</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_anak2' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_anak2); ?>' required> ";
                lhrank3 = "<label>Tanggal Lahir Nama Anak 3</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_anak3' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_anak3); ?>' required> ";
                $("#anak1").addClass('col-lg-6 mb-3');
                $("#anak2").addClass('col-lg-6 mb-3');
                $("#anak3").addClass('col-lg-6 mb-3');
                $("#lahirAnak1").addClass('col-lg-6 mb-3');
                $("#lahirAnak2").addClass('col-lg-6 mb-3');
                $("#lahirAnak3").addClass('col-lg-6 mb-3');

            }
            if (stat == 'K0' || stat == 'K1' || stat == 'K2' || stat == 'K3') {
                psg = "<label>Nama Suami / Istri</label>" +
                    "<input class='form-control py-4' name='pasangan' id='pasangan' type='text' placeholder='contoh : Juliet' value='<?= $detail->pasangan; ?>' required> ";
                lhrpsg = "<label>Tanggal Lahir Suami / Istri</label>" +
                    "<input class='form-control py-4' name='tgl_lahir_pasangan' id='tgl_lahir_pasangan' type='text' placeholder='contoh : 31-12-1990' value='<?= date_simple($detail->tgl_lahir_pasangan); ?>' required> ";
                $("#pasangannya").addClass('col-lg-6 mb-3');
                $("#lahirPasangan").addClass('col-lg-6 mb-3');
            }
            $("#anak1").html(ank1);
            $("#anak2").html(ank2);
            $("#anak3").html(ank3);
            $("#lahirAnak1").html(lhrank1);
            $("#lahirAnak2").html(lhrank2);
            $("#lahirAnak3").html(lhrank3);
            $("#pasangannya").html(psg);
            $("#lahirPasangan").html(lhrpsg);
        }

    });
</script>