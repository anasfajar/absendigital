<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-check mr-2"></span>Data Kandidat Pegawai</h1>
    <div class="card">
        <div class="card-header">

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered dashboard" id="kandidatListTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Kandidat</th> 
                            <th>Nama Lengkap</th> 
                            <th>Telp</th> 
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal View Kandidat -->
<div class="modal fade" id="viewkandidatmodal" tabindex="-1" role="dialog" aria-labelledby="viewpegawaimodal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="viewpegawaimodallabel"><span class="fas fa-user-tie mr-1"></span>FORM ASSESMENT DRIVER BARU</h5>
            </div>
            <div class="modal-body">
                <div id="viewdatakandidat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Tutup</button>
                
            </div>
        </div>
    </div>
</div>

<!-- Modal View Kandidat -->
<div class="modal fade" id="viewwamodal" tabindex="-1" role="dialog" aria-labelledby="viewpegawaimodal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="viewpegawaimodallabel"><span class="fas fa-user-tie mr-1"></span>FORM KONFIRMASI KANDIDAT</h5>
            </div>
            <div class="modal-body">
                <div id="viewdatakonfirmasi"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Tutup</button>
                
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $("body").on("click", "#detailkandidat", function(e) {
        e.preventDefault();
        var pgw_id = $(e.currentTarget).attr('data-kandidat-id');
        if (pgw_id === '') return;
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/datakandidat?type=viewkandidat'); ?>',
            data: {
                pgw_id: pgw_id
            },
            beforeSend: function() {
                swal.fire({
                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                    title: "Mempersiapkan Preview User",
                    text: "Please wait",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            success: function(data) {
                swal.close();
                $('#viewkandidatmodal').modal('show');
                $('#viewdatakandidat').html(data);

            },
            error: function() {
                swal.fire("Preview Pegawai Gagal", "Ada Kesalahan Saat menampilkan data pegawai!", "error");
            }
        });
    });
</script>

<script>
    $("body").on("click", "#whatsapp", function(e) {
        e.preventDefault();
        var pgw_id = $(e.currentTarget).attr('data-wa-id');
        if (pgw_id === '') return;
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/datakandidat?type=wakandidat'); ?>',
            data: {
                pgw_id: pgw_id
            },
            beforeSend: function() {
                swal.fire({
                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                    title: "Mempersiapkan Preview Data Konfirmasi",
                    text: "Please wait",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            success: function(data) {
                swal.close();
                $('#viewwamodal').modal('show');
                $('#viewdatakonfirmasi').html(data);

            },
            error: function() {
                swal.fire("Preview Pegawai Gagal", "Ada Kesalahan Saat menampilkan data pegawai!", "error");
            }
        });
    });
</script>