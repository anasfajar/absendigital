<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-check mr-2"></span>Absensi Pegawai</h1>
    <div class="card">
        <div class="card-header">
            <div class="float-right d-inline">
                <?php if ($this->session->userdata('role_id') == 1) : ?>
                    <div class="btn btn-danger" id="clear-absensi"><span class="fas fa-trash mr-1"></span>Clear All</div>
                <?php endif; ?>
                <!-- <a class="btn btn-success" href="<?= base_url('export'); ?>"><span class="fas fa-file mr-1"></span>Export Absensi</a> -->
                <!-- <div class="btn btn-primary" id="refresh-tabel-absensi"><span class="fas fa-sync-alt mr-1"></span>Refresh Tabel</div> -->
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <?php echo $this->session->flashdata('message'); ?>
                <table class="table table-bordered table-striped dashboard" id="list-absensi-all" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>JOBS</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;foreach($dpAbsen as $row){?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $row->nama;?></td>
                            <td><?php 
                                if(!empty($row->driver)){
                                    echo $row->driver;
                                }elseif(!empty($row->checker)){
                                    echo $row->checker;
                                }elseif(!empty($row->non_driver)){
                                    echo $row->non_driver;
                                }elseif(!empty($row->fico)){
                                    echo $row->fico;
                                }elseif(!empty($row->helper)){
                                    echo $row->helper;
                                };
                            ?></td>
                            <td><?php echo (empty($row->kode)) ? '-' : strtoupper($row->kode) ;?></td>
                            <td class="text-center"><button class="btn btn-primary" onclick=hadir(<?php echo $row->id.',"'.str_replace(' ','_',$row->nama).'","'.$row->nik.'"';?>)>Hadir</button>&nbsp<button class="btn btn-warning" onclick=aksilain(<?php echo $row->id.',"'.str_replace(' ','_',$row->nama).'","'.$row->nik.'"';?>) >Aksi Lain</button></td>
                        </tr>
                        <?php }?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>JOBS</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal View Absen -->
<div class="modal fade" id="viewabsensimodal" tabindex="-1" role="dialog" aria-labelledby="viewabsensimodal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="viewabsensimodallabel"><span class="fas fa-clock mr-1"></span>Absensi Lain</h5>
            </div>
            <div class="modal-body">
                <div id="aksilain">
                    <form action="admin/simpanAbsen" method="POST" id='formsimpanAbsen'>
                        <input class="form-control mb-1" type="hidden" id="idAbsen" name="idAbsen">
                        <input class="form-control mb-1" type="text" id="namaAbsen" name="namaAbsen" readonly>
                        <input class="form-control mb-1" type="hidden" id="nik" name="nik">
                        <select class="form-control mb-1" name="opsi" id="opsi" id="opsi" required>
                            <option value="">Pilih Status</option>
                            <option value="izin">IZIN</option>
                            <option value="sakit">SAKIT</option>
                            <option value="cuti">CUTI</option>
                        </select>
                        <textarea class="form-control mb-1" name="keterangan" id="keterangan" placeholder="isikan keterangan Izin/Cuti/Sakit" name="keterangan" cols="30" rows="3" required></textarea>
                        <input class="form-control mb-1 btn-danger" type="button" id="btnAksilain" value="Submit">
                        <input class="form-control mb-1 btn-danger d-none" type="submit" id="btnSubmit" value="Submit">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Tutup</button>
            </div>
        </div>
    </div>
</div>

</main>
<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
            </div>
            <div class="text-muted">
                Page rendered in <strong>{elapsed_time}</strong> detik.
            </div>
        </div>
    </div>
</footer>

<script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
<script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap-toggle-master/js/bootstrap4-toggle.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/jonthornton-jquery-timepicker/jquery.timepicker.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets'); ?>/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js" charset="UTF-8"></script>
<script src="<?= base_url('assets'); ?>/vendor/moment/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

<script type="text/javascript">
    <?php if ($this->session->flashdata('success')) { ?>
        toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php } else if ($this->session->flashdata('error')) {  ?>
        toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php } else if ($this->session->flashdata('warning')) {  ?>
        toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php } else if ($this->session->flashdata('info')) {  ?>
        toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>
</script>

        <script>
            $(".logout").click(function(event) {
            $.ajax({
                type: "POST",
                url: "<?= base_url('ajax/logoutajax'); ?>",
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Logging Out",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Logout',
                        text: 'Anda Telah Keluar!',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    location.reload();
                }
            });
            event.preventDefault();
        });

        function aksilain(a,b,c){
            let nama = b.replace('_', ' ').replace('_', ' ');

            $('#viewabsensimodal').modal('show');
            $('#idAbsen').val(a);
            $('#namaAbsen').val('Nama : '+nama);
            $('#nik').val(c);

            
        }

        function hadir(a,b,c){
            let nama = b.replace('_', ' ').replace('_', ' ');
            $.ajax({
                type: "POST",
                url: "<?= base_url('admin/simpanAbsen'); ?>",
                data: {
                    idAbsen : a,
                    namaAbsen : 'Nama : '+nama,
                    nik : c,
                    opsi : 'hadir'
                },
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Proses Absensi Hadir",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    location.reload();
                }
            });
            event.preventDefault();
        }

        $('#btnAksilain').on('click',function(){
            $('#btnSubmit').trigger('click');
        }) 
        
        </script>

</body>

</html>