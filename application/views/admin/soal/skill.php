<form action="<?php echo $action; ?>" method="post">
    <div class="container-fluid">
        <h1 class="my-4"><?= $title ?></h1>
        <div class="my-2">Kode Kandidat : <?= $kode_kandidat ?></div>
        <div class="my-2">Nama Lengkap : <?= $nama_lengkap ?></div>
        <div class="my-3 text-right">
            <h2>Score</h2>
        </div>
        <div class="my-3 text-right"> <input type="text" id="totalSum" name="<?= $tipe_soal ?>" class="form-control-lg text-right" readonly /></div>
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="kandidat_id" value="<?= $kandidat_id ?>">
                <input type="hidden" name="tipe_soal" value="<?= $tipe_soal ?>">
                <div class="row">
                    <?php
                    if (isset($soalList) && count($soalList) > 0) {
                        foreach ($soalList as $category => $val) {

                            if (isset($category) && !empty($category)) {
                                echo '<div class="col-12 mb-4">
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black font-weight-bold"
                                        for="form_informasi_objek_Pendanaan">' . $category . '</label>
                                </div>
                            </div>';
                            }

                            $no = 0;

                            foreach ($val as $key => $valFieldName) {
                                $no++;
                                echo '<div class="col-12 mb-4">
                                  <div>' . $no . '. ' . $valFieldName['pertanyaan'] . '</div>  
                            </div>';

                                echo '<div class="col-12 mb-4">';
                                foreach ($valFieldName['pilihan'] as $keyFieldName => $pilihValue) {
                                    echo  '<div style="margin-left: 20px;"><label class="form-control badge-light"><input type="radio" value="' . $pilihValue . '" name="soal' . $valFieldName['id'] . '" required> ' . $keyFieldName . '</label></div>';
                                }
                                echo '</div>';
                            }
                        }
                    }
                    ?>

                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-save icon" style="font-size:14px"></i> Submit </button>

                        <a href="<?php echo site_url('admin/kandidat') ?>" class="btn btn-secondary btn-lg">Back</a>

                    </div>

                </div>

            </div>

        </div>

    </div>
</form>