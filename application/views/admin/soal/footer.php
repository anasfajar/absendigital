
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                    <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                </div>
                <div class="text-muted">
                    Page rendered in <strong>{elapsed_time}</strong> detik.
                </div>
            </div>
        </div>
    </footer>
  
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/sweetalert2/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <script type="text/javascript">
            $(document).ready(function() {
                $("input[type=radio]").click(function() {
                    var total = 0;
                    $("input[type=radio]:checked").each(function() {
                        total += parseInt(this.value);
                    });

                    $("#totalSum").val(total);

                });

            });

    </script>

    <script type="text/javascript">
    <?php if($this->session->flashdata('success')){ ?>
        toastr.success("<?php echo $this->session->flashdata('success'); ?>");
    <?php }else if($this->session->flashdata('error')){  ?>
        toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php }else if($this->session->flashdata('warning')){  ?>
        toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
    <?php }else if($this->session->flashdata('info')){  ?>
        toastr.info("<?php echo $this->session->flashdata('info'); ?>");
    <?php } ?>
    $(document).ready(function() {
        $(".logout").click(function(event) {
            $.ajax({
                type: "POST",
                url: "<?= base_url('ajax/logoutajax'); ?>",
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Logging Out",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Logout',
                        text: 'Anda Telah Keluar!',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    location.reload();
                }
            });
            event.preventDefault();
        });
    });
    </script>
      
    </body>

    </html>
