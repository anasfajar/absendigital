<div class="container-fluid" style="margin-top: 40px">
  
    <div class="card">
        <div class="card-header">
        <h2 class="my-2"><?=$title?></h2>
        <div class="my-2">Kode Kandidat : <?=$kode_kandidat?></div>
        <div class="my-2">Nama Lengkap : <?=$nama_lengkap?></div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
        <input type="hidden" name="kandidat_id" value="<?=$kandidat_id?>">
        <div class="card-body">
        <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Catatan!</h4>
        <p>Jika Dokumen Kadidat <b>belum diceklis</b>, maka Keterangan <b>WAJIB</b> diisi!</p>
        </div>
            <div class="table-responsive">
                <table class="table table-bordered dashboard" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th>No</th>
                            <th>Jenis Dokumen</th>
                            <th class="text-center">Ceklis Dokumen</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>                      
                    <tbody>
                        <?php 
                            error_reporting(0);

                            if(isset($documentList) && count($documentList) > 0){
                               $no = 1;

                               $value = "";
                               // pemeriksaan menggunakan fungsi isset()
                               $value = isset($dokumenKandidat) ? $dokumenKandidat : ''; 

                                if(isset($value)){                                   
                                    foreach($documentList as $key=> $val) {    
                                        $checked = '';
                                        $keterangan = null;
        
                                        if(isset($selectedDokumen) && count($selectedDokumen) > 0){
                                           if($selectedDokumen[$val->id]['check'] == '1'){
                                              $checked = 'checked';
                                           }                                       
                                            $keterangan = $selectedDokumen[$val->id]['keterangan'];                                    
                                        }
                                        ?>                              
                                        <tr>
                                            <td><?php echo $no++ ?> </td>
                                            <td><?php echo $val->nama_dokumen ?></td>
                                            <td><input type="checkbox" class="form-control" name="dokumen[]" value="<?php echo $val->id ?>" <?php echo $checked ?> /></td>
                                            <td><input type="text" class="form-control" name="keterangan[]" value="<?php echo $keterangan ?>" ></td>
                                            
                                        </tr>
                                        <?php
                                        }
                                }

                        }
                        ?>
                    </tbody>

                      
                </table>
            </div>
        </div>
        <div class="card-footer">
               <div class="row">
                   <div class="col-12">
                   <button type="submit" class="btn btn-success btn-lg" name="waiting_approval"><i class="fa fa-save icon" style="font-size:16px"></i> Submit </button>
                    <a href="<?php echo site_url('admin/kandidat') ?>" class="btn btn-secondary btn-lg">Back</a>
                   </div>
               </div>    
        </div>
        </form>
    </div>
</div>

<!--<script
  src="https://code.jquery.com/jquery-2.0.2.js"
  integrity="sha256-0u0HIBCKddsNUySLqONjMmWAZMQYlxTRbA8RfvtCAW0="
  crossorigin="anonymous"></script>
<script>
            $(document).on('click', 'input[name="dokumen[]"]', function () {
                var checked = $(this).prop('checked');// true or false
                if (checked) {
                    $(this).parents('tr').find('td input[type="text"]').attr('disabled', 'disabled');
                }
                else {
                    $(this).parents('tr').find('td input[type="text"]').removeAttr('disabled');
                }
            });
</script>-->