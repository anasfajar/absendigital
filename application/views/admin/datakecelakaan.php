<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-truck mr-2"></span>Data Kecelakaan</h1>
    <div class="card mb-4">
        <div class="card-header">
            <div class="float-right">
                <a href="<?= base_url('tambahkecelakaan'); ?>" class="btn btn-primary"><span class="fas fa-plus mr-1"></span>Tambah Data</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="datakecelakaan" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Kecelakaan</th>
                            <th>Driver</th>
                            <th>No Polisi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Kecelakaan</th>
                            <th>Driver</th>
                            <th>No Polisi</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>