<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Tambah Data Pegawai</h1>     
    <form action="<?= base_url('simpanpgw')?>" method="post">	
 <div class="card mb-3" >
 <div class="card-header">
    BIODATA PEGAWAI
 </div>
 <div class="card-body">
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">NIK Pegawai</label>
    <input type="hidden" class="form-control" id="id" name="id" >
    <input type="text" class="form-control col-sm-3" id="nik" name="nik" value="<?= $nik; ?>" readonly>
    <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Lengkap Pegawai</label>
    <input type="text" class="form-control" id="nama" name="nama" value="<?= set_value('nama') ?>">
    <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">No. KTP Pegawai</label>
    <input type="number" class="form-control col-sm-6" id="no_ktp" name="no_ktp" placeholder="contoh : 123743xxx" value="<?= set_value('no_ktp') ?>">
    <?= form_error('no_ktp', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Berlaku</label>
    <input type="text" class="form-control col-sm-6" id="masa_berlaku" name="masa_berlaku" value="Seumur Hidup" readonly>
    
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">No Handphone</label>
    <input type="number" class="form-control col-sm-6" id="nomor_hp" name="nomor_hp" value="<?= set_value('nomor_hp') ?>"  placeholder="contoh : 08139xxx">
    <?= form_error('nomor_hp', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tempat Lahir</label>
    <input type="text" class="form-control col-sm-6" id="tempat_lahir" name="tempat_lahir" value="<?= set_value('tempat_lahir') ?>" >
    <?= form_error('tempat_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Lahir</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_lahir" name="tglblnthn_lahir" value="<?= set_value('tglblnthn_lahir') ?>">
    <?= form_error('tglblnthn_lahir', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Alamat</label>
    <textarea class="form-control" id="alamat" name="alamat"  rows="3"><?= set_value('alamat') ?></textarea>
    <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status</label>
    <select id="status_pajak" name="status_pajak" class="form-control col-sm-3" value="<?= set_value('status_pajak') ?>">
      <option value="KAWIN" >KAWIN</option>
      <option value="KAWIN ANAK 1" >KAWIN ANAK 1</option>
      <option value="KAWIN ANAK 2" >KAWIN ANAK 2</option>
      <option value="KAWIN ANAK 3" >KAWIN ANAK 3</option>
      <option value="KAWIN ANAK 4" >KAWIN ANAK 4</option>
      <option value="LAJANG" >LAJANG</option>
    </select>
</div>
<div class="mb-3">
<label for="inputState" class="form-label">Agama</label>
    <select id="agama" name="agama" class="form-control col-sm-3" value="<?= set_value('agama')?>" >
      <option value="ISLAM" >ISLAM</option>
      <option value="KRISTEN" >KRISTEN</option>
      <option value="BUDHA">BUDHA</option>
      <option value="KATHOLIK" >KATHOLIK</option>
      <option value="HINDU" >HINDU</option>
    </select>
</div>
<div class="mb-3">
<label for="inputState" class="form-label">Jenis Kelamin</label>
<div class="form-check">       
        <input class="form-check-input" type="radio" id="jenis_kelamin" name="jenis_kelamin"  <?php if(set_value('jenis_kelamin')=='LAKI-LAKI') echo 'checked'?> value="LAKI-LAKI"  >
        <label class="form-check-label" for="gridRadios1">
          Laki-laki
        </label>
      </div>      
      <div class="form-check">
        <input class="form-check-input" type="radio" id="jenis_kelamin" name="jenis_kelamin"  <?php if(set_value('jenis_kelamin')=='PEREMPUAN') echo 'checked'?> value="PEREMPUAN">       
        <label class="form-check-label" for="gridRadios2">
          Perempuan
        </label>
      </div>
      <?= form_error('jenis_kelamin', '<small class="text-danger pl-3">', '</small>'); ?> 
</div>
  <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Pendidikan Terakhir</label>
    <select id="pendidikan_terakhir" class="form-control col-sm-3" name="pendidikan_terakhir" >
      <option value="SD" <?php if(set_value('pendidikan_terakhir')== "SD"){ echo 'selected';}?> >SD</option>
      <option value="SMP" <?php if(set_value('pendidikan_terakhir')== "SMP"){ echo 'selected';}?> >SMP</option>      
      <option value="MTS" <?php if(set_value('pendidikan_terakhir')== "MTS"){ echo 'selected';}?> >MTS</option>
      <option value="MA" <?php if(set_value('pendidikan_terakhir')== "MA"){ echo 'selected';}?> >MA</option>
      <option value="SMA" <?php if(set_value('pendidikan_terakhir')== "SMA"){ echo 'selected';}?> >SMA</option>      
      <option value="PAKET B" <?php if(set_value('pendidikan_terakhir')== "PAKET B"){ echo 'selected';}?> >PAKET B</option>
      <option value="D1" <?php if(set_value('pendidikan_terakhir')== "D1"){ echo 'selected';}?> >D1</option>      
      <option value="D2" <?php if(set_value('pendidikan_terakhir')== "D2"){ echo 'selected';}?> >D2</option>
      <option value="D3" <?php if(set_value('pendidikan_terakhir')== "D3"){ echo 'selected';}?> >D3</option>
      <option value="S1" <?php if(set_value('pendidikan_terakhir')== "S1"){ echo 'selected';}?> >S1</option>
      <option value="S2" <?php if(set_value('pendidikan_terakhir')== "S2"){ echo 'selected';}?> >S2</option>
      <option value="S3" <?php if(set_value('pendidikan_terakhir')== "S3"){ echo 'selected';}?> >S3</option>
    </select>
  </div>
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">No Kartu Kerluarga</label>
    <input type="text" class="form-control col-sm-6" id="nomor_kartu_keluarga" name="nomor_kartu_keluarga" value="<?= set_value('nomor_kartu_keluarga')?>" >
    <?= form_error('nomor_kartu_keluarga', '<small class="text-danger pl-3">', '</small>'); ?>    
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">NPWP</label>
    <input type="text" class="form-control col-sm-6" id="npwp" name="npwp" value="<?= set_value('npwp')?>" >
    <?= form_error('npwp', '<small class="text-danger pl-3">', '</small>'); ?> 
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Email</label>
    <input type="email" class="form-control col-sm-6" id="email_address" placeholder="contoh : asc@gmail.com" name="email_address" value="<?= set_value('email_address')?>" >
    <?= form_error('email_address', '<small class="text-danger pl-3">', '</small>'); ?> 
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Vendor</label>
    <input type="text" class="form-control col-sm-3" id="vendor" name="vendor" value="PT. MBK" readonly>
    <?= form_error('vendor', '<small class="text-danger pl-3">', '</small>'); ?> 
</div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KETERANGAN BANK
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Nama diBank</label>
    <input type="text" class="form-control" id="nama_di_bank" placeholder="contoh : John" name="nama_di_bank" value="<?= set_value('nama_di_bank')?>" >
    <?= form_error('nama_di_bank', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Bank</label>
    <input type="text" class="form-control col-sm-6" id="bank" name="bank" placeholder="contoh : Mandiri" value="<?= set_value('bank')?>" >
    <?= form_error('bank', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">No. Rekening</label>
    <input type="number" class="form-control col-sm-6" id="no_account" name="no_account" value="<?= set_value('no_account')?>">
    <?= form_error('no_account', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    AKTIFIASI KARYAWAN
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Keterangan Aktifasi Karyawan</label>
    <input type="text" class="form-control" id="keterangan_aktivasi_karyawan" name="keterangan_aktivasi_karyawan" value="<?= set_value('keterangan_aktivasi_karyawan')?>" >
    <?= form_error('keterangan_aktivasi_karyawan', '<small class="text-danger pl-3">', '</small>'); ?>   
  </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nomor PO / Nama Driver Yang Diganti</label>
    <input type="text" class="form-control" id="nomor_po_nama_driver_yg_diganti" name="nomor_po_nama_driver_yg_diganti" value="<?= set_value('nomor_po_nama_driver_yg_diganti')?>">
    <?= form_error('nomor_po_nama_driver_yg_diganti', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal PO</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_po" name="tanggal_po" value="<?= set_value('tanggal_po')?>" >
    <?= form_error('tanggal_po', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Aktif Kerja</label>
    <input type="date" class="form-control col-sm-3" id="tgl_inaktif_kerja" name="tgl_inaktif_kerja" value="<?= set_value('tgl_inaktif_kerja')?>">
    <?= form_error('tgl_inaktif_kerja', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lead Team</label>
    <input type="text" class="form-control col-sm-6" id="lead_time" name="lead_time" value="<?= set_value('lead_time')?>">
    <?= form_error('lead_time', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="inputState" class="form-label">Jobs</label>
    <select name="jobs" class="form-control col-sm-3" id="jobs" >
      <option value="DRIVER" <?php if(set_value('jobs')== "DRIVER"){ echo 'selected';}?> >DRIVER</option>
      <option value="NON DRIVER" <?php if(set_value('jobs')== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
      <option value="FICO" <?php if(set_value('jobs')== "FICO"){ echo 'selected';}?> >FICO</option>
      <option value="HELPER" <?php if(set_value('jobs')== "HELPER"){ echo 'selected';}?> >HELPER</option>
      <option value="CHECKER" <?php if(set_value('jobs')== "CHECKER"){ echo 'selected';}?> >CHECKER</option>
      <option value="ADMIN" <?php if(set_value('jobs')== "ADMIN"){ echo 'selected';}?> >ADMIN</option>
      <option value="ADMIN DISPATCHER" <?php if(set_value('jobs')== "ADMIN DISPATCHER"){ echo 'selected';}?> >ADMIN DISPATCHER</option>
      <option value="DISPATCHER" <?php if(set_value('jobs')== "DISPATCHER"){ echo 'selected';}?> >DISPATCHER</option>
      <option value="DISPATCHER LEADER" <?php if(set_value('jobs')== "DISPATCHER LEADER"){ echo 'selected';}?> >DISPATCHER LEADER</option>
      <option value="DRIVER LEADER" <?php if(set_value('jobs')== "DRIVER LEADER"){ echo 'selected';}?> >DRIVER LEADER</option>
      <option value="FUEL MANAGEMENT" <?php if(set_value('jobs')== "FUEL MANAGEMENT"){ echo 'selected';}?> >FUEL MANAGEMENT</option>
      <option value="IT" <?php if(set_value('jobs')== "IT"){ echo 'selected';}?> >IT</option>
      <option value="MONITORING GPS" <?php if(set_value('jobs')== "MONITORING GPS"){ echo 'selected';}?> >MONITORING GPS</option>
      <option value="OPERATING CASHIER" <?php if(set_value('jobs')== "OPERATING CASHIER"){ echo 'selected';}?>>OPERATING CASHIER</option>
      <option value="OPERATING POINT COORDINATOR" <?php if(set_value('jobs')== "OPERATING POINT COORDINATOR"){ echo 'selected';}?> >OPERATING POINT COORDINATOR</option>
      <option value="PJS OPERATING POINT COORDINATOR" <?php if(set_value('jobs')== "PJS OPERATING POINT COORDINATOR"){ echo 'selected';}?> >PJS OPERATING POINT COORDINATOR</option>
      <option value="UNIT CONTROLLER" <?php if(set_value('jobs')== "UNIT CONTROLLER"){ echo 'selected';}?> >UNIT CONTROLLER</option>
      <option value="UNIT MANAGEMENT" <?php if(set_value('jobs')== "UNIT MANAGEMENT"){ echo 'selected';}?> >UNIT MANAGEMENT</option>
    </select>
</div>
<div class="mb-3">
    <label for="inputState" class="form-label">Operation Point</label>
    <select id="operation_poin" name="operation_poin" class="form-control col-sm-3" id="formGroupExampleInput2" > 
    <option value="ACEH" <?php if(set_value('operation_poin')== "ACEH"){ echo 'selected';}?> >ACEH</option>
      <option value="BALIKPAPAN" <?php if(set_value('operation_poin')== "BALIKPAPAN"){ echo 'selected';}?> >BALIKPAPAN</option>
      <option value="BANDUNG" <?php if(set_value('operation_poin')== "BANDUNG"){ echo 'selected';}?> >BANDUNG</option>
      <option value="BANJAR" <?php if(set_value('operation_poin')== "BANJAR"){ echo 'selected';}?> >BANJAR</option>
      <option value="BANJARMASIN" <?php if(set_value('operation_poin')== "BANJARMASIN"){ echo 'selected';}?> >BANJARMASIN</option>
      <option value="BATAM" <?php if(set_value('operation_poin')== "BATAM"){ echo 'selected';}?> >BATAM</option>
      <option value="BEKASI" <?php if(set_value('operation_poin')== "BEKASI"){ echo 'selected';}?> >BEKASI</option>
      <option value="BENGKULU" <?php if(set_value('operation_poin')== "BENGKULU"){ echo 'selected';}?> >BENGKULU</option>
      <option value="BIMA" <?php if(set_value('operation_poin')== "BIMA"){ echo 'selected';}?> >BIMA</option>
      <option value="BOGOR" <?php if(set_value('operation_poin')== "BOGOR"){ echo 'selected';}?> >BOGOR</option>
      <option value="CAKUNG" <?php if(set_value('operation_poin')== "CAKUNG"){ echo 'selected';}?> >CAKUNG</option>
      <option value="CIAMIS" <?php if(set_value('operation_poin')== "CIAMIS"){ echo 'selected';}?> >CIAMIS</option>
      <option value="CIANJUR" <?php if(set_value('operation_poin')== "CIANJUR"){ echo 'selected';}?> >CIANJUR</option>
      <option value="CIBITUNG" <?php if(set_value('operation_poin')== "CIBITUNG"){ echo 'selected';}?> >CIBITUNG</option>
      <option value="CIKARANG" <?php if(set_value('operation_poin')== "CIKARANG"){ echo 'selected';}?> >CIKARANG</option>
      <option value="CIREBON" <?php if(set_value('operation_poin')== "CIREBON"){ echo 'selected';}?> >CIREBON</option>
      <option value="DENPASAR" <?php if(set_value('operation_poin')== "DENPASAR"){ echo 'selected';}?> >DENPASAR</option>
      <option value="GARUT" <?php if(set_value('operation_poin')== "GARUT"){ echo 'selected';}?> >GARUT</option>
      <option value="GORONTALO" <?php if(set_value('operation_poin')== "GORONTALO"){ echo 'selected';}?> >GORONTALO</option>
      <option value="HALIM" <?php if(set_value('operation_poin')== "HALIM"){ echo 'selected';}?> >HALIM</option>
      <option value="INDRAMAYU" <?php if(set_value('operation_poin')== "INDRAMAYU"){ echo 'selected';}?> >INDRAMAYU</option>
      <option value="JAMBI" <?php if(set_value('operation_poin')== "JAMBI"){ echo 'selected';}?> >JAMBI</option>
      <option value="JEMBER" <?php if(set_value('operation_poin')== "JEMBER"){ echo 'selected';}?> >JEMBER</option>
      <option value="JOMBANG" <?php if(set_value('operation_poin')== "JOMBANG"){ echo 'selected';}?> >JOMBANG</option>
      <option value="KARAWANG" <?php if(set_value('operation_poin')== "KARAWANG"){ echo 'selected';}?> >KARAWANG</option>
      <option value="KUDUS" <?php if(set_value('operation_poin')== "KUDUS"){ echo 'selected';}?> >KUDUS</option>
      <option value="KUNINGAN" <?php if(set_value('operation_poin')== "KUNINGAN"){ echo 'selected';}?> >KUNINGAN</option>
      <option value="LAMONGAN" <?php if(set_value('operation_poin')== "LAMONGAN"){ echo 'selected';}?> >LAMONGAN</option>
      <option value="LAMPUNG" <?php if(set_value('operation_poin')== "LAMPUNG"){ echo 'selected';}?> >LAMPUNG</option>
      <option value="MADIUN" <?php if(set_value('operation_poin')== "MADIUN"){ echo 'selected';}?> >MADIUN</option>
      <option value="MADURA" <?php if(set_value('operation_poin')== "MADURA"){ echo 'selected';}?> >MADURA</option>
      <option value="MAKASAR" <?php if(set_value('operation_poin')== "MAKASAR"){ echo 'selected';}?> >MAKASAR</option>
      <option value="MALANG" <?php if(set_value('operation_poin')== "MALANG"){ echo 'selected';}?> >MALANG</option>
      <option value="MANADO" <?php if(set_value('operation_poin')== "MANADO"){ echo 'selected';}?> >MANADO</option>
      <option value="MATARAM" <?php if(set_value('operation_poin')== "MATARAM"){ echo 'selected';}?> >MATARAM</option>
      <option value="MEDAN" <?php if(set_value('operation_poin')== "MEDAN"){ echo 'selected';}?> >MEDAN</option>
      <option value="MOJOKERTO" <?php if(set_value('operation_poin')== "MOJOKERTO"){ echo 'selected';}?> >MOJOKERTO</option>
      <option value="PADANG" <?php if(set_value('operation_poin')== "PADANG"){ echo 'selected';}?> >PADANG</option>
      <option value="PALANGKARAYA" <?php if(set_value('operation_poin')== "PALANGKARAYa"){ echo 'selected';}?> >PALANGKARAYa</option>
      <option value="PALEMBANG" <?php if(set_value('operation_poin')== "PALEMBANG"){ echo 'selected';}?> >PALEMBANG</option>
      <option value="PALU" <?php if(set_value('operation_poin')== "PALU"){ echo 'selected';}?> >PALU</option>
      <option value="PANGKAL PINANG" <?php if(set_value('operation_poin')== "PANGKAL PINANG"){ echo 'selected';}?> >PANGKAL PINANG</option>
      <option value="PARE PARE" <?php if(set_value('operation_poin')== "PARE PARE"){ echo 'selected';}?> >PARE PARE</option>
      <option value="PASURUAN" <?php if(set_value('operation_poin')== "PASURUAN"){ echo 'selected';}?> >PASURUAN</option>
      <option value="PEKANBARU" <?php if(set_value('operation_poin')== "PEKANBARU"){ echo 'selected';}?> >PEKANBARU</option>
      <option value="PEMALANG" <?php if(set_value('operation_poin')== "PEMALANG"){ echo 'selected';}?> >PEMALANG</option>
      <option value="PONDOK PINANG" <?php if(set_value('operation_poin')== "PONDOK PINANG"){ echo 'selected';}?> >PONDOK PINANG</option>
      <option value="PONTIANAK" <?php if(set_value('operation_poin')== "PONTIANAK"){ echo 'selected';}?> >PONTIANAK</option>
      <option value="PROBOLINGGO" <?php if(set_value('operation_poin')== "PROBOLINGGO"){ echo 'selected';}?> >PROBOLINGGO</option>
      <option value="PURWAKARTA" <?php if(set_value('operation_poin')== "PURWAKARTA"){ echo 'selected';}?> >PURWAKARTA</option>
      <option value="PURWOKERTO" <?php if(set_value('operation_poin')== "PURWOKERTO"){ echo 'selected';}?> >PURWOKERTO</option>
      <option value="RAWA BUAYA" <?php if(set_value('operation_poin')== "RAWA BUAYA"){ echo 'selected';}?> >RAWA BUAYA</option>
      <option value="REMBANG" <?php if(set_value('operation_poin')== "REMBANG"){ echo 'selected';}?> >REMBANG</option>
      <option value="SEMARANG" <?php if(set_value('operation_poin')== "SEMARANG"){ echo 'selected';}?> >SEMARANG</option>
      <option value="SERANG" <?php if(set_value('operation_poin')== "SERANG"){ echo 'selected';}?> >SERANG</option>
      <option value="SERIRIT" <?php if(set_value('operation_poin')== "SERIRIT"){ echo 'selected';}?> >SERIRIT</option>
      <option value="SIDOARJO" <?php if(set_value('operation_poin')== "SIDOARJO"){ echo 'selected';}?> >SIDOARJO</option>
      <option value="SINGARAJA" <?php if(set_value('operation_poin')== "SINGARAJA"){ echo 'selected';}?> >SINGARAJA</option>
      <option value="SOLO" <?php if(set_value('operation_poin')== "SOLO"){ echo 'selected';}?> >SOLO</option>
      <option value="SUBANG" <?php if(set_value('operation_poin')== "SUBANG"){ echo 'selected';}?> >SUBANG</option>
      <option value="SUKABUMI" <?php if(set_value('operation_poin')== "SUKABUMI"){ echo 'selected';}?> >SUKABUMI</option>
      <option value="SUMBAWA" <?php if(set_value('operation_poin')== "SUMBAWA"){ echo 'selected';}?> >SUMBAWA</option>
      <option value="SUMEDANG" <?php if(set_value('operation_poin')== "SUMEDANG"){ echo 'selected';}?> >SUMEDANG</option>
      <option value="SUNTER" <?php if(set_value('operation_poin')== "SUNTER"){ echo 'selected';}?> >SUNTER</option>
      <option value="SURABAYA" <?php if(set_value('operation_poin')== "SURABAYA"){ echo 'selected';}?> >SURABAYA</option>
      <option value="TAMAN TEKNO" <?php if(set_value('operation_poin')== "TAMAN TEKNO"){ echo 'selected';}?> >TAMAN TEKNO</option>
      <option value="TAMBUN" <?php if(set_value('operation_poin')== "TAMBUN"){ echo 'selected';}?> >TAMBUN</option>
      <option value="TANGERANG" <?php if(set_value('operation_poin')== "TANGERANG"){ echo 'selected';}?> >TANGERANG</option>
      <option value="TASIK KOTA" <?php if(set_value('operation_poin')== "TASIK KOTA"){ echo 'selected';}?> >TASIK KOTA</option>
      <option value="TEGAL" <?php if(set_value('operation_poin')== "TEGAL"){ echo 'selected';}?> >TEGAL</option>      
      <option value="TIPAR" <?php if(set_value('operation_poin')== "TIPAR"){ echo 'selected';}?> >TIPAR</option>
      <option value="YOGYAKARTA" <?php if(set_value('operation_poin')== "YOGYAKARTA"){ echo 'selected';}?> >YOGYAKARTA</option>
    </select>
</div>
<div class="mb-3">
    <label for="inputState" class="form-label">Customer</label>
    <select id="customer" name="customer" class="form-control col-sm-3" id="formGroupExampleInput2" >
    <option value="AGRIAKU" <?php if(set_value('customer')== "Agriaku"){ echo 'selected';}?> >AGRIAKU</option>
      <option value="ANTERAJA" <?php if(set_value('customer')== "ANTERAJA"){ echo 'selected';}?> >ANTERAJA</option>
      <option value="AOP" <?php if(set_value('customer')== "AOP"){ echo 'selected';}?> >AOP</option>
      <option value="APL" <?php if(set_value('customer')== "APL"){ echo 'selected';}?> >APL</option>
      <option value="BAT 1" <?php if(set_value('customer')== "BAT 1"){ echo 'selected';}?> >BAT 1</option>
      <option value="CIRCLE K" <?php if(set_value('customer')== "CIRCLE K"){ echo 'selected';}?> >CIRCLE K</option>
      <option value="CREDIBOOK" <?php if(set_value('customer')== "CREDIBOOK"){ echo 'selected';}?> >CREDIBOOK</option>
      <option value="HMS" <?php if(set_value('customer')== "HMS"){ echo 'selected';}?> >HMS</option>
      <option value="MAYORA" <?php if(set_value('customer')== "MAYORA"){ echo 'selected';}?> >MAYORA</option>
      <option value="PT. ADI SARANA ARMADA. TBK" <?php if(set_value('customer')== "PT. ADI SARANA ARMADA. TBK"){ echo 'selected';}?> >PT. ADI SARANA ARMADA. TBK</option>
      <option value="PT. MEDAN DISTRIBUSINDO RAYA" <?php if(set_value('customer')== "PT. MEDAN DISTRIBUSINDO RAYA"){ echo 'selected';}?> >PT. MEDAN DISTRIBUSINDO RAYA</option>
      <option value="PT. SUMBER ALFARIA TRIJAYA TBK" <?php if(set_value('customer')== "PT. SUMBER ALFARIA TRIJAYA TBK"){ echo 'selected';}?> >PT. SUMBER ALFARIA TRIJAYA TBK</option>
      <option value="YAMAZAKI" <?php if(set_value('customer')== "YAMAZAKI"){ echo 'selected';}?> >YAMAZAKI</option>
    </select>
</div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KLASIFIKASI SKILL
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Skill</label>
    <select id="klasifikasi_skill" name="klasifikasi_skill"  class="form-control col-sm-3" >
      <option value="BV/CDE" <?php if(set_value('klasifikasi_skill')== "BV/CDE"){ echo 'selected';}?> >BV/CDE</option>
      <option value="BV/CDE/CDD" <?php if(set_value('klasifikasi_skill')== "BV/CDE/CDD"){ echo 'selected';}?> >BV/CDE/CDD</option>
      <option value="CDD" <?php if(set_value('klasifikasi_skill')== "CDD"){ echo 'selected';}?> >CDD</option>
      <option value="CDD LONG" <?php if(set_value('klasifikasi_skill')== "CDD LONG"){ echo 'selected';}?> >CDD LONG</option>
      <option value="CDD/CDL" <?php if(set_value('klasifikasi_skill')== "CDD/CDL"){ echo 'selected';}?> >CDD/CDL</option>
      <option value="CDD/FUSO" <?php if(set_value('klasifikasi_skill')== "CDD/FUSO"){ echo 'selected';}?> >CDD/FUSO</option>
      <option value="CDE" <?php if(set_value('klasifikasi_skill')== "CDE"){ echo 'selected';}?> >CDE</option>
      <option value="FUSO" <?php if(set_value('klasifikasi_skill')== "FUSO"){ echo 'selected';}?> >FUSO</option>
      <option value="HIJAU" <?php if(set_value('klasifikasi_skill')== "HIJAU"){ echo 'selected';}?> >HIJAU</option>
      <option value="HITAM" <?php if(set_value('klasifikasi_skill')== "HITAM"){ echo 'selected';}?> >HITAM</option>
      <option value="KUNING" <?php if(set_value('klasifikasi_skill')== "KUNING"){ echo 'selected';}?> >KUNING</option>
      <option value="MERAH" <?php if(set_value('klasifikasi_skill')== "MERAH"){ echo 'selected';}?> >MERAH</option>
      <option value="PUTIH" <?php if(set_value('klasifikasi_skill')== "PUTIH"){ echo 'selected';}?> >PUTIH</option>
      <option value="TRONTON" <?php if(set_value('klasifikasi_skill')== "TRONTON"){ echo 'selected';}?> >TRONTON</option>
      <option value="WING" <?php if(set_value('klasifikasi_skill')== "WING"){ echo 'selected';}?> >WING</option>
      <option value="NON DRIVER" <?php if(set_value('klasifikasi_skill')== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jenis SIM</label>
    <select id="jenis_sim" class="form-control col-sm-3" name="jenis_sim" >
      <option value="A" <?php if(set_value('jenis_sim')== "A"){ echo 'selected';}?> >A</option>
      <option value="B1" <?php if(set_value('jenis_sim')== "B1"){ echo 'selected';}?> >B1</option>
      <option value="B1 UMUM" <?php if(set_value('jenis_sim')== "B1 UMUM"){ echo 'selected';}?> >B1 UMUM</option>
      <option value="B2" <?php if(set_value('jenis_sim')== "B2"){ echo 'selected';}?> >B2</option>
      <option value="B2 UMUM" <?php if(set_value('jenis_sim')== "B2 UMUM"){ echo 'selected';}?> >B2 UMUM</option>
      <option value="NON DRIVER" <?php if(set_value('jenis_sim')== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nomor SIM</label>
    <input type="text" class="form-control col-sm-6" id="no_sim" name="no_sim" value="<?= set_value('no_sim')?>" >
    <?= form_error('no_sim', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Berlaku</label>
    <input type="date" class="form-control col-sm-3" id="masa_berlaku_sim" value="<?= set_value('masa_berlaku_sim')?>" name="masa_berlaku_sim" >
    <?= form_error('masa_berlaku_sim', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KONTRAK KERJA
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Start</label>
    <input type="date" class="form-control col-sm-3" id="start_kontrak" name="start_kontrak" value="<?= set_value('start_kontrak')?>" >
    <?= form_error('start_kontrak', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Finish</label>
    <input type="date" class="form-control col-sm-3" id="finish_kontrak" name="finish_kontrak" value="<?= set_value('finish_kontrak')?>" >
    <?= form_error('finish_kontrak', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Kontrak</label>
    <input type="text" class="form-control col-sm-6" id="status_kontrak" name="status_kontrak" value="<?= set_value('status_kontrak')?>">
    <?= form_error('status_kontrak', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <?php 
    //Menghitung Masa Berakhir Kerja
    //$start_date = new DateTime($pgw['finish_kontrak']);
    //$end_date = new DateTime(date('Y-m-d'));
    //$interval = $start_date->diff($end_date);

    //Menghitung Lama kerja
    //$awal  = new DateTime($pgw['start_kontrak']);
    //$akhir = new DateTime(); // Waktu sekarang
    //$diff  = $awal->diff($akhir);
    ?>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Berakhir</label>
    <input type="text" class="form-control col-sm-3" id="masa_kontrak_berakhir" value="<?= set_value('masa_kontrak_berakhir')?>" name="masa_kontrak_berakhir" >
    <?= form_error('masa_kontrak_berakhir', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lama Kerja</label>
    <input type="text" class="form-control col-sm-6" id="lama_kerja" name="lama_kerja" value="<?= set_value('lama_kerja')?>">
    <?= form_error('lama_kerja', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Seragam</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="seragam" name="seragam" <?php if(set_value('seragam')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="seragam" name="seragam"  <?php if(set_value('seragam')=='BELUM') echo 'checked'?> value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
      </div>
      <?= form_error('seragam', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Pengiriman Seragam</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_pengiriman_seragam" name="tanggal_pengiriman_seragam" value="<?= set_value('tanggal_pengiriman_seragam')?>">
    <?= form_error('tanggal_pengiriman_seragam', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">ID Card</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="id_card" name="id_card"  <?php if(set_value('id_card')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="id_card" name="id_card"  <?php if(set_value('id_card')=='BELUM') echo 'checked'?> value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    <?= form_error('id_card', '<small class="text-danger pl-3">', '</small>'); ?> 
</div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Pengiriman ID Card</label>
    <input type="date" class="form-control col-sm-3"  id="tanggal_pengiriman_id_card" name="tanggal_pengiriman_id_card" value="<?= set_value('tanggal_pengiriman_id_card')?>">
    <?= form_error('tanggal_pengiriman_id_card', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    TRAINING SAFETY DRIVING
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Ketarangan Training</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="training_safety_driving" value="<?= set_value('training_safety_driving')?>" name="training_safety_driving" <?php if(set_value('id_card')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="training_safety_driving" value="<?= set_value('training_safety_driving')?>" name="training_safety_driving" <?php if(set_value('id_card')=='BELUM') echo 'checked'?> value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    <?= form_error('training_safety_driving', '<small class="text-danger pl-3">', '</small>'); ?> 
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Pelaksanaan Training Terakhir</label>
    <input type="date" class="form-control col-sm-3" id="pelaksanaan_training_terakhir" value="<?= set_value('pelaksanaan_training_terakhir')?>" name="pelaksanaan_training_terakhir" >
    <?= form_error('pelaksanaan_training_terakhir', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Training</label>
    <input type="text" class="form-control" id="masa_training" name="masa_training" value="<?= set_value('masa_training')?>">
    <?= form_error('masa_training', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training</label>
    <select id="status_training" name="status_training" class="form-control col-sm-3" id="formGroupExampleInput2">
    <option value="IN CLASS" <?php if(set_value('status_training')== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if(set_value('status_training')== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
      <option value="NON DRIVER" <?php if(set_value('status_training')== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nilai Training Safety Driving (PREE TEST)</label>
    <input type="number" class="form-control col-sm-6" id="nilai_training_safety_driving_pree_test" value="<?= set_value('nilai_training_safety_driving_pree_test')?>" name="nilai_training_safety_driving_pree_test" >
    <?= form_error('nilai_training_safety_driving_pree_test', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nilai Training Safety Driving (POST TEST)</label>
    <input type="number" class="form-control col-sm-6" id="nilai_training_safety_driverng_post_test" value="<?= set_value('nilai_training_safety_driverng_post_test')?>" name="nilai_training_safety_driverng_post_test" >
    <?= form_error('nilai_training_safety_driverng_post_test', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KETERANGAN VAKSIN PEGAWAI
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Status Vaksin Pertama</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_pertama" name="status_vaksin_pertama"  <?php if(set_value('status_vaksin_pertama')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_pertama" name="status_vaksin_pertama"  <?php if(set_value('status_vaksin_pertama')=='BELUM') echo 'checked'?> value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    <?= form_error('status_vaksin_pertama', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Mengapa Belum Vaksin Pertama</label>
    <input type="text" class="form-control" id="mengapa_belum_vaksi_pertama" value="<?= set_value('mengapa_belum_vaksi_pertama')?>" name="mengapa_belum_vaksi_pertama" >
    <?= form_error('mengapa_belum_vaksi_pertama', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Vaksin Pertama</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_vaksin_pertama" value="<?= set_value('tanggal_vaksin_pertama')?>" name="tanggal_vaksin_pertama" >
    <?= form_error('tanggal_vaksin_pertama', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lokasi Vaksin Pertama</label>
    <input type="text" class="form-control" id="lokasi_vaksin_pertama" value="<?= set_value('lokasi_vaksin_pertama')?>" name="lokasi_vaksin_pertama" >
    <?= form_error('lokasi_vaksin_pertama', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>  
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Status Vaksin Kedua</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_kedua" name="status_vaksin_kedua" <?php if(set_value('status_vaksin_kedua')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_kedua" name="status_vaksin_kedua" <?php if(set_value('status_vaksin_kedua')=='BELUM') echo 'checked'?> value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    <?= form_error('status_vaksin_kedua', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Mengapa Belum Vaksin Kedua</label>
    <input type="text" class="form-control" id="mengapa_belum_vaksi_kedua" value="<?= set_value('mengapa_belum_vaksi_kedua')?>" name="mengapa_belum_vaksi_kedua" >
    <?= form_error('mengapa_belum_vaksi_kedua', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Vaksin Kedua</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_vaksin_kedua" value="<?= set_value('tanggal_vaksin_kedua')?>" name="tanggal_vaksin_kedua" >
    <?= form_error('tanggal_vaksin_kedua', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lokasi Vaksin Kedua</label>
    <input type="text" class="form-control" id="lokasi_vaksin_kedua" name="lokasi_vaksin_kedua" value="<?= set_value('lokasi_vaksin_kedua')?>" >
    <?= form_error('lokasi_vaksin_kedua', '<small class="text-danger pl-3">', '</small>'); ?>
    </div> 
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Status Vaksin Booster</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_booster" name="status_vaksin_booster" <?php if(set_value('status_vaksin_booster')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_booster" name="status_vaksin_booster" <?php if(set_value('status_vaksin_booster')=='BELUM') echo 'checked'?>  value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    <?= form_error('status_vaksin_booster', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Mengapa Belum Vaksin Booster</label>
    <input type="text" class="form-control" id="mengapa_belum_vaksin_booster" name="mengapa_belum_vaksin_booster" value="<?= set_value('mengapa_belum_vaksin_booster')?>" >
    <?= form_error('mengapa_belum_vaksin_booster', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Vaksin Booster</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_vaksin_booster" name="tanggal_vaksin_booster" value="<?= set_value('tanggal_vaksin_booster')?>">
    <?= form_error('tanggal_vaksin_booster', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lokasi Vaksin Booster</label>
    <input type="text" class="form-control" id="lokasi_vaksin_booster" name="lokasi_vaksin_booster" value="<?= set_value('lokasi_vaksin_booster')?>" >
    <?= form_error('lokasi_vaksin_booster', '<small class="text-danger pl-3">', '</small>'); ?>
    </div> 
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    TRAINING PEGAWAI
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Training Online Part 1</label>
    <input type="date" class="form-control col-sm-3" id="tgl_training_online_part_1" name="tgl_training_online_part_1" value="<?= set_value('tgl_training_online_part_1')?>" >
    <?= form_error('tgl_training_online_part_1', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training Online Part 1</label>
    <select id="status_training_online_part_1" name="status_training_online_part_1" class="form-control col-sm-3" id="formGroupExampleInput2">
      <option value="IN CLASS" <?php if(set_value('status_training_online_part_1')== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if(set_value('status_training_online_part_1')== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Point Part 1</label>
    <input type="text" class="form-control col-sm-6" id="point_part_1" name="point_part_1" value="<?= set_value('point_part_1')?>" >
    <?= form_error('nilai_training_safety_driverng_post_test', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Training Online Part 2</label>
    <input type="date" class="form-control col-sm-3" id="tgl_training_online_part_2" name="tgl_training_online_part_2" value="<?= set_value('tgl_training_online_part_2')?>">
    <?= form_error('tgl_training_online_part_2', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training Online Part 2</label>
    <select id="inputState" class="form-control col-sm-3" id="status_training_online_part_2" name="status_training_online_part_2"  >
    <option value="IN CLASS" <?php if(set_value('status_training_online_part_2')== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if(set_value('status_training_online_part_2')== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Point Part 2</label>
    <input type="text" class="form-control col-sm-6" id="point_part_2" name="point_part_2" value="<?= set_value('point_part_2')?>" >
    <?= form_error('point_part_2', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Training Attitude & Knowledge</label>
    <input type="date" class="form-control col-sm-3" id="tgl_training_attitude_n_knowledge" name="tgl_training_attitude_n_knowledge" value="<?= set_value('tgl_training_attitude_n_knowledge')?>" >
    <?= form_error('tgl_training_attitude_n_knowledge', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training Attitude & Knowledge</label>
    <select class="form-control col-sm-3" id="status_training_attitude_n_knowledge" name="status_training_attitude_n_knowledge" >
      <option value="IN CLASS" <?php if(set_value('status_training_attitude_n_knowledge')== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if(set_value('status_training_attitude_n_knowledge')== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Point Attitude & Knowledge</label>
    <input type="text" class="form-control col-sm-6" id="point_attitude_n_knowledge" name="point_attitude_n_knowledge" value="<?= set_value('point_attitude_n_knowledge')?>">
    <?= form_error('point_attitude_n_knowledge', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    BPJS & REWARD PEGAWAI 
  </div>
  <div class="card-body">
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Nomor BPJS Kesehatan</label>
    <input type="number" class="form-control" id="nomor_bpjs_kesehatan" name="nomor_bpjs_kesehatan" value="<?= set_value('nomor_bpjs_kesehatan')?>">
    <?= form_error('nomor_bpjs_kesehatan', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Keterangan Proses BPJS Kesehatan</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="keterangan_proses_bpjs_kesehatan" name="keterangan_proses_bpjs_kesehatan" <?php if(set_value('keterangan_proses_bpjs_kesehatan')=='SUDAH') echo 'checked'?> value="SUDAH" >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="keterangan_proses_bpjs_kesehatan" name="keterangan_proses_bpjs_kesehatan" <?php if(set_value('keterangan_proses_bpjs_kesehatan')=='BELUM') echo 'checked'?>  value="BELUM" >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    <?= form_error('keterangan_proses_bpjs_kesehatan', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Keterangan Status BPJS Kesehatan</label>
    <input type="text" class="form-control" id="keterangan_status_bpjs_kesehatan" name="keterangan_status_bpjs_kesehatan" value="<?= set_value('keterangan_status_bpjs_kesehatan')?>" >
    <?= form_error('keterangan_status_bpjs_kesehatan', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Nomor JAMSOSTEK</label>
    <input type="number" class="form-control" id="nomor_jamsostek" name="nomor_jamsostek" value="<?= set_value('nomor_jamsostek')?>">
    <?= form_error('nomor_jamsostek', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Keterangan JAMSOSTEK</label>
    <input type="text" class="form-control" id="keterangan_jamsostek" name="keterangan_jamsostek" value="<?= set_value('keterangan_jamsostek')?>">
    <?= form_error('keterangan_jamsostek', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Reward</label>
    <input type="text" class="form-control" id="reward" name="reward" value="<?= set_value('reward')?>" >
    <?= form_error('reward', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Pemberian Reward</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_pemberian_reward" value="<?= set_value('tglblnthn_pemberian_reward')?>" name="tglblnthn_pemberian_reward" >
    <?= form_error('tglblnthn_pemberian_reward', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    ACCIDENT
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Accident Report</label>
    <input type="text" class="form-control" id="accident_report" value="<?= set_value('accident_report')?>" name="accident_report" >
    <?= form_error('accident_report', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Accident</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_accident" value="<?= set_value('tglblnthn_accident')?>" name="tglblnthn_accident" >
    <?= form_error('tglblnthn_accident', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Case</label>
    <input type="text" class="form-control" id="kasus" name="kasus" value="<?= set_value('kasus')?>" >
    <?= form_error('kasus', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Case</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_kasus" name="tglblnthn_kasus" value="<?= set_value('tglblnthn_kasus')?>">
    <?= form_error('tglblnthn_kasus', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">SP 1</label>
    <input type="date" class="form-control col-sm-3" id="sp_i" name="sp_i" value="<?= set_value('sp_i')?>">
    <?= form_error('sp_i', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">SP 2</label>
    <input type="date" class="form-control col-sm-3" id="sp_ii" name="sp_ii" value="<?= set_value('sp_ii')?>" >
    <?= form_error('sp_ii', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">SP 3</label>
    <input type="date" class="form-control col-sm-3" id="sp_iii" name="sp_iii" value="<?= set_value('sp_iii')?>">
    <?= form_error('sp_iii', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Keterangan Out & Mutasi</label>
    <input type="text" class="form-control" id="keterangan_out_dan_mutasi" name="keterangan_out_dan_mutasi" value="<?= set_value('keterangan_out_dan_mutasi')?>">
    <?= form_error('keterangan_out_dan_mutasi', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    TRAINING
  </div>
  <div class="card-body">
  <? 
  
  ?>
  <div class="mb-3">
    <?php 
        //$tgl_now=date("Y-m-d"); //tanggal sekarang
        //$tgl_dibuat=$pgw['masa_berlaku_sim']; // tanggal dibuat aplikasi
        //$masa_berlaku = strtotime('-14 days', strtotime($tgl_dibuat)); // jangka waktu + 30 hari atau sebulan
        //$tgl_exp=date("Y-m-d",$masa_berlaku); //tanggal expired atau kadaluarsa
        
        //if($pgw['klasifikasi_skill']== "NON DRIVER"){
        //$berlakusim = "NON DRIVER";
        //}elseif($tgl_now >=$tgl_exp){
        //$berlakusim = "Masa Berlaku SIM akan segera habis";
        //}else{
        //$berlakusim = "SIM Masih Berlaku";
        //}
    ?>

    <label for="formGroupExampleInput" class="form-label">Masa Berlaku SIM</label>
    <input type="text" class="form-control" id="masa_berlaku_sim_2" value="<?= set_value('masa_berlaku_sim_2')?>" name="masa_berlaku_sim_2" value="SIM Masih Berlaku" readonly>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Training</label>
    <input type="text" class="form-control" id="masa_training_2" value="<?= set_value('masa_training_2')?>" name="masa_training_2" >
    <?= form_error('masa_training_2', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Ketarangan Training</label>
    <input type="text" class="form-control" id="keterangan_training_2" value="<?= set_value('keterangan_training_2')?>" name="keterangan_training_2" >
    <?= form_error('keterangan_training_2', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jadwal Training</label>
    <input type="date" class="form-control col-sm-3" id="jadwal_training_2" value="<?= set_value('jadwal_training_2')?>" name="jadwal_training_2" >
    <?= form_error('jadwal_training_2', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Driver Leader</label>
    <input type="text" class="form-control" id="driver_leader" value="<?= set_value('driver_leader')?>" name="driver_leader" >
    <?= form_error('driver_leader', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jabatan</label>
    <select id="inputState" class="form-control col-sm-3" id="jabatan" name="jabatan" >
      <option value="DRIVER" <?php if(set_value('jabatan')== "DRIVER"){ echo 'selected';}?> >DRIVER</option>
      <option value="NON DRIVER" <?php if(set_value('jabatan')== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
      <option value="FICO" <?php if(set_value('jabatan')== "FICO"){ echo 'selected';}?> >FICO</option>
      <option value="HELPER" <?php if(set_value('jabatan')== "HELPER"){ echo 'selected';}?> >HELPER</option>
      <option value="CHECKER" <?php if(set_value('jabatan')== "CHECKER"){ echo 'selected';}?> >CHECKER</option>
      <option value="ADMIN" <?php if(set_value('jabatan')== "ADMIN"){ echo 'selected';}?> >ADMIN</option>
      <option value="ADMIN DISPATCHER" <?php if(set_value('jabatan')== "ADMIN DISPATCHER"){ echo 'selected';}?>>ADMIN DISPATCHER</option>
      <option value="DISPATCHER" <?php if(set_value('jabatan')== "DISPATCHER"){ echo 'selected';}?> >DISPATCHER</option>
      <option value="DISPATCHER LEADER" <?php if(set_value('jabatan')== "DISPATCHER LEADER"){ echo 'selected';}?>>DISPATCHER LEADER</option>
      <option value="DRIVER LEADER" <?php if(set_value('jabatan')== "DRIVER LEADER"){ echo 'selected';}?> >DRIVER LEADER</option>
      <option value="FUEL MANAGEMENT" <?php if(set_value('jabatan')== "FUEL MANAGEMENT"){ echo 'selected';}?> >FUEL MANAGEMENT</option>
      <option value="IT" <?php if(set_value('jabatan')== "IT"){ echo 'selected';}?> >IT</option>
      <option value="MONITORING GPS" <?php if(set_value('jabatan')== "MONITORING GPS"){ echo 'selected';}?> >MONITORING GPS</option>
      <option value="OPERATING CASHIER" <?php if(set_value('jabatan')== "OPERATING CASHIER"){ echo 'selected';}?> >OPERATING CASHIER</option>
      <option value="OPERATING POINT COORDINATOR" <?php if(set_value('jabatan')== "OPERATING POINT COORDINATOR"){ echo 'selected';}?> >OPERATING POINT COORDINATOR</option>
      <option value="PJS OPERATING POINT COORDINATOR" <?php if(set_value('jabatan')== "PJS OPERATING POINT COORDINATOR"){ echo 'selected';}?> >PJS OPERATING POINT COORDINATOR</option>
      <option value="UNIT CONTROLLER" <?php if(set_value('jabatan')== "UNIT CONTROLLER"){ echo 'selected';}?> >UNIT CONTROLLER</option>
      <option value="UNIT MANAGEMENT" <?php if(set_value('jabatan')== "UNIT MANAGEMENT"){ echo 'selected';}?> >UNIT MANAGEMENT</option>
    </select>
    </div>    
  </div>
 </div>
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Simpan</button>
</form>
</div>