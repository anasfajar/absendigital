<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updatetrainingpegawai')?>" method="post">	
 <div class="card mb-3">
  <div class="card-header">
    TRAINING PEGAWAI
  </div>
  <div class="card-body">
  <div class="mb-3">    
  <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <label for="formGroupExampleInput" class="form-label">Tanggal Training Online Part 1</label>
    <input type="date" class="form-control col-sm-3" id="tgl_training_online_part_1" name="tgl_training_online_part_1" value="<?= $pgw['tgl_training_online_part_1'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training Online Part 1</label>
    <select id="status_training_online_part_1" name="status_training_online_part_1" class="form-control col-sm-3" id="formGroupExampleInput2">
      <option value="IN CLASS" <?php if($pgw['status_training_online_part_1']== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if($pgw['status_training_online_part_1']== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Point Part 1</label>
    <input type="text" class="form-control col-sm-6" id="point_part_1" name="point_part_1" value="<?= $pgw['point_part_1'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Training Online Part 2</label>
    <input type="date" class="form-control col-sm-3" id="tgl_training_online_part_2" name="tgl_training_online_part_2" value="<?= $pgw['tgl_training_online_part_2'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training Online Part 2</label>
    <select id="inputState" class="form-control col-sm-3" id="status_training_online_part_2" name="status_training_online_part_2" value="<?= $pgw['status_training_online_part_2'] ?>">
      <option selected>IN CLASS</option>
      <option >ONLINE</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Point Part 2</label>
    <input type="text" class="form-control col-sm-6" id="point_part_2" name="point_part_2" value="<?= $pgw['point_part_2'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Training Attitude & Knowledge</label>
    <input type="date" class="form-control col-sm-3" id="tgl_training_attitude_n_knowledge" name="tgl_training_attitude_n_knowledge" value="<?= $pgw['tgl_training_attitude_n_knowledge'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training Attitude & Knowledge</label>
    <select class="form-control col-sm-3" id="status_training_attitude_n_knowledge" name="status_training_attitude_n_knowledge" value="<?= $pgw['status_training_attitude_n_knowledge'] ?>">
      <option value="IN CLASS" <?php if($pgw['status_training_attitude_n_knowledge']== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if($pgw['status_training_attitude_n_knowledge']== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Point Attitude & Knowledge</label>
    <input type="text" class="form-control col-sm-6" id="point_attitude_n_knowledge" name="point_attitude_n_knowledge" value="<?= $pgw['point_attitude_n_knowledge'] ?>" >
    </div>
  </div>
 </div>

 
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>