<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updatepgw')?>" method="post">	
 <div class="card mb-3" >
 <div class="card-header">
    BIODATA PEGAWAI
 </div>
 <div class="card-body">
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">NIK Pegawai</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="number" class="form-control col-sm-3" id="nik" name="nik" value="<?= $pgw['nik'] ?>">
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Lengkap Pegawai</label>
    <input type="text" class="form-control" id="nama" name="nama" value="<?= $pgw['nama'] ?>">
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">No. KTP Pegawai</label>
    <input type="number" class="form-control col-sm-6" id="no_ktp" name="no_ktp" value="<?= $pgw['no_ktp'] ?>">
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Berlaku</label>
    <input type="text" class="form-control col-sm-6" id="masa_berlaku" name="masa_berlaku" value="Seumur Hidup" readonly>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">No Handphone</label>
    <input type="number" class="form-control col-sm-6" id="nomor_hp" name="nomor_hp" value="<?= $pgw['nomor_hp'] ?>" >
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tempat Lahir</label>
    <input type="text" class="form-control col-sm-6" id="tempat_lahir" name="tempat_lahir" value="<?= $pgw['tempat_lahir'] ?>" >
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Lahir</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_lahir" name="tglblnthn_lahir" value="<?= $pgw['tglblnthn_lahir'] ?>" >
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Alamat</label>
    <textarea class="form-control" id="alamat" name="alamat"  rows="3"><?= $pgw['alamat'] ?></textarea>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status</label>
    <select id="status_pajak" name="status_pajak" class="form-control col-sm-3" >
      <option value="KAWIN" <?php if($pgw['status_pajak']== "KAWIN"){ echo 'selected';}?> >KAWIN</option>
      <option value="KAWIN ANAK 1" <?php if($pgw['status_pajak']== "KAWIN ANAK 1"){ echo 'selected';}?> >KAWIN ANAK 1</option>
      <option value="KAWIN ANAK 2" <?php if($pgw['status_pajak']== "KAWIN ANAK 2"){ echo 'selected';}?> >KAWIN ANAK 2</option>
      <option value="KAWIN ANAK 3" <?php if($pgw['status_pajak']== "KAWIN ANAK 3"){ echo 'selected';}?> >KAWIN ANAK 3</option>
      <option value="KAWIN ANAK 4" <?php if($pgw['status_pajak']== "KAWIN ANAK 4"){ echo 'selected';}?> >KAWIN ANAK 4</option>
      <option value="LAJANG" <?php if($pgw['status_pajak']== "LAJANG"){ echo 'selected';}?> >LAJANG</option>
    </select>
</div>
<div class="mb-3">
<label for="inputState" class="form-label">Agama</label>
    <select id="agama" name="agama" class="form-control col-sm-3" >
      <option value="ISLAM" <?php if($pgw['agama']== "ISLAM"){ echo 'selected';}?> >ISLAM</option>
      <option value="KRISTEN" <?php if($pgw['agama']== "KRISTEN"){ echo 'selected';}?> >KRISTEN</option>
      <option value="BUDHA" <?php if($pgw['agama']== "BUDHA"){ echo 'selected';}?> >BUDHA</option>
      <option value="KATHOLIK" <?php if($pgw['agama']== "KATOLIK"){ echo 'selected';}?> >KATHOLIK</option>
      <option value="HINDU" <?php if($pgw['agama']== "HINDU"){ echo 'selected';}?> >HINDU</option>
    </select>
</div>
<div class="mb-3">
<label for="inputState" class="form-label">Jenis Kelamin</label>
<div class="form-check">       
        <input class="form-check-input" type="radio" id="jenis_kelamin" name="jenis_kelamin"  value="LAKI-LAKI" <?php if($pgw['jenis_kelamin']=='LAKI-LAKI') echo 'checked'?> >
        <label class="form-check-label" for="gridRadios1">
          Laki-laki
        </label>
      </div>      
      <div class="form-check">
        <input class="form-check-input" type="radio" id="jenis_kelamin" name="jenis_kelamin"  <?php if($pgw['jenis_kelamin']=='PEREMPUAN') echo 'checked'?> value="PEREMPUAN">       
        <label class="form-check-label" for="gridRadios2">
          Perempuan
        </label>
      </div>
</div>
  <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Pendidikan Terakhir</label>
    <select id="pendidikan_terakhir" class="form-control col-sm-3" name="pendidikan_terakhir">
      <option value="SD" <?php if($pgw['pendidikan_terakhir']== "SD"){ echo 'selected';}?> >SD</option>
      <option value="SMP" <?php if($pgw['pendidikan_terakhir']== "SMP"){ echo 'selected';}?> >SMP</option>      
      <option value="MTS" <?php if($pgw['pendidikan_terakhir']== "MTS"){ echo 'selected';}?> >MTS</option>
      <option value="MA" <?php if($pgw['pendidikan_terakhir']== "MA"){ echo 'selected';}?> >MA</option>
      <option value="SMA" <?php if($pgw['pendidikan_terakhir']== "SMA"){ echo 'selected';}?> >SMA</option>      
      <option value="PAKET B" <?php if($pgw['pendidikan_terakhir']== "PAKET B"){ echo 'selected';}?> >PAKET B</option>
      <option value="D1" <?php if($pgw['pendidikan_terakhir']== "D1"){ echo 'selected';}?> >D1</option>      
      <option value="D2" <?php if($pgw['pendidikan_terakhir']== "D2"){ echo 'selected';}?> >D2</option>
      <option value="D3" <?php if($pgw['pendidikan_terakhir']== "D3"){ echo 'selected';}?> >D3</option>
      <option value="S1" <?php if($pgw['pendidikan_terakhir']== "S1"){ echo 'selected';}?> >S1</option>
      <option value="S2" <?php if($pgw['pendidikan_terakhir']== "S2"){ echo 'selected';}?> >S2</option>
      <option value="S3" <?php if($pgw['pendidikan_terakhir']== "S3"){ echo 'selected';}?> >S3</option>
    </select>
  </div>
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">No Kartu Kerluarga</label>
    <input type="text" class="form-control col-sm-6" id="nomor_kartu_keluarga" name="nomor_kartu_keluarga" value="<?= $pgw['nomor_kartu_keluarga'] ?>" >
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">NPWP</label>
    <input type="text" class="form-control col-sm-6" id="npwp" name="npwp" value="<?= $pgw['npwp'] ?>" >
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Email</label>
    <input type="email" class="form-control col-sm-6" id="email_address" name="email_address" value="<?= $pgw['email_address'] ?>" >
</div>
<div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Vendor</label>
    <input type="text" class="form-control col-sm-3" id="vendor" name="vendor" value="<?= $pgw['vendor'] ?>" readonly>
</div>
  </div>
 </div>

 
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>