<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     

    <?= form_open_multipart('updatefotopgw');?>

 <div class="card mb-3">
  <div class="card-header">
  BIODATA PEGAWAI
  </div>
  <div class="card-body">
  <? 
  
  ?>
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">NIK Pegawai</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="number" class="form-control col-sm-3" id="nik" name="nik" value="<?= $pgw['nik'] ?>" readonly>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Lengkap Pegawai</label>
    <input type="text" class="form-control col-sm-6" id="nama" name="nama" value="<?= $pgw['nama'] ?>" readonly>
    </div>  
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Photo</label>
    <input type="file" class="form-control col-sm-6" id="pasfoto" name="pasfoto">
    </div>
    <div class="mb-3">    
    <img src="<?= ($pgw['image'] == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('assets/img/photoprofil/' . $pgw['image'])); ?>" class="card-img" style="width:20%;">
    </div>   
  </div>
 </div>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>