<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Data Pegawai</h1>     

 <div class="card mb-4" >
 <div class="card-header">
    BIODATA PEGAWAI
  <div class="float-right">
            <a href="<?= base_url('editpegawaimbk') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
  </div>
   </div>
 <div class="card-body">
 
                      <div class="row detail">                        
                        <div class="col-md-9 col-sm-8 col-6">
                            <dl class="row">
                                <dd class="col-sm-4">NIK</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['nik'] ?></dd>
                                <dd class="col-sm-4">Nama Lengkap</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['nama'] ?></dd>
                                <dd class="col-sm-4">No. KTP</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['no_ktp'] ?></dd>
                                <dd class="col-sm-4">Masa Berlaku</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['masa_berlaku'] ?></dd>
                                <dd class="col-sm-4">No Handphone</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['nomor_hp'] ?></dd>
                                <dd class="col-sm-4">Tempat Lahir</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['tempat_lahir'] ?></dd>
                                <dd class="col-sm-4">Tanggal Lahir</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['tglblnthn_lahir'] ?></dd>
                                <dd class="col-sm-4">Alamat</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['alamat'] ?></dd>
                                <dd class="col-sm-4">Status</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['status_pajak'] ?></dd>
                                <dd class="col-sm-4">Agama</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['agama'] ?></dd>
                                <dd class="col-sm-4">Jenis Kelamin</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['jenis_kelamin'] ?></dd>
                                <dd class="col-sm-4">Pendidikan Terakhir</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['pendidikan_terakhir'] ?></dd>
                                <dd class="col-sm-4">No Kartu Kerluarga</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['nomor_kartu_keluarga'] ?></dd>
                                <dd class="col-sm-4">NPWP</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['npwp'] ?></dd>
                                <dd class="col-sm-4">Email</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['email_address'] ?></dd>
                                <dd class="col-sm-4">Vendor</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['vendor'] ?></dd>
                            </dl>
                        </div>
                        <div class="col-md-3 col-sm-4 col-6 p-2">                           
                            <img src="<?= ($viewpgw['image'] == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('assets/img/photoprofil/' . $viewpgw['image'])); ?>" class="card-img" style="width:100%;">
                           
                            <div class="float-right">
                            <a href="<?= base_url('editphotopegawaimbk') .'?id='. $viewpgw['id'];?>" class="btn btn-primary btn-sm"><span class="fas fa-image"></span> Edit Photo</a>
                            </div>
                        </div>
                    </div>

  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KETERANGAN BANK
    <div class="float-right">
            <a href="<?= base_url('editketeranganbank') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-9 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Atas Nama diBank</dd>
    <dd class="col-sm-8">: <?= $viewpgw['nama_di_bank'] ?></dd>
    <dd class="col-sm-4">Nama Bank</dd>
    <dd class="col-sm-8">: <?= $viewpgw['bank'] ?></dd>
    <dd class="col-sm-4">No. Account</dd>
    <dd class="col-sm-8">: <?= $viewpgw['no_account'] ?></dd>
    </dl>
    </div>
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    AKTIFIASI KARYAWAN
    <div class="float-right">
            <a href="<?= base_url('editaktifasikaryawan') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-9 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Keterangan Aktifasi Karyawan</dd>
    <dd class="col-sm-8">: <?= $viewpgw['keterangan_aktivasi_karyawan'] ?></dd>
    <dd class="col-sm-4">Nomor PO / Nama Driver Yang Diganti</dd>
    <dd class="col-sm-8">: <?= $viewpgw['nomor_po_nama_driver_yg_diganti'] ?></dd>
    <dd class="col-sm-4">Tanggal PO</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tanggal_po'] ?></dd>
    <dd class="col-sm-4">Tanggal Aktif Kerja</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tgl_inaktif_kerja'] ?></dd>
    <dd class="col-sm-4">Jobs</dd>
    <dd class="col-sm-8">: <?= $viewpgw['jobs'] ?></dd>
    <dd class="col-sm-4">Lead Time</dd>
    <dd class="col-sm-8">: <?= $viewpgw['lead_time'] ?></dd>
    <dd class="col-sm-4">Operation Point</dd>
    <dd class="col-sm-8">: <?= $viewpgw['operation_poin'] ?></dd>
    <dd class="col-sm-4">Customer</dd>
    <dd class="col-sm-8">: <?= $viewpgw['customer'] ?></dd>
    <dd class="col-sm-4">District</dd>
    <dd class="col-sm-8">: <?= $viewpgw['district'] ?></dd>
    </dl>
    </div>
    </div>    
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KLASIFIKASI SKILL
    <div class="float-right">
            <a href="<?= base_url('editklasifikasiskill') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-9 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Skill</dd>
    <dd class="col-sm-8">: <?= $viewpgw['klasifikasi_skill'] ?></dd>
    <dd class="col-sm-4">Jenis SIM</dd>
    <dd class="col-sm-8">: <?= $viewpgw['jenis_sim'] ?></dd>
    <dd class="col-sm-4">Nomor SIM</dd>
    <dd class="col-sm-8">: <?= $viewpgw['no_sim'] ?></dd>
    <?php //Menghitung Masa Berakhir Kerja
    $startdate = new DateTime($viewpgw['masa_berlaku_sim']);
    $enddate = new DateTime(date('Y-m-d'));
    $intervalsim = $startdate->diff($enddate);?>

    <dd class="col-sm-4">Masa Berlaku</dd>
    <dd class="col-sm-8">: 
    <?php if($intervalsim->days <= "30"){ 
        ?>Sisa <?php echo $intervalsim->days; ?> Hari
     <?php }else{
       echo $viewpgw['masa_berlaku_sim'];
      }
      ?>
    </dd>
    </dl>
    </div>
    </div>   
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KONTRAK KERJA
    <div class="float-right">
            <a href="<?= base_url('editkontrakkerja') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-9 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Start</dd>
    <dd class="col-sm-8">: <?= $viewpgw['start_kontrak'] ?></dd>
    <dd class="col-sm-4">Finish</dd>
    <dd class="col-sm-8">: <?= $viewpgw['finish_kontrak'] ?></dd>
    <dd class="col-sm-4">Status Kontrak</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_kontrak'] ?></dd>
    
    <?php //Menghitung Masa Berakhir Kerja
    $start_date = new DateTime($viewpgw['finish_kontrak']);
    $end_date = new DateTime(date('Y-m-d'));
    $interval = $start_date->diff($end_date);

    //Menghitung Lama kerja
    $awal  = new DateTime($viewpgw['start_kontrak']);
    $akhir = new DateTime(); // Waktu sekarang
    $diff  = $awal->diff($akhir);?>

    <dd class="col-sm-4">Masa Berakhir</dd>
    <dd class="col-sm-8">: Sisa <?= $interval->days; ?> Hari</dd>
    <dd class="col-sm-4">Lama Kerja</dd>
    <dd class="col-sm-8">: <?= $diff->y; ?> Tahun, <?= $diff->m; ?> Bulan, <?= $diff->d; ?> Hari</dd>
    <dd class="col-sm-4">Seragam</dd>
    <dd class="col-sm-8">: <?= $viewpgw['seragam'] ?></dd>
    <dd class="col-sm-4">Tanggal Pengiriman Seragam</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tanggal_pengiriman_seragam'] ?></dd>
    <dd class="col-sm-4">ID Card</dd>
    <dd class="col-sm-8">: <?= $viewpgw['id_card'] ?></dd>
    <dd class="col-sm-4">Tanggal Pengiriman ID Card</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tanggal_pengiriman_id_card'] ?></dd>
    </dl>
    </div>
    </div>    
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    TRAINING SAFETY DRIVING
    <div class="float-right">
            <a href="<?= base_url('edittrainingsafetydriving') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-10 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Keterangan Training</dd>
    <dd class="col-sm-8">: <?= $viewpgw['training_safety_driving'] ?></dd>
    <dd class="col-sm-4">Tanggal Pelaksanaan Training Terakhir</dd>
    <dd class="col-sm-8">: <?= $viewpgw['pelaksanaan_training_terakhir'] ?></dd>
    <dd class="col-sm-4">Masa Training</dd>
    <dd class="col-sm-8">: <?= $viewpgw['masa_training'] ?></dd>
    <dd class="col-sm-4">Status Training</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_training'] ?></dd>
    <dd class="col-sm-4">Nilai Training Safety Driving (PREE TEST)</dd>
    <dd class="col-sm-8">: <?= $viewpgw['nilai_training_safety_driving_pree_test'] ?></dd>
    <dd class="col-sm-4">Nilai Training Safety Driving (POST TEST)</dd>
    <dd class="col-sm-8">: <?= $viewpgw['nilai_training_safety_driverng_post_test'] ?></dd>
    </dl>
    </div>
    </div> 
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    KETERANGAN VAKSIN PEGAWAI
    <div class="float-right">
            <a href="<?= base_url('editketeranganvaksinpegawai') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-10 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Status Vaksin Pertama</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_vaksin_pertama'] ?></dd>
    <dd class="col-sm-4">Mengapa Belum Vaksin Pertama</dd>
    <dd class="col-sm-8">: <?= $viewpgw['mengapa_belum_vaksi_pertama'] ?></dd>
    <dd class="col-sm-4">Tanggal Vaksin Pertama</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tanggal_vaksin_pertama'] ?></dd>
    <dd class="col-sm-4">Lokasi Vaksin Pertama</dd>
    <dd class="col-sm-8">: <?= $viewpgw['lokasi_vaksin_pertama'] ?></dd>
    <dd class="col-sm-4">Status Vaksin Kedua</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_vaksin_kedua'] ?></dd>
    <dd class="col-sm-4">Mengapa Belum Vaksin Kedua</dd>
    <dd class="col-sm-8">: <?= $viewpgw['mengapa_belum_vaksi_kedua'] ?></dd>
    <dd class="col-sm-4">Tanggal Vaksin Kedua</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tanggal_vaksin_kedua'] ?></dd>
    <dd class="col-sm-4">Lokasi Vaksin Kedua</dd>
    <dd class="col-sm-8">: <?= $viewpgw['lokasi_vaksin_kedua'] ?></dd>
    <dd class="col-sm-4">Status Vaksin Booster</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_vaksin_booster'] ?></dd>
    <dd class="col-sm-4">Mengapa Belum Vaksin Booster</dd>
    <dd class="col-sm-8">: <?= $viewpgw['mengapa_belum_vaksin_booster'] ?></dd>
    <dd class="col-sm-4">Tanggal Vaksin Booster</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tanggal_vaksin_booster'] ?></dd>
    <dd class="col-sm-4">Lokasi Vaksin Booster</dd>
    <dd class="col-sm-8">: <?= $viewpgw['lokasi_vaksin_booster'] ?></dd>
    </dl>
    </div>
    </div> 
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    TRAINING PEGAWAI
    <div class="float-right">
            <a href="<?= base_url('edittrainingpegawai') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-10 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Tanggal Training Online Part 1</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tgl_training_online_part_1'] ?></dd>
    <dd class="col-sm-4">Status Training Online Part 1</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_training_online_part_1'] ?></dd>
    <dd class="col-sm-4">Point Part 1</dd>
    <dd class="col-sm-8">: <?= $viewpgw['point_part_1'] ?></dd>
    <dd class="col-sm-4">Tanggal Training Online Part 2</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tgl_training_online_part_2'] ?></dd>
    <dd class="col-sm-4">Status Training Online Part 2</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_training_online_part_2'] ?></dd>
    <dd class="col-sm-4">Point Part 2</dd>
    <dd class="col-sm-8">: <?= $viewpgw['point_part_2'] ?></dd>
    <dd class="col-sm-4">Status Training Attitude & Knowledge</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tgl_training_attitude_n_knowledge'] ?></dd>
    <dd class="col-sm-4">Point Attitude & Knowledge</dd>
    <dd class="col-sm-8">: <?= $viewpgw['status_training_attitude_n_knowledge'] ?></dd>
    </dl>
    </div>
    </div>
</div>
</div>

<div class="card mb-3">
  <div class="card-header">
    KETERANGAN BPJS, JAMSOSTEK DAN REWARD
    <div class="float-right">
            <a href="<?= base_url('editketeranganbpjs') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-10 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Nomor BPJS Kesehatan</dd>
    <dd class="col-sm-8">: <?= $viewpgw['nomor_bpjs_kesehatan'] ?></dd>
    <dd class="col-sm-4">Keterangan Proses BPJS Kesehatan</dd>
    <dd class="col-sm-8">: <?= $viewpgw['keterangan_proses_bpjs_kesehatan'] ?></dd>
    <dd class="col-sm-4">Keterangan Status BPJS Kesehatan</dd>
    <dd class="col-sm-8">: <?= $viewpgw['keterangan_status_bpjs_kesehatan'] ?></dd>
    <dd class="col-sm-4">Nomor JAMSOSTEK</dd>
    <dd class="col-sm-8">: <?= $viewpgw['nomor_jamsostek'] ?></dd>
    <dd class="col-sm-4">Keterangan JAMSOSTEK</dd>
    <dd class="col-sm-8">: <?= $viewpgw['keterangan_jamsostek'] ?></dd>
    <dd class="col-sm-4">Reward</dd>
    <dd class="col-sm-8">: <?= $viewpgw['reward'] ?></dd>
    <dd class="col-sm-4">Tanggal Pemberian Reward</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tglblnthn_pemberian_reward'] ?></dd>
    </dl>
    </div>
    </div> 
  </div>
 </div>
 
 <div class="card mb-3">
  <div class="card-header">
  ACCIDENT
  <div class="float-right">
            <a href="<?= base_url('editaccident') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-10 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Accident Report</dd>
    <dd class="col-sm-8">: <?= $viewpgw['accident_report'] ?></dd>
    <dd class="col-sm-4">Tanggal Accident</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tglblnthn_accident'] ?></dd>
    <dd class="col-sm-4">Case</dd>
    <dd class="col-sm-8">: <?= $viewpgw['kasus'] ?></dd>
    <dd class="col-sm-4">Tanggal Case</dd>
    <dd class="col-sm-8">: <?= $viewpgw['tglblnthn_kasus'] ?></dd>
    <dd class="col-sm-4">SP 1</dd>
    <dd class="col-sm-8">: <?= $viewpgw['sp_i'] ?></dd>
    <dd class="col-sm-4">SP 2</dd>
    <dd class="col-sm-8">: <?= $viewpgw['sp_ii'] ?></dd>
    <dd class="col-sm-4">SP 3</dd>
    <dd class="col-sm-8">: <?= $viewpgw['sp_iii'] ?></dd>
    <dd class="col-sm-4">Keterangan Out & Mutasi</dd>
    <dd class="col-sm-8">: <?= $viewpgw['keterangan_out_dan_mutasi'] ?></dd>
    </dl>
    </div>
    </div> 
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
    TRAINING
    <div class="float-right">
            <a href="<?= base_url('edittraining') .'?id='. $viewpgw['id'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
</div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-10 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Masa Berlaku SIM</dd>
    <dd class="col-sm-8">: <?= $viewpgw['masa_berlaku_sim_2'] ?></dd>
    <dd class="col-sm-4">Masa Training</dd>
    <dd class="col-sm-8">: <?= $viewpgw['masa_training_2'] ?></dd>
    <dd class="col-sm-4">Ketarangan Training</dd>
    <dd class="col-sm-8">: <?= $viewpgw['keterangan_training_2'] ?></dd>
    <dd class="col-sm-4">Jadwal Training</dd>
    <dd class="col-sm-8">: <?= $viewpgw['jadwal_training_2'] ?></dd>
    <dd class="col-sm-4">Driver Leader</dd>
    <dd class="col-sm-8">: <?= $viewpgw['driver_leader'] ?></dd>
    <dd class="col-sm-4">Jabatan</dd>
    <dd class="col-sm-8">: <?= $viewpgw['jabatan'] ?></dd>
    </dl>
    </div>
    </div> 
  </div>
 </div>


</div>