<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updateketeranganvaksinpegawai')?>" method="post">	
 <div class="card mb-3">
  <div class="card-header">
    KETERANGAN VAKSIN PEGAWAI
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Status Vaksin Pertama</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_pertama" name="status_vaksin_pertama"  value="SUDAH" <?php if($pgw['status_vaksin_pertama']=='SUDAH') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_pertama" name="status_vaksin_pertama"  value="BELUM" <?php if($pgw['status_vaksin_pertama']=='BELUM') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Mengapa Belum Vaksin Pertama</label>
    <input type="text" class="form-control" id="mengapa_belum_vaksi_pertama" name="mengapa_belum_vaksi_pertama" value="<?= $pgw['mengapa_belum_vaksi_pertama'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Vaksin Pertama</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_vaksin_pertama" name="tanggal_vaksin_pertama" value="<?= $pgw['tanggal_vaksin_pertama'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lokasi Vaksin Pertama</label>
    <input type="text" class="form-control" id="lokasi_vaksin_pertama" name="lokasi_vaksin_pertama" value="<?= $pgw['lokasi_vaksin_pertama'] ?>" >
    </div>  
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Status Vaksin Kedua</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_kedua" name="status_vaksin_kedua"  value="SUDAH" <?php if($pgw['status_vaksin_kedua']=='SUDAH') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_kedua" name="status_vaksin_kedua"  value="BELUM" <?php if($pgw['status_vaksin_kedua']=='BELUM') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Mengapa Belum Vaksin Kedua</label>
    <input type="text" class="form-control" id="mengapa_belum_vaksi_kedua" name="mengapa_belum_vaksi_kedua" value="<?= $pgw['mengapa_belum_vaksi_kedua'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Vaksin Kedua</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_vaksin_kedua" name="tanggal_vaksin_kedua" value="<?= $pgw['tanggal_vaksin_kedua'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lokasi Vaksin Kedua</label>
    <input type="text" class="form-control" id="lokasi_vaksin_kedua" name="lokasi_vaksin_kedua" value="<?= $pgw['lokasi_vaksin_kedua'] ?>" >
    </div> 
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Status Vaksin Booster</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_booster" name="status_vaksin_booster"  value="SUDAH" <?php if($pgw['status_vaksin_booster']=='SUDAH') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="status_vaksin_booster" name="status_vaksin_booster"  value="BELUM" <?php if($pgw['status_vaksin_booster']=='BELUM') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Mengapa Belum Vaksin Booster</label>
    <input type="text" class="form-control" id="mengapa_belum_vaksin_booster" name="mengapa_belum_vaksin_booster" value="<?= $pgw['mengapa_belum_vaksin_booster'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Vaksin Booster</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_vaksin_booster" name="tanggal_vaksin_booster" value="<?= $pgw['tanggal_vaksin_booster'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lokasi Vaksin Booster</label>
    <input type="text" class="form-control" id="lokasi_vaksin_booster" name="lokasi_vaksin_booster" value="<?= $pgw['lokasi_vaksin_booster'] ?>" >
    </div> 
  </div>
 </div>

 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>