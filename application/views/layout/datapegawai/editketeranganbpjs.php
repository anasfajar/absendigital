<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updateketeranganbpjs')?>" method="post">	
 <div class="card mb-3">
  <div class="card-header">
    BPJS & REWARD PEGAWAI 
  </div>
  <div class="card-body">
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Nomor BPJS Kesehatan</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="number" class="form-control" id="nomor_bpjs_kesehatan" name="nomor_bpjs_kesehatan" value="<?= $pgw['nomor_bpjs_kesehatan'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Keterangan Proses BPJS Kesehatan</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="keterangan_proses_bpjs_kesehatan" name="keterangan_proses_bpjs_kesehatan"  value="SUDAH" <?php if($pgw['keterangan_proses_bpjs_kesehatan']=='SUDAH') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="keterangan_proses_bpjs_kesehatan" name="keterangan_proses_bpjs_kesehatan"  value="BELUM" <?php if($pgw['keterangan_proses_bpjs_kesehatan']=='BELUM') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Keterangan Status BPJS Kesehatan</label>
    <input type="text" class="form-control" id="keterangan_status_bpjs_kesehatan" name="keterangan_status_bpjs_kesehatan" value="<?= $pgw['keterangan_status_bpjs_kesehatan'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Nomor JAMSOSTEK</label>
    <input type="number" class="form-control" id="nomor_jamsostek" name="nomor_jamsostek" value="<?= $pgw['nomor_jamsostek'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Keterangan JAMSOSTEK</label>
    <input type="text" class="form-control" id="keterangan_jamsostek" name="keterangan_jamsostek" value="<?= $pgw['keterangan_jamsostek'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Reward</label>
    <input type="text" class="form-control" id="reward" name="reward" value="<?= $pgw['reward'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Tanggal Pemberian Reward</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_pemberian_reward" name="tglblnthn_pemberian_reward" value="<?= $pgw['tglblnthn_pemberian_reward'] ?>" >
    </div>
  </div>
 </div> 
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>