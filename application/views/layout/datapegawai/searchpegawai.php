<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Data Pegawai</h1>     
<form action="<?= base_url('datapegawaifico'); ?>" method="post">
 <div class="card mb-3" >
    <div class="card-header">
        Operation Point
    </div>
    <div class="card-body">
      <div class="mb-3">
        <label for="inputState" class="form-label">Operating Point</label>
          <select id="inputOp" name="inputOp" class="form-control" id="formGroupExampleInput2">
            <option value="" selected>-- Pilih --</option>
            <?php foreach($op as $row){ 
              echo  "<option value='".$row->nama_area."'>".$row->nama_area."</option>";
            }?>
          </select>
      </div>
      <div class="mb-3">
        <label for="formGroupExampleInput2" class="form-label">Customer</label>
        <select id="inputCustomer" name="inputCustomer" class="form-control" id="formGroupExampleInput2">
          <option value = "" selected>-- Pilih --</option>
        </select>
      </div>
      <button type="submit" class="btn btn-block btn-primary" id="caripgw-btn"><span class="fas fa-search mr-1"></span>Cari</button>

    </div>
  </div>

</form>
</div>
</main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                    <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                </div>
                <div class="text-muted">
                    Page rendered in <strong>{elapsed_time}</strong> detik.
                </div>
            </div>
        </div>
    </footer>
    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>
    <script>
      // $(document).ready(function(){
        
      // })

      $('#inputOp').on('change',function(){
        let op = this.value;
          $.getJSON('<?= base_url('ajax/get_customer?op='); ?>' + op,function(data){
            $('#inputCustomer').empty();
            $('#inputCustomer').prepend('<option selected>Pilih Customer</option>');

                for (let i = 0; i < data.length; i++) {
                    $('#inputCustomer').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
              console.log(data)
          })
      })

      $(".logout").click(function(event) {
            $.ajax({
                type: "POST",
                url: "<?= base_url('ajax/logoutajax'); ?>",
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Logging Out",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Logout',
                        text: 'Anda Telah Keluar!',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    location.reload();
                }
            });
            event.preventDefault();
        });
    </script>
    </body>

    </html>
