<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updatekontrakkerja')?>" method="post">
 <div class="card mb-3">
  <div class="card-header">
    KONTRAK KERJA
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Start</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="date" class="form-control col-sm-3" id="start_kontrak" name="start_kontrak" value="<?= $pgw['start_kontrak'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Finish</label>
    <input type="date" class="form-control col-sm-3" id="finish_kontrak" name="finish_kontrak" value="<?= $pgw['finish_kontrak'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Kontrak</label>
    <input type="text" class="form-control col-sm-6" id="status_kontrak" name="status_kontrak" value="<?= $pgw['status_kontrak'] ?>" >
    </div>
    <?php 
    //Menghitung Masa Berakhir Kerja
    $start_date = new DateTime($pgw['finish_kontrak']);
    $end_date = new DateTime(date('Y-m-d'));
    $interval = $start_date->diff($end_date);

    //Menghitung Lama kerja
    $awal  = new DateTime($pgw['start_kontrak']);
    $akhir = new DateTime(); // Waktu sekarang
    $diff  = $awal->diff($akhir);
    ?>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Berakhir</label>
    <input type="text" class="form-control col-sm-3" id="masa_kontrak_berakhir" name="masa_kontrak_berakhir" value="<?= $interval->days; ?> Hari" readonly>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lama Kerja</label>
    <input type="text" class="form-control col-sm-6" id="lama_kerja" name="lama_kerja" value="<?= $diff->y; ?> Tahun, <?= $diff->m; ?> Bulan, <?= $diff->d; ?> Hari, " readonly>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Seragam</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="seragam" name="seragam"  value="SUDAH" <?php if($pgw['seragam']=='SUDAH') echo 'checked'?> >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="seragam" name="seragam"  value="BELUM" <?php if($pgw['seragam']=='BELUM') echo 'checked'?> >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
      </div>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Pengiriman Seragam</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_pengiriman_seragam" name="tanggal_pengiriman_seragam" value="<?= $pgw['tanggal_pengiriman_seragam'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">ID Card</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" id="id_card" name="id_card"  value="SUDAH" <?php if($pgw['id_card']=='SUDAH') echo 'checked'?> >
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="id_card" name="id_card"  value="BELUM" <?php if($pgw['id_card']=='BELUM') echo 'checked'?> >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
</div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Pengiriman ID Card</label>
    <input type="date" class="form-control col-sm-3"  id="tanggal_pengiriman_id_card" name="tanggal_pengiriman_id_card" value="<?= $pgw['tanggal_pengiriman_id_card'] ?>">
    </div>
  </div>
 </div>

 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>