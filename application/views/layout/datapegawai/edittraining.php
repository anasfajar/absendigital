<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updatetraining')?>" method="post">	
 <div class="card mb-3">
  <div class="card-header">
    TRAINING
  </div>
  <div class="card-body">
  <? 
  
  ?>
  <div class="mb-3">
    <?php 
        $tgl_now=date("Y-m-d"); //tanggal sekarang
        $tgl_dibuat=$pgw['masa_berlaku_sim']; // tanggal dibuat aplikasi
        $masa_berlaku = strtotime('-14 days', strtotime($tgl_dibuat)); // jangka waktu + 30 hari atau sebulan
        $tgl_exp=date("Y-m-d",$masa_berlaku); //tanggal expired atau kadaluarsa
        
        if($pgw['klasifikasi_skill']== "NON DRIVER"){
        $berlakusim = "NON DRIVER";
        }elseif($tgl_now >=$tgl_exp){
        $berlakusim = "Masa Berlaku SIM akan segera habis";
        }else{
        $berlakusim = "SIM Masih Berlaku";
        }
    ?>

    <label for="formGroupExampleInput" class="form-label">Masa Berlaku SIM</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="text" class="form-control" id="masa_berlaku_sim_2" name="masa_berlaku_sim_2" value="<?= $berlakusim; ?>" readonly>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Training</label>
    <input type="text" class="form-control" id="masa_training_2" name="masa_training_2" value="<?= $pgw['masa_training_2'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Ketarangan Training</label>
    <input type="text" class="form-control" id="keterangan_training_2" name="keterangan_training_2" value="<?= $pgw['keterangan_training_2'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jadwal Training</label>
    <input type="date" class="form-control col-sm-3" id="jadwal_training_2" name="jadwal_training_2" value="<?= $pgw['jadwal_training_2'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Driver Leader</label>
    <input type="text" class="form-control" id="driver_leader" name="driver_leader" value="<?= $pgw['driver_leader'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jabatan</label>
    <select id="inputState" class="form-control col-sm-3" id="jabatan" name="jabatan" value="<?= $pgw['jabatan'] ?>">
      <option value="DRIVER" <?php if($pgw['customer']== "DRIVER"){ echo 'selected';}?> >DRIVER</option>
      <option value="NON DRIVER" <?php if($pgw['customer']== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
      <option value="FICO" <?php if($pgw['customer']== "FICO"){ echo 'selected';}?> >FICO</option>
      <option value="HELPER" <?php if($pgw['customer']== "HELPER"){ echo 'selected';}?> >HELPER</option>
      <option value="CHECKER" <?php if($pgw['customer']== "CHECKER"){ echo 'selected';}?> >CHECKER</option>
      <option value="ADMIN" <?php if($pgw['customer']== "ADMIN"){ echo 'selected';}?> >ADMIN</option>
      <option value="ADMIN DISPATCHER" <?php if($pgw['customer']== "ADMIN DISPATCHER"){ echo 'selected';}?>>ADMIN DISPATCHER</option>
      <option value="DISPATCHER" <?php if($pgw['customer']== "DISPATCHER"){ echo 'selected';}?> >DISPATCHER</option>
      <option value="DISPATCHER LEADER" <?php if($pgw['customer']== "DISPATCHER LEADER"){ echo 'selected';}?>>DISPATCHER LEADER</option>
      <option value="DRIVER LEADER" <?php if($pgw['customer']== "DRIVER LEADER"){ echo 'selected';}?> >DRIVER LEADER</option>
      <option value="FUEL MANAGEMENT" <?php if($pgw['customer']== "FUEL MANAGEMENT"){ echo 'selected';}?> >FUEL MANAGEMENT</option>
      <option value="IT" <?php if($pgw['customer']== "IT"){ echo 'selected';}?> >IT</option>
      <option value="MONITORING GPS" <?php if($pgw['customer']== "MONITORING GPS"){ echo 'selected';}?> >MONITORING GPS</option>
      <option value="OPERATING CASHIER" <?php if($pgw['customer']== "OPERATING CASHIER"){ echo 'selected';}?> >OPERATING CASHIER</option>
      <option value="OPERATING POINT COORDINATOR" <?php if($pgw['customer']== "OPERATING POINT COORDINATOR"){ echo 'selected';}?> >OPERATING POINT COORDINATOR</option>
      <option value="PJS OPERATING POINT COORDINATOR" <?php if($pgw['customer']== "PJS OPERATING POINT COORDINATOR"){ echo 'selected';}?> >PJS OPERATING POINT COORDINATOR</option>
      <option value="UNIT CONTROLLER" <?php if($pgw['customer']== "UNIT CONTROLLER"){ echo 'selected';}?> >UNIT CONTROLLER</option>
      <option value="UNIT MANAGEMENT" <?php if($pgw['customer']== "UNIT MANAGEMENT"){ echo 'selected';}?> >UNIT MANAGEMENT</option>
    </select>
    </div>    
  </div>
 </div>
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>