<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updatetrainingsafetydriving')?>" method="post">
 <div class="card mb-3">
  <div class="card-header">
    TRAINING SAFETY DRIVING
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Ketarangan Training</label>
    <div class="form-check">
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
        <input class="form-check-input" type="radio" id="training_safety_driving" name="training_safety_driving"  value="SUDAH" <?php if($pgw['training_safety_driving']=='SUDAH') echo 'checked'?>>
        <label class="form-check-label" for="gridRadios1">
          Sudah
        </label>
      </div>
      <div class="form-check">
        <input class="form-check-input" type="radio" id="training_safety_driving" name="training_safety_driving"  value="BELUM" <?php if($pgw['training_safety_driving']=='BELUM') echo 'checked'?> >
        <label class="form-check-label" for="gridRadios2">
          Belum
        </label>
    </div>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Pelaksanaan Training Terakhir</label>
    <input type="date" class="form-control col-sm-3" id="pelaksanaan_training_terakhir" name="pelaksanaan_training_terakhir" value="<?= $pgw['pelaksanaan_training_terakhir'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Training</label>
    <input type="text" class="form-control" id="masa_training" name="masa_training" value="<?= $pgw['masa_training'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Status Training</label>
    <select id="status_training" name="status_training" class="form-control col-sm-3" id="formGroupExampleInput2">
      <option value="IN CLASS" <?php if($pgw['status_training']== "IN CLASS"){ echo 'selected';}?> >IN CLASS</option>
      <option value="ONLINE" <?php if($pgw['status_training']== "ONLINE"){ echo 'selected';}?> >ONLINE</option>
      <option value="NON DRIVER" <?php if($pgw['status_training']== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nilai Training Safety Driving (PREE TEST)</label>
    <input type="text" class="form-control col-sm-6" id="nilai_training_safety_driving_pree_test" name="nilai_training_safety_driving_pree_test" value="<?= $pgw['nilai_training_safety_driving_pree_test'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nilai Training Safety Driving (POST TEST)</label>
    <input type="text" class="form-control col-sm-6" id="nilai_training_safety_driverng_post_test" name="nilai_training_safety_driverng_post_test" value="<?= $pgw['nilai_training_safety_driverng_post_test'] ?>" >
    </div>
  </div>
 </div>

 
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>