<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Keterangan Bank Pegawai</h1>     
    <form action="<?= base_url('updateketeranganbank')?>" method="post">	

 <div class="card mb-3">
  <div class="card-header">
    KETERANGAN BANK
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Nama diBank</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="text" class="form-control" id="nama_di_bank" name="nama_di_bank" value="<?= $pgw['nama_di_bank'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Bank</label>
    <input type="text" class="form-control col-sm-6" id="bank" name="bank" value="<?= $pgw['bank'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">No. Account</label>
    <input type="number" class="form-control col-sm-6" id="no_account" name="no_account" value="<?= $pgw['no_account'] ?>" >
    </div>
  </div>
 </div>


 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>