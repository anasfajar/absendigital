<div class="row detail">
    <div class="col-md-2 col-sm-4 col-6 p-2">
        <!--<img class="img-thumbnail" src="<//?= ($datapegawai['image'] == 'default.png' ? base_url('assets/img/default-profile.png') : base_url('storage/profile/' . $datapegawai['image'])); ?>" class="card-img">-->
    </div>
    <div class="col-md-10 col-sm-8 col-6">
        <dl class="row">
            <dt class="col-sm-5">NIK:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nik'] ?></dd>
            <dt class="col-sm-5">Nama Lengkap:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nama'] ?></dd>
            <dt class="col-sm-5">Vendor:</dt>
            <dd class="col-sm-7"><?= $datapegawai['vendor'] ?><div class="ml-1 d-inline"></div></dd>
            <dt class="col-sm-5">Keterangan Aktivasi Karyawan:</dt>
            <dd class="col-sm-7 text-truncate"><?= $datapegawai['keterangan_aktivasi_karyawan'] ?></dd>
            <dt class="col-sm-5">Nomor PO :</dt>
            <dd class="col-sm-7"><?= $datapegawai['nomor_po_nama_driver_yg_diganti'] ?></dd>
            <dt class="col-sm-5">Tanggal PO:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tanggal_po'] ?></dd>
            <dt class="col-sm-5">Tanggal Inaktif Kerja:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tgl_inaktif_kerja'] ?></dd>
            <dt class="col-sm-5">Lead Time:</dt>
            <dd class="col-sm-7"><?= $datapegawai['lead_time'] ?></dd>
            <dt class="col-sm-5">Nomor Hp:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nomor_hp'] ?></dd>
            <dt class="col-sm-5">Driver:</dt>
            <dd class="col-sm-7"><?= $datapegawai['driver'] ?></dd>
            <dt class="col-sm-5">Fico:</dt>
            <dd class="col-sm-7"><?= $datapegawai['fico'] ?></dd>
            <dt class="col-sm-5">Helper:</dt>
            <dd class="col-sm-7"><?= $datapegawai['helper'] ?></dd>
            <dt class="col-sm-5">Checker:</dt>
            <dd class="col-sm-7"><?= $datapegawai['checker'] ?></dd>
            <dt class="col-sm-5">Non Driver:</dt>
            <dd class="col-sm-7"><?= $datapegawai['non_driver'] ?></dd>
            <dt class="col-sm-5">Operation Poin:</dt>
            <dd class="col-sm-7"><?= $datapegawai['operation_poin'] ?></dd>
            <dt class="col-sm-5">Customer:</dt>
            <dd class="col-sm-7"><?= $datapegawai['customer'] ?></dd>
            <dt class="col-sm-5">District:</dt>
            <dd class="col-sm-7"><?= $datapegawai['district'] ?></dd>
            <dt class="col-sm-5">No KTP:</dt>
            <dd class="col-sm-7"><?= $datapegawai['no_ktp'] ?></dd>
            <dt class="col-sm-5">Masa Berlaku:</dt>
            <dd class="col-sm-7"><?= $datapegawai['masa_berlaku'] ?></dd>
            <dt class="col-sm-5">Klasifikasi Skill:</dt>
            <dd class="col-sm-7"><?= $datapegawai['klasifikasi_skill'] ?></dd>
            <dt class="col-sm-5">Jenis SIM:</dt>
            <dd class="col-sm-7"><?= $datapegawai['jenis_sim'] ?></dd>
            <dt class="col-sm-5">No SIM:</dt>
            <dd class="col-sm-7"><?= $datapegawai['no_sim'] ?></dd>
            <dt class="col-sm-5">Masa Berlaku SIM:</dt>
            <dd class="col-sm-7"><?= $datapegawai['masa_berlaku_sim'] ?></dd>
            <dt class="col-sm-5">Tempat Lahir:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tempat_lahir'] ?></dd>
            <dt class="col-sm-5">Tanggal Lahir:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tglblnthn_lahir'] ?></dd>
            <dt class="col-sm-5">Alamat:</dt>
            <dd class="col-sm-7"><?= $datapegawai['alamat'] ?></dd>
            <dt class="col-sm-5">Staus Pajak:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_pajak'] ?></dd>
            <dt class="col-sm-5">Agama:</dt>
            <dd class="col-sm-7"><?= $datapegawai['agama'] ?></dd>
            <dt class="col-sm-5">Jenis Kelamin:</dt>
            <dd class="col-sm-7"><?= $datapegawai['jenis_kelamin'] ?></dd>
            <dt class="col-sm-5">Pendidikan Terakhir:</dt>
            <dd class="col-sm-7"><?= $datapegawai['pendidikan_terakhir'] ?></dd>
            <dt class="col-sm-5">Jenis Kelamin:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nama_di_bank'] ?></dd>
            <dt class="col-sm-5">Bank:</dt>
            <dd class="col-sm-7"><?= $datapegawai['bank'] ?></dd>
            <dt class="col-sm-5">No Account:</dt>
            <dd class="col-sm-7"><?= $datapegawai['no_account'] ?></dd>
            <dt class="col-sm-5">Nomor Kartu Keluarga:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nomor_kartu_keluarga'] ?></dd>
            <dt class="col-sm-5">NPWP:</dt>
            <dd class="col-sm-7"><?= $datapegawai['npwp'] ?></dd>
            <dt class="col-sm-5">Start Kontrak:</dt>
            <dd class="col-sm-7"><?= $datapegawai['start_kontrak'] ?></dd>
            <dt class="col-sm-5">Finish Kontrak:</dt>
            <dd class="col-sm-7"><?= $datapegawai['finish_kontrak'] ?></dd>
            <dt class="col-sm-5">Status Kontrak:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_kontrak'] ?></dd>
            <dt class="col-sm-5">Masa Kontak Berakhir:</dt>
            <dd class="col-sm-7"><?= $datapegawai['masa_kontrak_berakhir'] ?></dd>
            <dt class="col-sm-5">Lama Kerja:</dt>
            <dd class="col-sm-7"><?= $datapegawai['lama_kerja'] ?></dd>
            <dt class="col-sm-5">Seragam:</dt>
            <dd class="col-sm-7"><?= $datapegawai['seragam'] ?></dd>
            <dt class="col-sm-5">Tanggal Pengiriman Seragam:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tanggal_pengiriman_seragam'] ?></dd>
            <dt class="col-sm-5">ID Card:</dt>
            <dd class="col-sm-7"><?= $datapegawai['id_card'] ?></dd>
            <dt class="col-sm-5">Tanggal Pengiriman ID Card:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tanggal_pengiriman_id_card'] ?></dd>
            <dt class="col-sm-5">Training Safety Driving:</dt>
            <dd class="col-sm-7"><?= $datapegawai['training_safety_driving'] ?></dd>
            <dt class="col-sm-5">Pelaksanaan Training Terakhir:</dt>
            <dd class="col-sm-7"><?= $datapegawai['pelaksanaan_training_terakhir'] ?></dd>
            <dt class="col-sm-5">Masa Training:</dt>
            <dd class="col-sm-7"><?= $datapegawai['masa_training'] ?></dd>
            <dt class="col-sm-5">Status Training:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_training'] ?></dd>
            <dt class="col-sm-5">Nilai Training Safety Driving Pree Test:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nilai_training_safety_driving_pree_test'] ?></dd>
            <dt class="col-sm-5">Nilai Training Safety Driverng Pree Test:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nilai_training_safety_driverng_post_test'] ?></dd>
            <dt class="col-sm-5">Status Vaksin Pertama:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_vaksin_pertama'] ?></dd>
            <dt class="col-sm-5">Mengapa Belum Vaksin Pertama:</dt>
            <dd class="col-sm-7"><?= $datapegawai['mengapa_belum_vaksi_pertama'] ?></dd>
            <dt class="col-sm-5">Tanggal Vaksin Pertama:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tanggal_vaksin_pertama'] ?></dd>
            <dt class="col-sm-5">Lokasi Vaksin Pertama:</dt>
            <dd class="col-sm-7"><?= $datapegawai['lokasi_vaksin_pertama'] ?></dd>
            <dt class="col-sm-5">Status Vaksin Pertama:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_vaksin_kedua'] ?></dd>
            <dt class="col-sm-5">Mengapa Belum Vaksin Kedua:</dt>
            <dd class="col-sm-7"><?= $datapegawai['mengapa_belum_vaksi_kedua'] ?></dd>
            <dt class="col-sm-5">Tanggal Vaksin Kedua:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tanggal_vaksin_kedua'] ?></dd>
            <dt class="col-sm-5">Lokasi Vaksin Kedua:</dt>
            <dd class="col-sm-7"><?= $datapegawai['lokasi_vaksin_kedua'] ?></dd>
            <dt class="col-sm-5">Status Vaksin Booster:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_vaksin_booster'] ?></dd>
            <dt class="col-sm-5">Mengapa Belum Vaksin Booster:</dt>
            <dd class="col-sm-7"><?= $datapegawai['mengapa_belum_vaksin_booster'] ?></dd>
            <dt class="col-sm-5">Tanggal Vaksin Booster:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tanggal_vaksin_booster'] ?></dd>
            <dt class="col-sm-5">Lokasi Vaksin Booster:</dt>
            <dd class="col-sm-7"><?= $datapegawai['lokasi_vaksin_booster'] ?></dd>
            <dt class="col-sm-5">Tanggal Training Online Part 1:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tgl_training_online_part_1'] ?></dd>
            <dt class="col-sm-5">Status Training Online Part 1:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_training_online_part_1'] ?></dd>
            <dt class="col-sm-5">Point Part 1:</dt>
            <dd class="col-sm-7"><?= $datapegawai['point_part_1'] ?></dd>
            <dt class="col-sm-5">Tanggal Training Online Part 2:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tgl_training_online_part_2'] ?></dd>
            <dt class="col-sm-5">Status Training Online Part 2:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_training_online_part_2'] ?></dd>
            <dt class="col-sm-5">Point Part 2:</dt>
            <dd class="col-sm-7"><?= $datapegawai['point_part_2'] ?></dd>
            <dt class="col-sm-5">Tanggal Training Attitude & Knowledge:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tgl_training_attitude_n_knowledge'] ?></dd>
            <dt class="col-sm-5">Status Training Attitude & Knowledge:</dt>
            <dd class="col-sm-7"><?= $datapegawai['status_training_attitude_n_knowledge'] ?></dd>
            <dt class="col-sm-5">Point Attitude & Knowledge:</dt>
            <dd class="col-sm-7"><?= $datapegawai['point_attitude_n_knowledge'] ?></dd>
            <dt class="col-sm-5">Nomor BPJS Kesehatan:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nomor_bpjs_kesehatan'] ?></dd>
            <dt class="col-sm-5">Keterangan BPJS Kesegatan:</dt>
            <dd class="col-sm-7"><?= $datapegawai['keterangan_proses_bpjs_kesehatan'] ?></dd>
            <dt class="col-sm-5">Nomor Jamsostek:</dt>
            <dd class="col-sm-7"><?= $datapegawai['nomor_jamsostek'] ?></dd>
            <dt class="col-sm-5">Keterangan Jamsostek:</dt>
            <dd class="col-sm-7"><?= $datapegawai['keterangan_jamsostek'] ?></dd>
            <dt class="col-sm-5">Reward:</dt>
            <dd class="col-sm-7"><?= $datapegawai['reward'] ?></dd>
            <dt class="col-sm-5">Tanggal Pemberian Reward:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tglblnthn_pemberian_reward'] ?></dd>
            <dt class="col-sm-5">Accident Report:</dt>
            <dd class="col-sm-7"><?= $datapegawai['accident_report'] ?></dd>
            <dt class="col-sm-5">Tanggal Accident:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tglblnthn_accident'] ?></dd>
            <dt class="col-sm-5">Kasus:</dt>
            <dd class="col-sm-7"><?= $datapegawai['kasus'] ?></dd>
            <dt class="col-sm-5">Tanggal Kasus:</dt>
            <dd class="col-sm-7"><?= $datapegawai['tglblnthn_kasus'] ?></dd>
            <dt class="col-sm-5">Email:</dt>
            <dd class="col-sm-7"><?= $datapegawai['email_address'] ?></dd>
            <dt class="col-sm-5">SP I:</dt>
            <dd class="col-sm-7"><?= $datapegawai['sp_i'] ?></dd>
            <dt class="col-sm-5">SP II:</dt>
            <dd class="col-sm-7"><?= $datapegawai['sp_ii'] ?></dd>
            <dt class="col-sm-5">SP III:</dt>
            <dd class="col-sm-7"><?= $datapegawai['sp_iii'] ?></dd>
            <dt class="col-sm-5">Keterangan Out & Mutasi:</dt>
            <dd class="col-sm-7"><?= $datapegawai['keterangan_out_dan_mutasi'] ?></dd>
            <dt class="col-sm-5">Masa Berlaku SIM 2:</dt>
            <dd class="col-sm-7"><?= $datapegawai['masa_berlaku_sim_2'] ?></dd>
            <dt class="col-sm-5">Masa Training 2:</dt>
            <dd class="col-sm-7"><?= $datapegawai['masa_training_2'] ?></dd>
            <dt class="col-sm-5">Keterangan & Jadwal Training:</dt>
            <dd class="col-sm-7"><?= $datapegawai['keterangan_dan_jadwal_training'] ?></dd>
            <dt class="col-sm-5">Driver Leader:</dt>
            <dd class="col-sm-7"><?= $datapegawai['driver_leader'] ?></dd>
            <dt class="col-sm-5">Jabatan:</dt>
            <dd class="col-sm-7"><?= $datapegawai['jabatan'] ?></dd>
        </dl>
    </div>
</div>