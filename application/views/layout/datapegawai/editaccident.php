<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Pegawai</h1>     
    <form action="<?= base_url('updateaccident')?>" method="post">	

 <div class="card mb-3">
  <div class="card-header">
    ACCIDENT
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Accident Report</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="text" class="form-control" id="accident_report" name="accident_report" value="<?= $pgw['accident_report'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Accident</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_accident" name="tglblnthn_accident" value="<?= $pgw['tglblnthn_accident'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Case</label>
    <input type="text" class="form-control" id="kasus" name="kasus" value="<?= $pgw['kasus'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Case</label>
    <input type="date" class="form-control col-sm-3" id="tglblnthn_kasus" name="tglblnthn_kasus" value="<?= $pgw['tglblnthn_kasus'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">SP 1</label>
    <input type="date" class="form-control col-sm-3" id="sp_i" name="sp_i" value="<?= $pgw['sp_i'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">SP 2</label>
    <input type="date" class="form-control col-sm-3" id="sp_ii" name="sp_ii" value="<?= $pgw['sp_ii'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">SP 3</label>
    <input type="date" class="form-control col-sm-3" id="sp_iii" name="sp_iii" value="<?= $pgw['sp_iii'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Keterangan Out & Mutasi</label>
    <input type="text" class="form-control" id="keterangan_out_dan_mutasi" name="keterangan_out_dan_mutasi" value="<?= $pgw['keterangan_out_dan_mutasi'] ?>" >
    </div>
  </div>
 </div>

 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>