<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Data Aktifasi Karyawan Pegawai</h1>     
    <form action="<?= base_url('updateaktifasikaryawan')?>" method="post">	

 <div class="card mb-3">
  <div class="card-header">
    AKTIFIASI KARYAWAN
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Keterangan Aktifasi Karyawan</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <input type="text" class="form-control" id="keterangan_aktivasi_karyawan" name="keterangan_aktivasi_karyawan" value="<?= $pgw['keterangan_aktivasi_karyawan'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nomor PO / Nama Driver Yang Diganti</label>
    <input type="text" class="form-control" id="nomor_po_nama_driver_yg_diganti" name="nomor_po_nama_driver_yg_diganti" value="<?= $pgw['nomor_po_nama_driver_yg_diganti'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal PO</label>
    <input type="date" class="form-control col-sm-3" id="tanggal_po" name="tanggal_po" value="<?= $pgw['tanggal_po'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tanggal Aktif Kerja</label>
    <input type="date" class="form-control col-sm-3" id="tgl_inaktif_kerja" name="tgl_inaktif_kerja" value="<?= $pgw['tgl_inaktif_kerja'] ?>" >
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lead Team</label>
    <input type="text" class="form-control col-sm-6" id="lead_time" name="lead_time" value="<?= $pgw['lead_time'] ?>" >
    </div>
    <div class="mb-3">
    <label for="inputState" class="form-label">Jobs</label>
    <select name="jobs" class="form-control col-sm-3" id="jobs">
      <option value="DRIVER" <?php if($pgw['jobs']== "DRIVER"){ echo 'selected';}?> >DRIVER</option>
      <option value="NON DRIVER" <?php if($pgw['jobs']== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
      <option value="FICO" <?php if($pgw['jobs']== "FICO"){ echo 'selected';}?> >FICO</option>
      <option value="HELPER" <?php if($pgw['jobs']== "HELPER"){ echo 'selected';}?> >HELPER</option>
      <option value="CHECKER" <?php if($pgw['jobs']== "CHECKER"){ echo 'selected';}?> >CHECKER</option>
      <option value="ADMIN" <?php if($pgw['jobs']== "ADMIN"){ echo 'selected';}?> >ADMIN</option>
      <option value="ADMIN DISPATCHER" <?php if($pgw['jobs']== "ADMIN DISPATCHER"){ echo 'selected';}?>>ADMIN DISPATCHER</option>
      <option value="DISPATCHER" <?php if($pgw['jobs']== "DISPATCHER"){ echo 'selected';}?> >DISPATCHER</option>
      <option value="DISPATCHER LEADER" <?php if($pgw['jobs']== "DISPATCHER LEADER"){ echo 'selected';}?>>DISPATCHER LEADER</option>
      <option value="DRIVER LEADER" <?php if($pgw['jobs']== "DRIVER LEADER"){ echo 'selected';}?> >DRIVER LEADER</option>
      <option value="FUEL MANAGEMENT" <?php if($pgw['jobs']== "FUEL MANAGEMENT"){ echo 'selected';}?> >FUEL MANAGEMENT</option>
      <option value="IT" <?php if($pgw['jobs']== "IT"){ echo 'selected';}?> >IT</option>
      <option value="MONITORING GPS" <?php if($pgw['jobs']== "MONITORING GPS"){ echo 'selected';}?> >MONITORING GPS</option>
      <option value="OPERATING CASHIER" <?php if($pgw['jobs']== "OPERATING CASHIER"){ echo 'selected';}?> >OPERATING CASHIER</option>
      <option value="OPERATING POINT COORDINATOR" <?php if($pgw['jobs']== "OPERATING POINT COORDINATOR"){ echo 'selected';}?> >OPERATING POINT COORDINATOR</option>
      <option value="PJS OPERATING POINT COORDINATOR" <?php if($pgw['jobs']== "PJS OPERATING POINT COORDINATOR"){ echo 'selected';}?> >PJS OPERATING POINT COORDINATOR</option>
      <option value="UNIT CONTROLLER" <?php if($pgw['jobs']== "UNIT CONTROLLER"){ echo 'selected';}?> >UNIT CONTROLLER</option>
      <option value="UNIT MANAGEMENT" <?php if($pgw['jobs']== "UNIT MANAGEMENT"){ echo 'selected';}?> >UNIT MANAGEMENT</option>
    </select>
</div>
<div class="mb-3">
    <label for="inputState" class="form-label">Operation Point</label>
    <select id="operation_poin" name="operation_poin" class="form-control col-sm-3" id="formGroupExampleInput2"> 
      <option value="ACEH" <?php if($pgw['operation_poin']== "ACEH"){ echo 'selected';}?> >ACEH</option>
      <option value="BALIKPAPAN" <?php if($pgw['operation_poin']== "BALIKPAPAN"){ echo 'selected';}?> >BALIKPAPAN</option>
      <option value="BANDUNG" <?php if($pgw['operation_poin']== "BANDUNG"){ echo 'selected';}?> >BANDUNG</option>
      <option value="BANJAR" <?php if($pgw['operation_poin']== "BANJAR"){ echo 'selected';}?> >BANJAR</option>
      <option value="BANJARMASIN" <?php if($pgw['operation_poin']== "BANJARMASIN"){ echo 'selected';}?> >BANJARMASIN</option>
      <option value="BATAM" <?php if($pgw['operation_poin']== "BATAM"){ echo 'selected';}?> >BATAM</option>
      <option value="BEKASI" <?php if($pgw['operation_poin']== "BEKASI"){ echo 'selected';}?> >BEKASI</option>
      <option value="BENGKULU" <?php if($pgw['operation_poin']== "BENGKULU"){ echo 'selected';}?> >BENGKULU</option>
      <option value="BIMA" <?php if($pgw['operation_poin']== "BIMA"){ echo 'selected';}?> >BIMA</option>
      <option value="BOGOR" <?php if($pgw['operation_poin']== "BOGOR"){ echo 'selected';}?> >BOGOR</option>
      <option value="CAKUNG" <?php if($pgw['operation_poin']== "CAKUNG"){ echo 'selected';}?> >CAKUNG</option>
      <option value="CIAMIS" <?php if($pgw['operation_poin']== "CIAMIS"){ echo 'selected';}?> >CIAMIS</option>
      <option value="CIANJUR" <?php if($pgw['operation_poin']== "CIANJUR"){ echo 'selected';}?> >CIANJUR</option>
      <option value="CIBITUNG" <?php if($pgw['operation_poin']== "CIBITUNG"){ echo 'selected';}?> >CIBITUNG</option>
      <option value="CIKARANG" <?php if($pgw['operation_poin']== "CIKARANG"){ echo 'selected';}?> >CIKARANG</option>
      <option value="CIREBON" <?php if($pgw['operation_poin']== "CIREBON"){ echo 'selected';}?> >CIREBON</option>
      <option value="DENPASAR" <?php if($pgw['operation_poin']== "DENPASAR"){ echo 'selected';}?> >DENPASAR</option>
      <option value="GARUT" <?php if($pgw['operation_poin']== "GARUT"){ echo 'selected';}?> >GARUT</option>
      <option value="GORONTALO" <?php if($pgw['operation_poin']== "GORONTALO"){ echo 'selected';}?> >GORONTALO</option>
      <option value="HALIM" <?php if($pgw['operation_poin']== "HALIM"){ echo 'selected';}?> >HALIM</option>
      <option value="INDRAMAYU" <?php if($pgw['operation_poin']== "INDRAMAYU"){ echo 'selected';}?> >INDRAMAYU</option>
      <option value="JAMBI" <?php if($pgw['operation_poin']== "JAMBI"){ echo 'selected';}?> >JAMBI</option>
      <option value="JEMBER" <?php if($pgw['operation_poin']== "JEMBER"){ echo 'selected';}?> >JEMBER</option>
      <option value="JOMBANG" <?php if($pgw['operation_poin']== "JOMBANG"){ echo 'selected';}?> >JOMBANG</option>
      <option value="KARAWANG" <?php if($pgw['operation_poin']== "KARAWANG"){ echo 'selected';}?> >KARAWANG</option>
      <option value="KUDUS" <?php if($pgw['operation_poin']== "KUDUS"){ echo 'selected';}?> >KUDUS</option>
      <option value="KUNINGAN" <?php if($pgw['operation_poin']== "KUNINGAN"){ echo 'selected';}?> >KUNINGAN</option>
      <option value="LAMONGAN" <?php if($pgw['operation_poin']== "LAMONGAN"){ echo 'selected';}?> >LAMONGAN</option>
      <option value="LAMPUNG" <?php if($pgw['operation_poin']== "LAMPUNG"){ echo 'selected';}?> >LAMPUNG</option>
      <option value="MADIUN" <?php if($pgw['operation_poin']== "MADIUN"){ echo 'selected';}?> >MADIUN</option>
      <option value="MADURA" <?php if($pgw['operation_poin']== "MADURA"){ echo 'selected';}?> >MADURA</option>
      <option value="MAKASAR" <?php if($pgw['operation_poin']== "MAKASAR"){ echo 'selected';}?> >MAKASAR</option>
      <option value="MALANG" <?php if($pgw['operation_poin']== "MALANG"){ echo 'selected';}?> >MALANG</option>
      <option value="MANADO" <?php if($pgw['operation_poin']== "MANADO"){ echo 'selected';}?> >MANADO</option>
      <option value="MATARAM" <?php if($pgw['operation_poin']== "MATARAM"){ echo 'selected';}?> >MATARAM</option>
      <option value="MEDAN" <?php if($pgw['operation_poin']== "MEDAN"){ echo 'selected';}?> >MEDAN</option>
      <option value="MOJOKERTO" <?php if($pgw['operation_poin']== "MOJOKERTO"){ echo 'selected';}?> >MOJOKERTO</option>
      <option value="PADANG" <?php if($pgw['operation_poin']== "PADANG"){ echo 'selected';}?> >PADANG</option>
      <option value="PALANGKARAYA" <?php if($pgw['operation_poin']== "PALANGKARAYa"){ echo 'selected';}?> >PALANGKARAYa</option>
      <option value="PALEMBANG" <?php if($pgw['operation_poin']== "PALEMBANG"){ echo 'selected';}?> >PALEMBANG</option>
      <option value="PALU" <?php if($pgw['operation_poin']== "PALU"){ echo 'selected';}?> >PALU</option>
      <option value="PANGKAL PINANG" <?php if($pgw['operation_poin']== "PANGKAL PINANG"){ echo 'selected';}?> >PANGKAL PINANG</option>
      <option value="PARE PARE" <?php if($pgw['operation_poin']== "PARE PARE"){ echo 'selected';}?> >PARE PARE</option>
      <option value="PASURUAN" <?php if($pgw['operation_poin']== "PASURUAN"){ echo 'selected';}?> >PASURUAN</option>
      <option value="PEKANBARU" <?php if($pgw['operation_poin']== "PEKANBARU"){ echo 'selected';}?> >PEKANBARU</option>
      <option value="PEMALANG" <?php if($pgw['operation_poin']== "PEMALANG"){ echo 'selected';}?> >PEMALANG</option>
      <option value="PONDOK PINANG" <?php if($pgw['operation_poin']== "PONDOK PINANG"){ echo 'selected';}?> >PONDOK PINANG</option>
      <option value="PONTIANAK" <?php if($pgw['operation_poin']== "PONTIANAK"){ echo 'selected';}?> >PONTIANAK</option>
      <option value="PROBOLINGGO" <?php if($pgw['operation_poin']== "PROBOLINGGO"){ echo 'selected';}?> >PROBOLINGGO</option>
      <option value="PURWAKARTA" <?php if($pgw['operation_poin']== "PURWAKARTA"){ echo 'selected';}?> >PURWAKARTA</option>
      <option value="PURWOKERTO" <?php if($pgw['operation_poin']== "PURWOKERTO"){ echo 'selected';}?> >PURWOKERTO</option>
      <option value="RAWA BUAYA" <?php if($pgw['operation_poin']== "RAWA BUAYA"){ echo 'selected';}?> >RAWA BUAYA</option>
      <option value="REMBANG" <?php if($pgw['operation_poin']== "REMBANG"){ echo 'selected';}?> >REMBANG</option>
      <option value="SEMARANG" <?php if($pgw['operation_poin']== "SEMARANG"){ echo 'selected';}?> >SEMARANG</option>
      <option value="SERANG" <?php if($pgw['operation_poin']== "SERANG"){ echo 'selected';}?> >SERANG</option>
      <option value="SERIRIT" <?php if($pgw['operation_poin']== "SERIRIT"){ echo 'selected';}?> >SERIRIT</option>
      <option value="SIDOARJO" <?php if($pgw['operation_poin']== "SIDOARJO"){ echo 'selected';}?> >SIDOARJO</option>
      <option value="SINGARAJA" <?php if($pgw['operation_poin']== "SINGARAJA"){ echo 'selected';}?> >SINGARAJA</option>
      <option value="SOLO" <?php if($pgw['operation_poin']== "SOLO"){ echo 'selected';}?> >SOLO</option>
      <option value="SUBANG" <?php if($pgw['operation_poin']== "SUBANG"){ echo 'selected';}?> >SUBANG</option>
      <option value="SUKABUMI" <?php if($pgw['operation_poin']== "SUKABUMI"){ echo 'selected';}?> >SUKABUMI</option>
      <option value="SUMBAWA" <?php if($pgw['operation_poin']== "SUMBAWA"){ echo 'selected';}?> >SUMBAWA</option>
      <option value="SUMEDANG" <?php if($pgw['operation_poin']== "SUMEDANG"){ echo 'selected';}?> >SUMEDANG</option>
      <option value="SUNTER" <?php if($pgw['operation_poin']== "SUNTER"){ echo 'selected';}?> >SUNTER</option>
      <option value="SURABAYA" <?php if($pgw['operation_poin']== "SURABAYA"){ echo 'selected';}?> >SURABAYA</option>
      <option value="TAMAN TEKNO" <?php if($pgw['operation_poin']== "TAMAN TEKNO"){ echo 'selected';}?> >TAMAN TEKNO</option>
      <option value="TAMBUN" <?php if($pgw['operation_poin']== "TAMBUN"){ echo 'selected';}?> >TAMBUN</option>
      <option value="TANGERANG" <?php if($pgw['operation_poin']== "TANGERANG"){ echo 'selected';}?> >TANGERANG</option>
      <option value="TASIK KOTA" <?php if($pgw['operation_poin']== "TASIK KOTA"){ echo 'selected';}?> >TASIK KOTA</option>
      <option value="TEGAL" <?php if($pgw['operation_poin']== "TEGAL"){ echo 'selected';}?> >TEGAL</option>      
      <option value="TIPAR" <?php if($pgw['operation_poin']== "TIPAR"){ echo 'selected';}?> >TIPAR</option>
      <option value="YOGYAKARTA" <?php if($pgw['operation_poin']== "YOGYAKARTA"){ echo 'selected';}?> >YOGYAKARTA</option>
    </select>
</div>
<div class="mb-3">
    <label for="inputState" class="form-label">Customer</label>
    <select id="customer" name="customer" class="form-control col-sm-3" id="formGroupExampleInput2">
      <option value="AGRIAKU" <?php if($pgw['customer']== "Agriaku"){ echo 'selected';}?> >AGRIAKU</option>
      <option value="ANTERAJA" <?php if($pgw['customer']== "ANTERAJA"){ echo 'selected';}?> >ANTERAJA</option>
      <option value="AOP" <?php if($pgw['customer']== "AOP"){ echo 'selected';}?> >AOP</option>
      <option value="APL" <?php if($pgw['customer']== "APL"){ echo 'selected';}?> >APL</option>
      <option value="BAT 1" <?php if($pgw['customer']== "BAT 1"){ echo 'selected';}?> >BAT 1</option>
      <option value="CIRCLE K" <?php if($pgw['customer']== "CIRCLE K"){ echo 'selected';}?> >CIRCLE K</option>
      <option value="CREDIBOOK" <?php if($pgw['customer']== "CREDIBOOK"){ echo 'selected';}?> >CREDIBOOK</option>
      <option value="HMS" <?php if($pgw['customer']== "HMS"){ echo 'selected';}?> >HMS</option>
      <option value="MAYORA" <?php if($pgw['customer']== "MAYORA"){ echo 'selected';}?> >MAYORA</option>
      <option value="PT. ADI SARANA ARMADA. TBK" <?php if($pgw['customer']== "PT. ADI SARANA ARMADA. TBK"){ echo 'selected';}?> >PT. ADI SARANA ARMADA. TBK</option>
      <option value="PT. MEDAN DISTRIBUSINDO RAYA" <?php if($pgw['customer']== "PT. MEDAN DISTRIBUSINDO RAYA"){ echo 'selected';}?> >PT. MEDAN DISTRIBUSINDO RAYA</option>
      <option value="PT. SUMBER ALFARIA TRIJAYA TBK" <?php if($pgw['customer']== "PT. SUMBER ALFARIA TRIJAYA TBK"){ echo 'selected';}?> >PT. SUMBER ALFARIA TRIJAYA TBK</option>
      <option value="YAMAZAKI" <?php if($pgw['customer']== "YAMAZAKI"){ echo 'selected';}?> >YAMAZAKI</option>
    </select>
</div>
  </div>
 </div>

 
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>