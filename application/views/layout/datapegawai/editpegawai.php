<?= form_open_multipart('#', ['id' => 'editpegawai']) ?>
<input type="hidden" name="id_pegawai_edit" id="id_pegawai_edit" value="<?= $datapegawai['id'] ?>">
<div class="form-group row">
    <label for="nama_pegawai_edit" class="col-sm-4 col-form-label">NIK</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nik" name="nik" value="<?= $datapegawai['nik'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nama_pegawai_edit" class="col-sm-4 col-form-label">Nama Lengkap</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nama" name="nama" value="<?= $datapegawai['nama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="kode_pegawai_edit" class="col-sm-4 col-form-label">Vendor</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="vendor" name="vendor" value="<?= $datapegawai['vendor'] ?>" title="Kode telah otomatis digenerate secara acak pada saat penambahan pegawai">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Keterangan Aktivasi Karyawan</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="keterangan_aktivasi_karyawan" name="keterangan_aktivasi_karyawan" value="<?= $datapegawai['keterangan_aktivasi_karyawan'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Nomor PO</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nomor_po_nama_driver_yg_diganti" name="nomor_po_nama_driver_yg_diganti" value="<?= $datapegawai['nomor_po_nama_driver_yg_diganti'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Tanggal PO</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tanggal_po" name="tanggal_po" value="<?= $datapegawai['tanggal_po'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Jabatan</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="jabatan_pegawai_edit" name="jabatan_pegawai_edit" value="<?= $datapegawai['jabatan'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Tanggal Inaktif Kerja</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="tgl_inaktif_kerja" name="tgl_inaktif_kerja" value="<?= $datapegawai['tgl_inaktif_kerja'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Lead Team</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="lead_time" name="lead_time" value="<?= $datapegawai['lead_time'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">No Handphone</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nomor_hp" name="nomor_hp" value="<?= $datapegawai['nomor_hp'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Driver</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="driver" name="driver" value="<?= $datapegawai['driver'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Fico</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="fico" name="fico" value="<?= $datapegawai['fico'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="helper" class="col-sm-4 col-form-label">Helper</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="helper" name="helper" value="<?= $datapegawai['helper'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="checker" class="col-sm-4 col-form-label">Checker</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="checker" name="checker" value="<?= $datapegawai['checker'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="non_driver" class="col-sm-4 col-form-label">Non_Driver</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="non_driver" name="non_driver" value="<?= $datapegawai['non_driver'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Jabatan</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="jabatan_pegawai_edit" name="jabatan_pegawai_edit" value="<?= $datapegawai['jabatan'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="operation_poin" class="col-sm-4 col-form-label">Operation_Poin</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="operation_poin" name="operation_poin" value="<?= $datapegawai['operation_poin'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="customer" class="col-sm-4 col-form-label">Customer</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="customer" name="customer" value="<?= $datapegawai['customer'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="district" class="col-sm-4 col-form-label">District</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="district" name="district" value="<?= $datapegawai['district'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="no_ktp" class="col-sm-4 col-form-label">No_ktp</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="no_ktp" name="no_ktp" value="<?= $datapegawai['no_ktp'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="masa_berlaku" class="col-sm-4 col-form-label">Masa_Berlaku</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="masa_berlaku" name="masa_berlaku" value="<?= $datapegawai['masa_berlaku'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="klasifikasi_skill" class="col-sm-4 col-form-label">Klasifikasi Skill</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="klasifikasi_skill" name="klasifikasi_skill" value="<?= $datapegawai['klasifikasi_skill'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jenis_sim" class="col-sm-4 col-form-label">Jenis Sim</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="jenis_sim" name="jenis_sim" value="<?= $datapegawai['jenis_sim'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="no_sim" class="col-sm-4 col-form-label">No Sim</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="no_sim" name="no_sim" value="<?= $datapegawai['no_sim'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="masa_berlaku_sim" class="col-sm-4 col-form-label">Masa Berlaku SIM</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="masa_berlaku_sim" name="masa_berlaku_sim" value="<?= $datapegawai['masa_berlaku_sim'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan_pegawai_edit" class="col-sm-4 col-form-label">Tempat Lahir</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?= $datapegawai['tempat_lahir'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tglblnthn_lahir" class="col-sm-4 col-form-label">Tanggal Lahir</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tglblnthn_lahir" name="tglblnthn_lahir" value="<?= $datapegawai['tglblnthn_lahir'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="alamat" class="col-sm-4 col-form-label">alamat</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="alamat" name="alamatt" value="<?= $datapegawai['alamat'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_pajak" class="col-sm-4 col-form-label">Status Pajak</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_pajak" name="status_pajak" value="<?= $datapegawai['status_pajak'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="agama" class="col-sm-4 col-form-label">Agama</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="agama" name="agama" value="<?= $datapegawai['agama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jenis_kelamin" class="col-sm-4 col-form-label">Jenis Kelamin</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?= $datapegawai['jenis_kelamin'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="pendidikan_terakhir" class="col-sm-4 col-form-label">Pendidikan Terakhir</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="pendidikan_terakhir" name="pendidikan_terakhir" value="<?= $datapegawai['pendidikan_terakhir'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nama_di_bank" class="col-sm-4 col-form-label">Nama diBank</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nama_di_bank" name="nama_di_bank" value="<?= $datapegawai['nama_di_bank'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="bank" class="col-sm-4 col-form-label">Bank</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="bank" name="bank" value="<?= $datapegawai['bank'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jenis_kelamin" class="col-sm-4 col-form-label">jenis_kelamin</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?= $datapegawai['jenis_kelamin'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="no_account" class="col-sm-4 col-form-label">No Account</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="no_account" name="jenis_kelamin" value="<?= $datapegawai['no_account'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nomor_kartu_keluarga" class="col-sm-4 col-form-label">Nomor Kartu Keluarga</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nomor_kartu_keluarga" name="nomor_kartu_keluarga" value="<?= $datapegawai['nomor_kartu_keluarga'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="npwp" class="col-sm-4 col-form-label">NPWP</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="npwp" name="npwp" value="<?= $datapegawai['npwp'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="start_kontrak" class="col-sm-4 col-form-label">Start Kontrak</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="start_kontrak" name="start_kontrak" value="<?= $datapegawai['start_kontrak'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="finish_kontrak" class="col-sm-4 col-form-label">Finish Kontrak</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="finish_kontrak" name="finish_kontrak" value="<?= $datapegawai['finish_kontrak'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_kontrak" class="col-sm-4 col-form-label">Status Kontrak</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_kontrak" name="status_kontrak" value="<?= $datapegawai['status_kontrak'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="masa_kontrak_berakhir" class="col-sm-4 col-form-label">Masa Kontrak Berakhir</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="masa_kontrak_berakhir" name="masa_kontrak_berakhir" value="<?= $datapegawai['masa_kontrak_berakhir'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="lama_kerja" class="col-sm-4 col-form-label">Lama Kerja</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="lama_kerja" name="lama_kerjan" value="<?= $datapegawai['lama_kerja'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="seragam" class="col-sm-4 col-form-label">Seragam</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="seragam" name="seragam" value="<?= $datapegawai['seragam'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_pengiriman_seragam" class="col-sm-4 col-form-label">Tanggal Pengiriman Seragam</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tanggal_pengiriman_seragam" name="tanggal_pengiriman_seragamn" value="<?= $datapegawai['tanggal_pengiriman_seragam'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="id_card" class="col-sm-4 col-form-label">ID Card</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="id_card" name="jenis_kelamin" value="<?= $datapegawai['id_card'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_pengiriman_id_card" class="col-sm-4 col-form-label">Tanggal Pengiriman Id Card</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tanggal_pengiriman_id_card" name="tanggal_pengiriman_id_card" value="<?= $datapegawai['tanggal_pengiriman_id_card'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="training_safety_driving" class="col-sm-4 col-form-label">Training Safety Driving</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="training_safety_driving" name="training_safety_driving" value="<?= $datapegawai['training_safety_driving'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="pelaksanaan_training_terakhir" class="col-sm-4 col-form-label">Pelaksanaan Training Terakhir</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="pelaksanaan_training_terakhir" name="pelaksanaan_training_terakhir" value="<?= $datapegawai['pelaksanaan_training_terakhir'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="masa_training" class="col-sm-4 col-form-label">Masa Training</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="masa_training" name="masa_training" value="<?= $datapegawai['masa_training'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_training" class="col-sm-4 col-form-label">Status Training</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_training" name="status_training" value="<?= $datapegawai['status_training'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nilai_training_safety_driving_pree_test" class="col-sm-4 col-form-label">Nilai Training Safety Driving</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nilai_training_safety_driving_pree_test" name="nilai_training_safety_driving_pree_test" value="<?= $datapegawai['nilai_training_safety_driving_pree_test'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nilai_training_safety_driverng_post_test" class="col-sm-4 col-form-label">Nilai Training Safety Driverng Post Test</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nilai_training_safety_driverng_post_test" name="nilai_training_safety_driverng_post_test" value="<?= $datapegawai['nilai_training_safety_driverng_post_test'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_vaksin_pertama" class="col-sm-4 col-form-label">Status Vaksin Pertama</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_vaksin_pertama" name="status_vaksin_pertama" value="<?= $datapegawai['status_vaksin_pertama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="mengapa_belum_vaksi_pertama" class="col-sm-4 col-form-label">Mengapa Belum Vaksin Pertama</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="mengapa_belum_vaksi_pertama" name="mengapa_belum_vaksi_pertama" value="<?= $datapegawai['mengapa_belum_vaksi_pertama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_vaksin_pertama" class="col-sm-4 col-form-label">Tanggal Vaksin Pertama</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tanggal_vaksin_pertama" name="tanggal_vaksin_pertama value="<?= $datapegawai['tanggal_vaksin_pertama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_vaksin_pertama" class="col-sm-4 col-form-label">Status Vaksin Pertama</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_vaksin_pertama" name="status_vaksin_pertama" value="<?= $datapegawai['status_vaksin_pertama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="lokasi_vaksin_pertama" class="col-sm-4 col-form-label">Lokasi Vaksin Pertama</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="lokasi_vaksin_pertama" name="lokasi_vaksin_pertama" value="<?= $datapegawai['lokasi_vaksin_pertama'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_vaksin_kedua" class="col-sm-4 col-form-label">Status Vaksin Kedua</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_vaksin_kedua" name="status_vaksin_kedua" value="<?= $datapegawai['status_vaksin_kedua'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="mengapa_belum_vaksi_kedua" class="col-sm-4 col-form-label">Mengapa Belum Vaksin Kedua</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="mengapa_belum_vaksi_kedua" name="mengapa_belum_vaksi_kedua" value="<?= $datapegawai['mengapa_belum_vaksi_kedua'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_vaksin_kedua" class="col-sm-4 col-form-label">Tanggal Vaksin Kedua</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tanggal_vaksin_kedua" name="tanggal_vaksin_kedua" value="<?= $datapegawai['tanggal_vaksin_kedua'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="lokasi_vaksin_kedua" class="col-sm-4 col-form-label">Lokasi Vaksin Kedua</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="lokasi_vaksin_kedua" name="lokasi_vaksin_kedua" value="<?= $datapegawai['lokasi_vaksin_kedua'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_vaksin_booster" class="col-sm-4 col-form-label">Status Vaksin Booster</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_vaksin_booster" name="status_vaksin_booster" value="<?= $datapegawai['status_vaksin_booster'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="mengapa_belum_vaksin_booster" class="col-sm-4 col-form-label">Mengapa Belum Vaksin Booster</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="mengapa_belum_vaksin_booster" name="mengapa_belum_vaksin_booster" value="<?= $datapegawai['mengapa_belum_vaksin_booster'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tanggal_vaksin_booster" class="col-sm-4 col-form-label">Tanggal Vaksin Booster</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tanggal_vaksin_booster" name="tanggal_vaksin_booster" value="<?= $datapegawai['tanggal_vaksin_booster'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="lokasi_vaksin_booster" class="col-sm-4 col-form-label">Lokasi Vaksin Booster</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="lokasi_vaksin_booster" name="lokasi_vaksin_booster" value="<?= $datapegawai['lokasi_vaksin_booster'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tgl_training_online_part_1" class="col-sm-4 col-form-label">Tanggal Training Online Part 1</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tgl_training_online_part_1" name="tgl_training_online_part_1" value="<?= $datapegawai['tgl_training_online_part_1'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_training_online_part_1" class="col-sm-4 col-form-label">Status Training Online Part 1</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_training_online_part_1" name="status_training_online_part_1" value="<?= $datapegawai['status_training_online_part_1'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="point_part_1" class="col-sm-4 col-form-label">Point Part 1</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="point_part_1" name="point_part_1" value="<?= $datapegawai['point_part_1'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tgl_training_online_part_2" class="col-sm-4 col-form-label">Tanggal Training Online Part 2</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tgl_training_online_part_2" name="tgl_training_online_part_2" value="<?= $datapegawai['tgl_training_online_part_2'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_training_online_part_2" class="col-sm-4 col-form-label">Status Training Online Part 2</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_training_online_part_2" name="status_training_online_part_2" value="<?= $datapegawai['status_training_online_part_2'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="point_part_2" class="col-sm-4 col-form-label">Point Part 2</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="point_part_2" name="point_part_2" value="<?= $datapegawai['point_part_2'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tgl_training_attitude_n_knowledge" class="col-sm-4 col-form-label">Tanggal Training Attitude & Knowledge</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tgl_training_attitude_n_knowledge" name="tgl_training_attitude_n_knowledge" value="<?= $datapegawai['tgl_training_attitude_n_knowledge'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="status_training_attitude_n_knowledge" class="col-sm-4 col-form-label">Status Training Attitude & Knowledge</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="status_training_attitude_n_knowledge" name="status_training_attitude_n_knowledge" value="<?= $datapegawai['status_training_attitude_n_knowledge'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="point_attitude_n_knowledge" class="col-sm-4 col-form-label">Point Attitude & Knowledge</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="point_attitude_n_knowledge" name="point_attitude_n_knowledge" value="<?= $datapegawai['point_attitude_n_knowledge'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nomor_bpjs_kesehatan" class="col-sm-4 col-form-label">Nomor BPJS Kesehatan</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nomor_bpjs_kesehatan" name="nomor_bpjs_kesehatan" value="<?= $datapegawai['nomor_bpjs_kesehatan'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="keterangan_proses_bpjs_kesehatan" class="col-sm-4 col-form-label">Keterangan BPJS Kesegatan</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="keterangan_proses_bpjs_kesehatan" name="keterangan_proses_bpjs_kesehatan" value="<?= $datapegawai['keterangan_proses_bpjs_kesehatan'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="nomor_jamsostek" class="col-sm-4 col-form-label">Nomor Jamsostek</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="nomor_jamsostek" name="nomor_jamsostek" value="<?= $datapegawai['nomor_jamsostek'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="keterangan_jamsostek" class="col-sm-4 col-form-label">Keterangan Jamsostek</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="keterangan_jamsostek" name="keterangan_jamsostek" value="<?= $datapegawai['keterangan_jamsostek'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="reward" class="col-sm-4 col-form-label">Reward</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="reward" name="reward" value="<?= $datapegawai['reward'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tglblnthn_pemberian_reward" class="col-sm-4 col-form-label">Tanggal Pemberian Reward</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tglblnthn_pemberian_reward" name="tglblnthn_pemberian_reward" value="<?= $datapegawai['tglblnthn_pemberian_reward'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="accident_report" class="col-sm-4 col-form-label">Accident Report</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="accident_report" name="accident_report" value="<?= $datapegawai['accident_report'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tglblnthn_accident" class="col-sm-4 col-form-label">Tanggal Accident</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tglblnthn_accident" name="tglblnthn_accident" value="<?= $datapegawai['tglblnthn_accident'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="kasus" class="col-sm-4 col-form-label">Kasus</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="kasus" name="kasus" value="<?= $datapegawai['kasus'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="tglblnthn_kasus" class="col-sm-4 col-form-label">Tanggal Kasus</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" id="tglblnthn_kasus" name="tglblnthn_kasus" value="<?= $datapegawai['tglblnthn_kasus'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="email_address" class="col-sm-4 col-form-label">Email</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" id="email_address" name="email_address" value="<?= $datapegawai['email_address'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="sp_i" class="col-sm-4 col-form-label">SP I</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="sp_i" name="sp_i" value="<?= $datapegawai['sp_i'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="sp_ii" class="col-sm-4 col-form-label">SP II</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="sp_ii" name="sp_ii" value="<?= $datapegawai['sp_ii'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="sp_iii" class="col-sm-4 col-form-label">SP III</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="sp_iii" name="sp_iii" value="<?= $datapegawai['sp_iii'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="keterangan_out_dan_mutasi" class="col-sm-4 col-form-label">Keterangan Out & Mutasi</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="keterangan_out_dan_mutasi" name="keterangan_out_dan_mutasi" value="<?= $datapegawai['keterangan_out_dan_mutasi'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="masa_berlaku_sim_2" class="col-sm-4 col-form-label">Masa Berlaku SIM 2</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="masa_berlaku_sim_2" name="masa_berlaku_sim_2" value="<?= $datapegawai['masa_berlaku_sim_2'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="masa_training_2" class="col-sm-4 col-form-label">Masa Training 2</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="masa_training_2" name="masa_training_2" value="<?= $datapegawai['masa_training_2'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="keterangan_dan_jadwal_training" class="col-sm-4 col-form-label">Keterangan & Jadwal Training</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="keterangan_dan_jadwal_training" name="keterangan_dan_jadwal_training" value="<?= $datapegawai['keterangan_dan_jadwal_training'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="driver_leader" class="col-sm-4 col-form-label">Driver Leader</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="driver_leader" name="driver_leader" value="<?= $datapegawai['driver_leader'] ?>">
    </div>
</div>
<div class="form-group row">
    <label for="jabatan" class="col-sm-4 col-form-label">Jabatan</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="jabatan" name="jabatan" value="<?= $datapegawai['jabatan'] ?>">
    </div>
</div>
<div class="my-2" id="info-edit"></div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
    <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Edit</button>
</div>
</form>

<script>
    $("#sh_hd_pass button").on('click', function(event) {
        event.preventDefault();
        if ($('#sh_hd_pass input').attr("type") == "text") {
            $('#sh_hd_pass input').attr('type', 'password');
            $('#sh_hd_pass span').addClass("fa-eye-slash");
            $('#sh_hd_pass span').removeClass("fa-eye");
        } else if ($('#sh_hd_pass input').attr("type") == "password") {
            $('#sh_hd_pass input').attr('type', 'text');
            $('#sh_hd_pass span').removeClass("fa-eye-slash");
            $('#sh_hd_pass span').addClass("fa-eye");
        }
    });

    $('.edit-pas-pegawai').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.edit-pas-pegawai-label').addClass("selected").html(fileName);
    });
</script>