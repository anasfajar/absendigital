<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Klasifikasi Skill Data Pegawai</h1>     
    <form action="<?= base_url('updateklasifikasiskill')?>" method="post">	
 
 <div class="card mb-3">
  <div class="card-header">
    KLASIFIKASI SKILL
  </div>
  <div class="card-body">
  <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">Skill</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id'] ?>">
    <select id="klasifikasi_skill" name="klasifikasi_skill" class="form-control col-sm-3" >
      <option value="BV/CDE" <?php if($pgw['klasifikasi_skill']== "BV/CDE"){ echo 'selected';}?> >BV/CDE</option>
      <option value="BV/CDE/CDD" <?php if($pgw['klasifikasi_skill']== "BV/CDE/CDD"){ echo 'selected';}?> >BV/CDE/CDD</option>
      <option value="CDD" <?php if($pgw['klasifikasi_skill']== "CDD"){ echo 'selected';}?> >CDD</option>
      <option value="CDD LONG" <?php if($pgw['klasifikasi_skill']== "CDD LONG"){ echo 'selected';}?> >CDD LONG</option>
      <option value="CDD/CDL" <?php if($pgw['klasifikasi_skill']== "CDD/CDL"){ echo 'selected';}?> >CDD/CDL</option>
      <option value="CDD/FUSO" <?php if($pgw['klasifikasi_skill']== "CDD/FUSO"){ echo 'selected';}?> >CDD/FUSO</option>
      <option value="CDE" <?php if($pgw['klasifikasi_skill']== "CDE"){ echo 'selected';}?> >CDE</option>
      <option value="FUSO" <?php if($pgw['klasifikasi_skill']== "FUSO"){ echo 'selected';}?> >FUSO</option>
      <option value="HIJAU" <?php if($pgw['klasifikasi_skill']== "HIJAU"){ echo 'selected';}?> >HIJAU</option>
      <option value="HITAM" <?php if($pgw['klasifikasi_skill']== "HITAM"){ echo 'selected';}?> >HITAM</option>
      <option value="KUNING" <?php if($pgw['klasifikasi_skill']== "KUNING"){ echo 'selected';}?> >KUNING</option>
      <option value="MERAH" <?php if($pgw['klasifikasi_skill']== "MERAH"){ echo 'selected';}?> >MERAH</option>
      <option value="PUTIH" <?php if($pgw['klasifikasi_skill']== "PUTIH"){ echo 'selected';}?> >PUTIH</option>
      <option value="TRONTON" <?php if($pgw['klasifikasi_skill']== "TRONTON"){ echo 'selected';}?> >TRONTON</option>
      <option value="WING" <?php if($pgw['klasifikasi_skill']== "WING"){ echo 'selected';}?> >WING</option>
      <option value="NON DRIVER" <?php if($pgw['klasifikasi_skill']== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jenis SIM</label>
    <select id="jenis_sim" class="form-control col-sm-3" name="jenis_sim">
      <option value="A" <?php if($pgw['jenis_sim']== "A"){ echo 'selected';}?> >A</option>
      <option value="B1" <?php if($pgw['jenis_sim']== "B1"){ echo 'selected';}?> >B1</option>
      <option value="B1 UMUM" <?php if($pgw['jenis_sim']== "B1 UMUM"){ echo 'selected';}?> >B1 UMUM</option>
      <option value="B2" <?php if($pgw['jenis_sim']== "B2"){ echo 'selected';}?> >B2</option>
      <option value="B2 UMUM" <?php if($pgw['jenis_sim']== "B2 UMUM"){ echo 'selected';}?> >B2 UMUM</option>
      <option value="NON DRIVER" <?php if($pgw['jenis_sim']== "NON DRIVER"){ echo 'selected';}?> >NON DRIVER</option>
    </select>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nomor SIM</label>
    <input type="text" class="form-control col-sm-6" id="no_sim" name="no_sim" value="<?= $pgw['no_sim'] ?>">
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Masa Berlaku</label>
    <input type="date" class="form-control col-sm-3" id="masa_berlaku_sim" name="masa_berlaku_sim" value="<?= $pgw['masa_berlaku_sim'] ?>" >
    </div>
  </div>
 </div>
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>