<?php if ($this->session->userdata('logged_in') == true) : ?>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; <?= date("Y"); ?><a href="<?= base_url(); ?>" class="ml-1"><?= $appname = (empty($dataapp['nama_app_absensi'])) ? 'Absensi Online' : $dataapp['nama_app_absensi']; ?></a>
                    <div class="d-inline">Powered By<a href="https://wa.me/6281294660097" class="ml-1">IT MBK</a></div>
                </div>
                <div class="text-muted">
                    Page rendered in <strong>{elapsed_time}</strong> detik.
                </div>
            </div>
        </div>
    </footer>
    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap-toggle-master/js/bootstrap4-toggle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jonthornton-jquery-timepicker/jquery.timepicker.min.js"></script>
    <script src="<?= base_url('assets/'); ?>vendor/sweetalert2/sweetalert2.all.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js" charset="UTF-8"></script>
    <script src="<?= base_url('assets'); ?>/vendor/moment/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $('#yearpicker,#absen_tahun').datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            orientation: "bottom auto"
        });

        $('body').on('shown.bs.modal', function(e) {
            setTimeout(function() {
                map.invalidateSize()
            }, 500);
        })

        $('#setting-list a').on('click', function(e) {
            e.preventDefault()
            $(this).tab('show')
        })

        $('#absen_bulan').datepicker({
            format: "MM",
            minViewMode: 'months',
            maxViewMode: 'months',
            startView: 'months',
            language: "id",
            orientation: "bottom auto"
        });

        $(document).ready(function() {
            $('#datatables').DataTable();
        });

        $(document).ready(function() {
            $('table.dashboard').DataTable();
        });



        //KECELAKAAN

        $('#datakecelakaan').DataTable({
            "ajax": {
                url: "<?= base_url('ajax/get_datatbl?type=dataKecelakaan'); ?>",
                type: 'get',
                async: true,
                "processing": true,
                "serverSide": true,
                dataType: 'json',
                "bDestroy": true
            },
            rowCallback: function(row, data, iDisplayIndex) {
                $('td:eq(0)', row).html();
            }
        });

        $('#nik_driver').on('keyup', function() {
            var validNum = /^[0-9]+$/;
            var vnik = this.value;
            if (!vnik.match(validNum)) {
                $('.Nnik_driver').html('Nik Driver Hanya Angka');
            } else {
                $('.Nnik_driver').html('')
            }
        });

        $('#usia_driver').on('keyup', function() {
            var validNum = /^[0-9]+$/;
            var valnya = this.value;
            if (!valnya.match(validNum)) {
                $('.Nusia_driver').html('Usia Driver Hanya Angka');
            } else {
                $('.Nusia_driver').html('')
            }
        });

        $('#biaya_penanganan').on('keyup', function() {
            var validNum = /^[0-9]+$/;
            var valnya = this.value;
            if (!valnya.match(validNum)) {
                $('.Nbiaya_penanganan').html('Biaya Hanya Angka');
            } else {
                $('.Nbiaya_penanganan').html('')
            }
        });

        $('#formKecelakaan').submit(function() {
            event.preventDefault();
            var _this = $(this);

            if (!$('#nik_driver').val()) {
                $('.Nnik_driver').html('Nik Driver Harus Diisi..');
                $('#nik_driver').focus();
                return;
            } else {
                $('.Nnik_driver').hide();
            }
            if (!$('#driver').val()) {
                $('.Ndriver').html('Nama Driver Harus Diisi..');
                $('#driver').focus();
                return;
            } else {
                $('.Ndriver').hide();
            }
            if (!$('#usia_driver').val()) {
                $('.Nusia_driver').html('Usia Driver Harus Diisi..');
                $('#usia_driver').focus();
                return;
            } else {
                $('.Nusia_driver').hide();
            }
            if (!$('#no_polisi').val()) {
                $('.Nno_polisi').html('Nomor Polisi Harus Diisi..');
                $('#no_polisi').focus();
                return;
            } else {
                $('.Nno_polisi').hide();
            }
            if (!$('#lokasi').val()) {
                $('.Nlokasi').html('Lokasi Harus Diisi..');
                $('#lokasi').focus();
                return;
            } else {
                $('.Nlokasi').hide();
            }
            if (!$('#tanggal').val()) {
                $('.Ntanggal').html('Tanggal Harus Diisi..');
                $('#tanggal').focus();
                return;
            } else {
                $('.Ntanggal').hide();
            }
            if (!$('#customer').val()) {
                $('.Ncustomer').html('Customer Harus Diisi..');
                $('#customer').focus();
                return;
            } else {
                $('.Ncustomer').hide();
            }
            if (!$('#faktor').val()) {
                $('.Nfaktor').html('Faktor Harus Diisi..');
                $('#faktor').focus();
                return;
            } else {
                $('.Nfaktor').hide();
            }
            if (!$('#muatan').val()) {
                $('.Nmuatan').html('Harus Diisi..');
                $('#muatan').focus();
                return;
            } else {
                $('.Nmuatan').hide();
            }
            if (!$('#rute').val()) {
                $('.Nrute').html('Rute Harus Diisi..');
                $('#rute').focus();
                return;
            } else {
                $('.Nrute').hide();
            }
            if (!$('#kategori').val()) {
                $('.Nkategori').html('Kategori Harus Diisi..');
                $('#kategori').focus();
                return;
            } else {
                $('.Nkategori').hide();
            }
            if (!$('#kronologis').val()) {
                $('.Nkronologis').html('Kronologis Harus Diisi..');
                $('#kronologis').focus();
                return;
            } else {
                $('.Nkronologis').hide();
            }
            if (!$('#tindakan').val()) {
                $('.Ntindakan').html('Tindakan Harus Diisi..');
                $('#tindakan').focus();
                return;
            } else {
                $('.Ntindakan').hide();
            }
            if (!$('#biaya_penanganan').val()) {
                $('.Nbiaya_penanganan').html('Biaya Penanganan Harus Diisi..');
                $('#biaya_penanganan').focus();
                return;
            } else {
                $('.Nbiaya_penanganan').hide();
            }
            if (!$('#korban_lain').val()) {
                $('.Nkorban_lain').html('Korban Lain Harus Diisi..');
                $('#korban_lain').focus();
                return;
            } else {
                $('.Nkorban_lain').hide();
            }
            if (!$('#analisis').val()) {
                $('.Nanalisis').html('Analisis Harus Diisi..');
                $('#analisis').focus();
                return;
            } else {
                $('.Nanalisis').hide();
            }
            if (!$('#kesimpulan').val()) {
                $('.Nkesimpulan').html('Kesimpulan Harus Diisi..');
                $('#kesimpulan').focus();
                return;
            } else {
                $('.Nkesimpulan').hide();
            }
            if (!$('#antisipasi').val()) {
                $('.Nantisipasi').html('Antisipasi Harus Diisi..');
                $('#antisipasi').focus();
                return;
            } else {
                $('.Nantisipasi').hide();
            }


            _this.unbind('submit').submit();
        });
        $(document).ready(function() {
            $('#tanggalKecelakaan').datetimepicker({
                "allowInputToggle": true,
                "showClose": true,
                "showClear": true,
                "showTodayButton": true,
                "format": "DD-MM-YYYY hh:mm:ss",
            });
        });


        $('.dropify').dropify({
            messages: {
                default: 'Drag atau drop untuk memilih gambar',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    </script>


    <script>
        function load_process() {
            swal.fire({
                imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                title: "Refresh Data",
                text: "Please wait",
                showConfirmButton: false,
                allowOutsideClick: false,
                timer: 1500
            });
        }

        $(".logout").click(function(event) {
            $.ajax({
                type: "POST",
                url: "<?= base_url('ajax/logoutajax'); ?>",
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Logging Out",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Logout',
                        text: 'Anda Telah Keluar!',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    location.reload();
                }
            });
            event.preventDefault();
        });


        <?php if ($dataapp['maps_use'] == TRUE) : ?>
            let maps_absen = "searching...";
            if (document.getElementById("maps-absen")) {
                window.onload = function() {
                    var popup = L.popup();
                    var geolocationMap = L.map("maps-absen", {
                        center: [40.731701, -73.993411],
                        zoom: 15,
                    });

                    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
                        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    }).addTo(geolocationMap);

                    function geolocationErrorOccurred(geolocationSupported, popup, latLng) {
                        popup.setLatLng(latLng);
                        popup.setContent(
                            geolocationSupported ?
                            "<b>Error:</b> The Geolocation service failed." :
                            "<b>Error:</b> This browser doesn't support geolocation."
                        );
                        popup.openOn(geolocationMap);
                    }

                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            function(position) {
                                var latLng = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude,
                                };

                                var marker = L.marker(latLng).addTo(geolocationMap);
                                maps_absen = position.coords.latitude + ", " + position.coords.longitude;
                                geolocationMap.setView(latLng);
                            },
                            function() {
                                geolocationErrorOccurred(true, popup, geolocationMap.getCenter());
                                maps_absen = 'No Location';
                            }
                        );
                    } else {
                        //No browser support geolocation service
                        geolocationErrorOccurred(false, popup, geolocationMap.getCenter());
                        maps_absen = 'No Location';
                    }
                };
            }
        <?php elseif ($dataapp['maps_use'] == FALSE) : ?>
            maps_absen = 'No Location';
        <?php endif; ?>

        $("#btn-absensi").click(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            let ket_absen = $('#ket_absen').val();
            let tanggal = $('#datenow').text();
            let waktu = $('#clocknow').text();

            $.ajax({
                type: "POST",
                url: '<?= base_url('ajax/absenajax'); ?>',
                data: {
                    maps_absen: maps_absen,
                    ket_absen: ket_absen,
                    tanggal: tanggal,
                    waktu: waktu
                }, // serializes the form's elements.
                dataType: 'json',
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Proses Absensi",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(response) {
                    if (response.success == true) {
                        swal.fire({
                            icon: 'success',
                            title: 'Absen Sukses',
                            text: 'Anda Telah Absen!',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $('#func-absensi').load(location.href + " #func-absensi");
                    } else {
                        $("#infoabsensi").html(response.msgabsen).show().delay(3000).fadeOut();
                        swal.close()
                    }
                },
                error: function() {
                    swal.fire("Absen Gagal", "Ada Kesalahan Saat Absen!", "error");
                }
            });


        });
    </script>
    <!--Bagian CRUD Absen User-->
    <script>
        $("#listabsenku").on('click', '.detail-absen', function(e) {
            e.preventDefault();
            var absen_id = $(e.currentTarget).attr('data-absen-id');
            if (absen_id === '') return;
            $.ajax({
                type: "POST",
                url: '<?= base_url('ajax/dataabs?type=viewabs'); ?>',
                data: {
                    absen_id: absen_id
                },
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Mempersiapkan Preview Absensi",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.close();
                    $('#viewabsensimodal').modal('show');
                    $('#viewdataabsensi').html(data);

                },
                error: function() {
                    swal.fire("Preview Absensi Gagal", "Ada Kesalahan Saat menampilkan data absensi!", "error");
                }
            });
        });
    </script>

    <!--Bagian Setting User-->
    <script>
        $("#clear_rememberme").click(function(e) {
            Swal.fire({
                title: 'Hapus Semua Remember Me?',
                text: "Anda yakin ingin menghapus semua sesi remember me anda!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url('ajax/clear_rememberme?rmbtype=all'); ?>",
                        beforeSend: function() {
                            swal.fire({
                                imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                title: "Menghapus Semua Remember Me Anda",
                                text: "Please wait",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        success: function(data) {
                            swal.fire({
                                icon: 'success',
                                title: 'Menghapus Semua Remember Me Berhasil',
                                text: 'List remember me anda telah di hapus!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#remembersesslist').load(location.href + " #remembersesslist");
                        }
                    });
                }
            })
            e.preventDefault();
        });

        $(".sess_rememberme").click(function(e) {
            e.preventDefault();
            var sess_id = $(e.currentTarget).attr('data-sess-id');
            if (sess_id === '') return;
            Swal.fire({
                title: 'Hapus Sesi Remember Me Ini?',
                text: "Anda yakin ingin menghapus sesi di perangkat ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('ajax/clear_rememberme?rmbtype=self'); ?>',
                        data: {
                            sess_id: sess_id
                        },
                        beforeSend: function() {
                            swal.fire({
                                imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                title: "Menghapus Sesi Perangkat Ini",
                                text: "Please wait",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        success: function(data) {
                            swal.fire({
                                icon: 'success',
                                title: 'Menghapus Sesi Perangkat Ini Berhasil',
                                text: 'Anda telah menghapus sesi pada perangat ini!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $(e.currentTarget).parent().remove();
                        }
                    });
                }
            })
        });
    </script>
    <script>
        $('#chgpassuser').submit(function(e) {
            e.preventDefault();
            var form = this;
            $("#chgpass-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Mengganti Password").attr("disabled", true);
            var formdata = new FormData(form);
            $.ajax({
                url: "<?= base_url('ajax/usersetting?type=chgpwd'); ?>",
                type: 'POST',
                data: formdata,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function() {
                    $('.text-danger').remove();
                    $("#infopass").hide();
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Mengubah Password",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(response) {
                    if (response.success == true) {
                        $('.text-danger').remove();
                        swal.fire({
                            icon: 'success',
                            title: 'Ubah Password Berhasil',
                            text: 'Password anda sudah diubah!',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        form.reset();
                        $("#chgpass-btn").html("<span class='fas fa-key mr-1' aria-hidden='true' ></span>Ubah Password").attr("disabled", false);
                    } else {
                        swal.close()
                        $("#infopass").html(response.infopass).show();
                        swal.fire({
                            icon: 'error',
                            title: 'Ubah Password Gagal',
                            text: 'Password anda gagal diubah!',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $("#chgpass-btn").html("<span class='fas fa-key mr-1' aria-hidden='true' ></span>Ubah Password").attr("disabled", false);
                        $.each(response.messages, function(key, value) {
                            var element = $('#' + key);
                            element.closest('div.form-group')
                                .find('.text-danger')
                                .remove();
                            if (element.parent('.input-group').length) {
                                element.parent().after(value);
                            } else {
                                element.after(value);
                            }
                        });
                    }
                },
                error: function() {
                    swal.fire("Ubah Password", "Ada Kesalahan Saat pengubahan password!", "error");
                    $("#chgpass-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                }
            });

        });

        $('#settinguser').submit(function(e) {
            e.preventDefault();
            var form = this;
            $("#usrsetting-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Mengubah Data").attr("disabled", true);
            var formdata = new FormData(form);
            $.ajax({
                url: "<?= base_url('ajax/usersetting?type=basic'); ?>",
                type: 'POST',
                data: formdata,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Mengubah Data",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(response) {
                    if (response.success == true) {
                        $('.text-danger').remove();
                        swal.fire({
                            icon: 'success',
                            title: 'Ubah Profil Berhasil',
                            text: 'Profil anda sudah diubah!',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        location.reload();
                        $("#usrsetting-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                    } else {
                        swal.close()
                        $("#usrsetting-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                        $.each(response.messages, function(key, value) {
                            var element = $('#' + key);
                            element.closest('div.form-group')
                                .find('.text-danger')
                                .remove();
                            if (element.parent('.input-group').length) {
                                element.parent().after(value);
                            } else {
                                element.after(value);
                            }
                        });
                    }
                },
                error: function() {
                    swal.fire("Mengubah Profil Gagal", "Ada Kesalahan Saat pengubahan profil!", "error");
                    $("#usrsetting-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                }
            });

        });
    </script>
    <script>
        $("#refresh-tabel-absensi").click(function(e) {
            e.preventDefault();
            load_process();
            $('#listabsenku').DataTable().ajax.reload();
        });

        $('#listabsenku').DataTable({
            "ajax": {
                url: "<?= base_url('ajax/get_datatbl?type=allself'); ?>",
                type: 'get',
                async: true,
                "processing": true,
                "serverSide": true,
                dataType: 'json',
                "bDestroy": true
            },
            rowCallback: function(row, data, iDisplayIndex) {
                $('td:eq(0)', row).html();
            }
        });
    </script>

    <!-- CRUD IzinCutiSakit -->
    <script>
        $("#refresh-tabel-pengajuan-izin").click(function(e) {
            e.preventDefault();
            load_process();
            $('#dataizincutisakit').DataTable().ajax.reload();
        });

        $('#dataizincutisakit').DataTable({
            "ajax": {
                url: "<?= base_url('ajax/get_datatbl?type=dataizincutisakit&nik=') . $user['kode_pegawai'] . '&role=' . $user['role_id']; ?>",
                type: 'get',
                async: true,
                "processing": true,
                "serverSide": true,
                dataType: 'json',
                "bDestroy": true
            },
            rowCallback: function(row, data, iDisplayIndex) {
                $('td:eq(0)', row).html();
            }
        });

        $('#jumlah_hari').on('keyup', function() {
            let sisa_cuti = parseInt($('#sisa_cuti').val()) - parseInt($(this).val());
            if (sisa_cuti < 0) {
                swal.fire("Warning", `Sisa Cuti Anda ${$('#sisa_cuti').val()}`, "error");
                $(this).val('')
            } else {
                setTanggalAkhir();
            }
        })

        $('#tanggal_awal').on('change', function() {
            setTanggalAkhir();
        })

        function setTanggalAkhir() {
            let tgl_awal = $('#tanggal_awal').val();
            let jumlah_hari = parseInt($('#jumlah_hari').val())

            if (tgl_awal && jumlah_hari) {
                tanggal = new Date(tgl_awal);
                tanggal.setDate(tanggal.getDate() + (jumlah_hari - 1));
                tanggal_akhir = moment(tanggal).format('yyyy-MM-DD');
                $('#tanggal_akhir').val(tanggal_akhir);
            }
        }

        $('#addizincutisakit').submit(function(e) {
            e.preventDefault();
            var form = this;
            $("#addpgw-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Proses Penambahan").attr("disabled", true);
            var formdata = new FormData(form);
            $.ajax({
                url: "<?= base_url('ajax/dataizincutisakit?type=addpengajuan'); ?>",
                type: 'POST',
                data: formdata,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function() {
                    $("#info-data").hide();
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Menambahkan Pengajuan Izin",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(response) {
                    $("#info-data").html(response.messages).attr("disabled", false).show();
                    if (response.success == true) {
                        $('.text-danger').remove();
                        swal.fire({
                            icon: 'success',
                            title: 'Pengajuan Izin Berhasil',
                            text: 'Pengajuan sudah berhasil !',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $('#dataizincutisakit').DataTable().ajax.reload();
                        $('#addizincutisakitmodal').modal('hide');
                        form.reset();
                        $("#addizincutisakit-btn").html("<span class='fas fa-plus mr-1' aria-hidden='true' ></span>Simpan").attr("disabled", false);
                    } else {
                        swal.close()
                        $("#addizincutisakit-btn").html("<span class='fas fa-plus mr-1' aria-hidden='true' ></span>Simpan").attr("disabled", false);
                    }
                },
                error: function() {
                    swal.fire("Pengajuan Izin Gagal", "Ada Kesalahan Saat melakukan pengajuan!", "error");
                    $("#addizincutisakit-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                }
            });

        });

        $("#dataizincutisakit").on('click', '.approve-pengajuan-izin', function(e) {
            e.preventDefault();
            var pengajuan_id = $(e.currentTarget).data('pengajuan-izin-id');
            if (pengajuan_id === '') return;
            swal.fire({
                icon: 'question',
                title: 'Setujui Pengajuan izin ?',
                showCancelButton: true,
                confirmButtonText: 'Setujui',
            }).then((result) => {

                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('ajax/dataizincutisakit?type=approvepengajuan'); ?>',
                        data: {
                            pengajuan_id: pengajuan_id
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            swal.fire({
                                imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                title: "Menyetujui Pengajuan",
                                text: "Please wait",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        success: function(data) {
                            if (data.success) {
                                swal.fire({
                                    icon: 'success',
                                    title: 'Pengajuan Berhasil Disetujui Berhasil',
                                    text: 'Anda telah menyetujui pengajuan!',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#dataizincutisakit').DataTable().ajax.reload();
                            }
                        },
                        error: function() {
                            swal.fire("Gagal Menyetujui Pengajuan", "Ada Kesalahan Saat menyetujui pengajuan!", "error");
                        }
                    });
                }
            })
        });

        $("#dataizincutisakit").on('click', '.reject-pengajuan-izin', function(e) {
            e.preventDefault();
            var pengajuan_id = $(e.currentTarget).data('pengajuan-izin-id');
            if (pengajuan_id === '') return;
            swal.fire({
                icon: 'question',
                title: 'Tolak Pengajuan izin ?',
                showCancelButton: true,
                confirmButtonText: 'Tolak',
            }).then((result) => {

                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('ajax/dataizincutisakit?type=rejectpengajuan'); ?>',
                        data: {
                            pengajuan_id: pengajuan_id
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            swal.fire({
                                imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                title: "Menolak Pengajuan",
                                text: "Please wait",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        success: function(data) {
                            if (data.success) {
                                swal.fire({
                                    icon: 'success',
                                    title: 'Pengajuan Ditolak',
                                    text: 'Anda telah menolak pengajuan!',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#dataizincutisakit').DataTable().ajax.reload();
                            }
                        },
                        error: function() {
                            swal.fire("Gagal Menolak Pengajuan", "Ada Kesalahan Saat menolak pengajuan!", "error");
                        }
                    });
                }
            })
        });

        $("#dataizincutisakit").on('click', '.cancel-pengajuan-izin', function(e) {
            e.preventDefault();
            var pengajuan_id = $(e.currentTarget).data('pengajuan-izin-id');
            if (pengajuan_id === '') return;
            swal.fire({
                icon: 'question',
                title: 'Batalkan Pengajuan izin ?',
                showCancelButton: true,
                confirmButtonText: 'Batalkan',
            }).then((result) => {

                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: '<?= base_url('ajax/dataizincutisakit?type=cancelpengajuan'); ?>',
                        data: {
                            pengajuan_id: pengajuan_id
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            swal.fire({
                                imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                title: "Membatalkan Pengajuan",
                                text: "Please wait",
                                showConfirmButton: false,
                                allowOutsideClick: false
                            });
                        },
                        success: function(data) {
                            if (data.success) {
                                swal.fire({
                                    icon: 'success',
                                    title: 'Pengajuan Dibatalkan',
                                    text: 'Anda telah membatalkan pengajuan!',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#dataizincutisakit').DataTable().ajax.reload();
                            }
                        },
                        error: function() {
                            swal.fire("Gagal Membatalkan Pengajuan", "Ada Kesalahan Saat membatalkan pengajuan!", "error");
                        }
                    });
                }
            })
        });

        $("#dataizincutisakit").on('click', '.edit-pengajuan-izin', function(e) {
            e.preventDefault();
            var pengajuan_id = $(e.currentTarget).data('pengajuan-izin-id');
            if (pengajuan_id === '') return;
            $.ajax({
                type: "POST",
                url: '<?= base_url('ajax/dataizincutisakit?type=editpengajuan'); ?>',
                data: {
                    pengajuan_id: pengajuan_id
                },
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Mempersiapkan Edit Pengajuan",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.close();
                    $('#editizincutisakitmodal').modal('show');
                    $('#editdataizincutisakit').html(data);

                    $('#editizincutisakit').submit(function(e) {
                        e.preventDefault();
                        var form = this;
                        $("#editizincutisakit-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Menyimpan").attr("disabled", true);
                        var formdata = new FormData(form);
                        $.ajax({
                            url: "<?= base_url('ajax/dataizincutisakit?type=ubahpengajuan'); ?>",
                            type: 'POST',
                            data: formdata,
                            processData: false,
                            contentType: false,
                            dataType: 'json',
                            beforeSend: function() {
                                swal.fire({
                                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                    title: "Mengubah Data Pengajuan Izin",
                                    text: "Please wait",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });
                            },
                            success: function(response) {
                                if (response.success == true) {
                                    $('.text-danger').remove();
                                    swal.fire({
                                        icon: 'success',
                                        title: 'Edit Pengajuan Izin',
                                        text: 'Edit Pengajuan Izin Sudah Berhasil !',
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    $('#dataizincutisakit').DataTable().ajax.reload();
                                    $('#editizincutisakitmodal').modal('hide');
                                    form.reset();
                                    $("#editizincutisakit-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                                } else {
                                    swal.close()
                                    $("#editizincutisakit-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                                    $("#info-edit").html(response.messages);
                                }
                            },
                            error: function() {
                                swal.fire("Edit Pangajuan Izin Gagal", "Ada Kesalahan Saat mengedit pengajuan izin!", "error");
                                $("#editizincutisakit-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                            }
                        });

                    });
                },
                error: function() {
                    swal.fire("Edit Pangajuan Izin Gagal", "Ada Kesalahan Saat mengedit pengajuan izin!", "error");
                }
            });
        });

        $("#dataizincutisakit").on('click', '.view-pengajuan-izin', function(e) {
            e.preventDefault();
            var pengajuan_id = $(e.currentTarget).data('pengajuan-izin-id');
            if (pengajuan_id === '') return;
            $.ajax({
                type: "POST",
                url: '<?= base_url('ajax/dataizincutisakit?type=viewpengajuan'); ?>',
                data: {
                    pengajuan_id: pengajuan_id
                },
                beforeSend: function() {
                    swal.fire({
                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                        title: "Mempersiapkan Preview Pengajuan",
                        text: "Please wait",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                },
                success: function(data) {
                    swal.close();
                    $('#viewizincutisakitmodal').modal('show');
                    $('#viewizincutisakit').html(data);

                },
                error: function() {
                    swal.fire("Preview Pengajuan Gagal", "Ada Kesalahan Saat menampilkan Pengajuan Izin!", "error");
                }
            });
        });

        $('#add_nama_karyawan').on('change', function() {
            let nik = $(this).find(':selected').data('nik');
            $('#add_nik_karyawan').val(nik);
            $.getJSON('<?= base_url('ajax/get_jumlah_cuti?nik='); ?>' + nik, function(data) {
                $('#sisa_cuti').val(data.jumlah_cuti[0].cuti)
                $('#sudah_cuti').val(data.sudah_cuti[0].jumlah_cuti)
                console.log(data)
            })
        });

        $('#edit_nama_karyawan').on('change', function() {
            let nik = $(this).find(':selected').data('nik');
            $('#edit_nik_karyawan').val(nik);
        });

        $('#jenis_pengajuan').on('change', function() {
            if ($(this).val() == 'izin' || $(this).val() == 'cuti') {
                $('.input-skd').addClass('d-none');
            } else {
                $('.input-skd').removeClass('d-none');
            }
        })

        selectKaryawan();

        function selectKaryawan() {
            $.ajax({
                url: "<?= base_url('ajax/getKaryawan'); ?>",
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    let option = '<option value="" data-nik="">Select Employee</option>';
                    $.each(result, function(i, d) {
                        option += `<option value="${d.nama_lengkap}" data-nik="${d.nik}">${d.nama_lengkap}</option>`;
                    })
                    $('#add_nama_karyawan').html('')
                    $('#add_nama_karyawan').html(option)
                    $('#edit_nama_karyawan').append(option)
                }
            });
        }

        $("#dokumen_surat").on('change', function(data) {
            $("#skd").val($(this).val())

            var file = $("input[type=file]").get(0).files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function() {
                    let image = `<img src="${reader.result}" class="img-thumbnail ml-auto mb-3" style="width:250px">`
                    $(".preview-image").html(image);
                    let filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
                    $(".custom-file-label").html(filename);
                }
                reader.readAsDataURL(file);
            }
        })
    </script>
    <?php if ($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 4) : ?>
        <!-- Bagian Dashboard Admin-->
        <script>
            $("#sync-data-dashboard").click(function(e) {
                e.preventDefault();
                load_process();
                $('#list-absensi-masuk,#list-absensi-terlambat,#list-karyawan-simhabis,#list-karyawan-kontrakhabis').DataTable().ajax.reload();
            });

            $("#refresh-tabel-absensi").click(function(e) {
                e.preventDefault();
                load_process();
                $('#list-absensi-all').DataTable().ajax.reload();
            });

            $("#refresh-tabel-pegawai").click(function(e) {
                e.preventDefault();
                load_process();
                $('#datapegawai').DataTable().ajax.reload();
            });
        </script>
        <script>
            $('#list-absensi-masuk').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=getallmsk'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });
            $('#list-absensi-terlambat').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=getalltrl'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });
            $('#list-karyawan-simhabis').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=getsimhabis'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });
            $('#list-karyawan-kontrakhabis').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=getkontrakhabis'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });
        </script>
        <script>
            $('#list-absensi-all').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=all'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });
        </script>
        <!--CRUD Absen-->
        <script>
            $("#clear-absensi").on('click', function(e) {
                e.preventDefault();
                Swal.fire({
                    title: 'Hapus Semua Absen?',
                    text: "Anda yakin ingin menghapus absensi ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: '<?= base_url('ajax/dataabs?type=delallabs'); ?>',
                            beforeSend: function() {
                                swal.fire({
                                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                    title: "Menghapus Semua Absen",
                                    text: "Please wait",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });
                            },
                            success: function(data) {
                                swal.fire({
                                    icon: 'success',
                                    title: 'Menghapus Semua Absen Berhasil',
                                    text: 'Absen telah dihapus!',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#list-absensi-all').DataTable().ajax.reload();
                            },
                            error: function() {
                                swal.fire("Hapus Absensi Gagal", "Ada Kesalahan Saat menghapus semua absensi!", "error");
                            }
                        });
                    }
                })
            });


            $("#list-absensi-all").on('click', '.delete-absen', function(e) {
                e.preventDefault();
                var absen_id = $(e.currentTarget).attr('data-absen-id');
                if (absen_id === '') return;
                Swal.fire({
                    title: 'Hapus Absen Ini?',
                    text: "Anda yakin ingin menghapus absensi ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: '<?= base_url('ajax/dataabs?type=delabs'); ?>',
                            data: {
                                absen_id: absen_id
                            },
                            beforeSend: function() {
                                swal.fire({
                                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                    title: "Menghapus Absen",
                                    text: "Please wait",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });
                            },
                            success: function(data) {
                                swal.fire({
                                    icon: 'success',
                                    title: 'Menghapus Absen Berhasil',
                                    text: 'Absen telah dihapus!',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                $('#list-absensi-all').DataTable().ajax.reload();
                            },
                            error: function() {
                                swal.fire("Hapus Absensi Gagal", "Ada Kesalahan Saat menghapus absensi!", "error");
                            }
                        });
                    }
                })
            });

            $("#list-absensi-all").on('click', '.detail-absen', function(e) {
                e.preventDefault();
                var absen_id = $(e.currentTarget).attr('data-absen-id');
                if (absen_id === '') return;
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('ajax/dataabs?type=viewabs'); ?>',
                    data: {
                        absen_id: absen_id
                    },
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Mempersiapkan Preview Absensi",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(data) {
                        swal.close();
                        $('#viewabsensimodal').modal('show');
                        $('#viewdataabsensi').html(data);

                    },
                    error: function() {
                        swal.fire("Preview Absensi Gagal", "Ada Kesalahan Saat menampilkan data absensi!", "error");
                    }
                });
            });

            $("#list-absensi-all").on('click', '.print-absen', function(e) {
                e.preventDefault();
                var absen_id = $(e.currentTarget).attr('data-absen-id');
                if (absen_id === '') return;
                $('#printabsensimodal').on('show.bs.modal', function(e) {
                    $(this).find('.btn-print-direct').attr('href', '<?= base_url('cetak?id_absen='); ?>' + absen_id + '');
                });
                $("#printdataabsensi").html('<object type="application/pdf" data="<?= base_url('cetak?id_absen='); ?>' + absen_id + '" height="850" style="width: 100%; display: block;">Your browser does not support object tag</object>');
            });
        </script>
        <!--SLIP GAJI-->
        <script>
            $('#dataslipgaji').DataTable({
                   "ajax": {
                        url: "<?= base_url('ajax/get_datatbl?type=dataslip'); ?>",
                        type: 'GET',
                        async: true,
                        "processing": true,
                        "serverSide": true,
                        dataType: 'json',
                        "bDestroy": true
                    },
                    rowCallback: function(row, data, iDisplayIndex) {
                        $('td:eq(0)', row).html();
                    }
                });     

        </script>
        <!--CRUD Pegawai-->
        <script>
            $('#datapegawai').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=datapgw'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });

            $('#datapslipgaji').DataTable({

                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=dataslip'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            }); 

            $('#rekapslip').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=rekapdataslip'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });

            $('#addpegawai').submit(function(e) {
                e.preventDefault();
                alert("test");
                var form = this;
                $("#addpgw-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Proses Penambahan").attr("disabled", true);
                var formdata = new FormData(form);
                $.ajax({
                    url: "<?= base_url('ajax/datapgw?type=addpgw'); ?>",
                    type: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    beforeSend: function() {
                        $("#info-data").hide();
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Menambahkan Pegawai",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(response) {
                        $("#info-data").html(response.messages).attr("disabled", false).show();
                        if (response.success == true) {
                            $('.text-danger').remove();
                            swal.fire({
                                icon: 'success',
                                title: 'Penambahan Pegawai Berhasil',
                                text: 'Penambahan pegawai sudah berhasil !',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#datapegawai').DataTable().ajax.reload();
                            $('#addpegawaimodal').modal('hide');
                            form.reset();
                            $("#addpgw-btn").html("<span class='fas fa-plus mr-1' aria-hidden='true' ></span>Simpan").attr("disabled", false);
                        } else {
                            swal.close()
                            $("#addpgw-btn").html("<span class='fas fa-plus mr-1' aria-hidden='true' ></span>Simpan").attr("disabled", false);
                        }
                    },
                    error: function() {
                        swal.fire("Penambahan Pegawai Gagal", "Ada Kesalahan Saat penambahan pegawai!", "error");
                        $("#addpgw-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                    }
                });

            });

            $("#datapegawai").on('click', '.delete-pegawai', function(e) {
                e.preventDefault();
                var pgw_id = $(e.currentTarget).attr('data-pegawai-id');
                if (pgw_id === '') return;
                Swal.fire({
                    title: 'Hapus User Ini?',
                    text: "Anda yakin ingin menghapus user ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: '<?= base_url('ajax/datapgw?type=delpgw'); ?>',
                            data: {
                                pgw_id: pgw_id
                            },
                            beforeSend: function() {
                                swal.fire({
                                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                    title: "Menghapus User",
                                    text: "Please wait",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });
                            },
                            success: function(data) {
                                if (data.success == false) {
                                    swal.fire({
                                        icon: 'error',
                                        title: 'Menghapus User Gagal',
                                        text: data.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                } else {
                                    swal.fire({
                                        icon: 'success',
                                        title: 'Menghapus User Berhasil',
                                        text: data.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    $('#datapegawai').DataTable().ajax.reload();
                                }
                            },
                            error: function() {
                                swal.fire("Penghapusan Pegawai Gagal", "Ada Kesalahan Saat menghapus pegawai!", "error");
                            }
                        });
                    }
                })
            });

            $("#datapegawai").on('click', '.activate-pegawai', function(e) {
                e.preventDefault();
                var pgw_id = $(e.currentTarget).attr('data-pegawai-id');
                if (pgw_id === '') return;
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('ajax/datapgw?type=actpgw'); ?>',
                    data: {
                        pgw_id: pgw_id
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Aktivasi User",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(data) {
                        if (data.success) {
                            swal.fire({
                                icon: 'success',
                                title: 'Aktivasi User Berhasil',
                                text: 'Anda telah mengaktifkan user!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#datapegawai').DataTable().ajax.reload();
                        } else {
                            swal.fire({
                                icon: 'error',
                                title: 'User Sudah Diaktivasi',
                                text: 'User ini sudah diaktivasi!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#datapegawai').DataTable().ajax.reload();
                        }
                    },
                    error: function() {
                        swal.fire("Aktivasi Pegawai Gagal", "Ada Kesalahan Saat aktivasi pegawai!", "error");
                    }
                });
            });

            $("#datapegawai").on('click', '.view-pegawai', function(e) {
                e.preventDefault();
                var pgw_id = $(e.currentTarget).attr('data-pegawai-id');
                if (pgw_id === '') return;
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('ajax/datapgw?type=viewpgw'); ?>',
                    data: {
                        pgw_id: pgw_id
                    },
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Mempersiapkan Preview User",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(data) {
                        swal.close();
                        $('#viewpegawaimodal').modal('show');
                        $('#viewdatapegawai').html(data);

                    },
                    error: function() {
                        swal.fire("Preview Pegawai Gagal", "Ada Kesalahan Saat menampilkan data pegawai!", "error");
                    }
                });
            });

            $("#datapegawai").on('click', '.edit-pegawai', function(e) {
                e.preventDefault();
                var pgw_id = $(e.currentTarget).attr('data-pegawai-id');
                if (pgw_id === '') return;
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('ajax/datapgw?type=edtpgw'); ?>',
                    data: {
                        pgw_id: pgw_id
                    },
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Mempersiapkan Edit User",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(data) {
                        swal.close();
                        $('#editpegawaimodal').modal('show');
                        $('#editdatapegawai').html(data);

                        $('#editpegawai').submit(function(e) {
                            e.preventDefault();
                            var form = this;
                            $("#editpgw-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Menyimpan").attr("disabled", true);
                            var formdata = new FormData(form);
                            $.ajax({
                                url: "<?= base_url('ajax/editpgwbc?type=edtpgwalt'); ?>",
                                type: 'POST',
                                data: formdata,
                                processData: false,
                                contentType: false,
                                dataType: 'json',
                                beforeSend: function() {
                                    swal.fire({
                                        imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                        title: "Menyimpan Data Pegawai",
                                        text: "Please wait",
                                        showConfirmButton: false,
                                        allowOutsideClick: false
                                    });
                                },
                                success: function(response) {
                                    if (response.success == true) {
                                        $('.text-danger').remove();
                                        swal.fire({
                                            icon: 'success',
                                            title: 'Edit Pegawai Berhasil',
                                            text: 'Edit pegawai sudah berhasil !',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });
                                        $('#datapegawai').DataTable().ajax.reload();
                                        $('#editpegawaimodal').modal('hide');
                                        form.reset();
                                        $("#editpgw-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                                    } else {
                                        swal.close()
                                        $("#editpgw-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                                        $("#info-edit").html(response.messages);
                                    }
                                },
                                error: function() {
                                    swal.fire("Edit Pegawai Gagal", "Ada Kesalahan Saat pengeditan pegawai!", "error");
                                    $("#editpgw-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                                }
                            });

                        });
                    },
                    error: function() {
                        swal.fire("Edit Pegawai Gagal", "Ada Kesalahan Saat pengeditan pegawai!", "error");
                    }
                });
            });
        </script>

        <script>
            $('#datapegawaifico').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=datapgwfico'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });

        </script>
        <!-- Bagian Setting Aplikasi-->
        <script>
            $("#absen_mulai,#absen_sampai, #absen_pulang_sampai").timepicker({
                'timeFormat': 'H:i:s'
            });
            $('#setTimebtn').on('click', function() {
                $('#absen_mulai').timepicker('setTime', new Date());
            });

            $("#resetsettingapp").click(function(event) {
                Swal.fire({
                    title: 'Reset Settings App',
                    text: "Anda yakin ingin reset ulang settingan ini ke awal!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Reset!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "<?= base_url('ajax/initsettingapp?type=1'); ?>",
                            beforeSend: function() {
                                swal.fire({
                                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                                    title: "Resetting Setting App",
                                    text: "Please wait",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });
                            },
                            success: function(data) {
                                swal.fire("Reset!", "Settingan Telah Direset.", "success");
                                location.reload();
                            }
                        });
                    }
                })
                event.preventDefault();
            });

            $("#initsettingapp").click(function(event) {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('ajax/initsettingapp?type=2'); ?>",
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Initializing Setting App",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(data) {
                        swal.fire("Initialize Setting App", "Initialisasi Setting Aplikasi Sukses!", "success");
                        location.reload();
                    }
                });
                event.preventDefault();
            });

            $('#settingapp').submit(function(e) {
                e.preventDefault();
                $("#settingapp-btn").html("<span class='fas fa-spinner fa-pulse' aria-hidden='true' title=''></span> Saving").attr("disabled", true);
                var formdata = new FormData(this);
                $.ajax({
                    url: "<?= base_url('ajax/savingsettingapp'); ?>",
                    type: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Editing Setting App",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(response) {
                        if (response.success == true) {
                            $('.text-danger').remove();
                            swal.fire("Edit Setelan", "Edit Setelan Berhasil!", "success");
                            $("#settingapp-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                            location.reload();
                        } else {
                            swal.close()
                            swal.fire({
                                icon: 'error',
                                title: "Edit Setelan",
                                text: "Edit Setelan Gagal!",
                                showConfirmButton: false,
                                allowOutsideClick: false,
                                timer: 1500
                            });
                            $("#settingapp-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                            $.each(response.messages, function(key, value) {
                                var element = $('#' + key);
                                element.closest('div.form-group')
                                    .find('.text-danger')
                                    .remove();
                                if (element.parent('.input-group').length) {
                                    element.parent().after(value);
                                } else {
                                    element.after(value);
                                }
                            });
                        }
                    },
                    error: function() {
                        swal.fire("Setelan Aplikasi Gagal", "Ada Kesalahan Saat Edit Setelan!", "error");
                        $("#settingapp-btn").html("<span class='fas fa-pen mr-1' aria-hidden='true' ></span>Edit").attr("disabled", false);
                    }
                });

            });
        </script>
    <?php elseif ($this->session->userdata('role_id') == 2) : ?>
        <script>
            $('#list-absensi-all').DataTable({
                "ajax": {
                    url: "<?= base_url('ajax/get_datatbl?type=all'); ?>",
                    type: 'get',
                    async: true,
                    "processing": true,
                    "serverSide": true,
                    dataType: 'json',
                    "bDestroy": true
                },
                rowCallback: function(row, data, iDisplayIndex) {
                    $('td:eq(0)', row).html();
                }
            });
        </script>
        <!--CRUD Absen-->
        <script>
            $("#list-absensi-all").on('click', '.detail-absen', function(e) {
                e.preventDefault();
                var absen_id = $(e.currentTarget).attr('data-absen-id');
                if (absen_id === '') return;
                $.ajax({
                    type: "POST",
                    url: '<?= base_url('ajax/dataabs?type=viewabs'); ?>',
                    data: {
                        absen_id: absen_id
                    },
                    beforeSend: function() {
                        swal.fire({
                            imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                            title: "Mempersiapkan Preview Absensi",
                            text: "Please wait",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                    },
                    success: function(data) {
                        swal.close();
                        $('#viewabsensimodal').modal('show');
                        $('#viewdataabsensi').html(data);

                    },
                    error: function() {
                        swal.fire("Preview Absensi Gagal", "Ada Kesalahan Saat menampilkan data absensi!", "error");
                    }
                });
            });

            $("#list-absensi-all").on('click', '.print-absen', function(e) {
                e.preventDefault();
                var absen_id = $(e.currentTarget).attr('data-absen-id');
                if (absen_id === '') return;
                $('#printabsensimodal').on('show.bs.modal', function(e) {
                    $(this).find('.btn-print-direct').attr('href', '<?= base_url('cetak?id_absen='); ?>' + absen_id + '');
                });
                $("#printdataabsensi").html('<object type="application/pdf" data="<?= base_url('cetak?id_absen='); ?>' + absen_id + '" height="850" style="width: 100%; display: block;">Your browser does not support object tag</object>');
            });
        </script>
    <?php elseif ($this->session->userdata('role_id') == 5) : ?>
        <script>
            $('#getslipgajibyid').DataTable({
                   "ajax": {
                        url: "<?= base_url('ajax/get_datatbl?type=getslipgaji'); ?>",
                        type: 'get',
                        async: true,
                        "processing": true,
                        "serverSide": true,
                        dataType: 'json',
                        "bDestroy": true
                    },
                    rowCallback: function(row, data, iDisplayIndex) {
                        $('td:eq(0)', row).html();
                    }
            });
        </script>
    <?php endif; ?>
    </body>

    </html>
<?php else : ?>
    </main>
    </div>
    </div>
    <script src="<?= base_url('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('assets'); ?>/js/scripts.js"></script>
    <script src="<?= base_url('assets'); ?>/js/sb-admin-js.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url('assets'); ?>/vendor/sweetalert2/sweetalert2.all.min.js"></script>
    </body>

    </html>
<?php endif; ?>