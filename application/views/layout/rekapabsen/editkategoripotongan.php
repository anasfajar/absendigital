<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Kategori Pendapatan</h1>   
    <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } ?>  
    <form action="<?= base_url('updatekategoripotongan')?>" method="post">	

 <div class="card mb-3">
  <div class="card-header">
  KATEGORI POTONGAN
  </div>
  <div class="card-body">
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Potongan Absen</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id_slipgaji'] ?>">
    <input type="text" class="form-control col-sm-6" id="pot_absen" name="pot_absen" value="<?= $pgw['pot_absen'] ?>" required>
    <?= form_error('pot_absen', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">BPJS Ketenagakerjaan</label>
    <input type="text" class="form-control col-sm-6" id="bpjs_tk" name="bpjs_tk" value="<?= $pgw['bpjs_tk'] ?>" required>
    <?= form_error('bpjs_tk', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">BPJS Kesehatan</label>
    <input type="text" class="form-control col-sm-6" id="bpjs_kes" name="bpjs_kes" value="<?= $pgw['bpjs_kes'] ?>" required>
    <?= form_error('bpjs_kes', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Pensiun</label>
    <input type="text" class="form-control col-sm-6" id="pensiun" name="pensiun" value="<?= $pgw['pensiun'] ?>" required>
    <?= form_error('pensiun', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">PPH21</label>
    <input type="text" class="form-control col-sm-6" id="pph21" name="pph21" value="<?= $pgw['pph21'] ?>" required>
    <?= form_error('pph21', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Potongan Backup</label>
    <input type="text" class="form-control col-sm-6" id="pot_backup" name="pot_backup" value="<?= $pgw['pot_backup'] ?>" required>
    <?= form_error('pot_backup', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Potongan Seragam & Perlengkapan</label>
    <input type="text" class="form-control col-sm-6" id="pot_seragam" name="pot_seragam" value="<?= $pgw['pot_seragam'] ?>" required>
    <?= form_error('pot_seragam', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Potongan SPH</label>
    <input type="text" class="form-control col-sm-6" id="pot_sph" name="pot_sph" value="<?= $pgw['pot_sph'] ?>" required>
    <?= form_error('pot_sph', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Potongan Lain-lain</label>
    <input type="text" class="form-control col-sm-6" id="pot_lain" name="pot_lain" value="<?= $pgw['pot_lain'] ?>" required>
    <?= form_error('pot_lain', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>


 <a href="<?php echo base_url('viewdetailslipkaryawan?id=').$pgw['id_slipgaji']?>" class="btn btn-danger" ><span class="fas fa-times mr-1"></span>Kembali</a>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>