<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Data Detail Gaji Karyawan</h1>     

 <div class="card mb-4" >
 <div class="card-header">
    KATEGORI PROFILE
  <div class="float-right">
            <a href="<?= base_url('editkategoriprofile') .'?id='. $viewpgw['id_slipgaji'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
  </div>
   </div>
 <div class="card-body">
 
                      <div class="row detail">                        
                        <div class="col-md-9 col-sm-8 col-6">
                            <dl class="row">
                                <dd class="col-sm-4">NIK</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['nik'] ?></dd>
                                <dd class="col-sm-4">Nama Lengkap</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['nama'] ?></dd>
                                <dd class="col-sm-4">Jabatan</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['jabatan'] ?></dd>
                                <dd class="col-sm-4">Area</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['area'] ?></dd>
                                <dd class="col-sm-4">Customer</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['customer'] ?></dd>
                                <dd class="col-sm-4">HK</dd>
                                <dd class="col-sm-8">: <?= $viewpgw['hk'] ?></dd>
                            </dl>
                        </div>
                    </div>

  </div>
 </div>
<?php
function rupiah($angka){
	
	$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	return $hasil_rupiah;
 
}
?>
 <div class="card mb-3">
  <div class="card-header">
  KATEGORI PENDAPATAN
    <div class="float-right">
            <a href="<?= base_url('editkategoripendapatan') .'?id='. $viewpgw['id_slipgaji'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-9 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Gaji Pokok</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['gaji_pokok']) ?></dd>
    <dd class="col-sm-4">Rapel</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['rapel']) ?></dd>
    <dd class="col-sm-4">Insentif</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['insentif']) ?></dd>
    <dd class="col-sm-4">Lemburan</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['lemburan']) ?></dd>
    <dd class="col-sm-4">Premi Hadir</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['premi_hadir']) ?></dd>
    <dd class="col-sm-4">Tunjangan Pulsa</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['tj_pulsa']) ?></dd>
    <dd class="col-sm-4">Tunjangan Transport</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['tj_transport']) ?></dd>
    <dd class="col-sm-4">Tunjangan Uang Makan</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['tj_um']) ?></dd>
    </dl>
    </div>
    </div>
  </div>
 </div>

 <div class="card mb-3">
  <div class="card-header">
  KATEGORI POTONGAN
    <div class="float-right">
            <a href="<?= base_url('editkategoripotongan') .'?id='. $viewpgw['id_slipgaji'];?>" class="btn btn-primary"><span class="fas fa-user-edit"></span> Edit Data</a>
    </div>
  </div>
  <div class="card-body">
  <div class="row detail">                        
    <div class="col-md-9 col-sm-8 col-6">
    <dl class="row">
    <dd class="col-sm-4">Potongan Absen</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pot_absen']) ?></dd>
    <dd class="col-sm-4">BPJS Ketenagakerjaan</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['bpjs_tk']) ?></dd>
    <dd class="col-sm-4">BPJS Kesehatan</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['bpjs_kes']) ?></dd>
    <dd class="col-sm-4">Pensiun</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pensiun']) ?></dd>
    <dd class="col-sm-4">PPH21</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pph21']) ?></dd>
    <dd class="col-sm-4">Potongan Backup</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pot_backup']) ?></dd>
    <dd class="col-sm-4">Potongan Seragam & Perlengkapan</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pot_seragam']) ?></dd>
    <dd class="col-sm-4">Potongan SPH</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pot_sph']) ?></dd>
    <dd class="col-sm-4">Potongan Lain</dd>
    <dd class="col-sm-8">: <?= rupiah($viewpgw['pot_lain']) ?></dd>
    </dl>
    </div>
    </div>    
  </div>
  <div class="card-footer">
    <div class="float-right">
      <a href="<?php echo base_url('list_gaji?id=').$viewpgw['dokumen_id']?>" class="btn btn-danger" ><span class="fas fa-times mr-1"></span>Kembali</a>
    </div>
  </div>
 </div>



</div>