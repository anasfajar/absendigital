<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          KATEGORI PROFILE
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-10 col-sm-8 col-8">
          <dl class="row">
            <dt class="col-sm-5">NIK</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nik'] ?></dd>
            <dt class="col-sm-5">Nama</dt>
            <dd class="col-sm-7">: <?= $datakandidat['nama'] ?></dd>
            <dt class="col-sm-5">Jabatan</dt>
            <dd class="col-sm-7">: <?= $datakandidat['jabatan'] ?></dd>
            <dt class="col-sm-5">Area</dt>
            <dd class="col-sm-7">: <?= $datakandidat['area'] ?></dd>
            <dt class="col-sm-5">Customer</dt>
            <dd class="col-sm-7">: <?= $datakandidat['customer'] ?></dd>
            <dt class="col-sm-5">HK</dt>
            <dd class="col-sm-7">: <?= $datakandidat['hk'] ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          KATEGORI PENDAPATAN
        </button>
        <?php 
        function rupiah($angka){
	      $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
	      return $hasil_rupiah;
        } ?>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12 col-sm-8 col-8">
          <dl class="row">
            <dt class="col-sm-5">Gaji Pokok</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['gaji_pokok']) ?></dd>
            <dt class="col-sm-5">Rapel</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['rapel']) ?></dd>
            <dt class="col-sm-5">Insentif</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['insentif']) ?></dd>
            <dt class="col-sm-5">Lemburan</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['lemburan']) ?></dd>
            <dt class="col-sm-5">Premi Hadir</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['premi_hadir']) ?></dd>
            <dt class="col-sm-5">Tunjangan Pulsa</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['tj_pulsa']) ?></dd>
            <dt class="col-sm-5">Tunjangan Transport</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['tj_transport']) ?></dd>
            <dt class="col-sm-5">Tunjangan Uang Makan</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['tj_um']) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          KATEGORI POTONGAN
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-10 col-sm-8 col-8">
          <dl class="row">
            <dt class="col-sm-5">Potongan Absen</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['pot_absen']) ?></dd>
            <dt class="col-sm-5">BPJS Ketenagakerjaan</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['bpjs_tk']) ?></dd>
            <dt class="col-sm-5">BPJS Kesehatan</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['bpjs_kes']) ?></dd>
            <dt class="col-sm-5">Pensiun</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['pensiun']) ?></dd>
            <dt class="col-sm-5">PPH21</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['pph21']) ?></dd>
            <dt class="col-sm-5">POTONGAN BACKUP</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['pot_backup']) ?></dd>
            <dt class="col-sm-5">POTONGAN SPH</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['pot_sph']) ?></dd>
            <dt class="col-sm-5">POTONGAN LAIN</dt>
            <dd class="col-sm-7">: <?= rupiah($datakandidat['pot_lain']) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
</div>