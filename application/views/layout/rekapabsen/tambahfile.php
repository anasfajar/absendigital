<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-upload mr-2"></span>Import Data</h1>     
    <form action="<?= base_url('imporslipexcel')?>" method="post" enctype="multipart/form-data">	
 <div class="card mb-3" >
 <div class="card-header">
    Import Data Slip Gaji
 </div>
<div class="card-body">

<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">District</label>
    <input type="text" class="form-control col-sm-6" id="district" name="district" value="<?= set_value('district') ?>" >
    <?= form_error('district', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Upload</label>
    <input type="text" class="form-control col-sm-6" id="nama_upload" name="nama_upload" value="<?= set_value('nama_upload') ?>" >
    <?= form_error('nama_upload', '<small class="text-danger pl-3">', '</small>'); ?>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tahun</label>
    <select id="tahun_slip" class="form-control col-sm-3" name="tahun_slip">
      <option value="2022" >2022</option>
      <option value="2023" >2023</option>
      <option value="2024" >2024</option>
      <option value="2025" >2025</option>
    </select>
</div>
<div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Bulan</label>
    <select id="bulan_slip" class="form-control col-sm-3" name="bulan_slip">
      <option value="Januari" >Januari</option>
      <option value="Februari" >Februari</option>
      <option value="Maret" >Maret</option>
      <option value="April" >April</option>
      <option value="Mei" >Mei</option>
      <option value="Juni" >Juni</option>
      <option value="Juli" >Juli</option>
      <option value="Agustus" >Agustus</option>
      <option value="September" >September</option>
      <option value="Oktober" >Oktober</option>
      <option value="November" >November</option>
      <option value="Desember" >Desember</option>
    </select>
</div>
<div class="mb-3">
<p>Silahkan upload file dengan format .xls</p>
    <div class="custom-file">
    <input type="file" name="file" class="custom-file-input col-sm-3" >
    <label class="custom-file-label" for="customFileLang">Upload file .xls</label>
    <br>
    <?= form_error('file', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
</div>
      
  </div>
 </div>
 <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Batal</button>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Import</button>
</form>
</div>
