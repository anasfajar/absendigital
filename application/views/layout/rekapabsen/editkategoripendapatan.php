<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Kategori Pendapatan</h1>     
    <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } ?>
    <form action="<?= base_url('updatekategoripendapatan')?>" method="post">	

 <div class="card mb-3">
  <div class="card-header">
  KATEGORI PENDAPATAN
  </div>
  <div class="card-body">
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Gaji Pokok</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id_slipgaji'] ?>">
    <input type="text" class="form-control col-sm-6" id="gaji_pokok" name="gaji_pokok" value="<?= $pgw['gaji_pokok'] ?>" required>
    <?= form_error('gaji_pokok', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Rapel</label>
    <input type="text" class="form-control col-sm-6" id="rapel" name="rapel" value="<?= $pgw['rapel'] ?>" required>
    <?= form_error('rapel', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Insentif</label>
    <input type="text" class="form-control col-sm-6" id="insentif" name="insentif" value="<?= $pgw['insentif'] ?>" required>
    <?= form_error('insentif', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Lemburan</label>
    <input type="text" class="form-control col-sm-6" id="lemburan" name="lemburan" value="<?= $pgw['lemburan'] ?>" required>
    <?= form_error('lemburan', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Premi</label>
    <input type="text" class="form-control col-sm-6" id="premi_hadir" name="premi_hadir" value="<?= $pgw['premi_hadir'] ?>" required>
    <?= form_error('premi_hadir', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tunjangan Pulsa</label>
    <input type="text" class="form-control col-sm-6" id="tj_pulsa" name="tj_pulsa" value="<?= $pgw['tj_pulsa'] ?>" required>
    <?= form_error('tj_pulsa', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tunjangan Transport</label>
    <input type="text" class="form-control col-sm-6" id="tj_transport" name="tj_transport" value="<?= $pgw['tj_transport'] ?>" required>
    <?= form_error('tj_transport', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Tunjangan Uang Makan</label>
    <input type="text" class="form-control col-sm-6" id="tj_um" name="tj_um" value="<?= $pgw['tj_um'] ?>" required>
    <?= form_error('tj_um', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>


 <a href="<?php echo base_url('viewdetailslipkaryawan?id=').$pgw['id_slipgaji']?>" class="btn btn-danger" ><span class="fas fa-times mr-1"></span>Kembali</a>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>