<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Data Rekap Gaji Pegawai</h1>
    
    <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } else if($this->session->flashdata('error')){  ?>

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>

    <?php } ?>
    <div class="card mb-4">        
        <div class="card-header">
        <div class="float-left">
        <form action="<?= base_url('exportslipperiode')?>" method="post" enctype="multipart/form-data" class="form-inline">
        <div class="form-group mx-sm-3 mb-2">
                <select id="bulan_slip" class="form-control" name="bulan_slip">
                <option value="Januari" >Januari</option>
                <option value="Februari" >Februari</option>
                <option value="Maret" >Maret</option>
                <option value="April" >April</option>
                <option value="Mei" >Mei</option>
                <option value="Juni" >Juni</option>
                <option value="Juli" >Juli</option>
                <option value="Agustus" >Agustus</option>
                <option value="September" >September</option>
                <option value="Oktober" >Oktober</option>
                <option value="November" >November</option>
                <option value="Desember" >Desember</option>
                </select>
        </div>
        <div class="form-group mx-sm-3 mb-2">
                <select id="tahun_slip" class="form-control" name="tahun_slip">
                <option value="2022" >2022</option>
                <option value="2023" >2023</option>
                <option value="2024" >2024</option>
                <option value="2025" >2025</option>
                </select>
        </div>
        <button type="submit" class="btn btn-warning mb-2"><span class="fas fa-download mr-1"></span>Export</button>
        </form>        
        </div>    
            <div class="float-right">
                <div class="btn btn-primary" id="refresh-tabel-pegawai"><span class="fas fa-sync-alt mr-1"></span>Refresh Data</div>
                <a href="<?= base_url('importslip'); ?>" class="btn btn-primary"><span class="fas fa-file-import mr-1"></span>Import Excel</a>
                <a href="<?= base_url('templateExcel'); ?>" class="btn btn-warning"><span class="fas fa-download mr-1"></span>Unduh Template (xlxs) </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="rekapslip" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama File</th>
                            <th>Nama Upload</th>
                            <th>Distict</th>
                            <!--<th>Pas Foto</th>-->
                            <th>Tahun</th>
                            <th>Bulan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama File</th>
                            <th>Nama Upload</th>
                            <th>Distict</th>
                            <!--<th>Pas Foto</th>-->
                            <th>Tahun</th>
                            <th>Bulan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>