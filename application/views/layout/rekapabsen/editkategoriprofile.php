<div class="container-fluid">
    <h1 class="my-4"><span class="fas fa-user-tie mr-2"></span>Edit Kategori Profile</h1>     
    <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>

    <?php } ?>
    <form action="<?= base_url('updatekategoriprofile')?>" method="post" enctype="multipart/form-data">	

 <div class="card mb-3">
  <div class="card-header">
    KATEGORI PROFILE
  </div>
  <div class="card-body">
    <div class="mb-3">
    <label for="formGroupExampleInput" class="form-label">NIK</label>
    <input type="hidden" class="form-control" id="id" name="id" value="<?= $pgw['id_slipgaji'] ?>">
    <input type="text" class="form-control" id="nik" name="nik" value="<?= $pgw['nik'] ?>" disabled>
    <?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Nama Lengkap</label>
    <input type="text" class="form-control col-sm-6" id="nama" name="nama" value="<?= $pgw['nama'] ?>" disabled>
    <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Jabatan</label>
    <input type="text" class="form-control col-sm-6" id="jabatan" name="jabatan" value="<?= $pgw['jabatan'] ?>" disabled>
    <?= form_error('jabatan', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Area</label>
    <input type="text" class="form-control col-sm-6" id="area" name="area" value="<?= $pgw['area'] ?>" disabled>
    <?= form_error('area', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">Customer</label>
    <input type="text" class="form-control col-sm-6" id="customer" name="customer" value="<?= $pgw['customer'] ?>" disabled>
    <?= form_error('customer', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
    <div class="mb-3">
    <label for="formGroupExampleInput2" class="form-label">HK</label>
    <input type="text" class="form-control col-sm-6" id="hk" name="hk" value="<?= $pgw['hk'] ?>" required>
    <?= form_error('hk', '<small class="text-danger pl-3">', '</small>'); ?>
    </div>
  </div>
 </div>


 <a href="<?php echo base_url('viewdetailslipkaryawan?id=').$pgw['id_slipgaji']?>" class="btn btn-danger" ><span class="fas fa-times mr-1"></span>Kembali</a>
 <button type="submit" class="btn btn-primary" id="editpgw-btn"><span class="fas fa-pen mr-1"></span>Update</button>
</form>
</div>