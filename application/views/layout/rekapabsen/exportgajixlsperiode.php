<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Data Provinsi</title>
    </head>
    <body>
        <?php
        error_reporting(0);
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Rekap Gaji Karyawan $dokumen[bulan]-$dokumen[tahun].xls");


            $table = '
            <table border="1">
                <tr>
                    <th>NO.</th>
                    <th>FILE/nama_upload/Bulan/Tahun</th>
                    <th>NIK</th>
                    <th>NAMA</th>
                    <th>CUSTOMER</th>
                    <th>AREA</th>
                    <th>Status</th>
                    <th>TAKE AWAY SALARY</th>
                </tr>';
                $no = 1;
                foreach($rekap as $d){
                    $status = ($d->status == 1) ? 'Approved' : 'Belum Approved';
                    $table .='
                    <tr>
                        <td>'.$no++.'</td>
                        <td>'.$d->nama_dokumen.'/'.$d->nama_upload.'/'.$d->bulan.'/'.$d->tahun.'</td>
                        <td>'.$d->nik.'</td>
                        <td>'.$d->nama.'</td>
                        <td>'.$d->customer.'</td>
                        <td>'.$d->area.'</td>
                        <td>'. $status .'</td>
                        <td>'.$d->netto_sistem.'</td>
                    </tr>';
                }
                $table .='                
            </table>
            ';

            echo $table;
        ?>
    </body>
</html>