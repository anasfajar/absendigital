<div class="container-fluid">
    <h3 class="my-4"><span class="fas fa-user-tie mr-2"></span>Data Rekap Gaji Pegawai - <?php echo $dokumen['bulan']; ?> <?php echo $dokumen['tahun']; ?></h3>
    <div class="card mb-4">
        <div class="card-header">
            <div class="float-right">
                <div class="btn btn-primary" id="refresh-tabel-pegawai"><span class="fas fa-sync-alt mr-1"></span>Refresh Data</div>
                <a href="<?= base_url('exportslip'); ?>?id=<?= $slipgaji['dokumen_id']; ?>" class="btn btn-primary"><span class="fas fa-file-export mr-1"></span>Export Excel</a>
                <a href="<?= base_url('createslip'); ?>?id=000&dokid=<?= $slipgaji['dokumen_id']; ?>" class="btn btn-warning" target="_blank"><span class="fas fa-file-excel mr-1"></span>Buat Slip Gaji</a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataslipgaji" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK Pegawai</th>
                            <th>Nama Pegawai</th>
                            <!--<th>Pas Foto</th>-->
                            <th>Area</th>
                            <th>Take Home Salary</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>NIK Pegawai</th>
                            <th>Nama Pegawai</th>
                            <!--<th>Pas Foto</th>-->
                            <th>Area</th>
                            <th>Take Home Salary</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal View Gaji Karyawan -->
<div class="modal fade" id="viewslipgajimodal" tabindex="-1" role="dialog" aria-labelledby="viewpegawaimodal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="viewpegawaimodallabel"><span class="fas fa-user-tie mr-1"></span>DATA DETAIL GAJI KARYAWAN</h5>
            </div>
            <div class="modal-body">
                <div id="datadetailslipgaji"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fas fa-times mr-1"></span>Tutup</button>
                
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $("body").on("click", "#detailslipgaji", function(e) {
        e.preventDefault();
        var pgw_id = $(e.currentTarget).attr('data-slipgaji-id');
        if (pgw_id === '') return;
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/datakandidat?type=viewslipgaji'); ?>',
            data: {
                pgw_id: pgw_id
            },
            beforeSend: function() {
                swal.fire({
                    imageUrl: "<?= base_url('assets'); ?>/img/ajax-loader.gif",
                    title: "Mempersiapkan Preview User",
                    text: "Please wait",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            success: function(data) {
                swal.close();
                $('#viewslipgajimodal').modal('show');
                $('#datadetailslipgaji').html(data);

            },
            error: function() {
                swal.fire("Preview Pegawai Gagal", "Ada Kesalahan Saat menampilkan data Gaji Karyawan!", "error");
            }
        });
    });
</script>