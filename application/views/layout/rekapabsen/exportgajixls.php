<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Data Provinsi</title>
    </head>
    <body>
        <?php

        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Rekap Gaji Karyawan $dokumen[nama_dokumen]-$dokumen[nama_upload] $dokumen[bulan]/$dokumen[tahun].xls");


            $table = '
            <table border="1">
                <tr>
                    <th>NO.</th>
                    <th>NIK</th>
                    <th>NAMA</th>
                    <th>CUSTOMER</th>
                    <th>AREA</th>
                    <th>TAKE AWAY SALARY</th>
                </tr>';
                $no = 1;
                foreach($rekap as $d){
                    $table .='
                    <tr>
                        <td>'.$no++.'</td>
                        <td>'.$d->nik.'</td>
                        <td>'.$d->nama.'</td>
                        <td>'.$d->customer.'</td>
                        <td>'.$d->area.'</td>
                        <td>'.$d->netto_sistem.'</td>
                    </tr>';
                }
                $table .='                
            </table>
            ';

            echo $table;
        ?>
    </body>
</html>