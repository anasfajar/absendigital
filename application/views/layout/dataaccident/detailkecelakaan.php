<div class="container-fluid">
  <div class="mt-2">
    <?= $content; ?>
  </div>

  <div class="col-md-12 mt-3">
    <div class="row">
      <div>
        <a href="<?= base_url('accident') ?>" class="btn btn-warning">Kembali</a>
        <a href="<?= base_url('cetakkecelakaan?id=') . $id; ?>" target="_blank" class="btn btn-danger">Cetak PDF</a>
      </div>
    </div>
  </div>
</div>