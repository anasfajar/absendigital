<style type="text/css">
  .notif {
    color: red;
    font-size: small;
  }
</style>
<div class="container-fluid">
  <h1 class="my-4"><span class="fas fa-truck mr-2"></span>Data Kecelakaan</h1>
  <form id="formKecelakaan" action="<?= site_url('Admin/simpankecelakaan') ?>" method="POST" enctype="multipart/form-data">
    <div class="card mb-3">
      <div class="card-header">
        DATA KEJADIAN
      </div>
      <div class="card-body">
        <div class="mb-3">
          <label for="formGroupExampleInput" class="form-label">NIK Driver</label>
          <input type="text" class="form-control" name="nik_driver" id="nik_driver" placeholder="NIK Driver">
          <span class="ml-2 notif Nnik_driver"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Nama Driver</label>
          <input type="text" class="form-control" name="driver" id="driver" placeholder="Nama Driver">
          <span class="ml-2 notif Ndriver"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Usia Driver</label>
          <input type="text" class="form-control" name="usia_driver" id="usia_driver" placeholder="Usia Driver">
          <span class="ml-2 notif Nusia_driver"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">No Polisi</label>
          <input type="text" class="form-control" name="no_polisi" id="no_polisi" placeholder="No Polisi">
          <span class="ml-2 notif Nno_polisi"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Lokasi</label>
          <input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="Lokasi">
          <span class="ml-2 notif Nlokasi"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Tanggal</label>
          <div class="input-group date" id="tanggalKecelakaan">
            <input type="text" name="tanggal" id="tanggal" placeholder="Tanggal Kejadian" class="form-control" />
            <div class="input-group-addon input-group-append">
              <div class="input-group-text">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
              </div>
            </div>
          </div>
          <span class="ml-2 notif Ntanggal"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Customer</label>
          <input type="text" class="form-control" name="customer" id="customer" placeholder="Customer">
          <span class="ml-2 notif Ncustomer"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Faktor</label>
          <input type="text" class="form-control" name="faktor" id="faktor" placeholder="Faktor">
          <span class="ml-2 notif Nfaktor"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Muatan</label>
          <input type="text" class="form-control" name="muatan" id="muatan" placeholder="Muatan">
          <span class="ml-2 notif Nmuatan"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Rute</label>
          <input type="text" class="form-control" name="rute" id="rute" placeholder="Rute">
          <span class="ml-2 notif Nrute"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Kategori</label>
          <select name="kategori" id="kategori" class="form-control">
            <option value="">Pilih Kategori</option>
            <option value="fatality">Fatality</option>
            <option value="rankA">Rank A</option>
            <option value="rankB">Rank B</option>
            <option value="rankC">Rank C</option>
          </select>
          <span class="ml-2 notif Nkategori"></span>
        </div>

      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        Kronologis Kejadian
      </div>
      <div class="card-body">
        <textarea name="kronologis" id="kronologis" cols="30" rows="3" class="form-control"></textarea>
        <span class="ml-2 notif Nkronologis"></span>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        Tindakan Penanganan Kejadian
      </div>
      <div class="card-body">
        <textarea name="tindakan" id="tindakan" cols="30" rows="3" class="form-control"></textarea>
        <span class="ml-2 notif Ntindakan"></span>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        Biaya Penanganan Kejadian
      </div>
      <div class="card-body">
        <div class="mb-3">
          <label class="form-label">Biaya</label>
          <input type="text" class="form-control" name="biaya_penanganan" id="biaya_penanganan" placeholder="">
          <span class="ml-2 notif Nbiaya_penanganan"></span>
        </div>
        <div class="mb-3">
          <label class="form-label">Korban Lain</label>
          <select name="korban_lain" id="korban_lain" class="form-control">
            <option value="">Pilih Salah Satu</option>
            <option value="ada">Ada</option>
            <option value="tidak ada">Tidak Ada</option>
          </select>
          <span class="ml-2 notif Nkorban_lain"></span>
        </div>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        Analisis Kejadian
      </div>
      <div class="card-body">
        <textarea name="analisis" id="analisis" cols="30" rows="3" class="form-control"></textarea>
        <span class="ml-2 notif Nanalisis"></span>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        Kesimpulan Hasil Investigasi
      </div>
      <div class="card-body">
        <textarea name="kesimpulan" id="kesimpulan" cols="30" rows="3" class="form-control"></textarea>
        <span class="ml-2 notif Nkesimpulan"></span>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header">
        Tindakan Antisipasi Kejadian
      </div>
      <div class="card-body">
        <textarea name="antisipasi" id="antisipasi" cols="30" rows="3" class="form-control"></textarea>
        <span class="ml-2 notif Nantisipasi"></span>
      </div>
    </div>
    <div class="card mb-3">
      <div class="card-header">
        Foto Kejadian
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4"><input type="file" name="filefoto1" class="dropify" data-max-file-size="3M" data-height="200"></div>
          <div class="col-md-4"><input type="file" name="filefoto2" class="dropify" data-max-file-size="3M" data-height="200"></div>
          <div class="col-md-4"><input type="file" name="filefoto3" class="dropify" data-max-file-size="3M" data-height="200"></div>
        </div>
      </div>
    </div>

    <a href="<?= base_url('accident') ?>" class="btn btn-warning">Kembali</a>
    <button type="submit" class="btn btn-primary"><span class="fas fa-pen mr-1"></span>Simpan</button>

  </form>
</div>