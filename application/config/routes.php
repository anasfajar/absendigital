<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin/dashboard';
// $route['default_controller'] = 'home';
$route['404_override'] = 'err/notfound';
$route['translate_uri_dashes'] = FALSE;

//Admin URL
$route['settingapp'] = 'admin/settingapp';
$route['dashboard'] = 'admin/dashboard';
$route['datapegawai'] = 'admin/datapegawai';
$route['datapegawaifico'] = 'admin/datapegawaifico';
$route['searchdatapegawaifico'] = 'admin/searchdatapegawai';
$route['searchdatapegawaifico'] = 'admin/searchdatapegawai';
$route['tambahpegawai'] = 'admin/tambahpegawai';
$route['absensi'] = 'admin/absensi';
$route['viewpegawaimbk'] = 'admin/viewpegawaimbk';
$route['editpegawaimbk'] = 'admin/editpegawaimbk';
$route['editphotopegawaimbk'] = 'admin/editphotopegawaimbk';
$route['updatepgw'] = 'admin/updatepgw';
$route['updateketeranganbank'] = 'admin/updateketeranganbank';
$route['simpanpgw'] = 'admin/savepgw';
$route['updatefotopgw'] = 'admin/updatepgwfoto';
$route['lap_profilpegawai'] = 'admin/pdfpegawai';
$route['accident'] = 'admin/accident';
$route['tambahkecelakaan'] = 'admin/addaccident';
$route['detailkecelakaan'] = 'admin/detailaccident';
$route['cetakkecelakaan'] = 'admin/cetakaccident';
$route['kandidat-lulus'] = 'admin/listkandidatlulus';
$route['edit-kandidat/(:any)'] = 'admin/editkandidatlulus/$1';
$route['export-kandidat/(:any)'] = 'admin/exportkandidatlulus/$1';
$route['editketeranganbank'] = 'admin/editketeranganbank';
$route['editaktifasikaryawan'] = 'admin/editaktifasikaryawan';
$route['updateaktifasikaryawan'] = 'admin/updateaktifasikaryawan';
$route['editklasifikasiskill'] = 'admin/editklasifikasiskill';
$route['updateklasifikasiskill'] = 'admin/updateklasifikasiskill';
$route['editkontrakkerja'] = 'admin/editkontrakkerja';
$route['updatekontrakkerja'] = 'admin/updatekontrakkerja';
$route['edittrainingsafetydriving'] = 'admin/edittrainingsafetydriving';
$route['updatetrainingsafetydriving'] = 'admin/updatetrainingsafetydriving';
$route['editketeranganvaksinpegawai'] = 'admin/editketeranganvaksinpegawai';
$route['updateketeranganvaksinpegawai'] = 'admin/updateketeranganvaksinpegawai';
$route['edittrainingpegawai'] = 'admin/edittrainingpegawai';
$route['updatetrainingpegawai'] = 'admin/updatetrainingpegawai';
$route['editketeranganbpjs'] = 'admin/editketeranganbpjs';
$route['updateketeranganbpjs'] = 'admin/updateketeranganbpjs';
$route['editaccident'] = 'admin/editaccident';
$route['updateaccident'] = 'admin/updateaccident';
$route['edittraining'] = 'admin/edittraining';
$route['updatetraining'] = 'admin/updatetraining';

$route['lap_surat'] = 'admin/suratketkerja';

$route['dpAbsen'] = 'admin/datapegawaiAbsen';

$route['mutasi'] = 'admin/mutasi';
//rekapabsen
$route['rekap-gaji'] = 'Rekap_gaji/index';
$route['list_gaji'] = 'Rekap_gaji/list_gaji';
$route['importslip'] = 'Rekap_gaji/import_slip';
$route['exportslip'] = 'Rekap_gaji/exportslipgaji';
$route['templateExcel'] = 'Rekap_gaji/templateExcel';
$route['exportslipperiode'] = 'Rekap_gaji/exportslipgajiperiode';
$route['imporslipexcel'] = 'Rekap_gaji/excel';
$route['createslip'] = 'Rekap_gaji/create_slip';
$route['approve_dokumen'] = 'Rekap_gaji/approve';
$route['hapus_dokumen'] = 'Rekap_gaji/hapusDok';
$route['payroll'] = 'Registrasi/dashboard_pegawai';
$route['logout-payroll'] = 'Registrasi/logoutPayroll';
$route['proses-loginPayroll'] = 'Registrasi/loginProsesPayroll';
$route['cetak_slip'] = 'Registrasi/create_slip';
$route['login-payroll'] = 'Registrasi/login_payroll';


$route['viewdetailslipkaryawan'] = 'Rekap_gaji/viewdetailslip';
$route['editkategoriprofile'] = 'Rekap_gaji/editprofile';
$route['updatekategoriprofile'] = 'Rekap_gaji/updateprofile';
$route['editkategoripendapatan'] = 'Rekap_gaji/editpendapatan';
$route['updatekategoripendapatan'] = 'Rekap_gaji/updatependapatan';
$route['editkategoripotongan'] = 'Rekap_gaji/editpotongan';
$route['updatekategoripotongan'] = 'Rekap_gaji/updatepotongan';


//Kandidat
$route['registrasi'] = 'Registrasi';
$route['login-kandidat'] = 'Registrasi/login';
$route['logout-kandidat'] = 'Registrasi/logout';
$route['proses-login'] = 'Registrasi/loginProses';
$route['pengetahuan-test'] = 'Registrasi/pengetahuan';
$route['kelengkapan-data'] = 'Registrasi/kelengkapan';
$route['update-kandidat'] = 'Registrasi/update_kandidat';
$route['soal-pengetahuan'] = 'Registrasi/load_soal_pengetahuan';
$route['update-score-pengetahuan'] = 'Registrasi/update_nilai_pengetahuan';







//User URL
$route['setting'] = 'user/setting';
$route['profile'] = 'user/profile';
$route['absensiku'] = 'user/absensiku';
$route['kalenderabsen'] = 'user/kalenderabsen';
$route['pengajuan_izin'] = 'user/pengajuan_izin';
$route['form_pengajuan_izin'] = 'user/form_pengajuan_izin';

//Auth URL
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';

//Misc URL
$route['block'] = 'err/block';
$route['cetak'] = 'docs/print';
$route['export'] = 'docs/export';
$route['instantabsen'] = 'absen/instant';
$route['confirmabsen'] = 'absen/confirmabsen';

$route['reg'] = 'registrasi/proses_registrasi';
