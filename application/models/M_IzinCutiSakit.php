<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_IzinCutiSakit extends CI_Model
{

    public function crudizincutisakit($typesend)
    {
        if ($typesend == 'addpengajuan') {

            $upload_image = $_FILES['dok_surat']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size']      = '2048';
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = $this->config->item('SAVE_FOLDER_SKD');

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('dok_surat')) {
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('dok_surat', $new_image);
                }

            } else {
                $this->db->set('dok_surat', 'NULL');
            }

            $sendsave = [
                'nik_kary' => htmlspecialchars($this->input->post('nik_kary')),
                'nama_kary' => htmlspecialchars($this->input->post('nama_kary')),
                'jenis_pengajuan' => htmlspecialchars($this->input->post('jenis_pengajuan')),
                'ket' => htmlspecialchars($this->input->post('ket')),
                'no_hp' => htmlspecialchars($this->input->post('no_hp')),
                'alamat' => htmlspecialchars($this->input->post('alamat')),
                'tgl_awal' => htmlspecialchars($this->input->post('tgl_awal')),
                'tgl_akhir' => htmlspecialchars($this->input->post('tgl_akhir')),
                'jum_hari' => htmlspecialchars($this->input->post('jum_hari')),
                'created_at' => date("Y-m-d h:i:s"),
            ];
            $this->db->insert('tx_ijincutisakit', $sendsave);
        } elseif ($typesend == 'approvepengajuan') {
            $update = [
                'f_approve'   => 1,
                'tgl_approve' => date('Y-m-d'),
                'updated_at' => date('Y-m-d h:i:s'),
            ];
            $this->db->set($update);
            $this->db->where('id', htmlspecialchars($this->input->post('pengajuan_id', true)));
            $this->db->update('tx_ijincutisakit');
        } elseif ($typesend == 'rejectpengajuan') {
            $update = [
                'f_approve'   => 2,
                'tgl_approve' => date('Y-m-d'),
                'updated_at' => date('Y-m-d h:i:s'),
            ];
            $this->db->set($update);
            $this->db->where('id', htmlspecialchars($this->input->post('pengajuan_id', true)));
            $this->db->update('tx_ijincutisakit');
        } elseif ($typesend == 'cancelpengajuan') {
            $update = [
                'f_approve'   => 3,
                'tgl_approve' => date('Y-m-d'),
                'updated_at' => date('Y-m-d h:i:s'),
            ];
            $this->db->set($update);
            $this->db->where('id', htmlspecialchars($this->input->post('pengajuan_id', true)));
            $this->db->update('tx_ijincutisakit');
        } elseif ($typesend == 'ubahpengajuan') {

            $upload_image = $_FILES['dok_surat']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size']      = '2048';
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = $this->config->item('SAVE_FOLDER_SKD');

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('dok_surat')) {
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('dok_surat', $new_image);
                    unlink($this->config->item('SAVE_FOLDER_SKD') . $this->input->post('dok_surat_lama'));
                } 

            } else {
                $this->db->set('dok_surat', $this->input->post('dok_surat_lama'));
            }

            $sendsave = [
                'nik_kary' => htmlspecialchars($this->input->post('nik_kary')),
                'nama_kary' => htmlspecialchars($this->input->post('nama_kary')),
                'jenis_pengajuan' => htmlspecialchars($this->input->post('jenis_pengajuan')),
                'ket' => htmlspecialchars($this->input->post('ket')),
                'no_hp' => htmlspecialchars($this->input->post('no_hp')),
                'alamat' => htmlspecialchars($this->input->post('alamat')),
                'tgl_awal' => htmlspecialchars($this->input->post('tgl_awal')),
                'tgl_akhir' => htmlspecialchars($this->input->post('tgl_akhir')),
                'jum_hari' => htmlspecialchars($this->input->post('jum_hari')),
                'updated_at' => date("Y-m-d h:i:s"),
            ];
            $this->db->set($sendsave);
            $this->db->where('id', htmlspecialchars($this->input->post('id', true)));
            $this->db->update('tx_ijincutisakit');
        }
    }

}
