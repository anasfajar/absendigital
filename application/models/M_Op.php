<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Op extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function getAll()
    {
        $this->db->order_by('nama_area','ASC');
        $query = $this->db->get('ms_operation_poin');
        return $query->result();
    }

}