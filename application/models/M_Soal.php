<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Soal extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
    }

    public function querySoal($tipe)
    {
        $this->db->select('*');
        $this->db->from('soal_type');
        $this->db->join('soal', 'soal_type.id = soal.soal_type_id');
        $this->db->where('soal_type.tipe', $tipe);
        $this->db->order_by('category , sort asc');
        $query = $this->db->get();
        return $query->result_array();
    }
}
