<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Dokumen extends CI_Model
{

    public $table = 'ms_dokumen';
    public $id = 'id';
    var $order = ['id' => 'asc'];

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($orderby = NULL)
    {

        if ($orderby) {
            $this->order = $orderby;
        }
        $this->db->order_by($this->id, 'asc');
        return $this->db->get($this->table)->result();
    }

    public function getDokumenKandidat($kandidat_id)
    {
        $this->db->select('*');
        $this->db->from('dokumen_kandidat');
        $this->db->where('kandidat_id', $kandidat_id);
        $this->db->order_by('dokumen_id  asc');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function saveDokumenKandidat($kandidatId, $documentList, $documentSelected, $keteranganSelected)
    {

        //delete dokumen kandidat 
       $this->db->where('kandidat_id', $kandidatId);
       $this->db->delete('dokumen_kandidat');

        //add dokumen kandidat
        for ($i = 0; $i < count($documentList); $i++) {

            $keteranganValue = null;
            $valueSelected = 0;
          
            if(in_array($documentList[$i]->id, $documentSelected))
                 $valueSelected = 1;
                 

            if (isset($keteranganSelected[$i]) && !empty($keteranganSelected[$i]))
                $keteranganValue = $keteranganSelected[$i];

                //if($documentSelected[$i] = "0" && empty($keteranganValue)){
                    //$this->session->set_flashdata('danger', 'Harap Isi Terlebih Dahulu Data');
                    //redirect(site_url('dokumen/kandidat/').$kandidatId);
                //}else{
                    $dataAdded = ['kandidat_id' => $kandidatId, 'dokumen_id' => $documentList[$i]->id, 'value' => $valueSelected, 'keterangan' => $keteranganValue];
                    $this->db->insert('dokumen_kandidat', $dataAdded);
                //}           

        }

        //get sum of check dokumen_kandidat
    
        $this->db->select_sum('value');
        $this->db->where('kandidat_id', $kandidatId);
        $result = $this->db->get('dokumen_kandidat')->row(); 
        
        $status_dokumen = ($result->value != count($documentList)  ? 'Pending': 'Selesai');

        //update status dokumen 
        $this->db->set('status_dokumen', $status_dokumen);
        $this->db->where('id', $kandidatId);
        $this->db->update('kandidat');

    }

}
