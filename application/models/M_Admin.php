<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Admin extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
        $bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
        $this->get_today_date = $hari[(int)date("w")] . ', ' . date("j ") . $bulan[(int)date('m')] . date(" Y");
        $this->get_datasess = $this->db->get_where('user', ['username' =>
        $this->session->userdata('username')])->row_array();
        $this->appsetting = $this->db->get_where('db_setting', ['status_setting' => 1])->row_array();
    }

    public function hitungjumlahdata($typehitung)
    {
        $today = $this->get_today_date;
        if ($typehitung == 'jmlpgw') {

            $query = $this->db->get('master_karyawan');
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else {
                return 0;
            }
        } elseif ($typehitung == 'pgwtrl') {
            $query = $this->db->get_where('db_absensi', ['status_pegawai' => 2, 'tgl_absen' => $today]);
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else {
                return 0;
            }
        } elseif ($typehitung == 'pgwmsk') {
            $query = $this->db->get_where('db_absensi', ['status_pegawai' => 1, 'tgl_absen' => $today]);
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else {
                return 0;
            }
        }
    }

    public function fetchlistpegawai()
    {
        return $this->db->get_where('user')->result();
    }

    public function fetchlistpegawaibydriverleaderid(){
        $id = $this->session->userdata('id');

        $query = $this->db->query("SELECT a.nama,a.nik,a.id,a.driver,a.helper,a.checker,a.non_driver,a.fico, (SELECT case when status_absen = 'hadir' then concat(status_absen,' (',jam_masuk,')') ELSE status_absen END FROM db_absensi WHERE kode_pegawai = a.nik AND DATE_FORMAT(created_at,'%Y%m%d') = ".date('Ymd').") AS kode
        FROM master_karyawan a
        WHERE a.driver_leader_id =  $id 
        ORDER BY a.nama ASC");

        return $query->result();
    }

    public function fetchlistmutasidata(){
        $id = $this->session->userdata('id');

        $query = $this->db->query("SELECT a.nama,a.nik,a.id,a.driver,a.helper,a.checker,a.non_driver,a.fico, (SELECT case when status_absen = 'hadir' then concat(status_absen,' (',jam_masuk,')') ELSE status_absen END FROM db_absensi WHERE kode_pegawai = a.nik AND DATE_FORMAT(created_at,'%Y%m%d') = ".date('Ymd').") AS kode
        FROM master_karyawan a
        WHERE a.driver_leader_id =  $id 
        ORDER BY a.nama ASC");

        return $query->result();
  }

    public function fetchlistkecelakaan()
    {
        return $this->db->get_where('kecelakaan')->result();
    }

    public function getviewpegawaimbk($id)
    {
        return $this->db->get_where('master_karyawan', ['id' => $id])->row_array();
    }

    public function update_pegawai()
    {
        $data = [
            'nik' => $this->input->post('nik'),
            'nama' => $this->input->post('nama'),
            'no_ktp' => $this->input->post('no_ktp'),
            'masa_berlaku' => $this->input->post('masa_berlaku'),
            'nomor_hp' => $this->input->post('nomor_hp'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tglblnthn_lahir' => $this->input->post('tglblnthn_lahir'),
            'alamat' => $this->input->post('alamat'),
            'status_pajak' => $this->input->post('status_pajak'),
            'agama' => $this->input->post('agama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
            'nomor_kartu_keluarga' => $this->input->post('nomor_kartu_keluarga'),
            'npwp' => $this->input->post('npwp'),
            'email_address' => $this->input->post('email_address'),
            'vendor' => "PT.MBK",        

           
            
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function update_keteranganbank()
    {
        $data = [
            'nama_di_bank' => $this->input->post('nama_di_bank'),
            'bank' => $this->input->post('bank'),
            'no_account' => $this->input->post('no_account')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    function get_no_invoice()
    {
        //$q = $this->db->query("SELECT MAX(RIGHT(nik,4)) AS nik_pegawai FROM master_karyawan");
        //$kd = "";
        //if($q->num_rows()>0){
        //    foreach($q->result() as $k){
        //        $tmp = ((int)$k->nik_pegawai)+1;
        //        $kd = sprintf("%04s", $tmp);
        //    }
        //}else{
        //    $kd = "0001";
        //}
        $this->db->select('RIGHT(master_karyawan.nik,5) as nik', FALSE);
        $this->db->order_by('nik', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('master_karyawan');
        if ($query->num_rows() <> 0) {
            $data = $query->row();
            $kode = intval($data->nik) + 1;
        } else {
            $kode = 1;
        }
        $batas = str_pad($kode, 5);
        $kodetampil = $batas;
        return $kodetampil;
    }

    public function save_pegawai()
    {
        $data = [
          'nik' => $this->input->post('nik'),
          'nama' => $this->input->post('nama'),
          'no_ktp' => $this->input->post('no_ktp'),
          'masa_berlaku' => $this->input->post('masa_berlaku'),
          'nomor_hp' => $this->input->post('nomor_hp'),
          'tempat_lahir' => $this->input->post('tempat_lahir'),
          'tglblnthn_lahir' => $this->input->post('tglblnthn_lahir'),
          'alamat' => $this->input->post('alamat'),
          'status_pajak' => $this->input->post('status_pajak'),
          'agama' => $this->input->post('agama'),
          'jenis_kelamin' => $this->input->post('jenis_kelamin'),
          'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
          'nomor_kartu_keluarga' => $this->input->post('nomor_kartu_keluarga'),
          'npwp' => $this->input->post('npwp'),
          'email_address' => $this->input->post('email_address'),
          'vendor' => $this->input->post('vendor'),
          'nama_di_bank' => $this->input->post('nama_di_bank'),
          'bank' => $this->input->post('bank'),
          'no_account' => $this->input->post('no_account'),
          'keterangan_aktivasi_karyawan' => $this->input->post('keterangan_aktivasi_karyawan'),
          'nomor_po_nama_driver_yg_diganti' => $this->input->post('nomor_po_nama_driver_yg_diganti'),
          'tanggal_po' => $this->input->post('tanggal_po'),
          'tgl_inaktif_kerja' => $this->input->post('tgl_inaktif_kerja'),
          'lead_time' => $this->input->post('lead_time'),
          'jobs' => $this->input->post('jobs'),
          'operation_poin' => $this->input->post('operation_poin'),
          'customer' => $this->input->post('customer'),
          'klasifikasi_skill' => $this->input->post('klasifikasi_skill'),
          'jenis_sim' => $this->input->post('jenis_sim'),
          'no_sim' => $this->input->post('no_sim'),
          'masa_berlaku_sim' => $this->input->post('masa_berlaku_sim'),
          'start_kontrak' => $this->input->post('start_kontrak'),
          'finish_kontrak' => $this->input->post('finish_kontrak'),
          'status_kontrak' => $this->input->post('status_kontrak'),
          'masa_kontrak_berakhir' => $this->input->post('masa_kontrak_berakhir'),
          'lama_kerja' => $this->input->post('lama_kerja'),
          'seragam' => $this->input->post('seragam'),
          'tanggal_po' => $this->input->post('tanggal_po'),
          'id_card' => $this->input->post('id_card'),
          'tanggal_pengiriman_seragam' => $this->input->post('tanggal_pengiriman_seragam'),
          'training_safety_driving' => $this->input->post('training_safety_driving'),
          'pelaksanaan_training_terakhir' => $this->input->post('pelaksanaan_training_terakhir'),
          'masa_training' => $this->input->post('masa_training'),
          'status_training' => $this->input->post('status_training'),
          'nilai_training_safety_driving_pree_test' => $this->input->post('nilai_training_safety_driving_pree_test'),
          'nilai_training_safety_driverng_post_test' => $this->input->post('nilai_training_safety_driverng_post_test'),
          'status_vaksin_pertama' => $this->input->post('status_vaksin_pertama'),
          'mengapa_belum_vaksi_pertama' => $this->input->post('mengapa_belum_vaksi_pertama'),
          'tanggal_vaksin_pertama' => $this->input->post('tanggal_vaksin_pertama'),
          'lokasi_vaksin_pertama' => $this->input->post('lokasi_vaksin_pertama'),
          'status_vaksin_kedua' => $this->input->post('status_vaksin_kedua'),
          'mengapa_belum_vaksi_kedua' => $this->input->post('mengapa_belum_vaksi_kedua'),
          'tanggal_vaksin_kedua' => $this->input->post('tanggal_vaksin_kedua'),
          'lokasi_vaksin_kedua' => $this->input->post('lokasi_vaksin_kedua'),
          'status_vaksin_booster' => $this->input->post('status_vaksin_booster'),
          'mengapa_belum_vaksin_booster' => $this->input->post('mengapa_belum_vaksin_booster'),
          'tanggal_vaksin_booster' => $this->input->post('tanggal_vaksin_booster'),
          'lokasi_vaksin_booster' => $this->input->post('lokasi_vaksin_booster'),
          'tgl_training_online_part_1' => $this->input->post('tgl_training_online_part_1'),
          'status_training_online_part_1' => $this->input->post('status_training_online_part_1'),
          'point_part_1' => $this->input->post('point_part_1'),
          'tgl_training_online_part_2' => $this->input->post('tgl_training_online_part_2'),
          'status_training_online_part_2' => $this->input->post('status_training_online_part_2'),
          'point_part_2' => $this->input->post('point_part_2'),
          'tgl_training_attitude_n_knowledge' => $this->input->post('tgl_training_attitude_n_knowledge'),
          'status_training_attitude_n_knowledge' => $this->input->post('status_training_attitude_n_knowledge'),
          'point_attitude_n_knowledge' => $this->input->post('point_attitude_n_knowledge'),
          'nomor_bpjs_kesehatan' => $this->input->post('nomor_bpjs_kesehatan'),
          'keterangan_proses_bpjs_kesehatan' => $this->input->post('keterangan_proses_bpjs_kesehatan'),
          'keterangan_status_bpjs_kesehatan' => $this->input->post('keterangan_status_bpjs_kesehatan'),
          'nomor_jamsostek' => $this->input->post('nomor_jamsostek'),
          'keterangan_jamsostek' => $this->input->post('keterangan_jamsostek'),
          'reward' => $this->input->post('reward'),
          'tglblnthn_pemberian_reward' => $this->input->post('tglblnthn_pemberian_reward'),
          'accident_report' => $this->input->post('accident_report'),
          'tglblnthn_accident' => $this->input->post('tglblnthn_accident'),
          'kasus' => $this->input->post('kasus'),
          'tglblnthn_kasus' => $this->input->post('tglblnthn_kasus'),
          'sp_i' => $this->input->post('sp_i'),
          'sp_ii' => $this->input->post('sp_ii'),
          'sp_iii' => $this->input->post('sp_iii'),
          'keterangan_out_dan_mutasi' => $this->input->post('keterangan_out_dan_mutasi'),
          'masa_berlaku_sim_2' => $this->input->post('masa_berlaku_sim_2'),
          'masa_training_2' => $this->input->post('masa_training_2'),
          'keterangan_training_2' => $this->input->post('keterangan_training_2'),
          'jadwal_training_2' => $this->input->post('jadwal_training_2'),
          'driver_leader' => $this->input->post('driver_leader'),
          'jabatan' => $this->input->post('jabatan')
        ];
        $this->db->insert('master_karyawan', $data);
    }

    public function ceknikpegawai()
    {
        $query = $this->db->query("SELECT MAX(nik) as nik_pegawai from master_karyawan");
        $hasil = $query->row();
        return $hasil->nik_pegawai;
    }

    public function crudpgw($typesend)
    {
        if ($typesend == 'addpgw') {

            $kd_pegawai = random_string('numeric', 4);

            if (empty(htmlspecialchars($this->input->post('area_pegawai')))) {
                $rowarea = 'Tidak Ada';
            } else {
                $rowarea = $this->input->post('area_pegawai');
            }

            $upload_image = $_FILES['foto_pegawai']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size']      = '2048';
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = $this->config->item('SAVE_FOLDER_PROFILE');

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto_pegawai')) {
                    $gbr = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $this->config->item('SAVE_FOLDER_PROFILE') . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 300;
                    $config['height'] = 300;
                    $config['new_image'] = $this->config->item('SAVE_FOLDER_PROFILE') . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    return "default.png";
                }
            } else {
                $this->db->set('image', 'default.png');
            }

            if (!empty(htmlspecialchars($this->input->post('barcode_pegawai')))) {
                $this->load->library('ciqrcode'); //pemanggilan library QR CODE

                $config['cacheable']    = true; //boolean, the default is true
                $config['cachedir']     = $this->config->item('MISC_SAVE_FOLDER') . 'sys/cache/'; //string, the default is application/cache/
                $config['errorlog']     = $this->config->item('MISC_SAVE_FOLDER') . 'sys/log/'; //string, the default is application/logs/
                $config['imagedir']     = $this->config->item('SAVE_FOLDER_QRCODE'); //direktori penyimpanan qr code
                $config['quality']      = true; //boolean, the default is true
                $config['size']         = '1024'; //interger, the default is 1024
                $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
                $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
                $this->ciqrcode->initialize($config);

                $image_name = 'qr_code_' . $kd_pegawai . '.png'; //buat name dari qr code sesuai dengan nim

                $params['data'] = $kd_pegawai; //data yang akan di jadikan QR CODE
                $params['level'] = 'H'; //H=High
                $params['size'] = 10;
                $params['savename'] = $config['imagedir'] . $image_name; //simpan image QR CODE
                $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
                $senddata = [
                    'qr_code_image' => $image_name,
                    'qr_code_use' => 1
                ];
                $this->db->set($senddata);
            } elseif (empty(htmlspecialchars($this->input->post('barcode_pegawai')))) {
                $senddata = [
                    'qr_code_image' => 'no-qrcode.png',
                    'qr_code_use' => 0
                ];
                $this->db->set($senddata);
            }
            $sendsave = [
                'nama_lengkap' => htmlspecialchars($this->input->post('nama_pegawai')),
                'username' => htmlspecialchars($this->input->post('username_pegawai')),
                'password' => password_hash($this->input->post('password_pegawai'), PASSWORD_DEFAULT),
                'kode_pegawai' => $kd_pegawai,
                'jabatan' => htmlspecialchars($this->input->post('jabatan_pegawai')),
                'instansi' => $this->appsetting['nama_instansi'],
                'area' => $rowarea,
                'district' => htmlspecialchars($this->input->post('district_pegawai')),
                'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir_pegawai')),
                'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir_pegawai')),
                'jenis_kelamin' => htmlspecialchars($this->input->post('jenis_kelamin_pegawai')),
                'bagian_shift' => htmlspecialchars($this->input->post('shift_pegawai')),
                'is_active' => htmlspecialchars($this->input->post('verifikasi_pegawai')),
                'role_id' => htmlspecialchars($this->input->post('role_pegawai')),
                'date_created' => time()
            ];
            $this->db->insert('user', $sendsave);
        } elseif ($typesend == 'delpgw') {
            $query = $this->db->get_where('user', ['id_pegawai' => htmlspecialchars($this->input->post('pgw_id', true))])->row_array();

            $old_image = $query['image'];
            if ($old_image != 'default-profile.png') {
                unlink($this->config->item('SAVE_FOLDER_PROFILE') . $old_image);
            }
            $old_qrcode = $query['qr_code_image'];
            if ($old_qrcode != 'no-qrcode.png') {
                unlink($this->config->item('SAVE_FOLDER_QRCODE') . $old_qrcode);
            }
            $this->db->delete('user', ['id_pegawai' => htmlspecialchars($this->input->post('pgw_id', true))]);
        } elseif ($typesend == 'actpgw') {
            $this->db->set('is_active', 1);
            $this->db->where('id_pegawai', htmlspecialchars($this->input->post('pgw_id', true)));
            $this->db->update('user');
        } elseif ($typesend == 'edtpgwalt') {
            $query_user = $this->db->get_where('user', ['id_pegawai' => htmlspecialchars($this->input->post('id_pegawai_edit', true))])->row_array();
            $kd_pegawai = $query_user['kode_pegawai'];
            $queryimage = $query_user;
            if (empty(htmlspecialchars($this->input->post('area_pegawai_edit')))) {
                $rowarea = 'Tidak Ada';
            } else {
                $rowarea = $this->input->post('area_pegawai_edit');
            }

            if (!empty(htmlspecialchars($this->input->post('password_pegawai_edit')))) {
                $this->db->set('password', password_hash($this->input->post('password_pegawai_edit'), PASSWORD_DEFAULT));
            }

            if (empty($this->input->post('barcode_pegawai_edit'))) {
                $old_qrcode = $queryimage['qr_code_image'];
                if ($old_qrcode != 'no-qrcode.png') {
                    unlink($this->config->item('SAVE_FOLDER_QRCODE') . $old_qrcode);
                }
                $senddata = [
                    'qr_code_image' => 'no-qrcode.png',
                    'qr_code_use' => 0
                ];
                $this->db->set($senddata);
            } elseif (!empty($this->input->post('barcode_pegawai_edit')) && $queryimage['qr_code_image'] == 'no-qrcode.png') {
                $this->load->library('ciqrcode'); //pemanggilan library QR CODE

                $config['cacheable']    = true; //boolean, the default is true
                $config['cachedir']     = $this->config->item('MISC_SAVE_FOLDER') . 'sys/cache/'; //string, the default is application/cache/
                $config['errorlog']     = $this->config->item('MISC_SAVE_FOLDER') . 'sys/log/'; //string, the default is application/logs/
                $config['imagedir']     = $this->config->item('SAVE_FOLDER_QRCODE'); //direktori penyimpanan qr code
                $config['quality']      = true; //boolean, the default is true
                $config['size']         = '1024'; //interger, the default is 1024
                $config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
                $config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
                $this->ciqrcode->initialize($config);

                $image_name = 'qr_code_' . $kd_pegawai . '.png'; //buat name dari qr code sesuai dengan nim

                $params['data'] = $kd_pegawai; //data yang akan di jadikan QR CODE
                $params['level'] = 'H'; //H=High
                $params['size'] = 10;
                $params['savename'] = $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
                $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
                $senddata = [
                    'qr_code_image' => $image_name,
                    'qr_code_use' => 1
                ];
                $this->db->set($senddata);
            }

            $upload_image = $_FILES['foto_pegawai_edit']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size']      = '2048';
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = $this->config->item('SAVE_FOLDER_PROFILE');

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto_pegawai_edit')) {
                    $gbr = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $this->config->item('SAVE_FOLDER_PROFILE') . $gbr['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['width'] = 300;
                    $config['height'] = 300;
                    $config['new_image'] = $this->config->item('SAVE_FOLDER_PROFILE') . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $old_image = $queryimage['image'];
                    if ($old_image != 'default.png') {
                        unlink($this->config->item('SAVE_FOLDER_PROFILE') . $old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    return "default.png";
                }
            }

            $sendsave = [
                'nama_lengkap' => htmlspecialchars($this->input->post('nama_pegawai_edit')),
                'username' => htmlspecialchars($this->input->post('username_pegawai_edit')),
                'jabatan' => htmlspecialchars($this->input->post('jabatan_pegawai_edit')),
                'instansi' => $this->appsetting['nama_instansi'],
                'area' => $rowarea,
                'district' => htmlspecialchars($this->input->post('district_pegawai_edit')),
                'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir_pegawai_edit')),
                'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir_pegawai_edit')),
                'jenis_kelamin' => htmlspecialchars($this->input->post('jenis_kelamin_pegawai_edit')),
                'bagian_shift' => htmlspecialchars($this->input->post('shift_pegawai_edit')),
                'is_active' => htmlspecialchars($this->input->post('verifikasi_pegawai_edit')),
                'role_id' => htmlspecialchars($this->input->post('role_pegawai_edit')),
            ];
            $this->db->set($sendsave);
            $this->db->where('id_pegawai', htmlspecialchars($this->input->post('id_pegawai_edit', true)));
            $this->db->update('user');
        }
    }


    public function simpanKecelakaan($data)
    {
        return $this->db->insert('kecelakaan', $data);
    }


    public function detailKecelakaan()
    {
        $id = $_REQUEST['id'];
        $foto1 = '';
        $foto2 = '';
        $foto3 = '';
        $data = $this->db->query("SELECT * from kecelakaan where id='$id'")->row();
        if ($data->foto1) {
            $foto1 = "<img style='width: 320px;height: 300px;' src='" .  base_url('storage/kecelakaan/') . $data->foto1 . "'>";
        }
        if ($data->foto2) {
            $foto2 = "<img style='width: 320px;height: 300px;' src='" .  base_url('storage/kecelakaan/') . $data->foto2 . "'>";
        }
        if ($data->foto3) {
            $foto3 = "<img style='width: 320px;height: 300px;' src='" .  base_url('storage/kecelakaan/') . $data->foto3 . "'>";
        }
        ($data->kategori == 'fatality') ? $fatality = " bgcolor='orange' " : $fatality = " bgcolor='white' ";
        ($data->kategori == 'rankA') ? $rankA = " bgcolor='orange' " : $rankA = " bgcolor='white' ";
        ($data->kategori == 'rankB') ? $rankB = " bgcolor='orange' " : $rankB = " bgcolor='white' ";
        ($data->kategori == 'rankC') ? $rankC = " bgcolor='orange' " : $rankC = " bgcolor='white' ";
        $show = '';
        $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td width='5%'><img style='width: 70px;height: auto;' src='" . base_url('storage/kecelakaan/logo.png') . "'></td>
                        <td width='95%' align='center' style='font-size: 18px;font-weight: bold;'>FORM INVESTIGASI ACCIDENT</td>
                    </tr>
                    <tr>
                        <td colspan='2' align='center' style='font-weight:bold; font-size:13px;'>DATA KEJADIAN</td>
                    </tr>
                </table>";
        $show .= "<table width='100%' border='0' style='font-size:11px; border-collapse:collapse;' >
                    <tr>
                        <td style='border-left:solid 1px black;'>No Polisi</td>
                        <td>: " . $data->no_polisi . "</td>
                        <td style='border-left:solid 1px black;'>Nama Driver</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $data->driver . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>Lokasi Kejadian</td>
                        <td>: " . $data->lokasi . "</td>
                        <td style='border-left:solid 1px black;'>Nik Driver</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $data->nik_driver . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>Tanggal Kejadian</td>
                        <td>: " . date_indonesia($data->tanggal) . "</td>
                        <td style='border-left:solid 1px black;'>Usia Driver</td>
                        <td style='border-right:solid 1px black;' colspan='3'>: " . $data->usia_driver . "</td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>Jam Kejadian</td>
                        <td>: " . time_indo($data->tanggal) . "</td>
                        <td style='border-left:solid 1px black;'> </td>
                        <td style='border-right:solid 1px black;' colspan='3'></td>
                    </tr>
                    <tr>
                        <td width='20%' style='border-top:none;border-left:solid 1px black;'>Customer</td>
                        <td width='30%' style='border-top:none;'>: " . $data->customer . "</td>
                        <td width='15%' style='border-top:none;border-left:solid 1px black;'>Kategori</td>
                        <td width='5%' style='border:solid 1px black;' $fatality> </td>
                        <td width='5%' style='border-top:none;'>Fatality </td>
                        <td width='25%' style='border-top:none;border-right:solid 1px black;' align='center'>Tanda Tangan Driver</td>
                    </tr> 
                    <tr>
                        <td style='border-left:solid 1px black;'>Faktor </td>
                        <td>: " . $data->faktor . "</td>
                        <td style='border-left:solid 1px black;'> </td>
                        <td style='border:solid 1px black;' $rankA> </td>
                        <td>Rank A </td>
                        <td style='border-right:solid 1px black;'> </td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>Muatan </td>
                        <td>: " . $data->muatan . "</td>
                        <td style='border-left:solid 1px black;'> </td>
                        <td style='border:solid 1px black;' $rankB> </td>
                        <td>Rank B  </td>
                        <td style='border-right:solid 1px black;'> </td>
                    </tr>
                    <tr>
                        <td style='border-left:solid 1px black;'>Rute </td>
                        <td>: " . $data->rute . "</td>
                        <td style='border-left:solid 1px black;'> </td>
                        <td style='border:solid 1px black;' $rankC> </td>
                        <td>Rank C </td>
                        <td style='border-right:solid 1px black;' align='center'>" . $data->driver . "</td>
                    </tr>
                </table>";
        $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' > 
                    <tr>
                        <td style='font-weight:bold;'>A. Kronologis Kejadian</td>
                    </tr> 
                    <tr>
                        <td style='height:44px;' valign='top'>" . $data->kronologis . "</td>
                    </tr>
                    <tr>
                        <td style='font-weight:bold;'>B. Tindakan Penanganan Kejadian</td>
                    </tr> 
                    <tr>
                        <td style='height:44px;' valign='top'>" . $data->tindakan . "</td>
                    </tr>
                    <tr>
                        <td style='font-weight:bold;'>C. Biaya Penanganan Kejadian</td>
                    </tr> 
                    <tr>
                        <td style='height:44px;' valign='top'>
                            Rp. " . number_format($data->biaya_penanganan) . " <br>&nbsp;<br>
                            <u>KORBAN LAIN</u> <br>
                            " . $data->korban_lain . "
                        </td>
                    </tr> 
                    <tr>
                        <td style='font-weight:bold;'>D. Analisis Kejadian</td>
                    </tr> 
                    <tr>
                        <td style='height:44px;' valign='top'>" . $data->analisis . "</td>
                    </tr>
                </table>";


        $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' > 
                    <tr>
                        <td style='font-weight:bold;' colspan='3'>E. Sketsa Ilustrasi Kejadian</td>
                    </tr> 
                    <tr>
                        <td width='33%'>$foto1</td>
                        <td width='33%'>$foto2</td>
                        <td width='33%'>$foto3</td>
                    </tr>
                </table>";
        $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' > 
                    <tr>
                        <td style='font-weight:bold;'>F. Kesimpulan Hasil Investigasi</td>
                    </tr> 
                    <tr>
                        <td style='height:44px;' valign='top'>" . $data->kesimpulan . "</td>
                    </tr>
                    <tr>
                        <td style='font-weight:bold;'>G. Tindakan Antisipasi Kejadian</td>
                    </tr> 
                    <tr>
                        <td style='height:44px;' valign='top'>" . $data->antisipasi . "</td>
                    </tr>
                </table>";
        $show .= "<table width='100%' border='1' style='font-size:11px; border-collapse:collapse;' > 
                    <tr>
                    <td align='center'>Dibuat Oleh,</td>
                    <td align='center' colspan='2'>Disetujui Oleh,</td>
                    </tr>
                    <tr>
                    <td align='center' width='33%' style='height:100px;'>&nbsp;</td>
                    <td align='center' width='33%' style='height:100px;'></td>
                    <td align='center' width='33%' style='height:100px;'></td>
                    </tr>
                    <tr>
                    <td align='center' style='border-top:hidden;'>(Operating Point Coordinator)</td>
                    <td align='center' style='border-top:hidden;'>(District Head)</td>
                    <td align='center' style='border-top:hidden;'>(Operating Function Head)</td>
                    </tr>
                </table>";

        return $show;
    }

    public function detailKecelakaanold()
    {
        $id = $_REQUEST['id'];
        $foto1 = '';
        $foto2 = '';
        $foto3 = '';
        $data = $this->db->query("SELECT * from kecelakaan where id='$id'")->row();
        if ($data->foto1) {
            $foto1 = "<img style='width: 320px;height: auto;' src='" .  base_url('storage/kecelakaan/') . $data->foto1 . "'>";
        }
        if ($data->foto2) {
            $foto2 = "<img style='width: 320px;height: auto;' src='" .  base_url('storage/kecelakaan/') . $data->foto2 . "'>";
        }
        if ($data->foto3) {
            $foto3 = "<img style='width: 320px;height: auto;' src='" .  base_url('storage/kecelakaan/') . $data->foto3 . "'>";
        }
        $show = '';
        $show .= "<table width='100%' border='1' style='font-size:12px; border-collapse:collapse;' >
          <tr>
            <td colspan='2'>
              <table border='0' width='100%'>
                <tr>
                  <td width='5%'><img style='width: 70px;height: auto;' src='" . base_url('storage/kecelakaan/logo.png') . "'></td>
                  <td width='95%' align='center' style='font-size: large;font-weight: bold;'>FORM INVESTIGASI ACCIDENT</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan='2' style='font-weight:bold; font-size:13px;' align='center'>DATA KEJADIAN</td>
          </tr>
          <tr>
            <td width='50%' >
              <table width='100%' style='font-size:12px;'>
                <tr>
                  <td width='30%'>Nomor Polisi Unit <span style='float:right;'>:</span></td>
                  <td width='70%'> " . $data->no_polisi . "</td>
                </tr>
                <tr>
                  <td>Lokasi Kejadian <span style='float:right;'>:</span></td>
                  <td>" . $data->lokasi . "</td>
                </tr>
                <tr>
                  <td>Tanggal Kejadian <span style='float:right;'>:</span></td>
                  <td>" . $data->tanggal . "</td>
                </tr>
                <tr>
                  <td>Jam Kejadian <span style='float:right;'>:</span></td>
                  <td>" . $data->tanggal . "</td>
                </tr>
                <tr>
                  <td>Customer <span style='float:right;'>:</span></td>
                  <td>" . $data->customer . "</td>
                </tr>
                <tr>
                  <td>Faktor <span style='float:right;'>:</span></td>
                  <td>" . $data->faktor . "</td>
                </tr>
                <tr>
                  <td>Muatan <span style='float:right;'>:</span></td>
                  <td>" . $data->muatan . "</td>
                </tr>
                <tr>
                  <td>Rute <span style='float:right;'>:</span></td>
                  <td>" . $data->rute . "</td>
                </tr>
              </table>
            </td>
      
            <td width='50%'>
              <table width='100%' border='0' style='font-size:12px;'>
                <tr>
                  <td>Nama Driver <span style='float:right;'>:</span></td>
                  <td colspan='2'>" . $data->driver . "</td>
                </tr>
                <tr>
                  <td>NIK Driver <span style='float:right;'>:</span></td>
                  <td colspan='2'>" . $data->nik_driver . "</td>
                </tr>
                <tr>
                  <td>Usia Driver <span style='float:right;'>:</span></td>
                  <td colspan='2'>" . $data->usia_driver . "</td>
                </tr>
                <tr>
                  <td width='30%'>Kategori <span style='float:right;'>:</span></td>
                  <td width='40%'>
                    <div class='row'>
                      <div style='border-style: solid;height: 20px;width: 50px;background-color: orange;margin: 0px 10px 0px 12px;'></div>
                      Fatality
                    </div>
                  </td>
                  <td width='30%' align='center'>Tanda Tangan Driver</td>
                </tr>
                <tr>
                  <td> </td>
                  <td>
                    <div class='row'>
                      <div style='border-style: solid;height: 20px;width: 50px;background-color: white;margin: 0px 10px 0px 12px;'></div>
                      Rank A
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td> </td>
                  <td>
                    <div class='row'>
                      <div style='border-style: solid;height: 20px;width: 50px;background-color: white;margin: 0px 10px 0px 12px;'></div>
                      Rank B
                    </div>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td> </td>
                  <td>
                    <div class='row'>
                      <div style='border-style: solid;height: 20px;width: 50px;background-color: white;margin: 0px 10px 0px 12px;'></div>
                      Rank C
                    </div>
                  </td>
                  <td align='center'><span>" . $data->driver . "</span></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan='2'>A. Kronologis Kejadian</td>
          </tr>
          <tr>
            <td colspan='2'>
              <p>" . $data->kronologis . "</p>
            </td>
          </tr>
      
          <tr>
            <td colspan='2'>B. Tindakan Penanganan Kejadian</td>
          </tr>
          <tr>
            <td colspan='2'>
              <p>" . $data->tindakan . "</p>
            </td>
          </tr>
      
          <tr>
            <td colspan='2'>C. Biaya Penanganan Kejadian</td>
          </tr>
          <tr>
            <td colspan='2'>
              <p>Rp " . $data->biaya_penanganan . "</p> <br>
              <u>KORBAN LAIN </u>
              <p>" . $data->korban_lain . "</p>
            </td>
          </tr>
      
          <tr>
            <td colspan='2'>D. Analisis Kejadian</td>
          </tr>
          <tr>
            <td colspan='2'>
              <p>" . $data->analisis . "</p>
            </td>
          </tr>
      
          <tr>
            <td colspan='2'>E. Sketsa Ilustrasi Kejadian</td>
          </tr>
          <tr>
            <td colspan='2'>
              <table width='100%'>
                <tr>
                  <td width='33%' align='center'>foto1</td>
                  <td width='33%' align='center'>foto2</td>
                  <td width='33%' align='center'>foto3</td>
                </tr>
              </table>
            </td>
          </tr>
      
          <tr>
            <td colspan='2'>F. Kesimpulan Hasil Investigasi</td>
          </tr>
          <tr>
            <td colspan='2'>
              <p>" . $data->kesimpulan . "</p>
            </td>
          </tr>
      
          <tr>
            <td colspan='2'>G. Tindakan Antisipasi Kejadian</td>
          </tr>
          <tr>
            <td colspan='2'>
              <p>" . $data->antisipasi . "</p>
            </td>
          </tr>
          <tr>
            <td colspan='2'>
              <table width='100%' border='1' style='font-size:12px; border-collapse:collapse;'>
                <tr>
                  <td align='center'>Dibuat Oleh,</td>
                  <td align='center' colspan='2'>Disetujui Oleh,</td>
                </tr>
                <tr>
                  <td align='center' width='33%' style='height:100px;'>&nbsp;</td>
                  <td align='center' width='33%' style='height:100px;'></td>
                  <td align='center' width='33%' style='height:100px;'></td>
                </tr>
                <tr>
                  <td align='center' style='border-top:hidden;'>(Operating Point Coordinator)</td>
                  <td align='center' style='border-top:hidden;'>(District Head)</td>
                  <td align='center' style='border-top:hidden;'>(Operating Function Head)</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>";
        return $show;
    }

    public function updateaktifasikaryawan()
    {
        $data = [
          'keterangan_aktivasi_karyawan' => $this->input->post('keterangan_aktivasi_karyawan'),
          'nomor_po_nama_driver_yg_diganti' => $this->input->post('nomor_po_nama_driver_yg_diganti'),
          'tanggal_po' => $this->input->post('tanggal_po'),
          'tgl_inaktif_kerja' => $this->input->post('tgl_inaktif_kerja'),
          'lead_time' => $this->input->post('lead_time'),
          'jobs' => $this->input->post('jobs'),
          'operation_poin' => $this->input->post('operation_poin'),
          'customer' => $this->input->post('customer')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function updatekontrakkerja()
    {
        $data = [
            'start_kontrak' => $this->input->post('start_kontrak'),
            'finish_kontrak' => $this->input->post('finish_kontrak'),
            'status_kontrak' => $this->input->post('status_kontrak'),
            'masa_kontrak_berakhir' => $this->input->post('masa_kontrak_berakhir'),
            'lama_kerja' => $this->input->post('lama_kerja'),
            'seragam' => $this->input->post('seragam'),
            'tanggal_pengiriman_seragam' => $this->input->post('tanggal_pengiriman_seragam'),
            'id_card' => $this->input->post('id_card'),
            'tanggal_pengiriman_id_card' => $this->input->post('tanggal_pengiriman_id_card')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function updateklasifikasiskill()
    {
        $data = [
          'klasifikasi_skill' => $this->input->post('klasifikasi_skill'),
          'jenis_sim' => $this->input->post('jenis_sim'),
          'no_sim' => $this->input->post('no_sim'),
          'masa_berlaku_sim' => $this->input->post('masa_berlaku_sim')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function updatetrainingsafetydriving()
    {
        $data = [
          'training_safety_driving' => $this->input->post('training_safety_driving'),
          'pelaksanaan_training_terakhir' => $this->input->post('pelaksanaan_training_terakhir'),
          'masa_training' => $this->input->post('masa_training'),
          'status_training' => $this->input->post('status_training'),
          'nilai_training_safety_driving_pree_test' => $this->input->post('nilai_training_safety_driving_pree_test'),
          'nilai_training_safety_driverng_post_test' => $this->input->post('nilai_training_safety_driverng_post_test')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function updateketeranganvaksinpegawai()
    {
        $data = [
          'status_vaksin_pertama' => $this->input->post('status_vaksin_pertama'),
          'mengapa_belum_vaksi_pertama' => $this->input->post('mengapa_belum_vaksi_pertama'),
          'tanggal_vaksin_pertama' => $this->input->post('tanggal_vaksin_pertama'),
          'lokasi_vaksin_pertama' => $this->input->post('lokasi_vaksin_pertama'),
          'status_vaksin_kedua' => $this->input->post('status_vaksin_kedua'),
          'mengapa_belum_vaksi_kedua' => $this->input->post('mengapa_belum_vaksi_kedua'),
          'tanggal_vaksin_kedua' => $this->input->post('tanggal_vaksin_kedua'),
          'lokasi_vaksin_kedua' => $this->input->post('lokasi_vaksin_kedua'),
          'status_vaksin_booster' => $this->input->post('status_vaksin_booster'),
          'mengapa_belum_vaksin_booster' => $this->input->post('mengapa_belum_vaksin_booster'),
          'tanggal_vaksin_booster' => $this->input->post('tanggal_vaksin_booster'),
          'lokasi_vaksin_booster' => $this->input->post('lokasi_vaksin_booster')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function updatetrainingpegawai()
    {
        $data = [
          'tgl_training_online_part_1' => $this->input->post('tgl_training_online_part_1'),
            'status_training_online_part_1' => $this->input->post('status_training_online_part_1'),
            'point_part_1' => $this->input->post('point_part_1'),
            'tgl_training_online_part_2' => $this->input->post('tgl_training_online_part_2'),
            'status_training_online_part_2' => $this->input->post('status_training_online_part_2'),
            'point_part_2' => $this->input->post('point_part_2'),
            'tgl_training_attitude_n_knowledge' => $this->input->post('tgl_training_attitude_n_knowledge'),
            'status_training_attitude_n_knowledge' => $this->input->post('status_training_attitude_n_knowledge'),
            'point_attitude_n_knowledge' => $this->input->post('point_attitude_n_knowledge')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    }

    public function updateketeranganbpjs()
    {
        $data = [
            'nomor_bpjs_kesehatan' => $this->input->post('nomor_bpjs_kesehatan'),
            'keterangan_proses_bpjs_kesehatan' => $this->input->post('keterangan_proses_bpjs_kesehatan'),
            'keterangan_status_bpjs_kesehatan' => $this->input->post('keterangan_status_bpjs_kesehatan'),
            'nomor_jamsostek' => $this->input->post('nomor_jamsostek'),
            'keterangan_jamsostek' => $this->input->post('keterangan_jamsostek'),
            'reward' => $this->input->post('reward'),
            'tglblnthn_pemberian_reward' => $this->input->post('tglblnthn_pemberian_reward')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    } 
    
    public function updateaccident()
    {
        $data = [
          'accident_report' => $this->input->post('accident_report'),
          'tglblnthn_accident' => $this->input->post('tglblnthn_accident'),
          'kasus' => $this->input->post('kasus'),
          'tglblnthn_kasus' => $this->input->post('tglblnthn_kasus'),
          'sp_i' => $this->input->post('sp_i'),
          'sp_ii' => $this->input->post('sp_ii'),
          'sp_iii' => $this->input->post('sp_iii'),
          'keterangan_out_dan_mutasi' => $this->input->post('keterangan_out_dan_mutasi'),
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    } 

    public function updatetraining()
    {
        $data = [
          'masa_berlaku_sim_2' => $this->input->post('masa_berlaku_sim_2'),
          'masa_training_2' => $this->input->post('masa_training_2'),
          'keterangan_training_2' => $this->input->post('keterangan_training_2'),
          'jadwal_training_2' => $this->input->post('jadwal_training_2'),
          'driver_leader' => $this->input->post('driver_leader'),
          'jabatan' => $this->input->post('jabatan')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('master_karyawan', $data);
    } 

  

}
