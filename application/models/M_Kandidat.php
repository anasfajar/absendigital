<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Kandidat extends CI_Model
{

    var $order = ['kandidat.id' => 'DESC'];
    var $column_order = array(null, 'kode_kandidat', 'nama_lengkap', 'tempat_lahir', 'tanggal_lahir', 'status_dokumen', 'alamat_domisili', 'ktp', 'telp', 'nilai_test_hr', 'nilai_test_pengetahuan', 'nilai_test_skill', 'status_kandidat');
    var $column_search = ['kode_kandidat', 'nama_lengkap', 'ktp', 'alamat_domisili', 'status_kandidat'];


    public function __construct()
    {
        parent::__construct();
    }



    function countKandidatDataList()
    {
        $this->queryKandidatList();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function getById($id)
    {
        $this->db->select('*');
        $this->db->from('kandidat');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getDetailById($id)
    {
        $this->db->select('*');
        $this->db->from('kandidat_detail');
        $this->db->where('kandidat_id', $id);
        return $this->db->get()->row();
    }

    function Dokumenkandidat($id)
    {
        $this->db->select('*');
        $this->db->from('dokumen_kandidat');
        $this->db->join('ms_dokumen', 'dokumen_kandidat.dokumen_id = ms_dokumen.id');
        $this->db->where(['kandidat_id' => $id, 'dokumen_id !=' => 1]);
        return $this->db->get()->result_array();
    }

    public function getviewkandidat($id)
    {
        //return $this->db->get_where('kandidat', ['id' => $id])->row_array();
        $this->db->select('*');
        $this->db->from('kandidat');
        $this->db->join('kandidat_detail', 'kandidat.id = kandidat_detail.kandidat_id');
        $this->db->join('m_jobs', 'kandidat.jobs = m_jobs.id');
        $this->db->where(['kandidat.id' => $id]);
        return $this->db->get()->row_array();
    }

    public function queryKandidatList()
    {
        // var_dump([$this->session->userdata('role_id'),$this->session->userdata('area')]);die();
        $this->db->select('kandidat.id, kode_kandidat,kandidat.created_at, nama_lengkap, tempat_lahir, tanggal_lahir, status_dokumen, nilai_test_hr, nilai_test_pengetahuan, nilai_test_skill,  alamat_domisili, ktp, telp, status_kandidat ');
        $this->db->from('kandidat');
        if($this->session->userdata('role_id') == 4){
            $area = $this->db->query("SELECT kode_op from master_driver_leader where driver_leader_id = ".$this->session->userdata('id')." limit 1")->row();
            if($area){
                $this->db->where('area',$area->kode_op);
            }
        }
        $this->db->join('kandidat_detail', 'kandidat.id = kandidat_detail.kandidat_id', 'left');

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    public function getKandidatDataTables()
    {
        $this->queryKandidatList();

        if (isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function saveNilaiInterview($kandidat_id, $field_name,  $nilai)
    {
        $this->db->set($field_name, $nilai);
        $this->db->where('id', $kandidat_id);
        $this->db->update('kandidat');
    }

    function District()
    {
        $this->db->order_by('nama_district', 'ASC');
        return $this->db->from('master_district')
            ->get()
            ->result();
    }

    function getArea()
    {
        $this->db->order_by('operation_poin', 'ASC');
        $this->db->group_by('kode_op');
        return $this->db->from('master_driver_leader')
          ->get()
          ->result();
    }

    public function simpanKandidat()
    {
        $data = array(
            "kode_kandidat" => random_string('numeric', 8),
            "nama_lengkap" => $this->input->post('nama_lengkap'),
            "ktp" => $this->input->post('ktp'),
            "telp" => $this->input->post('no_telp'),
            "email" => $this->input->post('email'),
            "pekerjaan_sebelumnya" => $this->input->post('pekerjaan_sebelumnya'),
            "perusahaan_sebelumnya" => $this->input->post('perusahaan_sebelumnya'),
            "lama_bekerja" => $this->input->post('lama_bekerja'),
            "pendidikan_terakhir" => $this->input->post('pendidikan_terakhir'),
            "jobs" => $this->input->post('jobs'),
            "sim" => $this->input->post('sim'),
            "area" => $this->input->post('district'),
            "surat_lamaran" => $this->upload->data('file_name'),
            "status_dokumen" => "0",
            "nilai_test_hr" => "0",
            "nilai_test_pengetahuan" => "0",
            "nilai_test_skill" => "0",
            "status_kandidat" => "0",
            // "created_at" => "0",
            "updated_at" => "0"
        );
        $this->db->insert('kandidat', $data);
    }

    public function loginProses()
    {
        $telp = $this->input->post('telp');
        $pass = $this->input->post('pass');
        return $this->db->query("SELECT id,kode_kandidat,nama_lengkap,status_kandidat from kandidat where kode_kandidat='$pass' and telp='$telp'")->row();
    }

    public function updateKandidat($id_kandidat = '')
    {
        $this->load->library('upload');
        $config['upload_path'] = '../public/storage/kandidat';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);
        $gambar = '';
        if (!empty($_FILES['filefoto']['name'])) {
            if ($this->upload->do_upload('filefoto')) {
                $gbr = $this->upload->data();
                $gambar =  $gbr['file_name'];
            }
        }

        // $id_kandidat = $this->session->userdata('kandidat_id');
        $head = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'ktp' => $this->input->post('ktp'),
            'telp' => $this->input->post('no_telp'),
            'email' => $this->input->post('email'),
            'pekerjaan_sebelumnya' => $this->input->post('pekerjaan_sebelumnya'),
            'perusahaan_sebelumnya' => $this->input->post('perusahaan_sebelumnya'),
            'lama_bekerja' => $this->input->post('lama_bekerja'),
            'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
            'jobs' => $this->input->post('jobs'),
            'sim' => $this->input->post('sim')
        );

        $this->db->where('id', $id_kandidat);
        $this->db->update('kandidat', $head);
        $anak1 = '';
        $anak2 = '';
        $anak3 = '';
        $tglanak1 = '';
        $tglanak2 = '';
        $tglanak3 = '';
        if (isset($_POST['anak_1'])) {
            $anak1 = $this->input->post('anak_1');
        }
        if (isset($_POST['anak_2'])) {
            $anak2 = $this->input->post('anak_2');
        }
        if (isset($_POST['anak_3'])) {
            $anak3 = $this->input->post('anak_3');
        }
        if (isset($_POST['tgl_lahir_anak1'])) {
            $tglanak1 = mysql_date($this->input->post('tgl_lahir_anak1'));
        }
        if (isset($_POST['tgl_lahir_anak2'])) {
            $tglanak2 = mysql_date($this->input->post('tgl_lahir_anak2'));
        }
        if (isset($_POST['tgl_lahir_anak3'])) {
            $tglanak3 = mysql_date($this->input->post('tgl_lahir_anak3'));
        }

        if (isset($_POST['pasangan'])) {
            $pasangan = $this->input->post('pasangan');
        }
        if (isset($_POST['tgl_lahir_pasangan'])) {
            $tglpasangan = mysql_date($this->input->post('tgl_lahir_pasangan'));
        }

        $detail = array(
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'alamat_ktp' => $this->input->post('alamat_ktp'),
            'alamat_domisili' => $this->input->post('alamat_domisili'),
            'npwp' => $this->input->post('npwp'),
            'nomor_sim' => $this->input->post('nomor_sim'),
            'masa_berlaku_sim' => $this->input->post('masa_berlaku_sim'),
            'nomor_kk' => $this->input->post('nomor_kk'),
            'status_kawin' => $this->input->post('status_kawin'),
            'jumlah_anak' => $this->input->post('jumlah_anak'),
            'anak1' => $anak1,
            'tgl_lahir_anak1' => $tglanak1,
            'anak2' => $anak2,
            'tgl_lahir_anak2' => $tglanak2,
            'anak3' => $anak3,
            'tgl_lahir_anak3' => $tglanak3,
            'nama_ibu' => $this->input->post('nama_ibu'),
            'pasangan' => $pasangan,
            'tgl_lahir_pasangan' => $tglpasangan,
            'agama' => $this->input->post('agama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'bank' => $this->input->post('bank'),
            'nama_akun_bank' => $this->input->post('nama_akun_bank'),
            'no_rekening' => $this->input->post('no_rekening'),
            'status_vaksin' => $this->input->post('status_vaksin'),
            'no_bpjs' => $this->input->post('no_bpjs'),
            'no_jamsostek' => $this->input->post('no_jamsostek'),
            'no_skbn' => $this->input->post('no_skbn'),
            'no_pbi' => $this->input->post('no_pbi'),
            'ukuran_baju' => $this->input->post('ukuran_baju'),
            'ukuran_celana' => $this->input->post('ukuran_celana'),
            'ukuran_sepatu' => $this->input->post('ukuran_sepatu'),
        );
        if ($gambar) {
            $detail['foto'] = $gambar;
        }

        $cek_detail = $this->db->query("SELECT kandidat_id FROM kandidat_detail WHERE kandidat_id='$id_kandidat'")->row('kandidat_id');

        if ($cek_detail) {
            $this->db->where('kandidat_id', $id_kandidat);
            $this->db->update('kandidat_detail', $detail);
        } else {
            $detail['kandidat_id'] = $id_kandidat;
            $this->db->insert('kandidat_detail', $detail);
        }
    }

    public function getAgama()
    {
        return $this->db->get('ms_agama')->result_array();
    }

    public function get_soal($tipe)
    {
        return $this->db->query("SELECT id,pertanyaan,pilihan from soal where soal_type_id='$tipe'")->result_array();
    }

    public function getKandidatLulusDataTables()
    {
        $this->queryKandidaLulustList();
        if (isset($_POST['length']) && $_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    function countKandidatLulusDataList()
    {
        $this->queryKandidaLulustList();
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function queryKandidaLulustList()
    {
        $this->db->select('kandidat.id, kode_kandidat,kandidat.created_at, nama_lengkap, tempat_lahir, tanggal_lahir, status_dokumen, nilai_test_hr, nilai_test_pengetahuan, nilai_test_skill,  alamat_domisili, ktp, telp, status_kandidat ');
        $this->db->from('kandidat');
        $this->db->join('kandidat_detail', 'kandidat.id = kandidat_detail.kandidat_id', 'inner');

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function exportKandidat($id = '')
    {
        $show = '';
        $pendidikan = '';
        $sim = '';
        $kawin = '';
        $data = $this->db->query("SELECT a.*,b.*,c.nama_jobs FROM kandidat a 
        INNER JOIN kandidat_detail b on a.id=b.kandidat_id 
        LEFT JOIN m_jobs c on a.jobs=c.id WHERE a.id='$id'")->row();
        $foto = base_url('storage/kandidat/') . $data->foto;
        if ($data->pendidikan_terakhir == '1') {
            $pendidikan = 'SMP';
        } elseif ($data->pendidikan_terakhir == '2') {
            $pendidikan = 'SMA';
        } elseif ($data->pendidikan_terakhir == '3') {
            $pendidikan = 'DIPLOMA';
        } elseif ($data->pendidikan_terakhir == '4') {
            $pendidikan = 'SARJANA';
        }

        if ($data->sim == '1') {
            $sim = "SIM A";
        }
        if ($data->sim == '2') {
            $sim = "SIM B1";
        }
        if ($data->sim == '3') {
            $sim = "SIM B2";
        }
        if ($data->sim == '4') {
            $sim = "SIM C";
        }
        if ($data->sim == '5') {
            $sim = "SIM A Umum";
        }
        if ($data->sim == '6') {
            $sim = "SIM B1 Umum";
        }
        if ($data->sim == '7') {
            $sim = "SIM B2 Umum";
        }

        if ($data->status_kawin == "L") {
            $kawin = "Lajang";
        }
        if ($data->status_kawin == "K0") {
            $kawin = "Kawin";
        }
        if ($data->status_kawin == "K1") {
            $kawin = "Kawin Anak 1";
        }
        if ($data->status_kawin == "K2") {
            $kawin = "Kawin Anak 2";
        }
        if ($data->status_kawin == "K3") {
            $kawin = "Kawin Anak 3";
        }
        if ($data->status_kawin == "C0") {
            $kawin = "Duda / Janda ";
        }
        if ($data->status_kawin == "C1") {
            $kawin = "Duda / Janda Anak 1";
        }
        if ($data->status_kawin == "C2") {
            $kawin = "Duda / Janda Anak 2";
        }
        if ($data->status_kawin == "C3") {
            $kawin = "Duda / Janda Anak 3";
        }

        $show .= "<table width='100%' border='1' style='border-collapse:collapse;'> 
                    <tr>
                    <td align='center' colspan='3' style='font-size:large;font-weight:bold;'>DATA KANDIDAT</td>
                    </tr>
                    <tr>
                        <td width='30%'>Nama Lengkap</td>
                        <td width='50%'>" . $data->nama_lengkap . "</td>
                        <td width='20%' rowspan='13'><img src='$foto' style='width: 300px; height: 400px;'></td>
                    </tr>
                    <tr>
                        <td>KTP</td>
                        <td>" . $data->ktp . "</td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td>" . $data->telp . "</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>" . $data->email . "</td>
                    </tr>
                    <tr>
                        <td>Pekerjaan Sebelumnya</td>
                        <td>" . $data->pekerjaan_sebelumnya . "</td>
                    </tr>
                    <tr>
                        <td>Perusahaan Sebelumnya</td>
                        <td>" . $data->perusahaan_sebelumnya . "</td>
                    </tr>
                    <tr>
                        <td>Lama Bekerja</td>
                        <td>" . $data->lama_bekerja . " Tahun</td>
                    </tr>
                    <tr>
                        <td>Pendidikan Terakhir</td>
                        <td>" . $pendidikan . "</td>
                    </tr>
                    <tr>
                        <td>Jobs</td>
                        <td>" . $data->nama_jobs . "</td>
                    </tr>
                    <tr>
                        <td>SIM</td>
                        <td>" . $sim . "</td>
                    </tr>
                    <tr>
                        <td>Tempat Lahir</td>
                        <td>" . $data->tempat_lahir . "</td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td>
                        <td>" . $data->tanggal_lahir . "</td>
                    </tr>
                    <tr>
                        <td>Alamat Sesuai KTP</td>
                        <td>" . $data->alamat_ktp . "</td>
                    </tr>
                    <tr>
                        <td>Alamat Domisili</td>
                        <td colspan='2'>" . $data->alamat_domisili . "</td>
                    </tr>
                    <tr>
                        <td>NPWP</td>
                        <td colspan='2'>" . $data->npwp . "</td>
                    </tr>
                    <tr>
                        <td>Nomor Sim</td>
                        <td colspan='2'>" . $data->nomor_sim . "</td>
                    </tr>
                    <tr>
                        <td>Masa Berlaku Sim</td>
                        <td colspan='2'>" . $data->masa_berlaku_sim . "</td>
                    </tr>
                    <tr>
                        <td>Nomor KK</td>
                        <td colspan='2'>" . $data->nomor_kk . "</td>
                    </tr>
                    <tr>
                        <td>Status Kawin</td>
                        <td colspan='2'>" . $kawin . "</td>
                    </tr>
                    <tr>
                        <td>Jumlah Anak</td>
                        <td colspan='2'>" . $data->jumlah_anak . "</td>
                    </tr>
                    <tr>
                        <td>Nama Ibu</td>
                        <td colspan='2'>" . $data->nama_ibu . "</td>
                    </tr>
                    <tr>
                        <td>Agama</td>
                        <td colspan='2'>" . $data->agama . "</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td colspan='2'>" . $data->jenis_kelamin . "</td>
                    </tr>
                    <tr>
                        <td>Bank</td>
                        <td colspan='2'>" . $data->bank . "</td>
                    </tr>
                    <tr>
                        <td>Nama Akun Bank</td>
                        <td colspan='2'>" . $data->nama_akun_bank . "</td>
                    </tr>
                    <tr>
                        <td>Nomor Rekening</td>
                        <td colspan='2'>" . $data->no_rekening . "</td>
                    </tr>
                    <tr>
                        <td>Vaksin</td>
                        <td colspan='2'>" . $data->status_vaksin . "</td>
                    </tr>
                    <tr>
                        <td>Nomor Bpjs</td>
                        <td colspan='2'>" . $data->no_bpjs . "</td>
                    </tr>
                    <tr>
                        <td>Nomor Jamsostek</td>
                        <td colspan='2'>" . $data->no_jamsostek . "</td>
                    </tr>
                    <tr>
                        <td>Nomor SKBN</td>
                        <td colspan='2'>" . $data->no_skbn . "</td>
                    </tr>
                    <tr>
                        <td>Nomor PBI</td>
                        <td colspan='2'>" . $data->no_pbi . "</td>
                    </tr>
                    <tr>
                        <td>Ukuran Baju</td>
                        <td colspan='2'>" . $data->ukuran_baju . "</td>
                    </tr>
                    <tr>
                        <td>Ukuran Celana</td>
                        <td colspan='2'>" . $data->ukuran_celana . "</td>
                    </tr>
                    <tr>
                        <td>Ukuran Sepatu</td>
                        <td colspan='2'>" . $data->ukuran_sepatu . "</td>
                    </tr> 
                </table>";
        return $show;
    }
}
