-- --------------------------------------------------------
-- Host:                         ruangcode.com
-- Server version:               10.5.15-MariaDB-cll-lve - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for u1675520_absensi_mbk
CREATE DATABASE IF NOT EXISTS `u1675520_absensi_mbk` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `u1675520_absensi_mbk`;

-- Dumping structure for table u1675520_absensi_mbk.db_absensi
CREATE TABLE IF NOT EXISTS `db_absensi` (
  `id_absen` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_absen` varchar(100) NOT NULL,
  `nama_pegawai` varchar(125) NOT NULL,
  `kode_pegawai` varchar(125) NOT NULL,
  `tgl_absen` varchar(125) NOT NULL,
  `jam_masuk` varchar(13) NOT NULL,
  `jam_pulang` varchar(13) NOT NULL,
  `status_pegawai` int(1) NOT NULL,
  `keterangan_absen` varchar(100) NOT NULL,
  `maps_absen` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_absen`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.db_absensi_test
CREATE TABLE IF NOT EXISTS `db_absensi_test` (
  `id_absen` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_absen` varchar(100) NOT NULL,
  `nama_pegawai` varchar(125) NOT NULL,
  `kode_pegawai` varchar(125) NOT NULL,
  `tgl_absen` varchar(125) NOT NULL,
  `jam_masuk` varchar(13) NOT NULL,
  `jam_pulang` varchar(13) NOT NULL,
  `status_pegawai` int(1) NOT NULL,
  `keterangan_absen` varchar(100) NOT NULL,
  `area_op_id` int(2) NOT NULL,
  `maps_absen_in` varchar(255) NOT NULL,
  `radius_poin_in` varchar(255) NOT NULL,
  `maps_absen_out` varchar(255) NOT NULL,
  `radius_poin_out` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_absen`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.db_rememberme
CREATE TABLE IF NOT EXISTS `db_rememberme` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pegawai` varchar(125) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_agent` varchar(35) NOT NULL,
  `agent_string` varchar(255) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `user_ip` varchar(35) NOT NULL,
  `cookie_hash` varchar(255) NOT NULL,
  `expired` int(128) NOT NULL,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.db_setting
CREATE TABLE IF NOT EXISTS `db_setting` (
  `status_setting` int(1) NOT NULL DEFAULT 0,
  `nama_instansi` varchar(255) NOT NULL,
  `jumbotron_lead_set` varchar(125) NOT NULL,
  `nama_app_absensi` varchar(50) NOT NULL DEFAULT 'Absensi Online',
  `logo_instansi` varchar(255) NOT NULL,
  `timezone` varchar(35) NOT NULL,
  `absen_mulai` varchar(13) NOT NULL,
  `absen_mulai_to` varchar(13) NOT NULL,
  `absen_pulang` varchar(13) NOT NULL,
  `maps_use` int(1) NOT NULL,
  PRIMARY KEY (`status_setting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.kandidat
CREATE TABLE IF NOT EXISTS `kandidat` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `kode_kandidat` varchar(50) DEFAULT NULL,
  `nama_lengkap` varchar(150) DEFAULT NULL,
  `ktp` varchar(16) DEFAULT NULL,
  `telp` varchar(16) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `pekerjaan_sebelumnya` varchar(50) DEFAULT NULL,
  `perusahaan_sebelumnya` varchar(50) DEFAULT NULL,
  `lama_bekerja` int(3) DEFAULT NULL,
  `pendidikan_terakhir` int(3) DEFAULT NULL,
  `jobs` int(3) DEFAULT NULL,
  `sim` int(3) DEFAULT NULL,
  `surat_lamaran` varchar(150) DEFAULT NULL COMMENT 'direktori penyimaban surat lamaran',
  `status_dokumen` varchar(150) DEFAULT NULL COMMENT '1:lengkap;0:tidaklengkap',
  `nilai_test_hr` int(20) DEFAULT NULL,
  `nilai_test_pengetahuan` int(20) DEFAULT NULL,
  `nilai_test_skill` int(20) DEFAULT NULL,
  `status_kandidat` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.kandidat_detail
CREATE TABLE IF NOT EXISTS `kandidat_detail` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `kandidat_id` int(3) DEFAULT NULL,
  `tempat_lahir` varchar(150) DEFAULT NULL,
  `tanggal_lahir` varchar(150) DEFAULT NULL,
  `alamat_ktp` text DEFAULT NULL,
  `alamat_domisili` text DEFAULT NULL,
  `npwp` varchar(20) DEFAULT NULL,
  `nomor_sim` varchar(50) DEFAULT NULL,
  `masa_berlaku_sim` varchar(50) DEFAULT NULL,
  `nomor_kk` varchar(50) DEFAULT NULL,
  `status_kawin` int(3) DEFAULT NULL,
  `jumlah_anak` int(3) DEFAULT NULL,
  `agama` int(3) DEFAULT NULL,
  `jenis_kelamin` int(3) DEFAULT NULL,
  `bank` int(3) DEFAULT NULL,
  `nama_akun_bank` varchar(150) DEFAULT NULL,
  `no_rekening` varchar(50) DEFAULT NULL,
  `status_vaksin` int(3) DEFAULT NULL COMMENT 'status vaksin ke : 1/2/3',
  `no_bpjs` int(3) DEFAULT NULL,
  `no_jamsostek` int(3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_kandidat_detail_kandidat` (`kandidat_id`),
  KEY `FK_kandidat_detail_ms_agama` (`agama`),
  CONSTRAINT `FK_kandidat_detail_kandidat` FOREIGN KEY (`kandidat_id`) REFERENCES `kandidat` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `FK_kandidat_detail_ms_agama` FOREIGN KEY (`agama`) REFERENCES `ms_agama` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.master_karyawan
CREATE TABLE IF NOT EXISTS `master_karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `keterangan_aktivasi_karyawan` varchar(255) DEFAULT NULL,
  `nomor_po_nama_driver_yg_diganti` varchar(255) DEFAULT NULL,
  `tanggal_po` date DEFAULT NULL,
  `tgl_inaktif_kerja` date DEFAULT NULL,
  `lead_time` varchar(255) DEFAULT NULL,
  `nomor_hp` varchar(255) DEFAULT NULL,
  `driver` varchar(255) DEFAULT NULL,
  `fico` varchar(255) DEFAULT NULL,
  `helper` varchar(255) DEFAULT NULL,
  `checker` varchar(255) DEFAULT NULL,
  `non_driver` varchar(255) DEFAULT NULL,
  `operation_poin` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `masa_berlaku` varchar(255) DEFAULT NULL,
  `klasifikasi_skill` varchar(255) DEFAULT NULL,
  `jenis_sim` varchar(255) DEFAULT NULL,
  `no_sim` varchar(255) DEFAULT NULL,
  `masa_berlaku_sim` date DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tglblnthn_lahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `status_pajak` varchar(255) DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `pendidikan_terakhir` varchar(255) DEFAULT NULL,
  `nama_di_bank` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `no_account` varchar(255) DEFAULT NULL,
  `nomor_kartu_keluarga` varchar(255) DEFAULT NULL,
  `npwp` varchar(255) DEFAULT NULL,
  `start_kontrak` date DEFAULT NULL,
  `finish_kontrak` date DEFAULT NULL,
  `status_kontrak` varchar(255) DEFAULT NULL,
  `masa_kontrak_berakhir` varchar(255) DEFAULT NULL,
  `lama_kerja` varchar(255) DEFAULT NULL,
  `seragam` varchar(255) DEFAULT NULL,
  `tanggal_pengiriman_seragam` date DEFAULT NULL,
  `id_card` varchar(255) DEFAULT NULL,
  `tanggal_pengiriman_id_card` date DEFAULT NULL,
  `training_safety_driving` varchar(255) DEFAULT NULL,
  `pelaksanaan_training_terakhir` varchar(255) DEFAULT NULL,
  `masa_training` varchar(255) DEFAULT NULL,
  `status_training` varchar(255) DEFAULT NULL,
  `nilai_training_safety_driving_pree_test` varchar(255) DEFAULT NULL,
  `nilai_training_safety_driverng_post_test` varchar(255) DEFAULT NULL,
  `status_vaksin_pertama` varchar(255) DEFAULT NULL,
  `mengapa_belum_vaksi_pertama` varchar(255) DEFAULT NULL,
  `tanggal_vaksin_pertama` date DEFAULT NULL,
  `lokasi_vaksin_pertama` varchar(255) DEFAULT NULL,
  `status_vaksin_kedua` varchar(255) DEFAULT NULL,
  `mengapa_belum_vaksi_kedua` varchar(255) DEFAULT NULL,
  `tanggal_vaksin_kedua` date DEFAULT NULL,
  `lokasi_vaksin_kedua` varchar(255) DEFAULT NULL,
  `status_vaksin_booster` varchar(255) DEFAULT NULL,
  `mengapa_belum_vaksin_booster` varchar(255) DEFAULT NULL,
  `tanggal_vaksin_booster` date DEFAULT NULL,
  `lokasi_vaksin_booster` varchar(255) DEFAULT NULL,
  `tgl_training_online_part_1` date DEFAULT NULL,
  `status_training_online_part_1` varchar(255) DEFAULT NULL,
  `point_part_1` varchar(255) DEFAULT NULL,
  `tgl_training_online_part_2` date DEFAULT NULL,
  `status_training_online_part_2` varchar(255) DEFAULT NULL,
  `point_part_2` varchar(255) DEFAULT NULL,
  `tgl_training_attitude_n_knowledge` date DEFAULT NULL,
  `status_training_attitude_n_knowledge` varchar(255) DEFAULT NULL,
  `point_attitude_n_knowledge` varchar(255) DEFAULT NULL,
  `nomor_bpjs_kesehatan` varchar(255) DEFAULT NULL,
  `keterangan_proses_bpjs_kesehatan` varchar(255) DEFAULT NULL,
  `keterangan_status_bpjs_kesehatan` varchar(255) DEFAULT NULL,
  `nomor_jamsostek` varchar(255) DEFAULT NULL,
  `keterangan_jamsostek` varchar(255) DEFAULT NULL,
  `reward` varchar(255) DEFAULT NULL,
  `tglblnthn_pemberian_reward` date DEFAULT NULL,
  `accident_report` varchar(255) DEFAULT NULL,
  `tglblnthn_accident` date DEFAULT NULL,
  `kasus` varchar(255) DEFAULT NULL,
  `tglblnthn_kasus` date DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `sp_i` date DEFAULT NULL,
  `sp_ii` date DEFAULT NULL,
  `sp_iii` date DEFAULT NULL,
  `keterangan_out_dan_mutasi` varchar(255) DEFAULT NULL,
  `masa_berlaku_sim_2` varchar(255) DEFAULT NULL,
  `masa_training_2` varchar(255) DEFAULT NULL,
  `keterangan_dan_jadwal_training` varchar(255) DEFAULT NULL,
  `driver_leader` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1788 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms _jabatan
CREATE TABLE IF NOT EXISTS `ms _jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(75) NOT NULL,
  `kategori_jab` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_agama
CREATE TABLE IF NOT EXISTS `ms_agama` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nama_agama` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_area_op
CREATE TABLE IF NOT EXISTS `ms_area_op` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_area` varchar(100) NOT NULL,
  `alamat_lengkap` text NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_customer
CREATE TABLE IF NOT EXISTS `ms_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_customer` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_district
CREATE TABLE IF NOT EXISTS `ms_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_district` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_karyawan
CREATE TABLE IF NOT EXISTS `ms_karyawan` (
  `id` int(11) NOT NULL DEFAULT 0,
  `nik` varchar(17) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `vendor` varchar(100) NOT NULL,
  `tgl_aktif` date NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `jabatan_id` int(2) NOT NULL,
  `kategori_jab` varchar(20) NOT NULL,
  `area_op_id` int(2) NOT NULL,
  `fico_id` int(2) NOT NULL,
  `customer_id` int(2) NOT NULL,
  `district_id` int(2) NOT NULL,
  `no_ktp` varchar(17) NOT NULL,
  `masa_berlaku_ktp` date NOT NULL,
  `jenis_sim` varchar(20) NOT NULL,
  `no_sim` varchar(17) NOT NULL,
  `masa_berlaku_sim` date NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat_lengkap` text NOT NULL,
  `status_pajak_id` int(2) NOT NULL,
  `agama_id` int(2) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `pendidikan_terakhir` varchar(35) NOT NULL,
  `nama_di_bank` varchar(50) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `bank_no_account` varchar(50) NOT NULL,
  `no_kk` varchar(20) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  `start_kontrak` date NOT NULL,
  `finish_kontrak` date NOT NULL,
  `status_kontrak` varchar(20) NOT NULL,
  `masa_kontrak_berakhir` varchar(50) NOT NULL,
  `lama_kerja` varchar(50) NOT NULL,
  `cuti` int(11) NOT NULL DEFAULT 12,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_status_pajak
CREATE TABLE IF NOT EXISTS `ms_status_pajak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_pajak` varchar(50) NOT NULL,
  `potongan_pph21` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.ms_umk
CREATE TABLE IF NOT EXISTS `ms_umk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `umk` int(11) NOT NULL,
  `district_id` int(2) NOT NULL,
  `area_op_id` int(2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `IX_Location_DistrictId` (`district_id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.trx_vaksin
CREATE TABLE IF NOT EXISTS `trx_vaksin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) DEFAULT NULL,
  `kandidat_id` int(11) DEFAULT NULL,
  `nama_vaksin` varchar(255) DEFAULT NULL,
  `vaksin_ke` int(11) DEFAULT NULL,
  `tanggal_vaksin` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.tx_ijincutisakit
CREATE TABLE IF NOT EXISTS `tx_ijincutisakit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik_kary` varchar(15) NOT NULL,
  `nama_kary` varchar(50) NOT NULL,
  `jenis_pengajuan` varchar(20) NOT NULL,
  `ket` text NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `jum_hari` varchar(10) NOT NULL,
  `dok_surat` varchar(200) NOT NULL,
  `f_approve` int(2) NOT NULL,
  `nik_approve` varchar(15) NOT NULL,
  `tgl_approve` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.user
CREATE TABLE IF NOT EXISTS `user` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(125) NOT NULL,
  `username` varchar(125) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(1) NOT NULL,
  `district` varchar(15) NOT NULL,
  `image` varchar(125) NOT NULL,
  `qr_code_image` varchar(125) NOT NULL,
  `kode_pegawai` varchar(125) NOT NULL,
  `instansi` varchar(125) NOT NULL,
  `jabatan` varchar(125) NOT NULL,
  `area` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `jenis_kelamin` varchar(25) NOT NULL,
  `bagian_shift` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `qr_code_use` int(2) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table u1675520_absensi_mbk.vaksin
CREATE TABLE IF NOT EXISTS `vaksin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `status_vaksin_pertama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1784 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

CREATE TABLE IF NOT EXISTS `soal_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipe` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `soal_type` DISABLE KEYS */;
INSERT INTO `soal_type` (`id`, `tipe`) VALUES
	(1, 'interview'),
	(2, 'pengetahuan'),
	(3, 'skill');

CREATE TABLE IF NOT EXISTS `soal` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `soal_type_id` int(11) unsigned NOT NULL COMMENT '1=interview, 2=pengetahuan, 3=skill',
  `pertanyaan` varchar(255) DEFAULT NULL,
  `category_title` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `pilihan` text DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_soal_type` (`soal_type_id`),
  CONSTRAINT `FK_soal_type` FOREIGN KEY (`soal_type_id`) REFERENCES `soal_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4;


INSERT INTO `soal` (`id`, `soal_type_id`, `pertanyaan`, `category_title`, `category`, `pilihan`, `sort`) VALUES
	(1, 1, 'Pendidikan Terakhir ?', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\n	"SMU/SMK/LAINNYA": 2,"SMP": 1\n}', 1),
	(2, 1, 'Lokasi Tempat Tinggal', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\n	"Jarak rumah ke kantor \\u00b1 30 menit": 3,\n	"Jarak rumah ke kantor kurang dari \\u00b1 1,5 jam": 2,\n	"Jarak rumah ke kantor \\u00b1 2 jam": 1\n}', 2),
	(3, 2, 'Berdasarkan grooming (penampilan), seorang pengemudi harus memperlihatkan ciri sebagai berikut, kecu', NULL, NULL, '{\r\n	"a. Rambut harus selalu rapi dan tidak gondrong (panjang) ": 0,\r\n	"b. Diperbolehkan menggunakan sendal": 5,\r\n	"c. Mengenakan pakaian seragam dimanapun bertugas": 0,\r\n	"d. Sepatu kerja bersih dan mengenakan kaos kaki":0\r\n}', 1),
	(4, 2, 'Ketika pertama kali bertemu dengan customer/user apa yang harus anda lakukan ?', NULL, NULL, '{\r\n	"a. Senyum, salam, sapa serta memperkenalkan diri dengan baik": 5,\r\n	"b. langsung mengajak masuk ke mobil": 0,\r\n	"c. Selalu memberikan hormat": 0,\r\n	"d. Bekerja sesuai dengan tugasnya": 0\r\n}', 2),
	(5, 2, 'Apa yang anda lakukan ketika sedang menunggu Customer ?', NULL, NULL, '{\r\n	"a. Tidur dimobil sambil menyalakan AC mobil": 0,\r\n	"b. Mencari tempat parkir & menunggu yang nyaman, serta mudah dihubungi": 5,\r\n	"c. Merokok dimobil sambil membuka kaca jendela mobil": 0,\r\n	"d. Parkir asal saja yang penting dapat tempat parkir": 0\r\n}', 3),
	(6, 2, 'Driver profesional adalah:', NULL, NULL, '{\r\n	"a. Driver yang mengemudi selalu cepat sampai tujuan.": 0,\r\n	"b. Driver yang mengemudi tidak pernah mengalami kecelakaan": 0,\r\n	"c. Driver yang selalu berorientasi kepada kepuasan pelanggan": 5,\r\n	"d. Driver yang patuh pada semua keingingan customer": 0\r\n}', 4),
	(7, 2, 'Driver: Good morning, Sir! <br/>\r\nCust   : Good morning, Andi.<br>\r\nDriver: _ _ _ _ _ _ _ _ _ _ _ _, Sir!<br>\r\nCust   : Im Fine thanks', NULL, NULL, '{\r\n	"a. What is your name": 0,\r\n	"b. How old are you": 0,\r\n	"c. How are you": 5,\r\n	"d. Excuse me": 0\r\n}', 5),
	(8, 2, '_ _ _ _ _ _ _ _ _ your name ? My name is Kemal', NULL, NULL, '{\r\n	"a. Who are": 0,\r\n	"b. Who is": 0,\r\n	"c. What are": 0,\r\n	"d. What is": 5\r\n}', 6),
	(9, 1, 'Lingkungan Tempat Tinggal ?', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\n	"Perumahan": 3,\n	"Kampung tertata": 2,\n	"Kampung (daerah zona rawan)": 1\n}', 3),
	(10, 1, 'Status Tempat Tinggal ?', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\n	"Rumah Sendiri / Orang Tua": 3,\n	"Menunpang rumah saudara": 2,\n	"Kontrak / Kost": 1\n}', 4),
	(11, 1, 'Usia ?', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\n	"23 \\u2013 28  tahun": 3,\n	"29 \\u2013 32  tahun": 2,\n	"33 \\u2013 36  tahun": 1\n}', 5),
	(12, 1, 'Status Perkawinan ?', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\n	"Kawin dengan ...anak": 3,\n	"Duda dengan  ...anak": 2,\n	"Tidak Kawin": 1\n}', 6),
	(13, 1, 'Penampilan', 'AMATI  SIKAP, PENAMPILAN DAN PENGETAHUAN', 2, '{\r\n  "Bersih rapi ( pakaian, rambut, jenggot, kumis )":7,\r\n  "Biasa saja  ( pakaian, rambut, jenggot, kumis )":6,\r\n  "Kurang rapi terkesan kurang merawat diri":3\r\n}', 1),
	(14, 3, 'Melakukan persiapan sebelum mengemudi ( Pre drive check - BALOK)', 'PENGUASAAN DASAR MENGEMUDI	', 1, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 1),
	(15, 3, 'Mengunci pintu ( central lock ) ?', 'PENGUASAAN DASAR MENGEMUDI	', 1, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 2),
	(16, 3, 'Mengendalikan pedal gas dengan baik ?', 'KETRAMPILAN DALAM MENGEMUDI', 2, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 1),
	(17, 1, 'Status kerja Istri', 'TENTANG DIRI & KELUARGA PELAMAR (Arahkan agar bisa bercerita tentang diri pelamar)', 1, '{\r\n	"Istri bekerja PNS / karyawan tetap Perusahaan": 3,\r\n	"Istri buka Warung / Karyawan kontrak swasta": 2,\r\n	"Istri tidak bekerja (sebagai ibu rumah tangga)": 1\r\n}', 7),
	(18, 1, 'Bahasa & cara bicara serta bahasa tubuh', 'AMATI  SIKAP, PENAMPILAN DAN PENGETAHUAN', 2, '{\n"Halus dan sopan": 7,\n"Biasa saja": 6,\n"Kasar dan Kurang Sopan": 3\n}', 2),
	(19, 1, 'Pengetahuan Umum ( beri contoh pertanyaan  politik / sosial / budaya )', 'AMATI  SIKAP, PENAMPILAN DAN PENGETAHUAN', 2, '{\n	"Paham": 3,\n	"Cukup Paham": 2,\n	"Tidak Paham": 1\n}', 3),
	(20, 1, 'Berapa lama tinggal di kota tsb', 'KEMAMPUAN PENGETAHUAN WILAYAH', 3, '{\r\n	"Lebih dari 5 tahun": 4,\r\n	"Antara 2 \\u2013 5 tahun": 3,\r\n	"Antara 0 \\u2013 2 tahun": 2\r\n}', 1),
	(21, 1, 'Pengetahuan Jalan ( sebutkan 1 titik lokasi ke titik lokasi lainnya )', 'KEMAMPUAN PENGETAHUAN WILAYAH', 3, '{\r\n	"Menguasai dan dapat menyebutkan jalan - jalan yang harus di lalui": 7,\r\n	"Menguasai dan dapat menyebutkan sebagian jalan - jalan yang harus di lalui": 6,\r\n	"Tidak mengusai dan salah menyebutkan nya": 4\r\n}', 2),
	(23, 1, 'Tanya Sepuluh Lokasi penting di masing-masing kota (Gedung Pemerintahan, Hotel, Layanan Publik, Mall dll)', 'KEMAMPUAN PENGETAHUAN WILAYAH', 3, '{\r\n	"Dijawan betul 8 – 10": 7,\r\n	"Dijawab betul 5 – 7": 6,\r\n	"Dijawab betul 0 – 4": 4\r\n}', 3),
	(24, 1, 'Pengalaman Menjadi Driver', 'MENGUKUR PENGALAMAN SEBAGAI DRIVER', 4, '{\r\n	"Lebih dari 5 tahun": 5,\r\n	"Antara 2 – 5 tahun": 4,\r\n	"Kurang dari 2 tahun": 3\r\n}', 1),
	(25, 1, 'Jenis Pekerjaan Driver yang pernah ditekuni ?', 'MENGUKUR PENGALAMAN SEBAGAI DRIVER', 4, '{\r\n "Taxi / perusahaan Rental / perusahan Logistics ": 4,\r\n " Instansi / Perusahaan, Perkantoran, Pribadi": 3,\r\n "Kendaraan Umum (Truk, Angkutan Umum dll)": 2\r\n}', 2),
	(26, 1, 'Bahasa Asing (Inggris / Jepang / lainnya )', 'PEMAHAMAN BAHASA ASING DAN  RAMBU LALULINTAS', 5, '{\r\n "Baik dan aktif menggunakan bahasa asing": 7,\r\n "Pasif  namun mengerti ": 6,\r\n "Sangat Pasif / tidak bisa": 4\r\n}', 1),
	(27, 1, 'Pengetahuan Rambu lalulintas ( buat 10 pertanyaan tentang arti Rambu & Marka jalan)', 'PEMAHAMAN BAHASA ASING DAN  RAMBU LALULINTAS', 5, '{\r\n "Dijawab betul 8 – 10 (faham tentang rambu lalulintas)": 7,\r\n "Dijawab betul 5 – 7 (cukup faham tentang rambu lalulintas)": 5,\r\n "Dijawab betul 0 – 4 (kurang paham tentang rambu lalulintas)": 3\r\n}', 2),
	(29, 1, 'Apa yang membuat Anda memilih untuk menjadi Driver?', 'PENELUSURAN MINAT TERHADAP PEKERJAAN ( gali lebih dalam sebab dia keluar dari Pekerjaannya )', 6, '{\r\n "Menjadi profesional dan jenjang karir": 5,\r\n "Tidak ada pilihan lain (pilihan terakhir)": 4,\r\n "Belajar & Menambah pengalaman": 3\r\n}', 1),
	(30, 1, 'Apa saja tugas utama sebagai Driver?', 'PENELUSURAN MINAT TERHADAP PEKERJAAN ( gali lebih dalam sebab dia keluar dari Pekerjaannya )', 6, '{\r\n "Sangat faham tentang tugas driver": 5,\r\n "Cukup faham tentang tugas driver": 4,\r\n "Kurang memahami tugas driver": 3\r\n}', 2),
	(31, 1, 'Apa yang bisa Anda ceritakan tentang tanggung jawab Driver, contohkan?', 'PENELUSURAN MINAT TERHADAP PEKERJAAN ( gali lebih dalam sebab dia keluar dari Pekerjaannya )', 6, '{\r\n "Sangat faham tentang tanggung jawab driver": 7,\r\n "Cukup faham tentang tanggung jawab driver": 6,\r\n "Kurang memahami tentang tanggung jawab driver": 1\r\n}', 3),
	(32, 1, 'Pengalaman terhadap pekerjaan sebelumnya?', 'PENELUSURAN MINAT TERHADAP PEKERJAAN ( gali lebih dalam sebab dia keluar dari Pekerjaannya )', 6, '{\r\n "Faham dan menguasai pekerjaan sebelumnya": 5,\r\n "Kurang faham dan kurang menguasai pekerjaan sebelumnya": 4,\r\n "Hanya coba-coba / Iseng / Batu Loncatan": 3\r\n}', 4),
	(33, 2, 'Oli mesin kendaraan pada umumnya dianjurkan diganti setiap kelipatan ?', NULL, NULL, '{\r\n	"a. 2000 km": 0,\r\n	"b. 3000 km": 0,\r\n	"c. 5000 km": 5,\r\n	"d. 8000 km": 0\r\n}', 7),
	(34, 2, 'Jika anda mengemudi di jalan bebas hambatan, berapakah kecepatan maximal yang di perbolehkan ?', NULL, NULL, '{\r\n	"a. 80 km/jam": 5,\r\n	"b. 100 km/jam": 0,\r\n	"c. 120 km/jam": 0,\r\n	"d. 150 km/jam": 0\r\n}', 8),
	(35, 2, 'Rambu dan marka jalan serta alat pemberi isyarat lalu-lintas,  digunakan untuk ?', NULL, NULL, '{\r\n	"a. Keselamatan, Keamanan dan kenyamanan jalan": 0,\r\n	"b. Mengatur ketertiban dan Kelancaran jalan": 0,\r\n	"c. Kemudahan bagi pemakai jalan": 0,\r\n	"d. Jawaban A, B dan C benar": 5\r\n}', 9),
	(36, 2, 'Kendaraan di bawah ini yang mendapatkan prioritas pertama di jalan raya :', NULL, NULL, '{\r\n	"a. Mobil jenazah": 0,\r\n	"b. Mobil pemadam kebakaran yang sedang bertugas": 5,\r\n	"c. Mobil yang ditumpangi tamu negara asing (presiden negara sahabat)": 0,\r\n	"d. Mobil ambulans 118 (darurat)": 0\r\n}', 10),
	(37, 2, 'Apabila lampu isyarat warna hijau telah berkedip dan akan berubah warna kuning, maka anda harus ?', NULL, NULL, '{\r\n	"a. Berhenti dengan cepat dan tepat di garis pembatas": 0,\r\n	"b. Mengikuti kendaraan lain saja": 0,\r\n	"c. Berhati-hati, kurangi kecepatan lalu berhenti tepat saat lampu kuning berubah menjadi merah": 5,\r\n	"d. Menambah kecepatan untuk menghindari berhenti saat lampu berganti warna merah": 0\r\n}', 11),
	(38, 2, 'Jarak iring kendaraan roda 4 yang aman adalah, kecuali : ', NULL, NULL, '{\r\n	"a. Seribu dan satu, seribu dan dua": 0,\r\n	"b. Tergantung pada kecepatan kendaraan": 5,\r\n	"c. Menggunakan aturan 2 detik (2 second rule)": 0,\r\n	"d. 10 meter untuk kecepatan 100 km/jam": 0\r\n}', 12),
	(39, 2, 'Kemampuan seorang pengemudi defensif antara lain ditentukan oleh : ', NULL, NULL, '{\r\n"a. Kemampuannya untuk mengetahui resiko bahaya pada waktu mengemudi dan cara mengatasinya": 5,\r\n"b. Seringnya pengemudi mengalami kecelakaan tetapi tidak mengalami kerugian": 0,\r\n"c. Belum pernah menghadapi suatu bahaya di jalan": 0,\r\n"d. Pengalamanya mengalami kecelakaan": 0\r\n}', 13),
	(41, 2, 'Di mana sebaiknya Anda menempatkan posisi tangan Anda pada saat mengemudi ?', NULL, NULL, '{\r\n"a. Tangan kiri pada posisi jam 11 dan tangan kanan pada jam 1": 0,\r\n"b. Tangan kiri pada posisi jam 9 dan tangan kanan pada jam 3": 5,\r\n"c. Tangan kiri pada posisi jam 10 dan tangan kanan pada jam 2": 0,\r\n"d. Jawaban A, B dan C benar": 0\r\n}', 14),
	(42, 2, 'Kapan sebaiknya anda menggunakan lampu Hazard ?', NULL, NULL, '{\r\n"a. Pada saat kendaraan dalam keadaan darurat": 5,\r\n"b. Pada saat cuaca buruk & hujan deras": 0,\r\n"c. Pada saat mundur": 0,\r\n"d. saat akan berhenti": 0\r\n}', 15),
	(43, 2, 'Kapan sebaiknya anda menggunakan lampu sign', NULL, NULL, '{\r\n"a. Pada saat mundur": 0,\r\n"b. Pada saat cuaca buruk ": 0,\r\n"c. Saat akan pindah lajur dan saat akan melakukan manuver belok": 5,\r\n"d. saat akan berhenti": 0\r\n}', 16),
	(44, 3, 'Mengatur posisi tempat duduk & menggunakan sabuk pengaman (Seat belt), posisi memegang steer?', 'PENGUASAAN DASAR MENGEMUDI	', 1, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 3),
	(45, 3, 'Mengatur posisi spion ( kanan, kiri dan dalam ) ?', 'PENGUASAAN DASAR MENGEMUDI	', 1, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 4),
	(46, 3, 'Paham menggunakan lampu sign, lampu dim, panel interior ?', 'PENGUASAAN DASAR MENGEMUDI	', 1, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 5),
	(47, 3, 'Menggunakan persnelling dengan baik ?', 'KETRAMPILAN DALAM MENGEMUDI', 2, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 2),
	(48, 3, 'Mampu  menggunakan fungsi rem ?', 'KETRAMPILAN DALAM MENGEMUDI', 2, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 3),
	(50, 3, 'Melanggar, menyenggol, perkiraan kurang tepat ?', 'KETRAMPILAN DALAM MENGEMUDI', 2, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 4),
	(51, 3, 'Parkir, Lurus, Parkir Paralel (Metode proses tdk lebih dari 2 kali)', 'KETRAMPILAN DALAM MENGEMUDI', 2, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 5),
	(52, 3, 'Mengendalikan kendaraan  saat berbelok kiri ?', 'MANUVER DALAM MENGEMUDI', 3, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 1),
	(53, 3, 'Mengendalikan kendraan  saat berbelok  kanan ?', 'MANUVER DALAM MENGEMUDI', 3, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 2),
	(54, 3, 'Mengendalikan kendraan  saat berputar  kiri ?', 'MANUVER DALAM MENGEMUDI', 3, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 3),
	(55, 3, 'Mengendalikan kendraan  saat berputar  kanan ?', 'MANUVER DALAM MENGEMUDI', 3, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 4),
	(56, 3, 'Mengendalikan kendraan  saat mundur ?', 'MANUVER DALAM MENGEMUDI', 3, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 5),
	(57, 3, 'Judgment (Pertimbangan) dalam mengemudi', 'DEFENSIVE DALAM MENGEMUDI', 4, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 1),
	(58, 3, 'Alertness (Kewaspadaan) dalam mengemudi', 'DEFENSIVE DALAM MENGEMUDI', 4, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 2),
	(59, 3, 'Skill (Ketrampilan) dalam mengemudi', 'DEFENSIVE DALAM MENGEMUDI', 4, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 3),
	(60, 3, 'Emosional dalam mengemudi', 'DEFENSIVE DALAM MENGEMUDI', 4, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 4),
	(61, 3, 'Attitude (Sikap) dalam mengemudi', 'DEFENSIVE DALAM MENGEMUDI', 4, '{\r\n  "Score 1":1,\r\n  "Score 2":2,\r\n  "Score 3":3,\r\n  "Score 4":4,\r\n  "Score 5":5\r\n}', 5);

CREATE TABLE IF NOT EXISTS `ms_dokumen` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_dokumen` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*!40000 ALTER TABLE `ms_dokumen` DISABLE KEYS */;
INSERT INTO `ms_dokumen` (`id`, `nama_dokumen`, `created_at`, `updated_at`) VALUES
	(1, 'Surat Lamaran', '2022-06-19 21:08:13', NULL),
	(2, 'Daftar Riwayat Hidup', '2022-06-19 21:08:30', NULL),
	(3, 'FC KTP Kandidat', '2022-06-19 21:09:02', NULL),
	(4, 'FC KTP Istri/Suami', '2022-06-19 21:09:11', NULL),
	(5, 'FC SIM', '2022-06-19 21:19:06', NULL),
	(6, 'FC. KK', '2022-06-19 21:38:19', NULL),
	(7, 'FC NPWP', '2022-06-19 21:44:04', NULL),
	(8, 'SKCK', '2022-06-19 21:44:14', NULL),
	(9, 'FC Buku Rekening', '2022-06-19 21:44:21', NULL),
	(10, 'FC Ijazah', '2022-06-19 21:44:28', NULL),
	(11, 'SKBN', '2022-06-19 21:44:36', NULL),
	(12, 'Surat Keterangan Domisili', '2022-06-19 21:44:45', NULL),
	(13, 'Sertifikat Vaksin', '2022-06-19 21:44:52', NULL);


CREATE TABLE IF NOT EXISTS `dokumen_kandidat` (
  `dokumen_id` int(11) unsigned NOT NULL,
  `kandidat_id` int(3) NOT NULL,
  `value` int(3) NOT NULL DEFAULT 0,
  `keterangan` varchar(100) DEFAULT NULL,
  KEY `FK_dokumen_id` (`dokumen_id`),
  KEY `FK_Kandidat_id` (`kandidat_id`),
  CONSTRAINT `FK_Kandidat_id` FOREIGN KEY (`kandidat_id`) REFERENCES `kandidat` (`id`),
  CONSTRAINT `FK_dokumen_id` FOREIGN KEY (`dokumen_id`) REFERENCES `ms_dokumen` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `dokumen_kandidat` (`dokumen_id`, `kandidat_id`, `value`, `keterangan`) VALUES
	(1, 1, 0, NULL),
	(2, 1, 1, NULL);


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
